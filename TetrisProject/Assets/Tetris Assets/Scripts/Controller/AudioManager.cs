﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioManager : MonoBehaviour
{
    public AudioClip clickClip;

    public AudioClip fallClip;

    public AudioClip ctrlClip;

    public AudioClip deleteRowClip;

    [HideInInspector]
    public AudioSource audioSource;

    private bool isPause;

    public bool IsPause
    {
        get
        {
            return isPause;
        }

        set
        {
            isPause = value;
        }
    }

    private void Awake()
    {
        this.isPause = false; 
        audioSource = this.GetComponent<AudioSource>();
    }

    /// <summary>
    /// 播放指定音效
    /// </summary>
    /// <param name="clip"></param>
    private void PlayAudio(AudioClip clip)
    {
        if(this.IsPause == false)
        {
            audioSource.clip = clip;
            audioSource.Play();
        }
    }

    /// <summary>
    /// 播放点击音效
    /// </summary>
    public void PlayClickAudio()
    {
        PlayAudio(clickClip);
    }

    /// <summary>
    /// 播放控制音效
    /// </summary>
    public void PlayCtrlAudio()
    {
        PlayAudio(ctrlClip);
    }

    /// <summary>
    /// 播放Shape下落音效
    /// </summary>
    public void PlayFallAudio()
    {
        PlayAudio(fallClip);
    }

    /// <summary>
    /// 播放Shape下落音效
    /// </summary>
    public void PlayDeletRowAudio()
    {
        PlayAudio(deleteRowClip);
    }
}
