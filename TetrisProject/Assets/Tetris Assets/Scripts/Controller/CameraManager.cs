﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraManager : MonoBehaviour
{
    private Camera myCamera;

    private void Awake()
    {
        myCamera = Camera.main;
    }

    public void ZoomOut()
    {
        myCamera.DOOrthoSize(18,0.8f);
    }

    public void ZoomGame()
    {
        myCamera.DOOrthoSize(14,0.8f);
    }
}
