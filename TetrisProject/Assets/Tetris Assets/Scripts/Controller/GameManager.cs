﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private bool isPause = true;

    public GameObject[] shapes;

    public Color[] colors;

    private GameObject currentShape = null;

    public Controller controller;

    public GameObject CurrentShape
    {
        get
        {
            return currentShape;
        }

        set
        {
            currentShape = value;
        }
    }

    private void Awake()
    {
        controller = GetComponent<Controller>();       
    }

    void Start ()
    {
		
	}
	
	
	void Update ()
    {
		if(isPause == true)
        {
            return;
        }

        if(currentShape == null)
        {
            SpawnShape();
        }
	}

    public void SpawnShape()
    {
        int index = Random.Range(0, shapes.Length);
        int indexColor = Random.Range(0, colors.Length);
        currentShape = GameObject.Instantiate(shapes[index]);
        currentShape.GetComponent<Shape>().Init(colors[indexColor], controller,this);      
    }

    public void StartGame()
    {       
        isPause = false;

        if (currentShape != null)
        {
            currentShape.GetComponent<Shape>().StartShapeFall();
        }
    }

    public void PauseGame()
    {
        isPause = true;

        if(currentShape != null)
        {
            currentShape.GetComponent<Shape>().StopShapeFall();
        }
    }

    public void IsGameOver()
    {
        if (controller.model.IsGameOver())
        {
            controller.gameManager.PauseGame();
            controller.Fsm.PerformTransition(Transition.GameOverButtonClick);
        }
    }
}
