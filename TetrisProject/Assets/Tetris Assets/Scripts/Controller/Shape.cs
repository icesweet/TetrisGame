﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{
    private Controller controller;

    private GameManager gameManager;

    private bool isPause = false;

    private float timer = 0;

    private float stepTime = 0.8f;

    private float stepUpTime = 0.05f;

    private float stepTempTime = 0;

    public GameObject Pivot;
    private bool startPosFlag;
    private int backValue = 0;

    private void Awake()
    {
        stepTempTime = stepTime;
    }

    private void Update()
    {
        if (isPause == true)
            return;

        timer += Time.deltaTime;

        if(timer>= stepTempTime)
        {
            Fall();
            timer = 0;
        }

        // Shape控制
        // ControllerInput();
    }

    /// <summary>
    /// 初始化
    /// </summary>
    /// <param name="color"></param>
    /// <param name="controller"></param>
    /// <param name="gameManager"></param>
    public void Init(Color color,Controller controller,GameManager gameManager)
    {
        foreach(Transform child in transform)
        {
            if(child.tag == "Block")
            {
                child.GetComponent<SpriteRenderer>().color = color;
            }
        }

        this.gameManager = gameManager;
        this.controller = controller;
    }

    /// <summary>
    /// 控制Shape
    /// </summary>
    public void ControllerInput(int direction)
    {            
        // Shape左移动
        if (Input.GetKeyDown(KeyCode.LeftArrow) || direction == -1)
        {
            controller.audioManager.PlayCtrlAudio();

            Vector3 pos = transform.position;
            pos.x -= 1;
            transform.position = pos;

            if (controller.model.IsValidMapPosition(this.transform) == false)
            {
                pos.x += 1;
                transform.position = pos;
            }                      
        }

        // Shape右移动
        if (Input.GetKeyDown(KeyCode.RightArrow) || direction == 1)
        {
            controller.audioManager.PlayCtrlAudio();

            Vector3 pos = transform.position;
            pos.x += 1;
            transform.position = pos;

            if (controller.model.IsValidMapPosition(this.transform) == false)
            {               
                pos.x -= 1;
                transform.position = pos;
            }
        }

        // Shape变换
        if (Input.GetKeyDown(KeyCode.UpArrow) || direction == 2)
        {
            controller.audioManager.PlayCtrlAudio();

            ShapeRotate();
        }

        // Shape快速下落
        if (Input.GetKeyDown(KeyCode.DownArrow) || direction == -2)
        {
            stepTempTime = stepUpTime;
        }
        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            stepTempTime = stepTime;
        }

       
    }

    /// <summary>
    /// 下落
    /// </summary>
    private void Fall()
    {
        Vector3 pos = transform.position;
        pos.y -= 1;
        transform.position = pos;

        if (controller.model.IsValidMapPosition(this.transform) == false)
        {
            pos.y += 1;
            transform.position = pos;

            // 停止当前Shape下落
            isPause = true;         
            
            // 设置当前Shape在地图中的占位 并返回是否有消除
            bool isLineClear = controller.model.SetBlockMap(this.transform);

            // 播放消除音效
            if(isLineClear)
            {
                controller.audioManager.PlayDeletRowAudio();
            }

            // 更新分数
            if(controller.model.isDataUpdate == true)
            {
                controller.view.UpdateGameUI(controller.model.Score, controller.model.HighScore);
            }

            // 生成新Shape
            gameManager.CurrentShape = null;


            // 判断是否游戏结束
            controller.gameManager.IsGameOver();
                                         
            return;
        }

        controller.audioManager.PlayFallAudio();
    }

    /// <summary>
    /// Shape旋转变换
    /// </summary>
    private void ShapeRotate()
    {
        transform.RotateAround(this.transform.Find("Pivot").transform.position, Vector3.forward, 90);
    }

    /// <summary>
    /// 开始Shape下落
    /// </summary>
    public void StartShapeFall()
    {
        isPause = false;
    }

    /// <summary>
    /// 停止Shape下落
    /// </summary>
    public void StopShapeFall()
    {
        isPause = true;
    }  
}
