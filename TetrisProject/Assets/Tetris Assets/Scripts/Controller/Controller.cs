﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    [HideInInspector]
    public Model model;
    [HideInInspector]
    public View view;
    [HideInInspector]
    public CameraManager cameraManager;
    [HideInInspector]
    public GameManager gameManager;
    [HideInInspector]
    public AudioManager audioManager;

    private FSMSystem fsm;

    public FSMSystem Fsm
    {
        get
        {
            return fsm;
        }

        set
        {
            fsm = value;
        }
    }

    private void Awake ()
    {
        model = GameObject.FindGameObjectWithTag("Model").GetComponent<Model>();
        view = GameObject.FindGameObjectWithTag("View").GetComponent<View>();
        gameManager = this.GetComponent<GameManager>();
        cameraManager = this.GetComponent<CameraManager>();
        audioManager = this.GetComponent<AudioManager>();
    }

    private void Start()
    {
        MakeFSM();
    }

    /// <summary>
    /// 启动FSM状态机
    /// </summary>
    private void MakeFSM()
    {
        fsm = new FSMSystem();
        FSMState[] states = this.GetComponentsInChildren<FSMState>();

        foreach(FSMState child in states)
        {
            fsm.AddState(child, this);
        }

        MenuState s = this.GetComponentInChildren<MenuState>();
        // 设置默认状态
        fsm.SetCurrentState(s);
    }	
}
