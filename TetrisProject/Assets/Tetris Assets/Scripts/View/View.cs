﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class View : MonoBehaviour
{
    private RectTransform logoName;
    private RectTransform menuUI;
    private RectTransform gameUI;
    private RectTransform gameOverUI;
    private RectTransform settingUI;
    private GameObject audioMute;
    private RectTransform rankUI;

    private GameObject ReStartButton;

    private Text score;
    private Text highScore;
    private Text gameOverScore;

    void Start ()
    {
        logoName = transform.Find("Canvas/LogoName") as RectTransform;
        menuUI = transform.Find("Canvas/MenuUI") as RectTransform;
        gameUI = transform.Find("Canvas/GameUI") as RectTransform;
        ReStartButton = transform.Find("Canvas/MenuUI/ReStartButton").gameObject;
        gameOverUI = transform.Find("Canvas/GameOverUI") as RectTransform;
        gameOverScore = transform.Find("Canvas/GameOverUI").Find("GameOverScore").GetComponent<Text>();
        settingUI = transform.Find("Canvas/SettingUI") as RectTransform;
        audioMute = transform.Find("Canvas/SettingUI/AudioButton/Mute").gameObject;
        rankUI = transform.Find("Canvas/RankUI") as RectTransform;

        score = transform.Find("Canvas/GameUI/ScoreLabel/Text").GetComponent<Text>();
        highScore = transform.Find("Canvas/GameUI/HighScoreLabel/Text").GetComponent<Text>();
    }
	
    /// <summary>
    /// 显示菜单组件
    /// </summary>
    public void ShowMenu()
    {
        logoName.gameObject.SetActive(true);
        logoName.DOAnchorPosY(-128f,0.8f);
        menuUI.gameObject.SetActive(true);
        menuUI.DOAnchorPosY(53f,0.8f);
    }

    /// <summary>
    /// 隐藏菜单组件
    /// </summary>
    public void HideMenu()
    {        
        logoName.DOAnchorPosY(119f, 0.8f).OnComplete(delegate { logoName.gameObject.SetActive(false); });        
        menuUI.DOAnchorPosY(-121f, 0.8f).OnComplete(delegate { menuUI.gameObject.SetActive(false); }); 
    }

    /// <summary>
    /// 显示游戏组件
    /// </summary>
    public void ShowGameUI(int score = 0,int highScore = 0)
    {
        this.score.text = score.ToString();
        this.highScore.text = highScore.ToString();
        gameUI.gameObject.SetActive(true);
        gameUI.DOAnchorPosY(-95.5f,0.8f);       
    }

    /// <summary>
    /// 更新当前分数
    /// </summary>
    /// <param name="score"></param>
    /// <param name="highScore"></param>
    public void UpdateGameUI(int score, int highScore)
    {
        this.score.text = score.ToString();
        this.highScore.text = highScore.ToString();
    }

    /// <summary>
    /// 隐藏游戏组件
    /// </summary>
    public void HideGameUI()
    {
        gameUI.DOAnchorPosY(107.25f, 0.8f).OnComplete(delegate { gameUI.gameObject.SetActive(false); });
    }

    /// <summary>
    /// 显示重新开始按钮
    /// </summary>
    public void ShowReStartButton()
    {
        ReStartButton.SetActive(true);
    }

    /// <summary>
    /// 隐藏重新开始按钮
    /// </summary>
    public void HideReStartButton()
    {
        ReStartButton.SetActive(false);
    }

    /// <summary>
    /// GameOver显示与隐藏
    /// </summary>
    /// <param name="score"></param>
    public void ShowGameOverUI(int score = 0)
    {
        gameOverUI.gameObject.SetActive(true);
        gameOverScore.text = score.ToString();
    }
    public void HideGameOverUI()
    {
        gameOverUI.gameObject.SetActive(false);
    }

    /// <summary>
    /// 显示SettingUI和隐藏
    /// </summary>
    public void ShowSettingUI()
    {
        settingUI.gameObject.SetActive(true);
    }
    public void HideSettingUI()
    {
        settingUI.gameObject.SetActive(false);
    }

    /// <summary>
    /// 是否显示音效开关按钮
    /// </summary>
    /// <param name="flag"></param>
    public void IsShowAudioLine(bool flag)
    {       
        audioMute.SetActive(flag);
    }

    /// <summary>
    /// 记录UI的显示和隐藏
    /// </summary>
    public void ShowRankUI(int score , int highScore , int numberGames)
    {
        rankUI.gameObject.SetActive(true);
        ShowRankData(score, highScore, numberGames);
    }
    public void HideRankUI()
    {
        rankUI.gameObject.SetActive(false);
    }

    /// <summary>
    /// 显示记录面板数据
    /// </summary>
    public void ShowRankData(int score, int highScore, int numberGames)
    {
        rankUI.transform.Find("ScoreLabel/Text").GetComponent<Text>().text = score.ToString();
        rankUI.transform.Find("HighLabel/Text").GetComponent<Text>().text = highScore.ToString();
        rankUI.transform.Find("NumberGamesLabel/Text").GetComponent<Text>().text = numberGames.ToString();
    }
   
}
