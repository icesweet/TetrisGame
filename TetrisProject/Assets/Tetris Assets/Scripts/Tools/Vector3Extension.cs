﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Vector3Extension
{
    public static Vector3 Vector3Round(this Vector3 v)
    {
        v.x = Mathf.RoundToInt(v.x);
        v.y = Mathf.RoundToInt(v.y);
        v.z = Mathf.RoundToInt(v.z);

        return v;        
    }
}
