﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Model : MonoBehaviour
{
    public const int NORMAL_ROWS = 20;
    public const int MAX_ROWS = 23;
    public const int MAX_COLUMNS = 10;

    private int score = 0;
    private int highScore = 0;
    private int numbersGame = 0;
    public int Score { get { return score; } }
    public int HighScore { get { return highScore; } }
    public int NumbersGame { get { return numbersGame; } }

    public bool isDataUpdate = false;

    public Transform[,] map;

    private void Awake()
    {
        map = new Transform[MAX_COLUMNS, MAX_ROWS];

        LoadData();
    }

    // 检测地图是否可用
    public bool IsValidMapPosition(Transform t)
    {
        foreach(Transform child in t)
        {
            // 是否为Block
            if (child.tag != "Block") continue;

            Vector3 pos = child.position.Vector3Round();

            // Shape是否在地图内
            if (IsInsideMap(pos) == false)
                return false;

            // 地图是否占用
            if (map[(int)pos.x, (int)pos.y] != null)
                return false;          
        }

        return true;
    }

    /// <summary>
    /// 判断是否在地图内
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private bool IsInsideMap(Vector3 pos)
    {
        return (pos.x >= 0 && pos.x < MAX_COLUMNS && pos.y >= 0);
    }

    /// <summary>
    /// 设置地图占用信息
    /// </summary>
    /// <param name="t"></param>
    public bool SetBlockMap(Transform t)
    {
        foreach(Transform child in t)
        {
            if (child.tag != "Block")
                continue;

            Vector3 pos = child.position.Vector3Round();

            map[(int)pos.x, (int)pos.y] = child;            
        }

        return CheckRowMap();
    }

    
    /// <summary>
    /// 检查是否需要消除行
    /// </summary>
    public bool CheckRowMap()
    {
        // 消除行计数器
        int count = 0;

        for(int i=0;i<MAX_ROWS;i++)
        {
            if(CheckIsRowFull(i) == true)
            {
                count++;

                DeleteRow(i);

                MoveDownRowsAbove(i+1);

                // 循环判断行的销毁
                i--;
            }          
        }

        if (count > 0)
        {
            score += (count * 100);
            if (score > highScore)
            {
                highScore = score;
            }
            isDataUpdate = true;
            return true;
        }
        else return false;
    }

    /// <summary>
    /// 判断当前行是否被填充满
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    private bool CheckIsRowFull(int row)
    {
        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            if (map[i, row] == null) return false;
        }
        return true;
    }

    /// <summary>
    /// 消除某一行
    /// </summary>
    /// <param name="row"></param>
    private void DeleteRow(int row)
    {
        for (int i = 0; i < MAX_COLUMNS; i++)
        {         
            Destroy(map[i, row].gameObject);
            map[i, row] = null;
        }
    }

    /// <summary>
    /// 消除后方格移动
    /// </summary>
    /// <param name="row"></param>
      private void MoveDownRowsAbove(int row)
    {
        for(int i = row; i<MAX_ROWS; i++)
        {
            MoveDownRow(i);
        }
    }
    private void MoveDownRow(int row)
    {
        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            if (map[i, row] != null)
            {
                map[i, row - 1] = map[i, row];
                map[i, row] = null;
                map[i, row - 1].position += new Vector3(0, -1, 0);
            }
        }
    }   

    /// <summary>
    /// 游戏结束
    /// </summary>
    /// <returns></returns>
    public bool IsGameOver()
    {
        for (int i = NORMAL_ROWS; i < MAX_ROWS; i++)
        {
            for (int j = 0; j < MAX_COLUMNS; j++)
            {
                if (map[j, i] != null)
                {
                    numbersGame++;
                    SaveData();
                    return true;
                }
            }
        }
        return false;
    }

    /// <summary>
    /// 加载数据
    /// </summary>
    private void LoadData()
    {
        highScore = PlayerPrefs.GetInt("HighScore", 0);
        numbersGame = PlayerPrefs.GetInt("NumbersGame", 0);
    }

    /// <summary>
    /// 保存数据
    /// </summary>
    private void SaveData()
    {
        PlayerPrefs.SetInt("HighScore", highScore);
        PlayerPrefs.SetInt("NumbersGame", numbersGame);
    }

    /// <summary>
    /// 重新开始
    /// </summary>
    public void Restart()
    {      
        for (int i = 0; i < MAX_COLUMNS; i++)
        {
            for (int j = 0; j < MAX_ROWS; j++)
            {
                if (map[i, j] != null)
                {
                    
                    Destroy(map[i, j].gameObject);
                    map[i, j] = null;
                }
            }
        }
        score = 0;
    }

    /// <summary>
    /// 清除数据
    /// </summary>
    public void ClearData()
    {
        score = 0;
        highScore = 0;
        numbersGame = 0;
        SaveData();
    }
}
