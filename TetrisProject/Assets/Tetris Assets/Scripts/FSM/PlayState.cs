﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayState : FSMState
{
    private void Awake()
    {
        stateID = StateID.Play;
        AddTransition(Transition.PauseButtonClick, StateID.Menu);
        AddTransition(Transition.GameOverButtonClick, StateID.GameOver);
    }

    public override void DoBeforeEntering()
    {
        base.DoBeforeEntering();

        controller.view.ShowGameUI(controller.model.Score,controller.model.HighScore);

        controller.cameraManager.ZoomGame();

        controller.gameManager.StartGame();
    }

    public override void DoBeforeLeaving()
    {
        base.DoBeforeLeaving();

        controller.view.HideGameUI();

        controller.view.ShowReStartButton();

        controller.gameManager.PauseGame();
    }

    public void PauseButtonClick()
    {
        controller.audioManager.PlayClickAudio();
        fsm.PerformTransition(Transition.PauseButtonClick);       
    }
}
