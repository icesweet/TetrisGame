﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverState : FSMState
{
    
    private void Awake()
    {
        stateID = StateID.GameOver;
        // 绑定状态事件
        AddTransition(Transition.StartButtonClick, StateID.Play);
    }

    public override void DoBeforeEntering()
    {
        base.DoBeforeEntering();

        // 显示GameOver菜单
        controller.view.ShowGameOverUI(controller.model.Score);
    }

    public override void DoBeforeLeaving()
    {
        base.DoBeforeLeaving();

        controller.view.HideGameOverUI();
    }

    /// <summary>
    /// 重新开始游戏
    /// </summary>
    public void ReStartClickButton()
    {
        controller.model.Restart();
        fsm.PerformTransition(Transition.StartButtonClick);       
    }

    /// <summary>
    /// 返回主页面
    /// </summary>
    public void HomeClickButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
