﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuState : FSMState
{
    private void Awake()
    {
        stateID = StateID.Menu;

        // 绑定状态事件
        AddTransition(Transition.StartButtonClick,StateID.Play);
    }

    public override void DoBeforeEntering()
    {
        base.DoBeforeEntering();

        // 显示菜单UI
        controller.view.ShowMenu();

        // 显示相机动画
        controller.cameraManager.ZoomOut();
    }

    public override void DoBeforeLeaving()
    {
        base.DoBeforeLeaving();
        controller.view.HideMenu();
    }

    /// <summary>
    /// 点击开始按钮事件回调
    /// </summary>
    public void StartButtonClick()
    {
        controller.audioManager.PlayClickAudio();
        fsm.PerformTransition(Transition.StartButtonClick);
    }

    /// <summary>
    /// 点击Seting按钮事件回调
    /// </summary>
    public void SetingButtonClick()
    {
        controller.audioManager.PlayClickAudio();
        controller.view.ShowSettingUI();
    }

    /// <summary>
    /// 设置是否播放音效
    /// </summary>
    public void SetAudioPauseButtonClick()
    {
        controller.audioManager.PlayClickAudio();        
        controller.audioManager.IsPause = !controller.audioManager.IsPause;
        controller.view.IsShowAudioLine(controller.audioManager.IsPause);
    }

    /// <summary>
    /// 退出设置面板
    /// </summary>
    public void ExitSettingUI()
    {
        controller.audioManager.PlayClickAudio();
        controller.view.HideSettingUI();
    }

    /// <summary>
    /// 重新开始游戏
    /// </summary>
    public void ReStartGame()
    {
        controller.audioManager.PlayClickAudio();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    /// <summary>
    /// 显示记录菜单
    /// </summary>
    public void RankButtonClick()
    {
        controller.audioManager.PlayClickAudio();
        controller.view.ShowRankUI(controller.model.Score,controller.model.HighScore,controller.model.NumbersGame);
    }

    /// <summary>
    /// 隐藏记录菜单
    /// </summary>
    public void ExitRankButtonClick()
    {
        controller.audioManager.PlayClickAudio();
        controller.view.HideRankUI();
    }

    /// <summary>
    /// 清除所有记录
    /// </summary>
    public void ClearClickButton()
    {
        controller.model.ClearData();
        controller.view.ShowRankData(controller.model.Score, controller.model.HighScore, controller.model.NumbersGame);
    }
}
