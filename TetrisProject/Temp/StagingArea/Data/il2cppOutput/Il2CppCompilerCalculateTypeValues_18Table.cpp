﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition605142169.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Selection3187567897.h"
#include "UnityEngine_UI_UnityEngine_UI_SetPropertyUtility4019374597.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider297367283.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction1525323322.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2111116400.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis375128448.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState1353336012.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial1630303189.h"
#include "UnityEngine_UI_UnityEngine_UI_StencilMaterial_MatE3157325053.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle3976754468.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleTransit1114673831.h"
#include "UnityEngine_UI_UnityEngine_UI_Toggle_ToggleEvent1896830814.h"
#include "UnityEngine_UI_UnityEngine_UI_ToggleGroup1030026315.h"
#include "UnityEngine_UI_UnityEngine_UI_ClipperRegistry1349564894.h"
#include "UnityEngine_UI_UnityEngine_UI_Clipping223789604.h"
#include "UnityEngine_UI_UnityEngine_UI_RectangularVertexCli3349113845.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter3114550109.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_As1166448724.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler2574720772.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMod987318053.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenM1916789528.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit3220761768.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter1325211874.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_Fi4030874534.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup1515633077.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corn1077473318.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis1431825778.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Cons3558160636.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalLayoutGrou2875670365.h"
#include "UnityEngine_UI_UnityEngine_UI_HorizontalOrVertical1968298610.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutElement2808691390.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup3962498969.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutGroup_U3CDelay3228926346.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder2155218138.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutUtility4076838048.h"
#include "UnityEngine_UI_UnityEngine_UI_VerticalLayoutGroup2468316403.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3343836395.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3928470916.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2260664863.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac3435657708.h"
#include "UnityEngine_UI_UnityEngine_UI_ReflectionMethodsCac2213949596.h"
#include "UnityEngine_UI_UnityEngine_UI_VertexHelper385374196.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseVertexEffect2504093552.h"
#include "UnityEngine_UI_UnityEngine_UI_BaseMeshEffect1728560551.h"
#include "UnityEngine_UI_UnityEngine_UI_Outline1417504278.h"
#include "UnityEngine_UI_UnityEngine_UI_PositionAsUV11102546563.h"
#include "UnityEngine_UI_UnityEngine_UI_Shadow4269599528.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "UnityEngine_UI_U3CPrivateImplementationDetailsU3E_1568637717.h"
#include "DOTween43_U3CModuleU3E3783534214.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43301896125.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334720.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334751.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334817.h"
#include "DOTween43_DG_Tweening_ShortcutExtensions43_U3CU3Ec_426334918.h"
#include "DOTween46_U3CModuleU3E3783534214.h"
#include "DOTween46_DG_Tweening_DOTweenUtils461550156519.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46705180652.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939845.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939942.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3371939977.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039116993.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3017480080.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130305.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124924.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec4039117214.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_308799891.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec3582130274.h"
#include "DOTween46_DG_Tweening_ShortcutExtensions46_U3CU3Ec_591124893.h"
#include "DOTweenPro_U3CModuleU3E3783534214.h"
#include "DOTweenPro_DG_Tweening_DOTweenVisualManager2945673405.h"
#include "DOTweenPro_DG_Tweening_HandlesDrawMode3273484032.h"
#include "DOTweenPro_DG_Tweening_HandlesType3201532857.h"
#include "DOTweenPro_DG_Tweening_DOTweenInspectorMode2739551672.h"
#include "DOTweenPro_DG_Tweening_DOTweenPath1397145371.h"
#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "DOTweenPro_DG_Tweening_Core_DOTweenAnimationType119935370.h"
#include "DOTweenPro_DG_Tweening_Core_OnDisableBehaviour125315118.h"
#include "DOTweenPro_DG_Tweening_Core_OnEnableBehaviour285142911.h"
#include "DOTweenPro_DG_Tweening_Core_TargetType2706200073.h"
#include "DOTweenPro_DG_Tweening_Core_VisualManagerPreset4087939440.h"
#include "UnityEngine_Analytics_U3CModuleU3E3783534214.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt2191537572.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Analyt1068911718.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka1304606600.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1800 = { sizeof (Transition_t605142169)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1800[5] = 
{
	Transition_t605142169::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1801 = { sizeof (SelectionState_t3187567897)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1801[5] = 
{
	SelectionState_t3187567897::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1802 = { sizeof (SetPropertyUtility_t4019374597), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1803 = { sizeof (Slider_t297367283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1803[15] = 
{
	Slider_t297367283::get_offset_of_m_FillRect_16(),
	Slider_t297367283::get_offset_of_m_HandleRect_17(),
	Slider_t297367283::get_offset_of_m_Direction_18(),
	Slider_t297367283::get_offset_of_m_MinValue_19(),
	Slider_t297367283::get_offset_of_m_MaxValue_20(),
	Slider_t297367283::get_offset_of_m_WholeNumbers_21(),
	Slider_t297367283::get_offset_of_m_Value_22(),
	Slider_t297367283::get_offset_of_m_OnValueChanged_23(),
	Slider_t297367283::get_offset_of_m_FillImage_24(),
	Slider_t297367283::get_offset_of_m_FillTransform_25(),
	Slider_t297367283::get_offset_of_m_FillContainerRect_26(),
	Slider_t297367283::get_offset_of_m_HandleTransform_27(),
	Slider_t297367283::get_offset_of_m_HandleContainerRect_28(),
	Slider_t297367283::get_offset_of_m_Offset_29(),
	Slider_t297367283::get_offset_of_m_Tracker_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1804 = { sizeof (Direction_t1525323322)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1804[5] = 
{
	Direction_t1525323322::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1805 = { sizeof (SliderEvent_t2111116400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1806 = { sizeof (Axis_t375128448)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1806[3] = 
{
	Axis_t375128448::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1807 = { sizeof (SpriteState_t1353336012)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1807[3] = 
{
	SpriteState_t1353336012::get_offset_of_m_HighlightedSprite_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_PressedSprite_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	SpriteState_t1353336012::get_offset_of_m_DisabledSprite_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1808 = { sizeof (StencilMaterial_t1630303189), -1, sizeof(StencilMaterial_t1630303189_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1808[1] = 
{
	StencilMaterial_t1630303189_StaticFields::get_offset_of_m_List_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1809 = { sizeof (MatEntry_t3157325053), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1809[10] = 
{
	MatEntry_t3157325053::get_offset_of_baseMat_0(),
	MatEntry_t3157325053::get_offset_of_customMat_1(),
	MatEntry_t3157325053::get_offset_of_count_2(),
	MatEntry_t3157325053::get_offset_of_stencilId_3(),
	MatEntry_t3157325053::get_offset_of_operation_4(),
	MatEntry_t3157325053::get_offset_of_compareFunction_5(),
	MatEntry_t3157325053::get_offset_of_readMask_6(),
	MatEntry_t3157325053::get_offset_of_writeMask_7(),
	MatEntry_t3157325053::get_offset_of_useAlphaClip_8(),
	MatEntry_t3157325053::get_offset_of_colorMask_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1810 = { sizeof (Text_t356221433), -1, sizeof(Text_t356221433_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1810[7] = 
{
	Text_t356221433::get_offset_of_m_FontData_28(),
	Text_t356221433::get_offset_of_m_Text_29(),
	Text_t356221433::get_offset_of_m_TextCache_30(),
	Text_t356221433::get_offset_of_m_TextCacheForLayout_31(),
	Text_t356221433_StaticFields::get_offset_of_s_DefaultText_32(),
	Text_t356221433::get_offset_of_m_DisableFontTextureRebuiltCallback_33(),
	Text_t356221433::get_offset_of_m_TempVerts_34(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1811 = { sizeof (Toggle_t3976754468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1811[5] = 
{
	Toggle_t3976754468::get_offset_of_toggleTransition_16(),
	Toggle_t3976754468::get_offset_of_graphic_17(),
	Toggle_t3976754468::get_offset_of_m_Group_18(),
	Toggle_t3976754468::get_offset_of_onValueChanged_19(),
	Toggle_t3976754468::get_offset_of_m_IsOn_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1812 = { sizeof (ToggleTransition_t1114673831)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1812[3] = 
{
	ToggleTransition_t1114673831::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1813 = { sizeof (ToggleEvent_t1896830814), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1814 = { sizeof (ToggleGroup_t1030026315), -1, sizeof(ToggleGroup_t1030026315_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1814[4] = 
{
	ToggleGroup_t1030026315::get_offset_of_m_AllowSwitchOff_2(),
	ToggleGroup_t1030026315::get_offset_of_m_Toggles_3(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	ToggleGroup_t1030026315_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1815 = { sizeof (ClipperRegistry_t1349564894), -1, sizeof(ClipperRegistry_t1349564894_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1815[2] = 
{
	ClipperRegistry_t1349564894_StaticFields::get_offset_of_s_Instance_0(),
	ClipperRegistry_t1349564894::get_offset_of_m_Clippers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1816 = { sizeof (Clipping_t223789604), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1817 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1818 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1819 = { sizeof (RectangularVertexClipper_t3349113845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1819[2] = 
{
	RectangularVertexClipper_t3349113845::get_offset_of_m_WorldCorners_0(),
	RectangularVertexClipper_t3349113845::get_offset_of_m_CanvasCorners_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1820 = { sizeof (AspectRatioFitter_t3114550109), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1820[4] = 
{
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectMode_2(),
	AspectRatioFitter_t3114550109::get_offset_of_m_AspectRatio_3(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Rect_4(),
	AspectRatioFitter_t3114550109::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1821 = { sizeof (AspectMode_t1166448724)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1821[6] = 
{
	AspectMode_t1166448724::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1822 = { sizeof (CanvasScaler_t2574720772), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1822[14] = 
{
	CanvasScaler_t2574720772::get_offset_of_m_UiScaleMode_2(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferencePixelsPerUnit_3(),
	CanvasScaler_t2574720772::get_offset_of_m_ScaleFactor_4(),
	CanvasScaler_t2574720772::get_offset_of_m_ReferenceResolution_5(),
	CanvasScaler_t2574720772::get_offset_of_m_ScreenMatchMode_6(),
	CanvasScaler_t2574720772::get_offset_of_m_MatchWidthOrHeight_7(),
	0,
	CanvasScaler_t2574720772::get_offset_of_m_PhysicalUnit_9(),
	CanvasScaler_t2574720772::get_offset_of_m_FallbackScreenDPI_10(),
	CanvasScaler_t2574720772::get_offset_of_m_DefaultSpriteDPI_11(),
	CanvasScaler_t2574720772::get_offset_of_m_DynamicPixelsPerUnit_12(),
	CanvasScaler_t2574720772::get_offset_of_m_Canvas_13(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevScaleFactor_14(),
	CanvasScaler_t2574720772::get_offset_of_m_PrevReferencePixelsPerUnit_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1823 = { sizeof (ScaleMode_t987318053)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1823[4] = 
{
	ScaleMode_t987318053::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1824 = { sizeof (ScreenMatchMode_t1916789528)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1824[4] = 
{
	ScreenMatchMode_t1916789528::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1825 = { sizeof (Unit_t3220761768)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1825[6] = 
{
	Unit_t3220761768::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1826 = { sizeof (ContentSizeFitter_t1325211874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1826[4] = 
{
	ContentSizeFitter_t1325211874::get_offset_of_m_HorizontalFit_2(),
	ContentSizeFitter_t1325211874::get_offset_of_m_VerticalFit_3(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Rect_4(),
	ContentSizeFitter_t1325211874::get_offset_of_m_Tracker_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1827 = { sizeof (FitMode_t4030874534)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1827[4] = 
{
	FitMode_t4030874534::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1828 = { sizeof (GridLayoutGroup_t1515633077), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1828[6] = 
{
	GridLayoutGroup_t1515633077::get_offset_of_m_StartCorner_10(),
	GridLayoutGroup_t1515633077::get_offset_of_m_StartAxis_11(),
	GridLayoutGroup_t1515633077::get_offset_of_m_CellSize_12(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Spacing_13(),
	GridLayoutGroup_t1515633077::get_offset_of_m_Constraint_14(),
	GridLayoutGroup_t1515633077::get_offset_of_m_ConstraintCount_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1829 = { sizeof (Corner_t1077473318)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1829[5] = 
{
	Corner_t1077473318::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1830 = { sizeof (Axis_t1431825778)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1830[3] = 
{
	Axis_t1431825778::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1831 = { sizeof (Constraint_t3558160636)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1831[4] = 
{
	Constraint_t3558160636::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1832 = { sizeof (HorizontalLayoutGroup_t2875670365), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1833 = { sizeof (HorizontalOrVerticalLayoutGroup_t1968298610), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1833[5] = 
{
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_Spacing_10(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandWidth_11(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildForceExpandHeight_12(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlWidth_13(),
	HorizontalOrVerticalLayoutGroup_t1968298610::get_offset_of_m_ChildControlHeight_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1835 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1837 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1838 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1839 = { sizeof (LayoutElement_t2808691390), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1839[7] = 
{
	LayoutElement_t2808691390::get_offset_of_m_IgnoreLayout_2(),
	LayoutElement_t2808691390::get_offset_of_m_MinWidth_3(),
	LayoutElement_t2808691390::get_offset_of_m_MinHeight_4(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredWidth_5(),
	LayoutElement_t2808691390::get_offset_of_m_PreferredHeight_6(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleWidth_7(),
	LayoutElement_t2808691390::get_offset_of_m_FlexibleHeight_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1840 = { sizeof (LayoutGroup_t3962498969), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1840[8] = 
{
	LayoutGroup_t3962498969::get_offset_of_m_Padding_2(),
	LayoutGroup_t3962498969::get_offset_of_m_ChildAlignment_3(),
	LayoutGroup_t3962498969::get_offset_of_m_Rect_4(),
	LayoutGroup_t3962498969::get_offset_of_m_Tracker_5(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalMinSize_6(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalPreferredSize_7(),
	LayoutGroup_t3962498969::get_offset_of_m_TotalFlexibleSize_8(),
	LayoutGroup_t3962498969::get_offset_of_m_RectChildren_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1841 = { sizeof (U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1841[4] = 
{
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_rectTransform_0(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24current_1(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24disposing_2(),
	U3CDelayedSetDirtyU3Ec__Iterator0_t3228926346::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1842 = { sizeof (LayoutRebuilder_t2155218138), -1, sizeof(LayoutRebuilder_t2155218138_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1842[9] = 
{
	LayoutRebuilder_t2155218138::get_offset_of_m_ToRebuild_0(),
	LayoutRebuilder_t2155218138::get_offset_of_m_CachedHashFromTransform_1(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_s_Rebuilders_2(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_3(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
	LayoutRebuilder_t2155218138_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1843 = { sizeof (LayoutUtility_t4076838048), -1, sizeof(LayoutUtility_t4076838048_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1843[8] = 
{
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_0(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_1(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_2(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_3(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_4(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_5(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_6(),
	LayoutUtility_t4076838048_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1844 = { sizeof (VerticalLayoutGroup_t2468316403), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1845 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1846 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1846[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1847 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1847[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1848 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1848[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1849 = { sizeof (ReflectionMethodsCache_t3343836395), -1, sizeof(ReflectionMethodsCache_t3343836395_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1849[5] = 
{
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3D_0(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast3DAll_1(),
	ReflectionMethodsCache_t3343836395::get_offset_of_raycast2D_2(),
	ReflectionMethodsCache_t3343836395::get_offset_of_getRayIntersectionAll_3(),
	ReflectionMethodsCache_t3343836395_StaticFields::get_offset_of_s_ReflectionMethodsCache_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1850 = { sizeof (Raycast3DCallback_t3928470916), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1851 = { sizeof (Raycast2DCallback_t2260664863), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1852 = { sizeof (RaycastAllCallback_t3435657708), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1853 = { sizeof (GetRayIntersectionAllCallback_t2213949596), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1854 = { sizeof (VertexHelper_t385374196), -1, sizeof(VertexHelper_t385374196_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1854[11] = 
{
	VertexHelper_t385374196::get_offset_of_m_Positions_0(),
	VertexHelper_t385374196::get_offset_of_m_Colors_1(),
	VertexHelper_t385374196::get_offset_of_m_Uv0S_2(),
	VertexHelper_t385374196::get_offset_of_m_Uv1S_3(),
	VertexHelper_t385374196::get_offset_of_m_Uv2S_4(),
	VertexHelper_t385374196::get_offset_of_m_Uv3S_5(),
	VertexHelper_t385374196::get_offset_of_m_Normals_6(),
	VertexHelper_t385374196::get_offset_of_m_Tangents_7(),
	VertexHelper_t385374196::get_offset_of_m_Indices_8(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultTangent_9(),
	VertexHelper_t385374196_StaticFields::get_offset_of_s_DefaultNormal_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1855 = { sizeof (BaseVertexEffect_t2504093552), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1856 = { sizeof (BaseMeshEffect_t1728560551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1856[1] = 
{
	BaseMeshEffect_t1728560551::get_offset_of_m_Graphic_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1857 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1858 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1859 = { sizeof (Outline_t1417504278), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1860 = { sizeof (PositionAsUV1_t1102546563), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1861 = { sizeof (Shadow_t4269599528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1861[4] = 
{
	Shadow_t4269599528::get_offset_of_m_EffectColor_3(),
	Shadow_t4269599528::get_offset_of_m_EffectDistance_4(),
	Shadow_t4269599528::get_offset_of_m_UseGraphicAlpha_5(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1862 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305142), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1862[1] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305142_StaticFields::get_offset_of_U24fieldU2D7BBE37982E6C057ED87163CAFC7FD6E5E42EEA46_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1863 = { sizeof (U24ArrayTypeU3D12_t1568637717)+ sizeof (Il2CppObject), sizeof(U24ArrayTypeU3D12_t1568637717 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1864 = { sizeof (U3CModuleU3E_t3783534221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1865 = { sizeof (ShortcutExtensions43_t301896125), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1866 = { sizeof (U3CU3Ec__DisplayClass2_0_t426334720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1866[1] = 
{
	U3CU3Ec__DisplayClass2_0_t426334720::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1867 = { sizeof (U3CU3Ec__DisplayClass3_0_t426334751), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1867[1] = 
{
	U3CU3Ec__DisplayClass3_0_t426334751::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1868 = { sizeof (U3CU3Ec__DisplayClass5_0_t426334817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1868[1] = 
{
	U3CU3Ec__DisplayClass5_0_t426334817::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1869 = { sizeof (U3CU3Ec__DisplayClass8_0_t426334918), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1869[1] = 
{
	U3CU3Ec__DisplayClass8_0_t426334918::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1870 = { sizeof (U3CModuleU3E_t3783534222), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1871 = { sizeof (DOTweenUtils46_t1550156519), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1872 = { sizeof (ShortcutExtensions46_t705180652), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1873 = { sizeof (U3CU3Ec__DisplayClass0_0_t3371939845), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1873[1] = 
{
	U3CU3Ec__DisplayClass0_0_t3371939845::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1874 = { sizeof (U3CU3Ec__DisplayClass3_0_t3371939942), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1874[1] = 
{
	U3CU3Ec__DisplayClass3_0_t3371939942::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1875 = { sizeof (U3CU3Ec__DisplayClass4_0_t3371939977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1875[1] = 
{
	U3CU3Ec__DisplayClass4_0_t3371939977::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1876 = { sizeof (U3CU3Ec__DisplayClass15_0_t4039116993), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1876[1] = 
{
	U3CU3Ec__DisplayClass15_0_t4039116993::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1877 = { sizeof (U3CU3Ec__DisplayClass16_0_t3017480080), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1877[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3017480080::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1878 = { sizeof (U3CU3Ec__DisplayClass22_0_t3582130305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1878[1] = 
{
	U3CU3Ec__DisplayClass22_0_t3582130305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1879 = { sizeof (U3CU3Ec__DisplayClass23_0_t591124924), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1879[1] = 
{
	U3CU3Ec__DisplayClass23_0_t591124924::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1880 = { sizeof (U3CU3Ec__DisplayClass25_0_t4039117214), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1880[1] = 
{
	U3CU3Ec__DisplayClass25_0_t4039117214::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1881 = { sizeof (U3CU3Ec__DisplayClass31_0_t308799891), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1881[1] = 
{
	U3CU3Ec__DisplayClass31_0_t308799891::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1882 = { sizeof (U3CU3Ec__DisplayClass32_0_t3582130274), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1882[1] = 
{
	U3CU3Ec__DisplayClass32_0_t3582130274::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1883 = { sizeof (U3CU3Ec__DisplayClass33_0_t591124893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1883[1] = 
{
	U3CU3Ec__DisplayClass33_0_t591124893::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1884 = { sizeof (U3CModuleU3E_t3783534223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1885 = { sizeof (DOTweenVisualManager_t2945673405), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1885[5] = 
{
	DOTweenVisualManager_t2945673405::get_offset_of_preset_2(),
	DOTweenVisualManager_t2945673405::get_offset_of_onEnableBehaviour_3(),
	DOTweenVisualManager_t2945673405::get_offset_of_onDisableBehaviour_4(),
	DOTweenVisualManager_t2945673405::get_offset_of__requiresRestartFromSpawnPoint_5(),
	DOTweenVisualManager_t2945673405::get_offset_of__animComponent_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1886 = { sizeof (HandlesDrawMode_t3273484032)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1886[3] = 
{
	HandlesDrawMode_t3273484032::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1887 = { sizeof (HandlesType_t3201532857)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1887[3] = 
{
	HandlesType_t3201532857::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1888 = { sizeof (DOTweenInspectorMode_t2739551672)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1888[5] = 
{
	DOTweenInspectorMode_t2739551672::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1889 = { sizeof (DOTweenPath_t1397145371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1889[38] = 
{
	DOTweenPath_t1397145371::get_offset_of_delay_19(),
	DOTweenPath_t1397145371::get_offset_of_duration_20(),
	DOTweenPath_t1397145371::get_offset_of_easeType_21(),
	DOTweenPath_t1397145371::get_offset_of_easeCurve_22(),
	DOTweenPath_t1397145371::get_offset_of_loops_23(),
	DOTweenPath_t1397145371::get_offset_of_id_24(),
	DOTweenPath_t1397145371::get_offset_of_loopType_25(),
	DOTweenPath_t1397145371::get_offset_of_orientType_26(),
	DOTweenPath_t1397145371::get_offset_of_lookAtTransform_27(),
	DOTweenPath_t1397145371::get_offset_of_lookAtPosition_28(),
	DOTweenPath_t1397145371::get_offset_of_lookAhead_29(),
	DOTweenPath_t1397145371::get_offset_of_autoPlay_30(),
	DOTweenPath_t1397145371::get_offset_of_autoKill_31(),
	DOTweenPath_t1397145371::get_offset_of_relative_32(),
	DOTweenPath_t1397145371::get_offset_of_isLocal_33(),
	DOTweenPath_t1397145371::get_offset_of_isClosedPath_34(),
	DOTweenPath_t1397145371::get_offset_of_pathResolution_35(),
	DOTweenPath_t1397145371::get_offset_of_pathMode_36(),
	DOTweenPath_t1397145371::get_offset_of_lockRotation_37(),
	DOTweenPath_t1397145371::get_offset_of_assignForwardAndUp_38(),
	DOTweenPath_t1397145371::get_offset_of_forwardDirection_39(),
	DOTweenPath_t1397145371::get_offset_of_upDirection_40(),
	DOTweenPath_t1397145371::get_offset_of_tweenRigidbody_41(),
	DOTweenPath_t1397145371::get_offset_of_wps_42(),
	DOTweenPath_t1397145371::get_offset_of_fullWps_43(),
	DOTweenPath_t1397145371::get_offset_of_path_44(),
	DOTweenPath_t1397145371::get_offset_of_inspectorMode_45(),
	DOTweenPath_t1397145371::get_offset_of_pathType_46(),
	DOTweenPath_t1397145371::get_offset_of_handlesType_47(),
	DOTweenPath_t1397145371::get_offset_of_livePreview_48(),
	DOTweenPath_t1397145371::get_offset_of_handlesDrawMode_49(),
	DOTweenPath_t1397145371::get_offset_of_perspectiveHandleSize_50(),
	DOTweenPath_t1397145371::get_offset_of_showIndexes_51(),
	DOTweenPath_t1397145371::get_offset_of_showWpLength_52(),
	DOTweenPath_t1397145371::get_offset_of_pathColor_53(),
	DOTweenPath_t1397145371::get_offset_of_lastSrcPosition_54(),
	DOTweenPath_t1397145371::get_offset_of_wpsDropdown_55(),
	DOTweenPath_t1397145371::get_offset_of_dropToFloorOffset_56(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1890 = { sizeof (ABSAnimationComponent_t2205594551), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1890[17] = 
{
	ABSAnimationComponent_t2205594551::get_offset_of_updateType_2(),
	ABSAnimationComponent_t2205594551::get_offset_of_isSpeedBased_3(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStart_4(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnPlay_5(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnUpdate_6(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnStepComplete_7(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnComplete_8(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnTweenCreated_9(),
	ABSAnimationComponent_t2205594551::get_offset_of_hasOnRewind_10(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStart_11(),
	ABSAnimationComponent_t2205594551::get_offset_of_onPlay_12(),
	ABSAnimationComponent_t2205594551::get_offset_of_onUpdate_13(),
	ABSAnimationComponent_t2205594551::get_offset_of_onStepComplete_14(),
	ABSAnimationComponent_t2205594551::get_offset_of_onComplete_15(),
	ABSAnimationComponent_t2205594551::get_offset_of_onTweenCreated_16(),
	ABSAnimationComponent_t2205594551::get_offset_of_onRewind_17(),
	ABSAnimationComponent_t2205594551::get_offset_of_tween_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1891 = { sizeof (DOTweenAnimationType_t119935370)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1891[23] = 
{
	DOTweenAnimationType_t119935370::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1892 = { sizeof (OnDisableBehaviour_t125315118)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1892[7] = 
{
	OnDisableBehaviour_t125315118::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1893 = { sizeof (OnEnableBehaviour_t285142911)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1893[5] = 
{
	OnEnableBehaviour_t285142911::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1894 = { sizeof (TargetType_t2706200073)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1894[17] = 
{
	TargetType_t2706200073::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1895 = { sizeof (VisualManagerPreset_t4087939440)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1895[3] = 
{
	VisualManagerPreset_t4087939440::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1896 = { sizeof (U3CModuleU3E_t3783534224), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1897 = { sizeof (AnalyticsTracker_t2191537572), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1897[5] = 
{
	AnalyticsTracker_t2191537572::get_offset_of_m_EventName_2(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Dict_3(),
	AnalyticsTracker_t2191537572::get_offset_of_m_PrevDictHash_4(),
	AnalyticsTracker_t2191537572::get_offset_of_m_TrackableProperty_5(),
	AnalyticsTracker_t2191537572::get_offset_of_m_Trigger_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1898 = { sizeof (Trigger_t1068911718)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1898[8] = 
{
	Trigger_t1068911718::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1899 = { sizeof (TrackableProperty_t1304606600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1899[2] = 
{
	0,
	TrackableProperty_t1304606600::get_offset_of_m_Fields_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
