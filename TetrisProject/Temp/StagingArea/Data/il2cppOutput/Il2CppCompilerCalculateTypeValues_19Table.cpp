﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_Analytics_UnityEngine_Analytics_Tracka2256174789.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimation858634588.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimationExte4140352346.h"
#include "AssemblyU2DCSharp_AudioManager4222704959.h"
#include "AssemblyU2DCSharp_CameraManager2379859346.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_JarodInputController1982328090.h"
#include "AssemblyU2DCSharp_Shape1303907469.h"
#include "AssemblyU2DCSharp_Transition3224717525.h"
#include "AssemblyU2DCSharp_StateID3221758012.h"
#include "AssemblyU2DCSharp_FSMState3109806909.h"
#include "AssemblyU2DCSharp_FSMSystem2363122359.h"
#include "AssemblyU2DCSharp_GameOverState4251729781.h"
#include "AssemblyU2DCSharp_MenuState2133158756.h"
#include "AssemblyU2DCSharp_PauseState1378269423.h"
#include "AssemblyU2DCSharp_PlayState1063998429.h"
#include "AssemblyU2DCSharp_Model873752437.h"
#include "AssemblyU2DCSharp_Vector3Extension2323347309.h"
#include "AssemblyU2DCSharp_View854181575.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1900 = { sizeof (FieldWithTarget_t2256174789), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1900[6] = 
{
	FieldWithTarget_t2256174789::get_offset_of_m_ParamName_0(),
	FieldWithTarget_t2256174789::get_offset_of_m_Target_1(),
	FieldWithTarget_t2256174789::get_offset_of_m_FieldPath_2(),
	FieldWithTarget_t2256174789::get_offset_of_m_TypeString_3(),
	FieldWithTarget_t2256174789::get_offset_of_m_DoStatic_4(),
	FieldWithTarget_t2256174789::get_offset_of_m_StaticString_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1901 = { sizeof (U3CModuleU3E_t3783534225), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1902 = { sizeof (DOTweenAnimation_t858634588), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1902[34] = 
{
	DOTweenAnimation_t858634588::get_offset_of_delay_19(),
	DOTweenAnimation_t858634588::get_offset_of_duration_20(),
	DOTweenAnimation_t858634588::get_offset_of_easeType_21(),
	DOTweenAnimation_t858634588::get_offset_of_easeCurve_22(),
	DOTweenAnimation_t858634588::get_offset_of_loopType_23(),
	DOTweenAnimation_t858634588::get_offset_of_loops_24(),
	DOTweenAnimation_t858634588::get_offset_of_id_25(),
	DOTweenAnimation_t858634588::get_offset_of_isRelative_26(),
	DOTweenAnimation_t858634588::get_offset_of_isFrom_27(),
	DOTweenAnimation_t858634588::get_offset_of_isIndependentUpdate_28(),
	DOTweenAnimation_t858634588::get_offset_of_autoKill_29(),
	DOTweenAnimation_t858634588::get_offset_of_isActive_30(),
	DOTweenAnimation_t858634588::get_offset_of_isValid_31(),
	DOTweenAnimation_t858634588::get_offset_of_target_32(),
	DOTweenAnimation_t858634588::get_offset_of_animationType_33(),
	DOTweenAnimation_t858634588::get_offset_of_targetType_34(),
	DOTweenAnimation_t858634588::get_offset_of_forcedTargetType_35(),
	DOTweenAnimation_t858634588::get_offset_of_autoPlay_36(),
	DOTweenAnimation_t858634588::get_offset_of_useTargetAsV3_37(),
	DOTweenAnimation_t858634588::get_offset_of_endValueFloat_38(),
	DOTweenAnimation_t858634588::get_offset_of_endValueV3_39(),
	DOTweenAnimation_t858634588::get_offset_of_endValueV2_40(),
	DOTweenAnimation_t858634588::get_offset_of_endValueColor_41(),
	DOTweenAnimation_t858634588::get_offset_of_endValueString_42(),
	DOTweenAnimation_t858634588::get_offset_of_endValueRect_43(),
	DOTweenAnimation_t858634588::get_offset_of_endValueTransform_44(),
	DOTweenAnimation_t858634588::get_offset_of_optionalBool0_45(),
	DOTweenAnimation_t858634588::get_offset_of_optionalFloat0_46(),
	DOTweenAnimation_t858634588::get_offset_of_optionalInt0_47(),
	DOTweenAnimation_t858634588::get_offset_of_optionalRotationMode_48(),
	DOTweenAnimation_t858634588::get_offset_of_optionalScrambleMode_49(),
	DOTweenAnimation_t858634588::get_offset_of_optionalString_50(),
	DOTweenAnimation_t858634588::get_offset_of__tweenCreated_51(),
	DOTweenAnimation_t858634588::get_offset_of__playCount_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1903 = { sizeof (DOTweenAnimationExtensions_t4140352346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1904 = { sizeof (AudioManager_t4222704959), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1904[6] = 
{
	AudioManager_t4222704959::get_offset_of_clickClip_2(),
	AudioManager_t4222704959::get_offset_of_fallClip_3(),
	AudioManager_t4222704959::get_offset_of_ctrlClip_4(),
	AudioManager_t4222704959::get_offset_of_deleteRowClip_5(),
	AudioManager_t4222704959::get_offset_of_audioSource_6(),
	AudioManager_t4222704959::get_offset_of_isPause_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1905 = { sizeof (CameraManager_t2379859346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1905[1] = 
{
	CameraManager_t2379859346::get_offset_of_myCamera_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1906 = { sizeof (Controller_t1937198888), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1906[6] = 
{
	Controller_t1937198888::get_offset_of_model_2(),
	Controller_t1937198888::get_offset_of_view_3(),
	Controller_t1937198888::get_offset_of_cameraManager_4(),
	Controller_t1937198888::get_offset_of_gameManager_5(),
	Controller_t1937198888::get_offset_of_audioManager_6(),
	Controller_t1937198888::get_offset_of_fsm_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1907 = { sizeof (GameManager_t2252321495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1907[5] = 
{
	GameManager_t2252321495::get_offset_of_isPause_2(),
	GameManager_t2252321495::get_offset_of_shapes_3(),
	GameManager_t2252321495::get_offset_of_colors_4(),
	GameManager_t2252321495::get_offset_of_currentShape_5(),
	GameManager_t2252321495::get_offset_of_controller_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1908 = { sizeof (JarodInputController_t1982328090), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1908[12] = 
{
	JarodInputController_t1982328090::get_offset_of_fingerActionSensitivity_2(),
	JarodInputController_t1982328090::get_offset_of_fingerBeginX_3(),
	JarodInputController_t1982328090::get_offset_of_fingerBeginY_4(),
	JarodInputController_t1982328090::get_offset_of_fingerCurrentX_5(),
	JarodInputController_t1982328090::get_offset_of_fingerCurrentY_6(),
	JarodInputController_t1982328090::get_offset_of_fingerSegmentX_7(),
	JarodInputController_t1982328090::get_offset_of_fingerSegmentY_8(),
	JarodInputController_t1982328090::get_offset_of_fingerTouchState_9(),
	JarodInputController_t1982328090::get_offset_of_FINGER_STATE_NULL_10(),
	JarodInputController_t1982328090::get_offset_of_FINGER_STATE_TOUCH_11(),
	JarodInputController_t1982328090::get_offset_of_FINGER_STATE_ADD_12(),
	JarodInputController_t1982328090::get_offset_of_controller_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1909 = { sizeof (Shape_t1303907469), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1909[10] = 
{
	Shape_t1303907469::get_offset_of_controller_2(),
	Shape_t1303907469::get_offset_of_gameManager_3(),
	Shape_t1303907469::get_offset_of_isPause_4(),
	Shape_t1303907469::get_offset_of_timer_5(),
	Shape_t1303907469::get_offset_of_stepTime_6(),
	Shape_t1303907469::get_offset_of_stepUpTime_7(),
	Shape_t1303907469::get_offset_of_stepTempTime_8(),
	Shape_t1303907469::get_offset_of_Pivot_9(),
	Shape_t1303907469::get_offset_of_startPosFlag_10(),
	Shape_t1303907469::get_offset_of_backValue_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1910 = { sizeof (Transition_t3224717525)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1910[5] = 
{
	Transition_t3224717525::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1911 = { sizeof (StateID_t3221758012)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1911[6] = 
{
	StateID_t3221758012::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1912 = { sizeof (FSMState_t3109806909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1912[4] = 
{
	FSMState_t3109806909::get_offset_of_controller_2(),
	FSMState_t3109806909::get_offset_of_fsm_3(),
	FSMState_t3109806909::get_offset_of_map_4(),
	FSMState_t3109806909::get_offset_of_stateID_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1913 = { sizeof (FSMSystem_t2363122359), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1913[3] = 
{
	FSMSystem_t2363122359::get_offset_of_states_0(),
	FSMSystem_t2363122359::get_offset_of_currentStateID_1(),
	FSMSystem_t2363122359::get_offset_of_currentState_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1914 = { sizeof (GameOverState_t4251729781), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1915 = { sizeof (MenuState_t2133158756), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1916 = { sizeof (PauseState_t1378269423), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1917 = { sizeof (PlayState_t1063998429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1918 = { sizeof (Model_t873752437), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1918[8] = 
{
	0,
	0,
	0,
	Model_t873752437::get_offset_of_score_5(),
	Model_t873752437::get_offset_of_highScore_6(),
	Model_t873752437::get_offset_of_numbersGame_7(),
	Model_t873752437::get_offset_of_isDataUpdate_8(),
	Model_t873752437::get_offset_of_map_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1919 = { sizeof (Vector3Extension_t2323347309), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1920 = { sizeof (View_t854181575), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1920[11] = 
{
	View_t854181575::get_offset_of_logoName_2(),
	View_t854181575::get_offset_of_menuUI_3(),
	View_t854181575::get_offset_of_gameUI_4(),
	View_t854181575::get_offset_of_gameOverUI_5(),
	View_t854181575::get_offset_of_settingUI_6(),
	View_t854181575::get_offset_of_audioMute_7(),
	View_t854181575::get_offset_of_rankUI_8(),
	View_t854181575::get_offset_of_ReStartButton_9(),
	View_t854181575::get_offset_of_score_10(),
	View_t854181575::get_offset_of_highScore_11(),
	View_t854181575::get_offset_of_gameOverScore_12(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
