﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "System_Core_System_Func_2_gen2212564818.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "mscorlib_System_Void1841601450.h"
#include "mscorlib_System_Single2076509932.h"
#include "mscorlib_System_AsyncCallback163412349.h"
#include "System_Core_System_Linq_Enumerable_U3CCreateWhereI4121738870.h"
#include "mscorlib_System_Int322071877448.h"
#include "System_Core_System_Func_2_gen3961629604.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_UInt322149682021.h"
#include "mscorlib_System_NotSupportedException1793819818.h"
#include "mscorlib_System_Nullable_1_gen765586611.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "mscorlib_System_String2029220233.h"
#include "mscorlib_System_InvalidOperationException721527559.h"
#include "mscorlib_System_ValueType3507792607.h"
#include "mscorlib_System_Nullable_1_gen1768791344.h"
#include "DOTween_DG_Tweening_LogBehaviour3505725029.h"
#include "mscorlib_System_Nullable_1_gen512284379.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "mscorlib_System_Nullable_1_gen2088641033.h"
#include "mscorlib_System_Nullable_1_gen339576247.h"
#include "mscorlib_System_Nullable_1_gen1693325264.h"
#include "mscorlib_System_TimeSpan3430258949.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "mscorlib_System_Nullable_1_gen506773895.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "mscorlib_System_Predicate_1_gen1897451453.h"
#include "mscorlib_System_Char3454481338.h"
#include "mscorlib_System_Predicate_1_gen514847563.h"
#include "mscorlib_System_Predicate_1_gen1132419410.h"
#include "mscorlib_System_Predicate_1_gen2832094954.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgum94157543.h"
#include "mscorlib_System_Predicate_1_gen4236135325.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg1498197914.h"
#include "mscorlib_System_Predicate_1_gen2348721464.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo3905751349.h"
#include "mscorlib_System_Predicate_1_gen3612454929.h"
#include "UnityEngine_UnityEngine_Color32874517518.h"
#include "mscorlib_System_Predicate_1_gen2759123787.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResul21186376.h"
#include "mscorlib_System_Predicate_1_gen1499606915.h"
#include "UnityEngine_UnityEngine_UICharInfo3056636800.h"
#include "mscorlib_System_Predicate_1_gen2064247989.h"
#include "UnityEngine_UnityEngine_UILineInfo3621277874.h"
#include "mscorlib_System_Predicate_1_gen3942196229.h"
#include "UnityEngine_UnityEngine_UIVertex1204258818.h"
#include "mscorlib_System_Predicate_1_gen686677694.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "mscorlib_System_Predicate_1_gen686677695.h"
#include "mscorlib_System_Predicate_1_gen686677696.h"
#include "UnityEngine_UnityEngine_Vector42243707581.h"
#include "mscorlib_System_Reflection_MonoProperty_Getter_2_g4179406139.h"

// System.Func`2<System.Object,System.Single>
struct Func_2_t2212564818;
// System.Object
struct Il2CppObject;
// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t164973122;
// System.NotSupportedException
struct NotSupportedException_t1793819818;
// System.InvalidOperationException
struct InvalidOperationException_t721527559;
// System.String
struct String_t;
// System.Predicate`1<System.Char>
struct Predicate_1_t1897451453;
// System.Predicate`1<System.Int32>
struct Predicate_1_t514847563;
// System.Predicate`1<System.Object>
struct Predicate_1_t1132419410;
// System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>
struct Predicate_1_t2832094954;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t4236135325;
// System.Predicate`1<UnityEngine.AnimatorClipInfo>
struct Predicate_1_t2348721464;
// System.Predicate`1<UnityEngine.Color32>
struct Predicate_1_t3612454929;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t2759123787;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t1499606915;
// System.Predicate`1<UnityEngine.UILineInfo>
struct Predicate_1_t2064247989;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t3942196229;
// System.Predicate`1<UnityEngine.Vector2>
struct Predicate_1_t686677694;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t686677695;
// System.Predicate`1<UnityEngine.Vector4>
struct Predicate_1_t686677696;
// System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>
struct Getter_2_t4179406139;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId;
extern Il2CppClass* NotSupportedException_t1793819818_il2cpp_TypeInfo_var;
extern const uint32_t U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId;
extern Il2CppClass* InvalidOperationException_t721527559_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2004437333;
extern const uint32_t Nullable_1_get_Value_m714447110_MetadataUsageId;
extern Il2CppClass* Ease_t2502520296_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m2034877224_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m1556343753_MetadataUsageId;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_ToString_m4253036926_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m4122144827_MetadataUsageId;
extern Il2CppClass* LogBehaviour_t3505725029_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m343412015_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m267509806_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m1733122945_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m119961898_MetadataUsageId;
extern Il2CppClass* LoopType_t2249218064_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m420035708_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m3459654167_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m1690932690_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m651735327_MetadataUsageId;
extern Il2CppClass* Boolean_t3825574718_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m1318699267_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m2478758066_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m678068069_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m2583189501_MetadataUsageId;
extern Il2CppClass* Single_t2076509932_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m1038420037_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m3764440182_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m940266439_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m1743067844_MetadataUsageId;
extern Il2CppClass* TimeSpan_t3430258949_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m3860982732_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m1253254751_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m1238126148_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m2929001762_MetadataUsageId;
extern Il2CppClass* Color_t2020392075_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m2692902174_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m2301070283_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m1180746670_MetadataUsageId;
extern const uint32_t Nullable_1_get_Value_m3715255517_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t Nullable_1_Equals_m1233761477_MetadataUsageId;
extern const uint32_t Nullable_1_GetValueOrDefault_m2630877366_MetadataUsageId;
extern const uint32_t Nullable_1_ToString_m769970515_MetadataUsageId;
extern Il2CppClass* Char_t3454481338_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856533029_MetadataUsageId;
extern Il2CppClass* Int32_t2071877448_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2559992383_MetadataUsageId;
extern Il2CppClass* CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1448216027_MetadataUsageId;
extern Il2CppClass* CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m1856151290_MetadataUsageId;
extern Il2CppClass* AnimatorClipInfo_t3905751349_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2336395304_MetadataUsageId;
extern Il2CppClass* Color32_t874517518_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2959352225_MetadataUsageId;
extern Il2CppClass* RaycastResult_t21186376_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m4068629879_MetadataUsageId;
extern Il2CppClass* UICharInfo_t3056636800_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3056726495_MetadataUsageId;
extern Il2CppClass* UILineInfo_t3621277874_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2329589669_MetadataUsageId;
extern Il2CppClass* UIVertex_t1204258818_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m3926587117_MetadataUsageId;
extern Il2CppClass* Vector2_t2243707579_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m866207434_MetadataUsageId;
extern const uint32_t Predicate_1_BeginInvoke_m1764756107_MetadataUsageId;
extern Il2CppClass* Vector4_t2243707581_il2cpp_TypeInfo_var;
extern const uint32_t Predicate_1_BeginInvoke_m2425667920_MetadataUsageId;



// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m4121137703_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, const MethodInfo* method);
// System.Void System.Nullable`1<DG.Tweening.Ease>::.ctor(T)
extern "C"  void Nullable_1__ctor_m414275061_gshared (Nullable_1_t765586611 * __this, int32_t ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1154243172_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method);
// T System.Nullable`1<DG.Tweening.Ease>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m714447110_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m686582867_gshared (Nullable_1_t765586611 * __this, Nullable_1_t765586611  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2034877224_gshared (Nullable_1_t765586611 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<DG.Tweening.Ease>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1726028236_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method);
// T System.Nullable`1<DG.Tweening.Ease>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1556343753_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method);
// System.String System.Nullable`1<DG.Tweening.Ease>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4253036926_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<DG.Tweening.LogBehaviour>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2449594948_gshared (Nullable_1_t1768791344 * __this, int32_t ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2864081451_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method);
// T System.Nullable`1<DG.Tweening.LogBehaviour>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m4122144827_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3712328580_gshared (Nullable_1_t1768791344 * __this, Nullable_1_t1768791344  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m343412015_gshared (Nullable_1_t1768791344 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<DG.Tweening.LogBehaviour>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m770430497_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method);
// T System.Nullable`1<DG.Tweening.LogBehaviour>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m267509806_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method);
// System.String System.Nullable`1<DG.Tweening.LogBehaviour>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1733122945_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<DG.Tweening.LoopType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1059478283_gshared (Nullable_1_t512284379 * __this, int32_t ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3610923540_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method);
// T System.Nullable`1<DG.Tweening.LoopType>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m119961898_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2126262385_gshared (Nullable_1_t512284379 * __this, Nullable_1_t512284379  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m420035708_gshared (Nullable_1_t512284379 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<DG.Tweening.LoopType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1378650604_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method);
// T System.Nullable`1<DG.Tweening.LoopType>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3459654167_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method);
// System.String System.Nullable`1<DG.Tweening.LoopType>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1690932690_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1785320616_gshared (Nullable_1_t2088641033 * __this, bool ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m452362759_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// T System.Nullable`1<System.Boolean>::get_Value()
extern "C"  bool Nullable_1_get_Value_m651735327_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2189684888_gshared (Nullable_1_t2088641033 * __this, Nullable_1_t2088641033  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1318699267_gshared (Nullable_1_t2088641033 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern "C"  bool Nullable_1_GetValueOrDefault_m2478758066_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.Boolean>::ToString()
extern "C"  String_t* Nullable_1_ToString_m678068069_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C"  void Nullable_1__ctor_m261372810_gshared (Nullable_1_t339576247 * __this, float ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2860645089_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method);
// T System.Nullable`1<System.Single>::get_Value()
extern "C"  float Nullable_1_get_Value_m2583189501_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3850685758_gshared (Nullable_1_t339576247 * __this, Nullable_1_t339576247  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1038420037_gshared (Nullable_1_t339576247 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method);
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.Single>::ToString()
extern "C"  String_t* Nullable_1_ToString_m940266439_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m1253254751_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2519817708_gshared (Nullable_1_t283458390 * __this, Color_t2020392075  ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<UnityEngine.Color>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m452334108_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method);
// T System.Nullable`1<UnityEngine.Color>::get_Value()
extern "C"  Color_t2020392075  Nullable_1_get_Value_m2929001762_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1377231269_gshared (Nullable_1_t283458390 * __this, Nullable_1_t283458390  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2692902174_gshared (Nullable_1_t283458390 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<UnityEngine.Color>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m730951602_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method);
// T System.Nullable`1<UnityEngine.Color>::GetValueOrDefault()
extern "C"  Color_t2020392075  Nullable_1_GetValueOrDefault_m2301070283_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method);
// System.String System.Nullable`1<UnityEngine.Color>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1180746670_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3622474896_gshared (Nullable_1_t506773895 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m127841985_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method);
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t2243707580  Nullable_1_get_Value_m3715255517_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2313387266_gshared (Nullable_1_t506773895 * __this, Nullable_1_t506773895  p0, const MethodInfo* method);
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1233761477_gshared (Nullable_1_t506773895 * __this, Il2CppObject * ___other0, const MethodInfo* method);
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1916216271_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method);
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m2630877366_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method);
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern "C"  String_t* Nullable_1_ToString_m769970515_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method);
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m762264976_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4047721271_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m527131606_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2060780095_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m744913181_gshared (Predicate_1_t2348721464 * __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m122788314_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3860206640_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3539717340_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m577088274_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2883675618_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3001657933_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2775223656_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2407726575_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m3338489829_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, const MethodInfo* method);

// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
#define Func_2_Invoke_m4121137703(__this, ___arg10, method) ((  float (*) (Func_2_t2212564818 *, Il2CppObject *, const MethodInfo*))Func_2_Invoke_m4121137703_gshared)(__this, ___arg10, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
extern "C"  int32_t Interlocked_CompareExchange_m3339239614 (Il2CppObject * __this /* static, unused */, int32_t* p0, int32_t p1, int32_t p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.NotSupportedException::.ctor()
extern "C"  void NotSupportedException__ctor_m3232764727 (NotSupportedException_t1793819818 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Nullable`1<DG.Tweening.Ease>::.ctor(T)
#define Nullable_1__ctor_m414275061(__this, ___value0, method) ((  void (*) (Nullable_1_t765586611 *, int32_t, const MethodInfo*))Nullable_1__ctor_m414275061_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::get_HasValue()
#define Nullable_1_get_HasValue_m1154243172(__this, method) ((  bool (*) (Nullable_1_t765586611 *, const MethodInfo*))Nullable_1_get_HasValue_m1154243172_gshared)(__this, method)
// System.Void System.InvalidOperationException::.ctor(System.String)
extern "C"  void InvalidOperationException__ctor_m2801133788 (InvalidOperationException_t721527559 * __this, String_t* ___message0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// T System.Nullable`1<DG.Tweening.Ease>::get_Value()
#define Nullable_1_get_Value_m714447110(__this, method) ((  int32_t (*) (Nullable_1_t765586611 *, const MethodInfo*))Nullable_1_get_Value_m714447110_gshared)(__this, method)
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m686582867(__this, p0, method) ((  bool (*) (Nullable_1_t765586611 *, Nullable_1_t765586611 , const MethodInfo*))Nullable_1_Equals_m686582867_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Object)
#define Nullable_1_Equals_m2034877224(__this, ___other0, method) ((  bool (*) (Nullable_1_t765586611 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2034877224_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<DG.Tweening.Ease>::GetHashCode()
#define Nullable_1_GetHashCode_m1726028236(__this, method) ((  int32_t (*) (Nullable_1_t765586611 *, const MethodInfo*))Nullable_1_GetHashCode_m1726028236_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.Ease>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m1556343753(__this, method) ((  int32_t (*) (Nullable_1_t765586611 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1556343753_gshared)(__this, method)
// System.String System.Nullable`1<DG.Tweening.Ease>::ToString()
#define Nullable_1_ToString_m4253036926(__this, method) ((  String_t* (*) (Nullable_1_t765586611 *, const MethodInfo*))Nullable_1_ToString_m4253036926_gshared)(__this, method)
// System.Void System.Nullable`1<DG.Tweening.LogBehaviour>::.ctor(T)
#define Nullable_1__ctor_m2449594948(__this, ___value0, method) ((  void (*) (Nullable_1_t1768791344 *, int32_t, const MethodInfo*))Nullable_1__ctor_m2449594948_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::get_HasValue()
#define Nullable_1_get_HasValue_m2864081451(__this, method) ((  bool (*) (Nullable_1_t1768791344 *, const MethodInfo*))Nullable_1_get_HasValue_m2864081451_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LogBehaviour>::get_Value()
#define Nullable_1_get_Value_m4122144827(__this, method) ((  int32_t (*) (Nullable_1_t1768791344 *, const MethodInfo*))Nullable_1_get_Value_m4122144827_gshared)(__this, method)
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m3712328580(__this, p0, method) ((  bool (*) (Nullable_1_t1768791344 *, Nullable_1_t1768791344 , const MethodInfo*))Nullable_1_Equals_m3712328580_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Object)
#define Nullable_1_Equals_m343412015(__this, ___other0, method) ((  bool (*) (Nullable_1_t1768791344 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m343412015_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<DG.Tweening.LogBehaviour>::GetHashCode()
#define Nullable_1_GetHashCode_m770430497(__this, method) ((  int32_t (*) (Nullable_1_t1768791344 *, const MethodInfo*))Nullable_1_GetHashCode_m770430497_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LogBehaviour>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m267509806(__this, method) ((  int32_t (*) (Nullable_1_t1768791344 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m267509806_gshared)(__this, method)
// System.String System.Nullable`1<DG.Tweening.LogBehaviour>::ToString()
#define Nullable_1_ToString_m1733122945(__this, method) ((  String_t* (*) (Nullable_1_t1768791344 *, const MethodInfo*))Nullable_1_ToString_m1733122945_gshared)(__this, method)
// System.Void System.Nullable`1<DG.Tweening.LoopType>::.ctor(T)
#define Nullable_1__ctor_m1059478283(__this, ___value0, method) ((  void (*) (Nullable_1_t512284379 *, int32_t, const MethodInfo*))Nullable_1__ctor_m1059478283_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::get_HasValue()
#define Nullable_1_get_HasValue_m3610923540(__this, method) ((  bool (*) (Nullable_1_t512284379 *, const MethodInfo*))Nullable_1_get_HasValue_m3610923540_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LoopType>::get_Value()
#define Nullable_1_get_Value_m119961898(__this, method) ((  int32_t (*) (Nullable_1_t512284379 *, const MethodInfo*))Nullable_1_get_Value_m119961898_gshared)(__this, method)
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m2126262385(__this, p0, method) ((  bool (*) (Nullable_1_t512284379 *, Nullable_1_t512284379 , const MethodInfo*))Nullable_1_Equals_m2126262385_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Object)
#define Nullable_1_Equals_m420035708(__this, ___other0, method) ((  bool (*) (Nullable_1_t512284379 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m420035708_gshared)(__this, ___other0, method)
// System.Int32 System.Nullable`1<DG.Tweening.LoopType>::GetHashCode()
#define Nullable_1_GetHashCode_m1378650604(__this, method) ((  int32_t (*) (Nullable_1_t512284379 *, const MethodInfo*))Nullable_1_GetHashCode_m1378650604_gshared)(__this, method)
// T System.Nullable`1<DG.Tweening.LoopType>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m3459654167(__this, method) ((  int32_t (*) (Nullable_1_t512284379 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3459654167_gshared)(__this, method)
// System.String System.Nullable`1<DG.Tweening.LoopType>::ToString()
#define Nullable_1_ToString_m1690932690(__this, method) ((  String_t* (*) (Nullable_1_t512284379 *, const MethodInfo*))Nullable_1_ToString_m1690932690_gshared)(__this, method)
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
#define Nullable_1__ctor_m1785320616(__this, ___value0, method) ((  void (*) (Nullable_1_t2088641033 *, bool, const MethodInfo*))Nullable_1__ctor_m1785320616_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
#define Nullable_1_get_HasValue_m452362759(__this, method) ((  bool (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_get_HasValue_m452362759_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::get_Value()
#define Nullable_1_get_Value_m651735327(__this, method) ((  bool (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_get_Value_m651735327_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m2189684888(__this, p0, method) ((  bool (*) (Nullable_1_t2088641033 *, Nullable_1_t2088641033 , const MethodInfo*))Nullable_1_Equals_m2189684888_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
#define Nullable_1_Equals_m1318699267(__this, ___other0, method) ((  bool (*) (Nullable_1_t2088641033 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1318699267_gshared)(__this, ___other0, method)
// System.Boolean System.Boolean::Equals(System.Object)
extern "C"  bool Boolean_Equals_m2118901528 (bool* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Boolean::GetHashCode()
extern "C"  int32_t Boolean_GetHashCode_m1894638460 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
#define Nullable_1_GetHashCode_m1645245653(__this, method) ((  int32_t (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_GetHashCode_m1645245653_gshared)(__this, method)
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m2478758066(__this, method) ((  bool (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2478758066_gshared)(__this, method)
// System.String System.Boolean::ToString()
extern "C"  String_t* Boolean_ToString_m1253164328 (bool* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.Boolean>::ToString()
#define Nullable_1_ToString_m678068069(__this, method) ((  String_t* (*) (Nullable_1_t2088641033 *, const MethodInfo*))Nullable_1_ToString_m678068069_gshared)(__this, method)
// System.Void System.Nullable`1<System.Single>::.ctor(T)
#define Nullable_1__ctor_m261372810(__this, ___value0, method) ((  void (*) (Nullable_1_t339576247 *, float, const MethodInfo*))Nullable_1__ctor_m261372810_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
#define Nullable_1_get_HasValue_m2860645089(__this, method) ((  bool (*) (Nullable_1_t339576247 *, const MethodInfo*))Nullable_1_get_HasValue_m2860645089_gshared)(__this, method)
// T System.Nullable`1<System.Single>::get_Value()
#define Nullable_1_get_Value_m2583189501(__this, method) ((  float (*) (Nullable_1_t339576247 *, const MethodInfo*))Nullable_1_get_Value_m2583189501_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m3850685758(__this, p0, method) ((  bool (*) (Nullable_1_t339576247 *, Nullable_1_t339576247 , const MethodInfo*))Nullable_1_Equals_m3850685758_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
#define Nullable_1_Equals_m1038420037(__this, ___other0, method) ((  bool (*) (Nullable_1_t339576247 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1038420037_gshared)(__this, ___other0, method)
// System.Boolean System.Single::Equals(System.Object)
extern "C"  bool Single_Equals_m3679433096 (float* __this, Il2CppObject * ___obj0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Single::GetHashCode()
extern "C"  int32_t Single_GetHashCode_m3102305584 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
#define Nullable_1_GetHashCode_m572762171(__this, method) ((  int32_t (*) (Nullable_1_t339576247 *, const MethodInfo*))Nullable_1_GetHashCode_m572762171_gshared)(__this, method)
// T System.Nullable`1<System.Single>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m3764440182(__this, method) ((  float (*) (Nullable_1_t339576247 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m3764440182_gshared)(__this, method)
// System.String System.Single::ToString()
extern "C"  String_t* Single_ToString_m1813392066 (float* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.Single>::ToString()
#define Nullable_1_ToString_m940266439(__this, method) ((  String_t* (*) (Nullable_1_t339576247 *, const MethodInfo*))Nullable_1_ToString_m940266439_gshared)(__this, method)
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
#define Nullable_1__ctor_m796575255(__this, ___value0, method) ((  void (*) (Nullable_1_t1693325264 *, TimeSpan_t3430258949 , const MethodInfo*))Nullable_1__ctor_m796575255_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
#define Nullable_1_get_HasValue_m3663286555(__this, method) ((  bool (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_get_HasValue_m3663286555_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
#define Nullable_1_get_Value_m1743067844(__this, method) ((  TimeSpan_t3430258949  (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_get_Value_m1743067844_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m1889119397(__this, p0, method) ((  bool (*) (Nullable_1_t1693325264 *, Nullable_1_t1693325264 , const MethodInfo*))Nullable_1_Equals_m1889119397_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
#define Nullable_1_Equals_m3860982732(__this, ___other0, method) ((  bool (*) (Nullable_1_t1693325264 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m3860982732_gshared)(__this, ___other0, method)
// System.Boolean System.TimeSpan::Equals(System.Object)
extern "C"  bool TimeSpan_Equals_m4102942751 (TimeSpan_t3430258949 * __this, Il2CppObject * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.TimeSpan::GetHashCode()
extern "C"  int32_t TimeSpan_GetHashCode_m550404245 (TimeSpan_t3430258949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
#define Nullable_1_GetHashCode_m1791015856(__this, method) ((  int32_t (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_GetHashCode_m1791015856_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m1253254751(__this, method) ((  TimeSpan_t3430258949  (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m1253254751_gshared)(__this, method)
// System.String System.TimeSpan::ToString()
extern "C"  String_t* TimeSpan_ToString_m2947282901 (TimeSpan_t3430258949 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<System.TimeSpan>::ToString()
#define Nullable_1_ToString_m1238126148(__this, method) ((  String_t* (*) (Nullable_1_t1693325264 *, const MethodInfo*))Nullable_1_ToString_m1238126148_gshared)(__this, method)
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(T)
#define Nullable_1__ctor_m2519817708(__this, ___value0, method) ((  void (*) (Nullable_1_t283458390 *, Color_t2020392075 , const MethodInfo*))Nullable_1__ctor_m2519817708_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<UnityEngine.Color>::get_HasValue()
#define Nullable_1_get_HasValue_m452334108(__this, method) ((  bool (*) (Nullable_1_t283458390 *, const MethodInfo*))Nullable_1_get_HasValue_m452334108_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Color>::get_Value()
#define Nullable_1_get_Value_m2929001762(__this, method) ((  Color_t2020392075  (*) (Nullable_1_t283458390 *, const MethodInfo*))Nullable_1_get_Value_m2929001762_gshared)(__this, method)
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m1377231269(__this, p0, method) ((  bool (*) (Nullable_1_t283458390 *, Nullable_1_t283458390 , const MethodInfo*))Nullable_1_Equals_m1377231269_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Object)
#define Nullable_1_Equals_m2692902174(__this, ___other0, method) ((  bool (*) (Nullable_1_t283458390 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m2692902174_gshared)(__this, ___other0, method)
// System.Boolean UnityEngine.Color::Equals(System.Object)
extern "C"  bool Color_Equals_m661618137 (Color_t2020392075 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Color::GetHashCode()
extern "C"  int32_t Color_GetHashCode_m3182525367 (Color_t2020392075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<UnityEngine.Color>::GetHashCode()
#define Nullable_1_GetHashCode_m730951602(__this, method) ((  int32_t (*) (Nullable_1_t283458390 *, const MethodInfo*))Nullable_1_GetHashCode_m730951602_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Color>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m2301070283(__this, method) ((  Color_t2020392075  (*) (Nullable_1_t283458390 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2301070283_gshared)(__this, method)
// System.String UnityEngine.Color::ToString()
extern "C"  String_t* Color_ToString_m4028093047 (Color_t2020392075 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<UnityEngine.Color>::ToString()
#define Nullable_1_ToString_m1180746670(__this, method) ((  String_t* (*) (Nullable_1_t283458390 *, const MethodInfo*))Nullable_1_ToString_m1180746670_gshared)(__this, method)
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
#define Nullable_1__ctor_m3622474896(__this, ___value0, method) ((  void (*) (Nullable_1_t506773895 *, Vector3_t2243707580 , const MethodInfo*))Nullable_1__ctor_m3622474896_gshared)(__this, ___value0, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
#define Nullable_1_get_HasValue_m127841985(__this, method) ((  bool (*) (Nullable_1_t506773895 *, const MethodInfo*))Nullable_1_get_HasValue_m127841985_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
#define Nullable_1_get_Value_m3715255517(__this, method) ((  Vector3_t2243707580  (*) (Nullable_1_t506773895 *, const MethodInfo*))Nullable_1_get_Value_m3715255517_gshared)(__this, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
#define Nullable_1_Equals_m2313387266(__this, p0, method) ((  bool (*) (Nullable_1_t506773895 *, Nullable_1_t506773895 , const MethodInfo*))Nullable_1_Equals_m2313387266_gshared)(__this, p0, method)
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
#define Nullable_1_Equals_m1233761477(__this, ___other0, method) ((  bool (*) (Nullable_1_t506773895 *, Il2CppObject *, const MethodInfo*))Nullable_1_Equals_m1233761477_gshared)(__this, ___other0, method)
// System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern "C"  bool Vector3_Equals_m2692262876 (Vector3_t2243707580 * __this, Il2CppObject * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Vector3::GetHashCode()
extern "C"  int32_t Vector3_GetHashCode_m1754570744 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
#define Nullable_1_GetHashCode_m1916216271(__this, method) ((  int32_t (*) (Nullable_1_t506773895 *, const MethodInfo*))Nullable_1_GetHashCode_m1916216271_gshared)(__this, method)
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
#define Nullable_1_GetValueOrDefault_m2630877366(__this, method) ((  Vector3_t2243707580  (*) (Nullable_1_t506773895 *, const MethodInfo*))Nullable_1_GetValueOrDefault_m2630877366_gshared)(__this, method)
// System.String UnityEngine.Vector3::ToString()
extern "C"  String_t* Vector3_ToString_m3857189970 (Vector3_t2243707580 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
#define Nullable_1_ToString_m769970515(__this, method) ((  String_t* (*) (Nullable_1_t506773895 *, const MethodInfo*))Nullable_1_ToString_m769970515_gshared)(__this, method)
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
#define Predicate_1_Invoke_m762264976(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1897451453 *, Il2CppChar, const MethodInfo*))Predicate_1_Invoke_m762264976_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
#define Predicate_1_Invoke_m695569038(__this, ___obj0, method) ((  bool (*) (Predicate_1_t514847563 *, int32_t, const MethodInfo*))Predicate_1_Invoke_m695569038_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
#define Predicate_1_Invoke_m4047721271(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1132419410 *, Il2CppObject *, const MethodInfo*))Predicate_1_Invoke_m4047721271_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
#define Predicate_1_Invoke_m527131606(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2832094954 *, CustomAttributeNamedArgument_t94157543 , const MethodInfo*))Predicate_1_Invoke_m527131606_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
#define Predicate_1_Invoke_m2060780095(__this, ___obj0, method) ((  bool (*) (Predicate_1_t4236135325 *, CustomAttributeTypedArgument_t1498197914 , const MethodInfo*))Predicate_1_Invoke_m2060780095_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
#define Predicate_1_Invoke_m744913181(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2348721464 *, AnimatorClipInfo_t3905751349 , const MethodInfo*))Predicate_1_Invoke_m744913181_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
#define Predicate_1_Invoke_m122788314(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3612454929 *, Color32_t874517518 , const MethodInfo*))Predicate_1_Invoke_m122788314_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
#define Predicate_1_Invoke_m3860206640(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2759123787 *, RaycastResult_t21186376 , const MethodInfo*))Predicate_1_Invoke_m3860206640_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
#define Predicate_1_Invoke_m3539717340(__this, ___obj0, method) ((  bool (*) (Predicate_1_t1499606915 *, UICharInfo_t3056636800 , const MethodInfo*))Predicate_1_Invoke_m3539717340_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
#define Predicate_1_Invoke_m577088274(__this, ___obj0, method) ((  bool (*) (Predicate_1_t2064247989 *, UILineInfo_t3621277874 , const MethodInfo*))Predicate_1_Invoke_m577088274_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
#define Predicate_1_Invoke_m2883675618(__this, ___obj0, method) ((  bool (*) (Predicate_1_t3942196229 *, UIVertex_t1204258818 , const MethodInfo*))Predicate_1_Invoke_m2883675618_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
#define Predicate_1_Invoke_m3001657933(__this, ___obj0, method) ((  bool (*) (Predicate_1_t686677694 *, Vector2_t2243707579 , const MethodInfo*))Predicate_1_Invoke_m3001657933_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
#define Predicate_1_Invoke_m2775223656(__this, ___obj0, method) ((  bool (*) (Predicate_1_t686677695 *, Vector3_t2243707580 , const MethodInfo*))Predicate_1_Invoke_m2775223656_gshared)(__this, ___obj0, method)
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
#define Predicate_1_Invoke_m2407726575(__this, ___obj0, method) ((  bool (*) (Predicate_1_t686677696 *, Vector4_t2243707581 , const MethodInfo*))Predicate_1_Invoke_m2407726575_gshared)(__this, ___obj0, method)
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
#define Getter_2_Invoke_m3338489829(__this, ____this0, method) ((  Il2CppObject * (*) (Getter_2_t4179406139 *, Il2CppObject *, const MethodInfo*))Getter_2_Invoke_m3338489829_gshared)(__this, ____this0, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Func`2<System.Object,System.Single>::.ctor(System.Object,System.IntPtr)
extern "C"  void Func_2__ctor_m1874497973_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// TResult System.Func`2<System.Object,System.Single>::Invoke(T)
extern "C"  float Func_2_Invoke_m4121137703_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Func_2_Invoke_m4121137703((Func_2_t2212564818 *)__this->get_prev_9(),___arg10, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef float (*FunctionPointerType) (void* __this, Il2CppObject * ___arg10, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef float (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___arg10,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Func`2<System.Object,System.Single>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Func_2_BeginInvoke_m669892004_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___arg10, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___arg10;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// TResult System.Func`2<System.Object,System.Single>::EndInvoke(System.IAsyncResult)
extern "C"  float Func_2_EndInvoke_m971580865_gshared (Func_2_t2212564818 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(float*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m1958283157_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2551263788((Il2CppObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m3602665650_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m269113779_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_U24current_5();
		return L_0;
	}
}
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m3279674866_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	{
		Il2CppObject* L_0 = ((  Il2CppObject* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return L_0;
	}
}
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C"  Il2CppObject* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m2682676065_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * V_0 = NULL;
	{
		int32_t* L_0 = (int32_t*)__this->get_address_of_U24PC_4();
		int32_t L_1 = Interlocked_CompareExchange_m3339239614(NULL /*static, unused*/, (int32_t*)L_0, (int32_t)0, (int32_t)((int32_t)-2), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_1) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0014;
		}
	}
	{
		return __this;
	}

IL_0014:
	{
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_2 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2));
		((  void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)(L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		V_0 = (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 *)L_2;
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_3 = V_0;
		Il2CppObject* L_4 = (Il2CppObject*)__this->get_U3CU24U3Esource_6();
		L_3->set_source_0(L_4);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_5 = V_0;
		Func_2_t3961629604 * L_6 = (Func_2_t3961629604 *)__this->get_U3CU24U3Epredicate_7();
		L_5->set_predicate_3(L_6);
		U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * L_7 = V_0;
		return L_7;
	}
}
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern "C"  bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m3533253043_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		V_1 = (bool)0;
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0023;
			}
			case 1:
			{
				goto IL_0037;
			}
		}
	}
	{
		goto IL_00be;
	}

IL_0023:
	{
		Il2CppObject* L_2 = (Il2CppObject*)__this->get_source_0();
		Il2CppObject* L_3 = InterfaceFuncInvoker0< Il2CppObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 4), (Il2CppObject*)L_2);
		__this->set_U3CU24s_120U3E__0_1(L_3);
		V_0 = (uint32_t)((int32_t)-3);
	}

IL_0037:
	try
	{ // begin try (depth: 1)
		{
			uint32_t L_4 = V_0;
			switch (((int32_t)((int32_t)L_4-(int32_t)1)))
			{
				case 0:
				{
					goto IL_0089;
				}
			}
		}

IL_0043:
		{
			goto IL_0089;
		}

IL_0048:
		{
			Il2CppObject* L_5 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			Il2CppObject * L_6 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), (Il2CppObject*)L_5);
			__this->set_U3CelementU3E__1_2(L_6);
			Func_2_t3961629604 * L_7 = (Func_2_t3961629604 *)__this->get_predicate_3();
			Il2CppObject * L_8 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			bool L_9 = ((  bool (*) (Func_2_t3961629604 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((Func_2_t3961629604 *)L_7, (Il2CppObject *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
			if (!L_9)
			{
				goto IL_0089;
			}
		}

IL_006f:
		{
			Il2CppObject * L_10 = (Il2CppObject *)__this->get_U3CelementU3E__1_2();
			__this->set_U24current_5(L_10);
			__this->set_U24PC_4(1);
			V_1 = (bool)1;
			IL2CPP_LEAVE(0xC0, FINALLY_009e);
		}

IL_0089:
		{
			Il2CppObject* L_11 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			bool L_12 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, (Il2CppObject *)L_11);
			if (L_12)
			{
				goto IL_0048;
			}
		}

IL_0099:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_009e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_009e;
	}

FINALLY_009e:
	{ // begin finally (depth: 1)
		{
			bool L_13 = V_1;
			if (!L_13)
			{
				goto IL_00a2;
			}
		}

IL_00a1:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00a2:
		{
			Il2CppObject* L_14 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_14)
			{
				goto IL_00ab;
			}
		}

IL_00aa:
		{
			IL2CPP_END_FINALLY(158)
		}

IL_00ab:
		{
			Il2CppObject* L_15 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_15);
			IL2CPP_END_FINALLY(158)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(158)
	{
		IL2CPP_JUMP_TBL(0xC0, IL_00c0)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		__this->set_U24PC_4((-1));
	}

IL_00be:
	{
		return (bool)0;
	}

IL_00c0:
	{
		return (bool)1;
	}
	// Dead block : IL_00c2: ldloc.2
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m1879652802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = (int32_t)__this->get_U24PC_4();
		V_0 = (uint32_t)L_0;
		__this->set_U24PC_4((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_003b;
			}
			case 1:
			{
				goto IL_0021;
			}
		}
	}
	{
		goto IL_003b;
	}

IL_0021:
	try
	{ // begin try (depth: 1)
		IL2CPP_LEAVE(0x3B, FINALLY_0026);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0026;
	}

FINALLY_0026:
	{ // begin finally (depth: 1)
		{
			Il2CppObject* L_2 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			if (L_2)
			{
				goto IL_002f;
			}
		}

IL_002e:
		{
			IL2CPP_END_FINALLY(38)
		}

IL_002f:
		{
			Il2CppObject* L_3 = (Il2CppObject*)__this->get_U3CU24s_120U3E__0_1();
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, (Il2CppObject *)L_3);
			IL2CPP_END_FINALLY(38)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(38)
	{
		IL2CPP_JUMP_TBL(0x3B, IL_003b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_003b:
	{
		return;
	}
}
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern "C"  void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t4121738870 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m1773515612_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1793819818 * L_0 = (NotSupportedException_t1793819818 *)il2cpp_codegen_object_new(NotSupportedException_t1793819818_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m3232764727(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0);
	}
}
// System.Void System.Nullable`1<DG.Tweening.Ease>::.ctor(T)
extern "C"  void Nullable_1__ctor_m414275061_gshared (Nullable_1_t765586611 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m414275061_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m414275061(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m1154243172_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m1154243172_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m1154243172(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<DG.Tweening.Ease>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m714447110_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m714447110_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m714447110_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m714447110(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2034877224_gshared (Nullable_1_t765586611 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m2034877224_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t765586611 ));
		UnBoxNullable(L_3, Ease_t2502520296_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m686582867((Nullable_1_t765586611 *)__this, (Nullable_1_t765586611 )((*(Nullable_1_t765586611 *)((Nullable_1_t765586611 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2034877224_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2034877224(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<DG.Tweening.Ease>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m686582867_gshared (Nullable_1_t765586611 * __this, Nullable_1_t765586611  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m686582867_AdjustorThunk (Il2CppObject * __this, Nullable_1_t765586611  ___other0, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m686582867(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<DG.Tweening.Ease>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1726028236_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1726028236_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1726028236(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<DG.Tweening.Ease>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1556343753_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1556343753_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Ease_t2502520296_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m1556343753_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m1556343753(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<DG.Tweening.Ease>::ToString()
extern "C"  String_t* Nullable_1_ToString_m4253036926_gshared (Nullable_1_t765586611 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m4253036926_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m4253036926_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t765586611  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m4253036926(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<DG.Tweening.LogBehaviour>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2449594948_gshared (Nullable_1_t1768791344 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2449594948_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2449594948(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2864081451_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2864081451_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2864081451(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<DG.Tweening.LogBehaviour>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m4122144827_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m4122144827_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m4122144827_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m4122144827(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m343412015_gshared (Nullable_1_t1768791344 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m343412015_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1768791344 ));
		UnBoxNullable(L_3, LogBehaviour_t3505725029_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m3712328580((Nullable_1_t1768791344 *)__this, (Nullable_1_t1768791344 )((*(Nullable_1_t1768791344 *)((Nullable_1_t1768791344 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m343412015_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m343412015(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<DG.Tweening.LogBehaviour>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3712328580_gshared (Nullable_1_t1768791344 * __this, Nullable_1_t1768791344  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m3712328580_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1768791344  ___other0, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3712328580(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<DG.Tweening.LogBehaviour>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m770430497_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m770430497_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m770430497(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<DG.Tweening.LogBehaviour>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m267509806_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m267509806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (LogBehaviour_t3505725029_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m267509806_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m267509806(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<DG.Tweening.LogBehaviour>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1733122945_gshared (Nullable_1_t1768791344 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1733122945_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1733122945_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1768791344  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1733122945(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<DG.Tweening.LoopType>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1059478283_gshared (Nullable_1_t512284379 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		int32_t L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1059478283_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1059478283(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3610923540_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3610923540_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3610923540(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<DG.Tweening.LoopType>::get_Value()
extern "C"  int32_t Nullable_1_get_Value_m119961898_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m119961898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		int32_t L_2 = (int32_t)__this->get_value_0();
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_get_Value_m119961898_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_get_Value_m119961898(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m420035708_gshared (Nullable_1_t512284379 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m420035708_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t512284379 ));
		UnBoxNullable(L_3, LoopType_t2249218064_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m2126262385((Nullable_1_t512284379 *)__this, (Nullable_1_t512284379 )((*(Nullable_1_t512284379 *)((Nullable_1_t512284379 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m420035708_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m420035708(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<DG.Tweening.LoopType>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2126262385_gshared (Nullable_1_t512284379 * __this, Nullable_1_t512284379  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		int32_t* L_3 = (int32_t*)(&___other0)->get_address_of_value_0();
		int32_t L_4 = (int32_t)__this->get_value_0();
		int32_t L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		Il2CppObject * L_7 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_3);
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.ValueType::Equals(System.Object) */, (ValueType_t3507792607 *)L_7, (Il2CppObject *)L_6);
		return L_8;
	}
}
extern "C"  bool Nullable_1_Equals_m2126262385_AdjustorThunk (Il2CppObject * __this, Nullable_1_t512284379  ___other0, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2126262385(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<DG.Tweening.LoopType>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1378650604_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(2 /* System.Int32 System.ValueType::GetHashCode() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1378650604_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1378650604(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<DG.Tweening.LoopType>::GetValueOrDefault()
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3459654167_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3459654167_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = (int32_t)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (LoopType_t2249218064_il2cpp_TypeInfo_var, (&V_0));
		int32_t L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  int32_t Nullable_1_GetValueOrDefault_m3459654167_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetValueOrDefault_m3459654167(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<DG.Tweening.LoopType>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1690932690_gshared (Nullable_1_t512284379 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1690932690_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		int32_t* L_1 = (int32_t*)__this->get_address_of_value_0();
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), L_1);
		String_t* L_3 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.ValueType::ToString() */, (ValueType_t3507792607 *)L_2);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_4;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1690932690_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t512284379  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<int32_t*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1690932690(&_thisAdjusted, method);
	*reinterpret_cast<int32_t*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Boolean>::.ctor(T)
extern "C"  void Nullable_1__ctor_m1785320616_gshared (Nullable_1_t2088641033 * __this, bool ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		bool L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m1785320616_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m1785320616(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Boolean>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m452362759_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m452362759_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m452362759(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::get_Value()
extern "C"  bool Nullable_1_get_Value_m651735327_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m651735327_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		bool L_2 = (bool)__this->get_value_0();
		return L_2;
	}
}
extern "C"  bool Nullable_1_get_Value_m651735327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_Value_m651735327(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1318699267_gshared (Nullable_1_t2088641033 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m1318699267_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t2088641033 ));
		UnBoxNullable(L_3, Boolean_t3825574718_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m2189684888((Nullable_1_t2088641033 *)__this, (Nullable_1_t2088641033 )((*(Nullable_1_t2088641033 *)((Nullable_1_t2088641033 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1318699267_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1318699267(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Boolean>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2189684888_gshared (Nullable_1_t2088641033 * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		bool* L_3 = (bool*)(&___other0)->get_address_of_value_0();
		bool L_4 = (bool)__this->get_value_0();
		bool L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Boolean_Equals_m2118901528((bool*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2189684888_AdjustorThunk (Il2CppObject * __this, Nullable_1_t2088641033  ___other0, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2189684888(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Boolean>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		int32_t L_2 = Boolean_GetHashCode_m1894638460((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1645245653_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1645245653(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Boolean>::GetValueOrDefault()
extern "C"  bool Nullable_1_GetValueOrDefault_m2478758066_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2478758066_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	bool G_B3_0 = false;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = (bool)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Boolean_t3825574718_il2cpp_TypeInfo_var, (&V_0));
		bool L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  bool Nullable_1_GetValueOrDefault_m2478758066_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_GetValueOrDefault_m2478758066(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Boolean>::ToString()
extern "C"  String_t* Nullable_1_ToString_m678068069_gshared (Nullable_1_t2088641033 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m678068069_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		bool* L_1 = (bool*)__this->get_address_of_value_0();
		String_t* L_2 = Boolean_ToString_m1253164328((bool*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m678068069_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t2088641033  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<bool*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m678068069(&_thisAdjusted, method);
	*reinterpret_cast<bool*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.Single>::.ctor(T)
extern "C"  void Nullable_1__ctor_m261372810_gshared (Nullable_1_t339576247 * __this, float ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		float L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m261372810_AdjustorThunk (Il2CppObject * __this, float ___value0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m261372810(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m2860645089_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m2860645089_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m2860645089(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::get_Value()
extern "C"  float Nullable_1_get_Value_m2583189501_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2583189501_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		float L_2 = (float)__this->get_value_0();
		return L_2;
	}
}
extern "C"  float Nullable_1_get_Value_m2583189501_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_get_Value_m2583189501(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1038420037_gshared (Nullable_1_t339576247 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m1038420037_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t339576247 ));
		UnBoxNullable(L_3, Single_t2076509932_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m3850685758((Nullable_1_t339576247 *)__this, (Nullable_1_t339576247 )((*(Nullable_1_t339576247 *)((Nullable_1_t339576247 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1038420037_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1038420037(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.Single>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m3850685758_gshared (Nullable_1_t339576247 * __this, Nullable_1_t339576247  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		float* L_3 = (float*)(&___other0)->get_address_of_value_0();
		float L_4 = (float)__this->get_value_0();
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Single_Equals_m3679433096((float*)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m3850685758_AdjustorThunk (Il2CppObject * __this, Nullable_1_t339576247  ___other0, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3850685758(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.Single>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		int32_t L_2 = Single_GetHashCode_m3102305584((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m572762171_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m572762171(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.Single>::GetValueOrDefault()
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m3764440182_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float G_B3_0 = 0.0f;
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		float L_1 = (float)__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Single_t2076509932_il2cpp_TypeInfo_var, (&V_0));
		float L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  float Nullable_1_GetValueOrDefault_m3764440182_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	float _returnValue = Nullable_1_GetValueOrDefault_m3764440182(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.Single>::ToString()
extern "C"  String_t* Nullable_1_ToString_m940266439_gshared (Nullable_1_t339576247 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m940266439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		float* L_1 = (float*)__this->get_address_of_value_0();
		String_t* L_2 = Single_ToString_m1813392066((float*)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m940266439_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t339576247  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<float*>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m940266439(&_thisAdjusted, method);
	*reinterpret_cast<float*>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C"  void Nullable_1__ctor_m796575255_gshared (Nullable_1_t1693325264 * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		TimeSpan_t3430258949  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m796575255_AdjustorThunk (Il2CppObject * __this, TimeSpan_t3430258949  ___value0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m796575255(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m3663286555_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m3663286555_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m3663286555(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m1743067844_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		TimeSpan_t3430258949  L_2 = (TimeSpan_t3430258949 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_get_Value_m1743067844_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_get_Value_m1743067844(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m3860982732_gshared (Nullable_1_t1693325264 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m3860982732_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t1693325264 ));
		UnBoxNullable(L_3, TimeSpan_t3430258949_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m1889119397((Nullable_1_t1693325264 *)__this, (Nullable_1_t1693325264 )((*(Nullable_1_t1693325264 *)((Nullable_1_t1693325264 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m3860982732_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m3860982732(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1889119397_gshared (Nullable_1_t1693325264 * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		TimeSpan_t3430258949 * L_3 = (TimeSpan_t3430258949 *)(&___other0)->get_address_of_value_0();
		TimeSpan_t3430258949  L_4 = (TimeSpan_t3430258949 )__this->get_value_0();
		TimeSpan_t3430258949  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = TimeSpan_Equals_m4102942751((TimeSpan_t3430258949 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1889119397_AdjustorThunk (Il2CppObject * __this, Nullable_1_t1693325264  ___other0, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1889119397(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		int32_t L_2 = TimeSpan_GetHashCode_m550404245((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1791015856_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1791015856(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<System.TimeSpan>::GetValueOrDefault()
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m1253254751_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m1253254751_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TimeSpan_t3430258949  V_0;
	memset(&V_0, 0, sizeof(V_0));
	TimeSpan_t3430258949  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		TimeSpan_t3430258949  L_1 = (TimeSpan_t3430258949 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (TimeSpan_t3430258949_il2cpp_TypeInfo_var, (&V_0));
		TimeSpan_t3430258949  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  TimeSpan_t3430258949  Nullable_1_GetValueOrDefault_m1253254751_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	TimeSpan_t3430258949  _returnValue = Nullable_1_GetValueOrDefault_m1253254751(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1238126148_gshared (Nullable_1_t1693325264 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1238126148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		TimeSpan_t3430258949 * L_1 = (TimeSpan_t3430258949 *)__this->get_address_of_value_0();
		String_t* L_2 = TimeSpan_ToString_m2947282901((TimeSpan_t3430258949 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1238126148_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t1693325264  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1238126148(&_thisAdjusted, method);
	*reinterpret_cast<TimeSpan_t3430258949 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(T)
extern "C"  void Nullable_1__ctor_m2519817708_gshared (Nullable_1_t283458390 * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Color_t2020392075  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m2519817708_AdjustorThunk (Il2CppObject * __this, Color_t2020392075  ___value0, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m2519817708(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m452334108_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m452334108_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m452334108(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Color>::get_Value()
extern "C"  Color_t2020392075  Nullable_1_get_Value_m2929001762_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m2929001762_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Color_t2020392075  L_2 = (Color_t2020392075 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Color_t2020392075  Nullable_1_get_Value_m2929001762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Color_t2020392075  _returnValue = Nullable_1_get_Value_m2929001762(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m2692902174_gshared (Nullable_1_t283458390 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m2692902174_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t283458390 ));
		UnBoxNullable(L_3, Color_t2020392075_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m1377231269((Nullable_1_t283458390 *)__this, (Nullable_1_t283458390 )((*(Nullable_1_t283458390 *)((Nullable_1_t283458390 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m2692902174_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2692902174(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Color>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m1377231269_gshared (Nullable_1_t283458390 * __this, Nullable_1_t283458390  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Color_t2020392075 * L_3 = (Color_t2020392075 *)(&___other0)->get_address_of_value_0();
		Color_t2020392075  L_4 = (Color_t2020392075 )__this->get_value_0();
		Color_t2020392075  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Color_Equals_m661618137((Color_t2020392075 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m1377231269_AdjustorThunk (Il2CppObject * __this, Nullable_1_t283458390  ___other0, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1377231269(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.Color>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m730951602_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Color_t2020392075 * L_1 = (Color_t2020392075 *)__this->get_address_of_value_0();
		int32_t L_2 = Color_GetHashCode_m3182525367((Color_t2020392075 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m730951602_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m730951602(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Color>::GetValueOrDefault()
extern "C"  Color_t2020392075  Nullable_1_GetValueOrDefault_m2301070283_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2301070283_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Color_t2020392075  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Color_t2020392075  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Color_t2020392075  L_1 = (Color_t2020392075 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Color_t2020392075_il2cpp_TypeInfo_var, (&V_0));
		Color_t2020392075  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Color_t2020392075  Nullable_1_GetValueOrDefault_m2301070283_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Color_t2020392075  _returnValue = Nullable_1_GetValueOrDefault_m2301070283(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.Color>::ToString()
extern "C"  String_t* Nullable_1_ToString_m1180746670_gshared (Nullable_1_t283458390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m1180746670_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Color_t2020392075 * L_1 = (Color_t2020392075 *)__this->get_address_of_value_0();
		String_t* L_2 = Color_ToString_m4028093047((Color_t2020392075 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m1180746670_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t283458390  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Color_t2020392075 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m1180746670(&_thisAdjusted, method);
	*reinterpret_cast<Color_t2020392075 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(T)
extern "C"  void Nullable_1__ctor_m3622474896_gshared (Nullable_1_t506773895 * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	{
		__this->set_has_value_1((bool)1);
		Vector3_t2243707580  L_0 = ___value0;
		__this->set_value_0(L_0);
		return;
	}
}
extern "C"  void Nullable_1__ctor_m3622474896_AdjustorThunk (Il2CppObject * __this, Vector3_t2243707580  ___value0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Nullable_1__ctor_m3622474896(&_thisAdjusted, ___value0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::get_HasValue()
extern "C"  bool Nullable_1_get_HasValue_m127841985_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		return L_0;
	}
}
extern "C"  bool Nullable_1_get_HasValue_m127841985_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_get_HasValue_m127841985(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::get_Value()
extern "C"  Vector3_t2243707580  Nullable_1_get_Value_m3715255517_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_get_Value_m3715255517_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t721527559 * L_1 = (InvalidOperationException_t721527559 *)il2cpp_codegen_object_new(InvalidOperationException_t721527559_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m2801133788(L_1, (String_t*)_stringLiteral2004437333, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0016:
	{
		Vector3_t2243707580  L_2 = (Vector3_t2243707580 )__this->get_value_0();
		return L_2;
	}
}
extern "C"  Vector3_t2243707580  Nullable_1_get_Value_m3715255517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t2243707580  _returnValue = Nullable_1_get_Value_m3715255517(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Object)
extern "C"  bool Nullable_1_Equals_m1233761477_gshared (Nullable_1_t506773895 * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_Equals_m1233761477_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Il2CppObject * L_0 = ___other0;
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		bool L_1 = (bool)__this->get_has_value_1();
		return (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}

IL_0010:
	{
		Il2CppObject * L_2 = ___other0;
		if (((Il2CppObject *)IsInst(L_2, IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0))))
		{
			goto IL_001d;
		}
	}
	{
		return (bool)0;
	}

IL_001d:
	{
		Il2CppObject * L_3 = ___other0;
		void* L_4 = alloca(sizeof(Nullable_1_t506773895 ));
		UnBoxNullable(L_3, Vector3_t2243707580_il2cpp_TypeInfo_var, L_4);
		bool L_5 = Nullable_1_Equals_m2313387266((Nullable_1_t506773895 *)__this, (Nullable_1_t506773895 )((*(Nullable_1_t506773895 *)((Nullable_1_t506773895 *)L_4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return L_5;
	}
}
extern "C"  bool Nullable_1_Equals_m1233761477_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___other0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m1233761477(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Boolean System.Nullable`1<UnityEngine.Vector3>::Equals(System.Nullable`1<T>)
extern "C"  bool Nullable_1_Equals_m2313387266_gshared (Nullable_1_t506773895 * __this, Nullable_1_t506773895  ___other0, const MethodInfo* method)
{
	{
		bool L_0 = (bool)(&___other0)->get_has_value_1();
		bool L_1 = (bool)__this->get_has_value_1();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		bool L_2 = (bool)__this->get_has_value_1();
		if (L_2)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)1;
	}

IL_0021:
	{
		Vector3_t2243707580 * L_3 = (Vector3_t2243707580 *)(&___other0)->get_address_of_value_0();
		Vector3_t2243707580  L_4 = (Vector3_t2243707580 )__this->get_value_0();
		Vector3_t2243707580  L_5 = L_4;
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_5);
		bool L_7 = Vector3_Equals_m2692262876((Vector3_t2243707580 *)L_3, (Il2CppObject *)L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
extern "C"  bool Nullable_1_Equals_m2313387266_AdjustorThunk (Il2CppObject * __this, Nullable_1_t506773895  ___other0, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	bool _returnValue = Nullable_1_Equals_m2313387266(&_thisAdjusted, ___other0, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Int32 System.Nullable`1<UnityEngine.Vector3>::GetHashCode()
extern "C"  int32_t Nullable_1_GetHashCode_m1916216271_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		return 0;
	}

IL_000d:
	{
		Vector3_t2243707580 * L_1 = (Vector3_t2243707580 *)__this->get_address_of_value_0();
		int32_t L_2 = Vector3_GetHashCode_m1754570744((Vector3_t2243707580 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
extern "C"  int32_t Nullable_1_GetHashCode_m1916216271_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	int32_t _returnValue = Nullable_1_GetHashCode_m1916216271(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// T System.Nullable`1<UnityEngine.Vector3>::GetValueOrDefault()
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m2630877366_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_GetValueOrDefault_m2630877366_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  G_B3_0;
	memset(&G_B3_0, 0, sizeof(G_B3_0));
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector3_t2243707580  L_1 = (Vector3_t2243707580 )__this->get_value_0();
		G_B3_0 = L_1;
		goto IL_001f;
	}

IL_0016:
	{
		Initobj (Vector3_t2243707580_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t2243707580  L_2 = V_0;
		G_B3_0 = L_2;
	}

IL_001f:
	{
		return G_B3_0;
	}
}
extern "C"  Vector3_t2243707580  Nullable_1_GetValueOrDefault_m2630877366_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	Vector3_t2243707580  _returnValue = Nullable_1_GetValueOrDefault_m2630877366(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.String System.Nullable`1<UnityEngine.Vector3>::ToString()
extern "C"  String_t* Nullable_1_ToString_m769970515_gshared (Nullable_1_t506773895 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Nullable_1_ToString_m769970515_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = (bool)__this->get_has_value_1();
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Vector3_t2243707580 * L_1 = (Vector3_t2243707580 *)__this->get_address_of_value_0();
		String_t* L_2 = Vector3_ToString_m3857189970((Vector3_t2243707580 *)L_1, /*hidden argument*/NULL);
		return L_2;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		return L_3;
	}
}
extern "C"  String_t* Nullable_1_ToString_m769970515_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Nullable_1_t506773895  _thisAdjusted;
	_thisAdjusted.set_value_0(*reinterpret_cast<Vector3_t2243707580 *>(__this + 1));
	_thisAdjusted.set_has_value_1(true);
	String_t* _returnValue = Nullable_1_ToString_m769970515(&_thisAdjusted, method);
	*reinterpret_cast<Vector3_t2243707580 *>(__this + 1) = _thisAdjusted.get_value_0();
	return _returnValue;
}
// System.Void System.Predicate`1<System.Char>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3460606476_gshared (Predicate_1_t1897451453 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Char>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m762264976_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m762264976((Predicate_1_t1897451453 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppChar ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Char>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856533029_gshared (Predicate_1_t1897451453 * __this, Il2CppChar ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856533029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Char_t3454481338_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Char>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1807093618_gshared (Predicate_1_t1897451453 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Int32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2826800414_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Int32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m695569038_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m695569038((Predicate_1_t514847563 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, int32_t ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Int32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2559992383_gshared (Predicate_1_t514847563 * __this, int32_t ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2559992383_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t2071877448_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Int32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1202813828_gshared (Predicate_1_t514847563 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2289454599_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Object>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m4047721271_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m4047721271((Predicate_1_t1132419410 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (void* __this, Il2CppObject * ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3556950370_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___obj0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3656575065_gshared (Predicate_1_t1132419410 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1767993638_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m527131606_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m527131606((Predicate_1_t2832094954 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeNamedArgument_t94157543  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1448216027_gshared (Predicate_1_t2832094954 * __this, CustomAttributeNamedArgument_t94157543  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1448216027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeNamedArgument_t94157543_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeNamedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m215026240_gshared (Predicate_1_t2832094954 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1292402863_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2060780095_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2060780095((Predicate_1_t4236135325 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, CustomAttributeTypedArgument_t1498197914  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1856151290_gshared (Predicate_1_t4236135325 * __this, CustomAttributeTypedArgument_t1498197914  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1856151290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(CustomAttributeTypedArgument_t1498197914_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m259774785_gshared (Predicate_1_t4236135325 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.AnimatorClipInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m541404361_gshared (Predicate_1_t2348721464 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m744913181_gshared (Predicate_1_t2348721464 * __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m744913181((Predicate_1_t2348721464 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, AnimatorClipInfo_t3905751349  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.AnimatorClipInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2336395304_gshared (Predicate_1_t2348721464 * __this, AnimatorClipInfo_t3905751349  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2336395304_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(AnimatorClipInfo_t3905751349_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.AnimatorClipInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1604508263_gshared (Predicate_1_t2348721464 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Color32>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3811123782_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m122788314_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m122788314((Predicate_1_t3612454929 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Color32_t874517518  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Color32>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2959352225_gshared (Predicate_1_t3612454929 * __this, Color32_t874517518  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2959352225_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Color32_t874517518_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Color32>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m924884444_gshared (Predicate_1_t3612454929 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1567825400_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3860206640_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3860206640((Predicate_1_t2759123787 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, RaycastResult_t21186376  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m4068629879_gshared (Predicate_1_t2759123787 * __this, RaycastResult_t21186376  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m4068629879_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(RaycastResult_t21186376_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.EventSystems.RaycastResult>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m973058386_gshared (Predicate_1_t2759123787 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m1020292372_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3539717340_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3539717340((Predicate_1_t1499606915 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UICharInfo_t3056636800  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UICharInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3056726495_gshared (Predicate_1_t1499606915 * __this, UICharInfo_t3056636800  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3056726495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UICharInfo_t3056636800_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2354180346_gshared (Predicate_1_t1499606915 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UILineInfo>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m784266182_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m577088274_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m577088274((Predicate_1_t2064247989 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UILineInfo_t3621277874  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UILineInfo>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2329589669_gshared (Predicate_1_t2064247989 * __this, UILineInfo_t3621277874  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2329589669_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UILineInfo_t3621277874_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UILineInfo>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3442731496_gshared (Predicate_1_t2064247989 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.UIVertex>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m549279630_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2883675618_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2883675618((Predicate_1_t3942196229 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, UIVertex_t1204258818  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.UIVertex>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m3926587117_gshared (Predicate_1_t3942196229 * __this, UIVertex_t1204258818  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m3926587117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(UIVertex_t1204258818_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.UIVertex>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m337889472_gshared (Predicate_1_t3942196229 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector2>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2863314033_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m3001657933_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m3001657933((Predicate_1_t686677694 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector2_t2243707579  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector2>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m866207434_gshared (Predicate_1_t686677694 * __this, Vector2_t2243707579  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m866207434_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector2_t2243707579_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector2>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m3406729927_gshared (Predicate_1_t686677694 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector3>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m3243601712_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2775223656_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2775223656((Predicate_1_t686677695 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector3_t2243707580  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector3>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m1764756107_gshared (Predicate_1_t686677695 * __this, Vector3_t2243707580  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m1764756107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector3>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m1035116514_gshared (Predicate_1_t686677695 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Predicate`1<UnityEngine.Vector4>::.ctor(System.Object,System.IntPtr)
extern "C"  void Predicate_1__ctor_m2995226103_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::Invoke(T)
extern "C"  bool Predicate_1_Invoke_m2407726575_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Predicate_1_Invoke_m2407726575((Predicate_1_t686677696 *)__this->get_prev_9(),___obj0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef bool (*FunctionPointerType) (Il2CppObject *, void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef bool (*FunctionPointerType) (void* __this, Vector4_t2243707581  ___obj0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),___obj0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Predicate`1<UnityEngine.Vector4>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Predicate_1_BeginInvoke_m2425667920_gshared (Predicate_1_t686677696 * __this, Vector4_t2243707581  ___obj0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Predicate_1_BeginInvoke_m2425667920_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Vector4_t2243707581_il2cpp_TypeInfo_var, &___obj0);
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// System.Boolean System.Predicate`1<UnityEngine.Vector4>::EndInvoke(System.IAsyncResult)
extern "C"  bool Predicate_1_EndInvoke_m2420144145_gshared (Predicate_1_t686677696 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return *(bool*)UnBox ((Il2CppCodeGenObject*)__result);
}
// System.Void System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
extern "C"  void Getter_2__ctor_m653998582_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___object0, IntPtr_t ___method1, const MethodInfo* method)
{
	__this->set_method_ptr_0((Il2CppMethodPointer)((MethodInfo*)___method1.get_m_value_0())->methodPointer);
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::Invoke(T)
extern "C"  Il2CppObject * Getter_2_Invoke_m3338489829_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, const MethodInfo* method)
{
	if(__this->get_prev_9() != NULL)
	{
		Getter_2_Invoke_m3338489829((Getter_2_t4179406139 *)__this->get_prev_9(),____this0, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->get_method_3().get_m_value_0()));
	if (__this->get_m_target_2() != NULL && ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (Il2CppObject *, void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(NULL,__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else if (__this->get_m_target_2() != NULL || ___methodIsStatic)
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, Il2CppObject * ____this0, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(__this->get_m_target_2(),____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
	else
	{
		typedef Il2CppObject * (*FunctionPointerType) (void* __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->get_method_ptr_0())(____this0,(MethodInfo*)(__this->get_method_3().get_m_value_0()));
	}
}
// System.IAsyncResult System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::BeginInvoke(T,System.AsyncCallback,System.Object)
extern "C"  Il2CppObject * Getter_2_BeginInvoke_m2080015031_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ____this0, AsyncCallback_t163412349 * ___callback1, Il2CppObject * ___object2, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ____this0;
	return (Il2CppObject *)il2cpp_codegen_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback1, (Il2CppObject*)___object2);
}
// R System.Reflection.MonoProperty/Getter`2<System.Object,System.Object>::EndInvoke(System.IAsyncResult)
extern "C"  Il2CppObject * Getter_2_EndInvoke_m977999903_gshared (Getter_2_t4179406139 * __this, Il2CppObject * ___result0, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
	return (Il2CppObject *)__result;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
