﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "AssemblyU2DCSharp_U3CModuleU3E3783534214.h"
#include "AssemblyU2DCSharp_AudioManager4222704959.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "UnityEngine_UnityEngine_AudioSource1135106623.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_AudioClip1932558630.h"
#include "AssemblyU2DCSharp_CameraManager2379859346.h"
#include "UnityEngine_UnityEngine_Camera189460977.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "mscorlib_System_Single2076509932.h"
#include "AssemblyU2DCSharp_Controller1937198888.h"
#include "AssemblyU2DCSharp_FSMSystem2363122359.h"
#include "mscorlib_System_String2029220233.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "AssemblyU2DCSharp_Model873752437.h"
#include "AssemblyU2DCSharp_View854181575.h"
#include "AssemblyU2DCSharp_GameManager2252321495.h"
#include "AssemblyU2DCSharp_FSMState3109806909.h"
#include "mscorlib_System_Int322071877448.h"
#include "AssemblyU2DCSharp_MenuState2133158756.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimation858634588.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Rect3681755626.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "DOTweenPro_DG_Tweening_Core_DOTweenAnimationType119935370.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "mscorlib_System_Object2689449295.h"
#include "DOTweenPro_DG_Tweening_Core_TargetType2706200073.h"
#include "mscorlib_System_Type1303803226.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Vector22243707579.h"
#include "UnityEngine_UnityEngine_RectTransform3349966182.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "UnityEngine_UnityEngine_Rigidbody2D502193897.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "UnityEngine_UnityEngine_Material193706927.h"
#include "UnityEngine_UI_UnityEngine_UI_Image2042527209.h"
#include "UnityEngine_UI_UnityEngine_UI_Text356221433.h"
#include "UnityEngine_UnityEngine_Light494725636.h"
#include "UnityEngine_UnityEngine_CanvasGroup3296560743.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "UnityEngine_UnityEngine_Renderer257310565.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3942566441.h"
#include "mscorlib_System_RuntimeTypeHandle2330101084.h"
#include "AssemblyU2DCSharp_DG_Tweening_DOTweenAnimationExte4140352346.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_g1643289790.h"
#include "AssemblyU2DCSharp_StateID3221758012.h"
#include "AssemblyU2DCSharp_Transition3224717525.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2478928041.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2013657715.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1593300101.h"
#include "AssemblyU2DCSharp_Shape1303907469.h"
#include "AssemblyU2DCSharp_GameOverState4251729781.h"
#include "UnityEngine_UnityEngine_SceneManagement_Scene1684909666.h"
#include "AssemblyU2DCSharp_JarodInputController1982328090.h"
#include "UnityEngine_UnityEngine_KeyCode2283395152.h"
#include "AssemblyU2DCSharp_PauseState1378269423.h"
#include "AssemblyU2DCSharp_PlayState1063998429.h"
#include "AssemblyU2DCSharp_Vector3Extension2323347309.h"

// AudioManager
struct AudioManager_t4222704959;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;
// System.Object
struct Il2CppObject;
// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// CameraManager
struct CameraManager_t2379859346;
// UnityEngine.Camera
struct Camera_t189460977;
// DG.Tweening.Tweener
struct Tweener_t760404022;
// Controller
struct Controller_t1937198888;
// FSMSystem
struct FSMSystem_t2363122359;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// System.String
struct String_t;
// Model
struct Model_t873752437;
// View
struct View_t854181575;
// GameManager
struct GameManager_t2252321495;
// FSMState[]
struct FSMStateU5BU5D_t1291452208;
// System.Object[]
struct ObjectU5BU5D_t3614634134;
// FSMState
struct FSMState_t3109806909;
// MenuState
struct MenuState_t2133158756;
// DG.Tweening.DOTweenAnimation
struct DOTweenAnimation_t858634588;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_t2205594551;
// DG.Tweening.Tween
struct Tween_t278478013;
// UnityEngine.Object
struct Object_t1021602117;
// System.Type
struct Type_t;
// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t502193897;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Renderer
struct Renderer_t257310565;
// UnityEngine.Material
struct Material_t193706927;
// UnityEngine.UI.Image
struct Image_t2042527209;
// UnityEngine.UI.Text
struct Text_t356221433;
// UnityEngine.Light
struct Light_t494725636;
// UnityEngine.CanvasGroup
struct CanvasGroup_t3296560743;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// DG.Tweening.DOTweenAnimation[]
struct DOTweenAnimationU5BU5D_t3410086325;
// System.Collections.Generic.List`1<DG.Tweening.Tween>
struct List_1_t3942566441;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// System.Collections.Generic.Dictionary`2<Transition,StateID>
struct Dictionary_2_t1643289790;
// System.String[]
struct StringU5BU5D_t1642385972;
// System.Collections.Generic.List`1<FSMState>
struct List_1_t2478928041;
// Shape
struct Shape_t1303907469;
// GameOverState
struct GameOverState_t4251729781;
// JarodInputController
struct JarodInputController_t1982328090;
// System.Collections.IEnumerator
struct IEnumerator_t1466026749;
// PauseState
struct PauseState_t1378269423;
// PlayState
struct PlayState_t1063998429;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var;
extern const uint32_t AudioManager_Awake_m3060117883_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponent_TisModel_t873752437_m356947676_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisView_t854181575_m3397135428_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisGameManager_t2252321495_m2672045306_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisCameraManager_t2379859346_m84123661_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioManager_t4222704959_m2168321546_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1601830955;
extern Il2CppCodeGenString* _stringLiteral1582260093;
extern const uint32_t Controller_Awake_m587736580_MetadataUsageId;
extern Il2CppClass* FSMSystem_t2363122359_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentsInChildren_TisFSMState_t3109806909_m3307343245_MethodInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisMenuState_t2133158756_m2656785355_MethodInfo_var;
extern const uint32_t Controller_MakeFSM_m2994105093_MetadataUsageId;
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenAnimation__ctor_m3603539837_MetadataUsageId;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t1368543263_il2cpp_TypeInfo_var;
extern Il2CppClass* RectTransform_t3349966182_il2cpp_TypeInfo_var;
extern Il2CppClass* Transform_t3275118058_il2cpp_TypeInfo_var;
extern Il2CppClass* Rigidbody2D_t502193897_il2cpp_TypeInfo_var;
extern Il2CppClass* Rigidbody_t4233889191_il2cpp_TypeInfo_var;
extern Il2CppClass* SpriteRenderer_t1209076198_il2cpp_TypeInfo_var;
extern Il2CppClass* Renderer_t257310565_il2cpp_TypeInfo_var;
extern Il2CppClass* Image_t2042527209_il2cpp_TypeInfo_var;
extern Il2CppClass* Text_t356221433_il2cpp_TypeInfo_var;
extern Il2CppClass* Light_t494725636_il2cpp_TypeInfo_var;
extern Il2CppClass* CanvasGroup_t3296560743_il2cpp_TypeInfo_var;
extern Il2CppClass* Camera_t189460977_il2cpp_TypeInfo_var;
extern Il2CppClass* Tweener_t760404022_il2cpp_TypeInfo_var;
extern Il2CppClass* TweenCallback_t3697142134_il2cpp_TypeInfo_var;
extern const MethodInfo* TweenSettingsExtensions_From_TisTweener_t760404022_m151127719_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetRelative_TisTween_t278478013_m4137017946_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetTarget_TisTween_t278478013_m1418126220_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetDelay_TisTween_t278478013_m986297435_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTween_t278478013_m618797951_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetAutoKill_TisTween_t278478013_m4205239577_MethodInfo_var;
extern const MethodInfo* DOTweenAnimation_U3CCreateTweenU3Em__0_m657452871_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnKill_TisTween_t278478013_m2880655110_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetSpeedBased_TisTween_t278478013_m783493617_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisTween_t278478013_m2274347319_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisTween_t278478013_m3479639913_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetId_TisTween_t278478013_m3480976796_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetUpdate_TisTween_t278478013_m696735837_MethodInfo_var;
extern const MethodInfo* UnityEvent_Invoke_m4163344491_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnStart_TisTween_t278478013_m3936654790_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnPlay_TisTween_t278478013_m1768194704_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnUpdate_TisTween_t278478013_m4256285963_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnStepComplete_TisTween_t278478013_m367519805_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTween_t278478013_m1217840999_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnRewind_TisTween_t278478013_m3534007205_MethodInfo_var;
extern const MethodInfo* TweenExtensions_Play_TisTween_t278478013_m2584288383_MethodInfo_var;
extern const MethodInfo* TweenExtensions_Pause_TisTween_t278478013_m3798767189_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2760277904;
extern Il2CppCodeGenString* _stringLiteral946533051;
extern Il2CppCodeGenString* _stringLiteral4290420597;
extern Il2CppCodeGenString* _stringLiteral388623391;
extern const uint32_t DOTweenAnimation_CreateTween_m1462837152_MetadataUsageId;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenAnimation_DOPlay_m1488723384_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayBackwards_m3876039504_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayForward_m1452188533_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPause_m1168707782_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOTogglePause_m3112936206_MetadataUsageId;
extern const MethodInfo* GameObject_GetComponents_TisDOTweenAnimation_t858634588_m3627101310_MethodInfo_var;
extern const uint32_t DOTweenAnimation_DORewind_m9460949_MetadataUsageId;
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenAnimation_DORestart_m4058931822_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOComplete_m4267256299_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOKill_m1786421626_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayById_m13501458_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayAllById_m682662527_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPauseAllById_m2716110985_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayBackwardsById_m262489098_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayBackwardsAllById_m168913475_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayForwardById_m885204937_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DOPlayForwardAllById_m2751131570_MetadataUsageId;
extern const MethodInfo* Component_GetComponents_TisDOTweenAnimation_t858634588_m1959229896_MethodInfo_var;
extern const uint32_t DOTweenAnimation_DOPlayNext_m590559157_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DORewindAndPlayNext_m473560461_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DORestartById_m2699658521_MetadataUsageId;
extern const uint32_t DOTweenAnimation_DORestartAllById_m1200781746_MetadataUsageId;
extern Il2CppClass* List_1_t3942566441_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1955107610_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2823288694_MethodInfo_var;
extern const uint32_t DOTweenAnimation_GetTweens_m2149779259_MetadataUsageId;
extern const Il2CppType* TargetType_t2706200073_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Enum_t2459695545_il2cpp_TypeInfo_var;
extern Il2CppClass* TargetType_t2706200073_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral372029316;
extern Il2CppCodeGenString* _stringLiteral3585144781;
extern Il2CppCodeGenString* _stringLiteral438863844;
extern const uint32_t DOTweenAnimation_TypeToDOTargetType_m2103550928_MetadataUsageId;
extern Il2CppClass* Vector3_t2243707580_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenAnimation_ReEvaluateRelativeTween_m314855898_MetadataUsageId;
extern Il2CppClass* Dictionary_2_t1643289790_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m2199854661_MethodInfo_var;
extern const uint32_t FSMState__ctor_m2907867150_MetadataUsageId;
extern Il2CppClass* StringU5BU5D_t1642385972_il2cpp_TypeInfo_var;
extern Il2CppClass* StateID_t3221758012_il2cpp_TypeInfo_var;
extern Il2CppClass* Transition_t3224717525_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_ContainsKey_m1345547490_MethodInfo_var;
extern const MethodInfo* Dictionary_2_Add_m3011419773_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2792294009;
extern Il2CppCodeGenString* _stringLiteral2764825468;
extern Il2CppCodeGenString* _stringLiteral1280487602;
extern Il2CppCodeGenString* _stringLiteral206097795;
extern Il2CppCodeGenString* _stringLiteral939293652;
extern const uint32_t FSMState_AddTransition_m3341888567_MetadataUsageId;
extern const MethodInfo* Dictionary_2_Remove_m2584370466_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1311624908;
extern Il2CppCodeGenString* _stringLiteral2820183298;
extern Il2CppCodeGenString* _stringLiteral56031043;
extern Il2CppCodeGenString* _stringLiteral716588638;
extern const uint32_t FSMState_DeleteTransition_m3401417729_MetadataUsageId;
extern const MethodInfo* Dictionary_2_get_Item_m2924153579_MethodInfo_var;
extern const uint32_t FSMState_GetOutputState_m2828798026_MetadataUsageId;
extern Il2CppClass* List_1_t2478928041_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m2113850620_MethodInfo_var;
extern const uint32_t FSMSystem__ctor_m3437460506_MetadataUsageId;
extern const MethodInfo* List_1_get_Count_m3256523046_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2731754952_MethodInfo_var;
extern const MethodInfo* List_1_GetEnumerator_m3953815739_MethodInfo_var;
extern const MethodInfo* Enumerator_get_Current_m3369313255_MethodInfo_var;
extern const MethodInfo* Enumerator_MoveNext_m653200627_MethodInfo_var;
extern const MethodInfo* Enumerator_Dispose_m3890009865_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3477464233;
extern Il2CppCodeGenString* _stringLiteral1491674556;
extern Il2CppCodeGenString* _stringLiteral1873843773;
extern const uint32_t FSMSystem_AddState_m3680092715_MetadataUsageId;
extern const MethodInfo* List_1_Remove_m2051816421_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral923137055;
extern Il2CppCodeGenString* _stringLiteral2374734972;
extern Il2CppCodeGenString* _stringLiteral3206117546;
extern const uint32_t FSMSystem_DeleteState_m2089744880_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral1366634108;
extern Il2CppCodeGenString* _stringLiteral4134666537;
extern Il2CppCodeGenString* _stringLiteral739081689;
extern const uint32_t FSMSystem_PerformTransition_m4026309299_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisController_t1937198888_m325252931_MethodInfo_var;
extern const uint32_t GameManager_Awake_m99497495_MetadataUsageId;
extern const uint32_t GameManager_Update_m969954595_MetadataUsageId;
extern const MethodInfo* Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var;
extern const uint32_t GameManager_SpawnShape_m2959733954_MetadataUsageId;
extern const uint32_t GameManager_StartGame_m4019248290_MetadataUsageId;
extern const uint32_t GameManager_PauseGame_m1634217600_MetadataUsageId;
extern const uint32_t JarodInputController_Awake_m3854285658_MetadataUsageId;
extern Il2CppClass* Input_t1785128008_il2cpp_TypeInfo_var;
extern const uint32_t JarodInputController_Update_m3112380776_MetadataUsageId;
extern Il2CppClass* Mathf_t2336485820_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1543969241;
extern Il2CppCodeGenString* _stringLiteral1367190538;
extern Il2CppCodeGenString* _stringLiteral109637592;
extern Il2CppCodeGenString* _stringLiteral3423761043;
extern const uint32_t JarodInputController_ToAddFingerAction_m3253792990_MetadataUsageId;
extern Il2CppClass* TransformU5BU2CU5D_t3764228912_il2cpp_TypeInfo_var;
extern const uint32_t Model_Awake_m3895385605_MetadataUsageId;
extern Il2CppClass* IEnumerator_t1466026749_il2cpp_TypeInfo_var;
extern Il2CppClass* IDisposable_t2427283555_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3745862197;
extern const uint32_t Model_IsValidMapPosition_m2662347148_MetadataUsageId;
extern const uint32_t Model_SetBlockMap_m4073711356_MetadataUsageId;
extern const uint32_t Model_CheckIsRowFull_m1669201880_MetadataUsageId;
extern const uint32_t Model_DeleteRow_m3074637084_MetadataUsageId;
extern const uint32_t Model_MoveDownRow_m1192001122_MetadataUsageId;
extern const uint32_t Model_IsGameOver_m4267625588_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3131029212;
extern Il2CppCodeGenString* _stringLiteral1969896486;
extern const uint32_t Model_LoadData_m951443834_MetadataUsageId;
extern const uint32_t Model_SaveData_m1825965529_MetadataUsageId;
extern const uint32_t Model_Restart_m1980656027_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859_MethodInfo_var;
extern const uint32_t Shape_Init_m2122446389_MetadataUsageId;
extern const uint32_t Shape_ControllerInput_m1474408371_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral2838555398;
extern const uint32_t Shape_ShapeRotate_m289486130_MetadataUsageId;
extern const uint32_t Vector3Extension_Vector3Round_m4246609207_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral373485295;
extern Il2CppCodeGenString* _stringLiteral1863692018;
extern Il2CppCodeGenString* _stringLiteral2447773569;
extern Il2CppCodeGenString* _stringLiteral3183887342;
extern Il2CppCodeGenString* _stringLiteral1933155027;
extern Il2CppCodeGenString* _stringLiteral4045373120;
extern Il2CppCodeGenString* _stringLiteral3358479857;
extern Il2CppCodeGenString* _stringLiteral3778205194;
extern Il2CppCodeGenString* _stringLiteral2029194541;
extern Il2CppCodeGenString* _stringLiteral3676749550;
extern Il2CppCodeGenString* _stringLiteral3060889074;
extern const uint32_t View_Start_m3247243028_MetadataUsageId;
extern const MethodInfo* View_U3CHideMenuU3Em__0_m60288546_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386_MethodInfo_var;
extern const MethodInfo* View_U3CHideMenuU3Em__1_m1606169511_MethodInfo_var;
extern const uint32_t View_HideMenu_m3662219881_MetadataUsageId;
extern const MethodInfo* View_U3CHideGameUIU3Em__2_m3610395925_MethodInfo_var;
extern const uint32_t View_HideGameUI_m3415331198_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3360931944;
extern Il2CppCodeGenString* _stringLiteral655579034;
extern Il2CppCodeGenString* _stringLiteral2256806354;
extern const uint32_t View_ShowRankData_m4259729890_MetadataUsageId;

// FSMState[]
struct FSMStateU5BU5D_t1291452208  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) FSMState_t3109806909 * m_Items[1];

public:
	inline FSMState_t3109806909 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline FSMState_t3109806909 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, FSMState_t3109806909 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline FSMState_t3109806909 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline FSMState_t3109806909 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, FSMState_t3109806909 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Keyframe_t1449471340  m_Items[1];

public:
	inline Keyframe_t1449471340  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t1449471340 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t1449471340  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t1449471340  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t1449471340 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t1449471340  value)
	{
		m_Items[index] = value;
	}
};
// DG.Tweening.DOTweenAnimation[]
struct DOTweenAnimationU5BU5D_t3410086325  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) DOTweenAnimation_t858634588 * m_Items[1];

public:
	inline DOTweenAnimation_t858634588 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline DOTweenAnimation_t858634588 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, DOTweenAnimation_t858634588 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline DOTweenAnimation_t858634588 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline DOTweenAnimation_t858634588 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, DOTweenAnimation_t858634588 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.String[]
struct StringU5BU5D_t1642385972  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) GameObject_t1756533147 * m_Items[1];

public:
	inline GameObject_t1756533147 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GameObject_t1756533147 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_t1756533147 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_t1756533147 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Color_t2020392075  m_Items[1];

public:
	inline Color_t2020392075  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Color_t2020392075 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Color_t2020392075  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Color_t2020392075  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Color_t2020392075 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Color_t2020392075  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Transform[0...,0...]
struct TransformU5BU2CU5D_t3764228912  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Transform_t3275118058 * m_Items[1];

public:
	inline Transform_t3275118058 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Transform_t3275118058 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3275118058 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Transform_t3275118058 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3275118058 * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, Transform_t3275118058 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Transform_t3275118058 * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline Transform_t3275118058 ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, Transform_t3275118058 * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0[] UnityEngine.Component::GetComponentsInChildren<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::From<System.Object>(!!0,System.Boolean)
extern "C"  Il2CppObject * TweenSettingsExtensions_From_TisIl2CppObject_m1482800441_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<System.Object>(!!0,System.Boolean)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetRelative_TisIl2CppObject_m424127801_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetDelay_TisIl2CppObject_m899641841_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, float p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, int32_t p2, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<System.Object>(!!0,System.Boolean)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetAutoKill_TisIl2CppObject_m3918711238_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnKill_TisIl2CppObject_m3077878346_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<System.Object>(!!0)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetSpeedBased_TisIl2CppObject_m2454034903_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,UnityEngine.AnimationCurve)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetEase_TisIl2CppObject_m3353632809_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, AnimationCurve_t3306541151 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetId_TisIl2CppObject_m3739856218_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<System.Object>(!!0,System.Boolean)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetUpdate_TisIl2CppObject_m1984066668_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnStart_TisIl2CppObject_m1379091048_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnPlay_TisIl2CppObject_m4140701164_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnStepComplete_TisIl2CppObject_m267308340_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnRewind_TisIl2CppObject_m1366008935_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenExtensions::Play<System.Object>(!!0)
extern "C"  Il2CppObject * TweenExtensions_Play_TisIl2CppObject_m335062998_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !!0 DG.Tweening.TweenExtensions::Pause<System.Object>(!!0)
extern "C"  Il2CppObject * TweenExtensions_Pause_TisIl2CppObject_m453341176_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !!0[] UnityEngine.GameObject::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* GameObject_GetComponents_TisIl2CppObject_m890532490_gshared (GameObject_t1756533147 * __this, const MethodInfo* method);
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C"  ObjectU5BU5D_t3614634134* Component_GetComponents_TisIl2CppObject_m3998315035_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C"  void List_1__ctor_m310736118_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<Transition,StateID>::.ctor()
extern "C"  void Dictionary_2__ctor_m2199854661_gshared (Dictionary_2_t1643289790 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<Transition,StateID>::ContainsKey(!0)
extern "C"  bool Dictionary_2_ContainsKey_m1345547490_gshared (Dictionary_2_t1643289790 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.Dictionary`2<Transition,StateID>::Add(!0,!1)
extern "C"  void Dictionary_2_Add_m3011419773_gshared (Dictionary_2_t1643289790 * __this, int32_t p0, int32_t p1, const MethodInfo* method);
// System.Boolean System.Collections.Generic.Dictionary`2<Transition,StateID>::Remove(!0)
extern "C"  bool Dictionary_2_Remove_m2584370466_gshared (Dictionary_2_t1643289790 * __this, int32_t p0, const MethodInfo* method);
// !1 System.Collections.Generic.Dictionary`2<Transition,StateID>::get_Item(!0)
extern "C"  int32_t Dictionary_2_get_Item_m2924153579_gshared (Dictionary_2_t1643289790 * __this, int32_t p0, const MethodInfo* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C"  int32_t List_1_get_Count_m2375293942_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t1593300101  List_1_GetEnumerator_m2837081829_gshared (List_1_t2058570427 * __this, const MethodInfo* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m2577424081_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m44995089_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m3736175406_gshared (Enumerator_t1593300101 * __this, const MethodInfo* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C"  bool List_1_Remove_m3164383811_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C"  Il2CppObject * Object_Instantiate_TisIl2CppObject_m447919519_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, method) ((  AudioSource_t1135106623 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean AudioManager::get_IsPause()
extern "C"  bool AudioManager_get_IsPause_m3310627223 (AudioManager_t4222704959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::set_clip(UnityEngine.AudioClip)
extern "C"  void AudioSource_set_clip_m738814682 (AudioSource_t1135106623 * __this, AudioClip_t1932558630 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioSource::Play()
extern "C"  void AudioSource_Play_m353744792 (AudioSource_t1135106623 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlayAudio(UnityEngine.AudioClip)
extern "C"  void AudioManager_PlayAudio_m3244485735 (AudioManager_t4222704959 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Camera UnityEngine.Camera::get_main()
extern "C"  Camera_t189460977 * Camera_get_main_m475173995 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOOrthoSize(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOOrthoSize_m3058874688 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern "C"  GameObject_t1756533147 * GameObject_FindGameObjectWithTag_m829057129 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.GameObject::GetComponent<Model>()
#define GameObject_GetComponent_TisModel_t873752437_m356947676(__this, method) ((  Model_t873752437 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<View>()
#define GameObject_GetComponent_TisView_t854181575_m3397135428(__this, method) ((  View_t854181575 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<GameManager>()
#define Component_GetComponent_TisGameManager_t2252321495_m2672045306(__this, method) ((  GameManager_t2252321495 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<CameraManager>()
#define Component_GetComponent_TisCameraManager_t2379859346_m84123661(__this, method) ((  CameraManager_t2379859346 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<AudioManager>()
#define Component_GetComponent_TisAudioManager_t4222704959_m2168321546(__this, method) ((  AudioManager_t4222704959 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void Controller::MakeFSM()
extern "C"  void Controller_MakeFSM_m2994105093 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMSystem::.ctor()
extern "C"  void FSMSystem__ctor_m3437460506 (FSMSystem_t2363122359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponentsInChildren<FSMState>()
#define Component_GetComponentsInChildren_TisFSMState_t3109806909_m3307343245(__this, method) ((  FSMStateU5BU5D_t1291452208* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentsInChildren_TisIl2CppObject_m3978412804_gshared)(__this, method)
// System.Void FSMSystem::AddState(FSMState,Controller)
extern "C"  void FSMSystem_AddState_m3680092715 (FSMSystem_t2363122359 * __this, FSMState_t3109806909 * ___s0, Controller_t1937198888 * ___controller1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponentInChildren<MenuState>()
#define Component_GetComponentInChildren_TisMenuState_t2133158756_m2656785355(__this, method) ((  MenuState_t2133158756 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m2461586036_gshared)(__this, method)
// System.Void FSMSystem::SetCurrentState(FSMState)
extern "C"  void FSMSystem_SetCurrentState_m2352184253 (FSMSystem_t2363122359 * __this, FSMState_t3109806909 * ___s0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C"  void Keyframe__ctor_m2042404667 (Keyframe_t1449471340 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2814448007 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Rect__ctor_m1220545469 (Rect_t3681755626 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
extern "C"  void ABSAnimationComponent__ctor_m1064982270 (ABSAnimationComponent_t2205594551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTweenAnimation::CreateTween()
extern "C"  void DOTweenAnimation_CreateTween_m1462837152 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsActive(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsActive_m918693289 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Kill_m3845739180 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Object::get_name()
extern "C"  String_t* Object_get_name_m2079638459 (Object_t1021602117 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Format(System.String,System.Object)
extern "C"  String_t* String_Format_m2024975688 (Il2CppObject * __this /* static, unused */, String_t* p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern "C"  void Debug_LogWarning_m1280021602 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Object::GetType()
extern "C"  Type_t * Object_GetType_m191970594 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::TypeToDOTargetType(System.Type)
extern "C"  int32_t DOTweenAnimation_TypeToDOTargetType_m2103550928 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern "C"  Vector3_t2243707580  Vector3_get_zero_m1527993324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 DG.Tweening.DOTweenUtils46::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern "C"  Vector2_t2243707579  DOTweenUtils46_SwitchToRectTransform_m2431809528 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * p0, RectTransform_t3349966182 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern "C"  Vector3_t2243707580  Vector2_op_Implicit_m176791411 (Il2CppObject * __this /* static, unused */, Vector2_t2243707579  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPos3D_m3279580891 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * p0, Vector3_t2243707580  p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOMove_m813241662 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern "C"  Vector2_t2243707579  Vector2_op_Implicit_m1064335535 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOMove_m620609262 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * p0, Vector2_t2243707579  p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOMove_m1933953753 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * p0, Vector3_t2243707580  p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalMove(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOLocalMove_m1432977613 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DORotate_m3434569207 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DORotate_m177144424 (Il2CppObject * __this /* static, unused */, Rigidbody2D_t502193897 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DORotate_m458975576 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * p0, Vector3_t2243707580  p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOLocalRotate(UnityEngine.Transform,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOLocalRotate_m2660994480 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern "C"  void Vector3__ctor_m2638739322 (Vector3_t2243707580 * __this, float p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOScale_m2754995414 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern "C"  void Vector2__ctor_m3067419446 (Vector2_t2243707579 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOSizeDelta_m2859745733 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * p0, Vector2_t2243707579  p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOColor_m243415068 (Il2CppObject * __this /* static, unused */, SpriteRenderer_t1209076198 * p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.Renderer::get_material()
extern "C"  Material_t193706927 * Renderer_get_material_m2553789785 (Renderer_t257310565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Material,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOColor_m1029787631 (Il2CppObject * __this /* static, unused */, Material_t193706927 * p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m3476021741 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOColor_m4265141523 (Il2CppObject * __this /* static, unused */, Text_t356221433 * p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Light,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOColor_m2540950828 (Il2CppObject * __this /* static, unused */, Light_t494725636 * p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions43::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions43_DOFade_m996156756 (Il2CppObject * __this /* static, unused */, SpriteRenderer_t1209076198 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFade(UnityEngine.Material,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOFade_m2221070795 (Il2CppObject * __this /* static, unused */, Material_t193706927 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m2221289113 (Il2CppObject * __this /* static, unused */, Image_t2042527209 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m2794982395 (Il2CppObject * __this /* static, unused */, Text_t356221433 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOIntensity(UnityEngine.Light,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOIntensity_m2263101207 (Il2CppObject * __this /* static, unused */, Light_t494725636 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOFade_m70814363 (Il2CppObject * __this /* static, unused */, CanvasGroup_t3296560743 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOText_m2936740990 (Il2CppObject * __this /* static, unused */, Text_t356221433 * p0, String_t* p1, float p2, bool p3, int32_t p4, String_t* p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOPunchAnchorPos_m1987664933 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * p0, Vector2_t2243707579  p1, float p2, int32_t p3, float p4, bool p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchPosition(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPunchPosition_m4022188990 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, int32_t p3, float p4, bool p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPunchScale_m3609052310 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, int32_t p3, float p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPunchRotation(UnityEngine.Transform,UnityEngine.Vector3,System.Single,System.Int32,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPunchRotation_m1103244634 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Vector3_t2243707580  p1, float p2, int32_t p3, float p4, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOShakeAnchorPos_m1105810756 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * p0, float p1, Vector2_t2243707579  p2, int32_t p3, float p4, bool p5, bool p6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakePosition(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOShakePosition_m2989795727 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, float p1, Vector3_t2243707580  p2, int32_t p3, float p4, bool p5, bool p6, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeScale(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOShakeScale_m2106531945 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, float p1, Vector3_t2243707580  p2, int32_t p3, float p4, bool p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOShakeRotation(UnityEngine.Transform,System.Single,UnityEngine.Vector3,System.Int32,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOShakeRotation_m3190170693 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, float p1, Vector3_t2243707580  p2, int32_t p3, float p4, bool p5, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOAspect(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOAspect_m4263503343 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOColor(UnityEngine.Camera,UnityEngine.Color,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOColor_m4070360217 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * p0, Color_t2020392075  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOFieldOfView(UnityEngine.Camera,System.Single,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOFieldOfView_m2729074193 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * p0, float p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DOPixelRect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DOPixelRect_m1451235077 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * p0, Rect_t3681755626  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions::DORect(UnityEngine.Camera,UnityEngine.Rect,System.Single)
extern "C"  Tweener_t760404022 * ShortcutExtensions_DORect_m1672543897 (Il2CppObject * __this /* static, unused */, Camera_t189460977 * p0, Rect_t3681755626  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::From<DG.Tweening.Tweener>(!!0,System.Boolean)
#define TweenSettingsExtensions_From_TisTweener_t760404022_m151127719(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, bool, const MethodInfo*))TweenSettingsExtensions_From_TisIl2CppObject_m1482800441_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetRelative<DG.Tweening.Tween>(!!0,System.Boolean)
#define TweenSettingsExtensions_SetRelative_TisTween_t278478013_m4137017946(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, bool, const MethodInfo*))TweenSettingsExtensions_SetRelative_TisIl2CppObject_m424127801_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetTarget<DG.Tweening.Tween>(!!0,System.Object)
#define TweenSettingsExtensions_SetTarget_TisTween_t278478013_m1418126220(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetTarget_TisIl2CppObject_m988600075_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Tween>(!!0,System.Single)
#define TweenSettingsExtensions_SetDelay_TisTween_t278478013_m986297435(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, float, const MethodInfo*))TweenSettingsExtensions_SetDelay_TisIl2CppObject_m899641841_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Tween>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTween_t278478013_m618797951(__this /* static, unused */, p0, p1, p2, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<DG.Tweening.Tween>(!!0,System.Boolean)
#define TweenSettingsExtensions_SetAutoKill_TisTween_t278478013_m4205239577(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, bool, const MethodInfo*))TweenSettingsExtensions_SetAutoKill_TisIl2CppObject_m3918711238_gshared)(__this /* static, unused */, p0, p1, method)
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback__ctor_m3479200459 (TweenCallback_t3697142134 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnKill_TisTween_t278478013_m2880655110(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnKill_TisIl2CppObject_m3077878346_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<DG.Tweening.Tween>(!!0)
#define TweenSettingsExtensions_SetSpeedBased_TisTween_t278478013_m783493617(__this /* static, unused */, p0, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, const MethodInfo*))TweenSettingsExtensions_SetSpeedBased_TisIl2CppObject_m2454034903_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Tween>(!!0,UnityEngine.AnimationCurve)
#define TweenSettingsExtensions_SetEase_TisTween_t278478013_m2274347319(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, AnimationCurve_t3306541151 *, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m3353632809_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Tween>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisTween_t278478013_m3479639913(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2802126737 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<DG.Tweening.Tween>(!!0,System.Object)
#define TweenSettingsExtensions_SetId_TisTween_t278478013_m3480976796(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetId_TisIl2CppObject_m3739856218_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<DG.Tweening.Tween>(!!0,System.Boolean)
#define TweenSettingsExtensions_SetUpdate_TisTween_t278478013_m696735837(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, bool, const MethodInfo*))TweenSettingsExtensions_SetUpdate_TisIl2CppObject_m1984066668_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnStart_TisTween_t278478013_m3936654790(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnStart_TisIl2CppObject_m1379091048_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnPlay_TisTween_t278478013_m1768194704(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnPlay_TisIl2CppObject_m4140701164_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnUpdate_TisTween_t278478013_m4256285963(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnStepComplete_TisTween_t278478013_m367519805(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnStepComplete_TisIl2CppObject_m267308340_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTween_t278478013_m1217840999(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<DG.Tweening.Tween>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnRewind_TisTween_t278478013_m3534007205(__this /* static, unused */, p0, p1, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnRewind_TisIl2CppObject_m1366008935_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Tween>(!!0)
#define TweenExtensions_Play_TisTween_t278478013_m2584288383(__this /* static, unused */, p0, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, const MethodInfo*))TweenExtensions_Play_TisIl2CppObject_m335062998_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Tween>(!!0)
#define TweenExtensions_Pause_TisTween_t278478013_m3798767189(__this /* static, unused */, p0, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, const MethodInfo*))TweenExtensions_Pause_TisIl2CppObject_m453341176_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m4163344491 (UnityEvent_t408735097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Play(System.Object)
extern "C"  int32_t DOTween_Play_m164738169 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object)
extern "C"  int32_t DOTween_PlayBackwards_m2839273377 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayForward(System.Object)
extern "C"  int32_t DOTween_PlayForward_m3077912960 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Pause(System.Object)
extern "C"  int32_t DOTween_Pause_m4110474415 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::TogglePause(System.Object)
extern "C"  int32_t DOTween_TogglePause_m1571016433 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.GameObject::GetComponents<DG.Tweening.DOTweenAnimation>()
#define GameObject_GetComponents_TisDOTweenAnimation_t858634588_m3627101310(__this, method) ((  DOTweenAnimationU5BU5D_t3410086325* (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponents_TisIl2CppObject_m890532490_gshared)(__this, method)
// System.Boolean DG.Tweening.TweenExtensions::IsInitialized(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsInitialized_m1900413101 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Rewind_m3286148139 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogNullTween(DG.Tweening.Tween)
extern "C"  void Debugger_LogNullTween_m832479276 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTweenAnimation::ReEvaluateRelativeTween()
extern "C"  void DOTweenAnimation_ReEvaluateRelativeTween_m314855898 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Boolean,System.Single)
extern "C"  int32_t DOTween_Restart_m570313680 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Complete(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Complete_m2005563247 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Kill(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Kill_m211473084 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Play(System.Object,System.Object)
extern "C"  int32_t DOTween_Play_m32308619 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayBackwards(System.Object,System.Object)
extern "C"  int32_t DOTween_PlayBackwards_m544554603 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::PlayForward(System.Object,System.Object)
extern "C"  int32_t DOTween_PlayForward_m1591690666 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0[] UnityEngine.Component::GetComponents<DG.Tweening.DOTweenAnimation>()
#define Component_GetComponents_TisDOTweenAnimation_t858634588_m1959229896(__this, method) ((  DOTweenAnimationU5BU5D_t3410086325* (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponents_TisIl2CppObject_m3998315035_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsPlaying(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsPlaying_m3897946637 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DG.Tweening.TweenExtensions::IsComplete(DG.Tweening.Tween)
extern "C"  bool TweenExtensions_IsComplete_m3095330022 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Rewind(System.Object,System.Boolean)
extern "C"  int32_t DOTween_Rewind_m660085617 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTweenAnimation::DOPlayNext()
extern "C"  void DOTweenAnimation_DOPlayNext_m590559157 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 DG.Tweening.DOTween::Restart(System.Object,System.Object,System.Boolean,System.Single)
extern "C"  int32_t DOTween_Restart_m590221510 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, bool p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::.ctor()
#define List_1__ctor_m1955107610(__this, method) ((  void (*) (List_1_t3942566441 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<DG.Tweening.Tween>::Add(!0)
#define List_1_Add_m2823288694(__this, p0, method) ((  void (*) (List_1_t3942566441 *, Tween_t278478013 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Int32 System.String::LastIndexOf(System.String)
extern "C"  int32_t String_LastIndexOf_m1975817115 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Substring(System.Int32)
extern "C"  String_t* String_Substring_m2032624251 (String_t* __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.String::IndexOf(System.String)
extern "C"  int32_t String_IndexOf_m4251815737 (String_t* __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C"  bool String_op_Inequality_m304203149 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C"  Type_t * Type_GetTypeFromHandle_m432505302 (Il2CppObject * __this /* static, unused */, RuntimeTypeHandle_t2330101084  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object System.Enum::Parse(System.Type,System.String)
extern "C"  Il2CppObject * Enum_Parse_m2561000069 (Il2CppObject * __this /* static, unused */, Type_t * p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C"  Vector3_t2243707580  Transform_get_localPosition_m2533925116 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<Transition,StateID>::.ctor()
#define Dictionary_2__ctor_m2199854661(__this, method) ((  void (*) (Dictionary_2_t1643289790 *, const MethodInfo*))Dictionary_2__ctor_m2199854661_gshared)(__this, method)
// System.Void UnityEngine.Debug::LogError(System.Object)
extern "C"  void Debug_LogError_m3715728798 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.Dictionary`2<Transition,StateID>::ContainsKey(!0)
#define Dictionary_2_ContainsKey_m1345547490(__this, p0, method) ((  bool (*) (Dictionary_2_t1643289790 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m1345547490_gshared)(__this, p0, method)
// System.String System.String::Concat(System.String[])
extern "C"  String_t* String_Concat_m626692867 (Il2CppObject * __this /* static, unused */, StringU5BU5D_t1642385972* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.Dictionary`2<Transition,StateID>::Add(!0,!1)
#define Dictionary_2_Add_m3011419773(__this, p0, p1, method) ((  void (*) (Dictionary_2_t1643289790 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m3011419773_gshared)(__this, p0, p1, method)
// System.Boolean System.Collections.Generic.Dictionary`2<Transition,StateID>::Remove(!0)
#define Dictionary_2_Remove_m2584370466(__this, p0, method) ((  bool (*) (Dictionary_2_t1643289790 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m2584370466_gshared)(__this, p0, method)
// !1 System.Collections.Generic.Dictionary`2<Transition,StateID>::get_Item(!0)
#define Dictionary_2_get_Item_m2924153579(__this, p0, method) ((  int32_t (*) (Dictionary_2_t1643289790 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m2924153579_gshared)(__this, p0, method)
// System.Void System.Object::.ctor()
extern "C"  void Object__ctor_m2551263788 (Il2CppObject * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<FSMState>::.ctor()
#define List_1__ctor_m2113850620(__this, method) ((  void (*) (List_1_t2478928041 *, const MethodInfo*))List_1__ctor_m310736118_gshared)(__this, method)
// StateID FSMState::get_ID()
extern "C"  int32_t FSMState_get_ID_m3972705597 (FSMState_t3109806909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMState::set_FSM(FSMSystem)
extern "C"  void FSMState_set_FSM_m3108072358 (FSMState_t3109806909 * __this, FSMSystem_t2363122359 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMState::set_Controller(Controller)
extern "C"  void FSMState_set_Controller_m2162207389 (FSMState_t3109806909 * __this, Controller_t1937198888 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<FSMState>::get_Count()
#define List_1_get_Count_m3256523046(__this, method) ((  int32_t (*) (List_1_t2478928041 *, const MethodInfo*))List_1_get_Count_m2375293942_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<FSMState>::Add(!0)
#define List_1_Add_m2731754952(__this, p0, method) ((  void (*) (List_1_t2478928041 *, FSMState_t3109806909 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<FSMState>::GetEnumerator()
#define List_1_GetEnumerator_m3953815739(__this, method) ((  Enumerator_t2013657715  (*) (List_1_t2478928041 *, const MethodInfo*))List_1_GetEnumerator_m2837081829_gshared)(__this, method)
// !0 System.Collections.Generic.List`1/Enumerator<FSMState>::get_Current()
#define Enumerator_get_Current_m3369313255(__this, method) ((  FSMState_t3109806909 * (*) (Enumerator_t2013657715 *, const MethodInfo*))Enumerator_get_Current_m2577424081_gshared)(__this, method)
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m612901809 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.Collections.Generic.List`1/Enumerator<FSMState>::MoveNext()
#define Enumerator_MoveNext_m653200627(__this, method) ((  bool (*) (Enumerator_t2013657715 *, const MethodInfo*))Enumerator_MoveNext_m44995089_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<FSMState>::Dispose()
#define Enumerator_Dispose_m3890009865(__this, method) ((  void (*) (Enumerator_t2013657715 *, const MethodInfo*))Enumerator_Dispose_m3736175406_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<FSMState>::Remove(!0)
#define List_1_Remove_m2051816421(__this, p0, method) ((  bool (*) (List_1_t2478928041 *, FSMState_t3109806909 *, const MethodInfo*))List_1_Remove_m3164383811_gshared)(__this, p0, method)
// StateID FSMState::GetOutputState(Transition)
extern "C"  int32_t FSMState_GetOutputState_m2828798026 (FSMState_t3109806909 * __this, int32_t ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String System.String::Concat(System.String,System.String,System.String,System.String)
extern "C"  String_t* String_Concat_m1561703559 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, String_t* p2, String_t* p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<Controller>()
#define Component_GetComponent_TisController_t1937198888_m325252931(__this, method) ((  Controller_t1937198888 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void GameManager::SpawnShape()
extern "C"  void GameManager_SpawnShape_m2959733954 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C"  int32_t Random_Range_m694320887 (Il2CppObject * __this /* static, unused */, int32_t p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t1756533147_m3664764861(__this /* static, unused */, p0, method) ((  GameObject_t1756533147 * (*) (Il2CppObject * /* static, unused */, GameObject_t1756533147 *, const MethodInfo*))Object_Instantiate_TisIl2CppObject_m447919519_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.GameObject::GetComponent<Shape>()
#define GameObject_GetComponent_TisShape_t1303907469_m1545819496(__this, method) ((  Shape_t1303907469 * (*) (GameObject_t1756533147 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2812611596_gshared)(__this, method)
// System.Void Shape::Init(UnityEngine.Color,Controller,GameManager)
extern "C"  void Shape_Init_m2122446389 (Shape_t1303907469 * __this, Color_t2020392075  ___color0, Controller_t1937198888 * ___controller1, GameManager_t2252321495 * ___gameManager2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shape::StartShapeFall()
extern "C"  void Shape_StartShapeFall_m3725894794 (Shape_t1303907469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shape::StopShapeFall()
extern "C"  void Shape_StopShapeFall_m1202009304 (Shape_t1303907469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Model::IsGameOver()
extern "C"  bool Model_IsGameOver_m4267625588 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::PauseGame()
extern "C"  void GameManager_PauseGame_m1634217600 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// FSMSystem Controller::get_Fsm()
extern "C"  FSMSystem_t2363122359 * Controller_get_Fsm_m320623870 (Controller_t1937198888 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMSystem::PerformTransition(Transition)
extern "C"  void FSMSystem_PerformTransition_m4026309299 (FSMSystem_t2363122359 * __this, int32_t ___trans0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMState::.ctor()
extern "C"  void FSMState__ctor_m2907867150 (FSMState_t3109806909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMState::AddTransition(Transition,StateID)
extern "C"  void FSMState_AddTransition_m3341888567 (FSMState_t3109806909 * __this, int32_t ___trans0, int32_t ___id1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMState::DoBeforeEntering()
extern "C"  void FSMState_DoBeforeEntering_m4256541916 (FSMState_t3109806909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Model::get_Score()
extern "C"  int32_t Model_get_Score_m1604402383 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowGameOverUI(System.Int32)
extern "C"  void View_ShowGameOverUI_m3030614956 (View_t854181575 * __this, int32_t ___score0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FSMState::DoBeforeLeaving()
extern "C"  void FSMState_DoBeforeLeaving_m770774864 (FSMState_t3109806909 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::HideGameOverUI()
extern "C"  void View_HideGameOverUI_m516871344 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::Restart()
extern "C"  void Model_Restart_m1980656027 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern "C"  Scene_t1684909666  SceneManager_GetActiveScene_m2964039490 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern "C"  int32_t Scene_get_buildIndex_m3735680091 (Scene_t1684909666 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern "C"  void SceneManager_LoadScene_m87258056 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Screen::get_width()
extern "C"  int32_t Screen_get_width_m41137238 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyDown_m1771960377 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern "C"  Vector3_t2243707580  Input_get_mousePosition_m146923508 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void JarodInputController::ToAddFingerAction()
extern "C"  void JarodInputController_ToAddFingerAction_m3253792990 (JarodInputController_t1982328090 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Input::GetKeyUp(UnityEngine.KeyCode)
extern "C"  bool Input_GetKeyUp_m1008512962 (Il2CppObject * __this /* static, unused */, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Debug::Log(System.Object)
extern "C"  void Debug_Log_m920475918 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject GameManager::get_CurrentShape()
extern "C"  GameObject_t1756533147 * GameManager_get_CurrentShape_m2990618538 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shape::ControllerInput(System.Int32)
extern "C"  void Shape_ControllerInput_m1474408371 (Shape_t1303907469 * __this, int32_t ___direction0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowMenu()
extern "C"  void View_ShowMenu_m110806820 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraManager::ZoomOut()
extern "C"  void CameraManager_ZoomOut_m37434308 (CameraManager_t2379859346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::HideMenu()
extern "C"  void View_HideMenu_m3662219881 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlayClickAudio()
extern "C"  void AudioManager_PlayClickAudio_m41531298 (AudioManager_t4222704959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowSettingUI()
extern "C"  void View_ShowSettingUI_m4255451743 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::set_IsPause(System.Boolean)
extern "C"  void AudioManager_set_IsPause_m3955952632 (AudioManager_t4222704959 * __this, bool ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::IsShowAudioLine(System.Boolean)
extern "C"  void View_IsShowAudioLine_m2780770722 (View_t854181575 * __this, bool ___flag0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::HideSettingUI()
extern "C"  void View_HideSettingUI_m2244255162 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Model::get_HighScore()
extern "C"  int32_t Model_get_HighScore_m635096529 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 Model::get_NumbersGame()
extern "C"  int32_t Model_get_NumbersGame_m3909748289 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowRankUI(System.Int32,System.Int32,System.Int32)
extern "C"  void View_ShowRankUI_m2821821284 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, int32_t ___numberGames2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::HideRankUI()
extern "C"  void View_HideRankUI_m2187104746 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::ClearData()
extern "C"  void Model_ClearData_m3016253009 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowRankData(System.Int32,System.Int32,System.Int32)
extern "C"  void View_ShowRankData_m4259729890 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, int32_t ___numberGames2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::LoadData()
extern "C"  void Model_LoadData_m951443834 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern "C"  Il2CppObject * Transform_GetEnumerator_m3479720613 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String UnityEngine.Component::get_tag()
extern "C"  String_t* Component_get_tag_m357168014 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 Vector3Extension::Vector3Round(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3Extension_Vector3Round_m4246609207 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Model::IsInsideMap(UnityEngine.Vector3)
extern "C"  bool Model_IsInsideMap_m1123263623 (Model_t873752437 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Model::CheckRowMap()
extern "C"  bool Model_CheckRowMap_m1906154264 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Model::CheckIsRowFull(System.Int32)
extern "C"  bool Model_CheckIsRowFull_m1669201880 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::DeleteRow(System.Int32)
extern "C"  void Model_DeleteRow_m3074637084 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::MoveDownRowsAbove(System.Int32)
extern "C"  void Model_MoveDownRowsAbove_m1285959532 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::MoveDownRow(System.Int32)
extern "C"  void Model_MoveDownRow_m1192001122 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C"  void Transform_set_position_m2469242620 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Model::SaveData()
extern "C"  void Model_SaveData_m1825965529 (Model_t873752437 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.PlayerPrefs::GetInt(System.String,System.Int32)
extern "C"  int32_t PlayerPrefs_GetInt_m136681260 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern "C"  void PlayerPrefs_SetInt_m3351928596 (Il2CppObject * __this /* static, unused */, String_t* p0, int32_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowGameUI(System.Int32,System.Int32)
extern "C"  void View_ShowGameUI_m3358246707 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraManager::ZoomGame()
extern "C"  void CameraManager_ZoomGame_m1271599204 (CameraManager_t2379859346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::StartGame()
extern "C"  void GameManager_StartGame_m4019248290 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::HideGameUI()
extern "C"  void View_HideGameUI_m3415331198 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::ShowReStartButton()
extern "C"  void View_ShowReStartButton_m2596324120 (View_t854181575 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.Time::get_deltaTime()
extern "C"  float Time_get_deltaTime_m2233168104 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shape::Fall()
extern "C"  void Shape_Fall_m2648392503 (Shape_t1303907469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C"  bool String_op_Equality_m1790663636 (Il2CppObject * __this /* static, unused */, String_t* p0, String_t* p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.SpriteRenderer::set_color(UnityEngine.Color)
extern "C"  void SpriteRenderer_set_color_m2339931967 (SpriteRenderer_t1209076198 * __this, Color_t2020392075  p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlayCtrlAudio()
extern "C"  void AudioManager_PlayCtrlAudio_m2583450153 (AudioManager_t4222704959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Model::IsValidMapPosition(UnityEngine.Transform)
extern "C"  bool Model_IsValidMapPosition_m2662347148 (Model_t873752437 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Shape::ShapeRotate()
extern "C"  void Shape_ShapeRotate_m289486130 (Shape_t1303907469 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Model::SetBlockMap(UnityEngine.Transform)
extern "C"  bool Model_SetBlockMap_m4073711356 (Model_t873752437 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlayDeletRowAudio()
extern "C"  void AudioManager_PlayDeletRowAudio_m3316311464 (AudioManager_t4222704959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void View::UpdateGameUI(System.Int32,System.Int32)
extern "C"  void View_UpdateGameUI_m3712888353 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::set_CurrentShape(UnityEngine.GameObject)
extern "C"  void GameManager_set_CurrentShape_m115997281 (GameManager_t2252321495 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::IsGameOver()
extern "C"  void GameManager_IsGameOver_m844888474 (GameManager_t2252321495 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AudioManager::PlayFallAudio()
extern "C"  void AudioManager_PlayFallAudio_m2178460935 (AudioManager_t4222704959 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern "C"  Transform_t3275118058 * Transform_Find_m3323476454 (Transform_t3275118058 * __this, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  void Transform_RotateAround_m3410686872 (Transform_t3275118058 * __this, Vector3_t2243707580  p0, Vector3_t2243707580  p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern "C"  int32_t Mathf_RoundToInt_m2927198556 (Il2CppObject * __this /* static, unused */, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
#define Component_GetComponent_TisText_t356221433_m1342661039(__this, method) ((  Text_t356221433 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C"  void GameObject_SetActive_m2887581199 (GameObject_t1756533147 * __this, bool p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Tweener DG.Tweening.ShortcutExtensions46::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern "C"  Tweener_t760404022 * ShortcutExtensions46_DOAnchorPosY_m1128768801 (Il2CppObject * __this /* static, unused */, RectTransform_t3349966182 * p0, float p1, float p2, bool p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Tweener>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386(__this /* static, unused */, p0, p1, method) ((  Tweener_t760404022 * (*) (Il2CppObject * /* static, unused */, Tweener_t760404022 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared)(__this /* static, unused */, p0, p1, method)
// System.String System.Int32::ToString()
extern "C"  String_t* Int32_ToString_m2960866144 (int32_t* __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AudioManager::.ctor()
extern "C"  void AudioManager__ctor_m653321928 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean AudioManager::get_IsPause()
extern "C"  bool AudioManager_get_IsPause_m3310627223 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isPause_7();
		return L_0;
	}
}
// System.Void AudioManager::set_IsPause(System.Boolean)
extern "C"  void AudioManager_set_IsPause_m3955952632 (AudioManager_t4222704959 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_isPause_7(L_0);
		return;
	}
}
// System.Void AudioManager::Awake()
extern "C"  void AudioManager_Awake_m3060117883 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AudioManager_Awake_m3060117883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isPause_7((bool)0);
		AudioSource_t1135106623 * L_0 = Component_GetComponent_TisAudioSource_t1135106623_m3920278003(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1135106623_m3920278003_MethodInfo_var);
		__this->set_audioSource_6(L_0);
		return;
	}
}
// System.Void AudioManager::PlayAudio(UnityEngine.AudioClip)
extern "C"  void AudioManager_PlayAudio_m3244485735 (AudioManager_t4222704959 * __this, AudioClip_t1932558630 * ___clip0, const MethodInfo* method)
{
	{
		bool L_0 = AudioManager_get_IsPause_m3310627223(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0022;
		}
	}
	{
		AudioSource_t1135106623 * L_1 = __this->get_audioSource_6();
		AudioClip_t1932558630 * L_2 = ___clip0;
		AudioSource_set_clip_m738814682(L_1, L_2, /*hidden argument*/NULL);
		AudioSource_t1135106623 * L_3 = __this->get_audioSource_6();
		AudioSource_Play_m353744792(L_3, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Void AudioManager::PlayClickAudio()
extern "C"  void AudioManager_PlayClickAudio_m41531298 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	{
		AudioClip_t1932558630 * L_0 = __this->get_clickClip_2();
		AudioManager_PlayAudio_m3244485735(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::PlayCtrlAudio()
extern "C"  void AudioManager_PlayCtrlAudio_m2583450153 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	{
		AudioClip_t1932558630 * L_0 = __this->get_ctrlClip_4();
		AudioManager_PlayAudio_m3244485735(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::PlayFallAudio()
extern "C"  void AudioManager_PlayFallAudio_m2178460935 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	{
		AudioClip_t1932558630 * L_0 = __this->get_fallClip_3();
		AudioManager_PlayAudio_m3244485735(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AudioManager::PlayDeletRowAudio()
extern "C"  void AudioManager_PlayDeletRowAudio_m3316311464 (AudioManager_t4222704959 * __this, const MethodInfo* method)
{
	{
		AudioClip_t1932558630 * L_0 = __this->get_deleteRowClip_5();
		AudioManager_PlayAudio_m3244485735(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraManager::.ctor()
extern "C"  void CameraManager__ctor_m2018575245 (CameraManager_t2379859346 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraManager::Awake()
extern "C"  void CameraManager_Awake_m1577381518 (CameraManager_t2379859346 * __this, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = Camera_get_main_m475173995(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_myCamera_2(L_0);
		return;
	}
}
// System.Void CameraManager::ZoomOut()
extern "C"  void CameraManager_ZoomOut_m37434308 (CameraManager_t2379859346 * __this, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = __this->get_myCamera_2();
		ShortcutExtensions_DOOrthoSize_m3058874688(NULL /*static, unused*/, L_0, (18.0f), (0.8f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraManager::ZoomGame()
extern "C"  void CameraManager_ZoomGame_m1271599204 (CameraManager_t2379859346 * __this, const MethodInfo* method)
{
	{
		Camera_t189460977 * L_0 = __this->get_myCamera_2();
		ShortcutExtensions_DOOrthoSize_m3058874688(NULL /*static, unused*/, L_0, (14.0f), (0.8f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::.ctor()
extern "C"  void Controller__ctor_m2477390111 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// FSMSystem Controller::get_Fsm()
extern "C"  FSMSystem_t2363122359 * Controller_get_Fsm_m320623870 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		FSMSystem_t2363122359 * L_0 = __this->get_fsm_7();
		return L_0;
	}
}
// System.Void Controller::set_Fsm(FSMSystem)
extern "C"  void Controller_set_Fsm_m1808292875 (Controller_t1937198888 * __this, FSMSystem_t2363122359 * ___value0, const MethodInfo* method)
{
	{
		FSMSystem_t2363122359 * L_0 = ___value0;
		__this->set_fsm_7(L_0);
		return;
	}
}
// System.Void Controller::Awake()
extern "C"  void Controller_Awake_m587736580 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_Awake_m587736580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral1601830955, /*hidden argument*/NULL);
		Model_t873752437 * L_1 = GameObject_GetComponent_TisModel_t873752437_m356947676(L_0, /*hidden argument*/GameObject_GetComponent_TisModel_t873752437_m356947676_MethodInfo_var);
		__this->set_model_2(L_1);
		GameObject_t1756533147 * L_2 = GameObject_FindGameObjectWithTag_m829057129(NULL /*static, unused*/, _stringLiteral1582260093, /*hidden argument*/NULL);
		View_t854181575 * L_3 = GameObject_GetComponent_TisView_t854181575_m3397135428(L_2, /*hidden argument*/GameObject_GetComponent_TisView_t854181575_m3397135428_MethodInfo_var);
		__this->set_view_3(L_3);
		GameManager_t2252321495 * L_4 = Component_GetComponent_TisGameManager_t2252321495_m2672045306(__this, /*hidden argument*/Component_GetComponent_TisGameManager_t2252321495_m2672045306_MethodInfo_var);
		__this->set_gameManager_5(L_4);
		CameraManager_t2379859346 * L_5 = Component_GetComponent_TisCameraManager_t2379859346_m84123661(__this, /*hidden argument*/Component_GetComponent_TisCameraManager_t2379859346_m84123661_MethodInfo_var);
		__this->set_cameraManager_4(L_5);
		AudioManager_t4222704959 * L_6 = Component_GetComponent_TisAudioManager_t4222704959_m2168321546(__this, /*hidden argument*/Component_GetComponent_TisAudioManager_t4222704959_m2168321546_MethodInfo_var);
		__this->set_audioManager_6(L_6);
		return;
	}
}
// System.Void Controller::Start()
extern "C"  void Controller_Start_m165415507 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	{
		Controller_MakeFSM_m2994105093(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Controller::MakeFSM()
extern "C"  void Controller_MakeFSM_m2994105093 (Controller_t1937198888 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Controller_MakeFSM_m2994105093_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FSMStateU5BU5D_t1291452208* V_0 = NULL;
	FSMState_t3109806909 * V_1 = NULL;
	FSMStateU5BU5D_t1291452208* V_2 = NULL;
	int32_t V_3 = 0;
	MenuState_t2133158756 * V_4 = NULL;
	{
		FSMSystem_t2363122359 * L_0 = (FSMSystem_t2363122359 *)il2cpp_codegen_object_new(FSMSystem_t2363122359_il2cpp_TypeInfo_var);
		FSMSystem__ctor_m3437460506(L_0, /*hidden argument*/NULL);
		__this->set_fsm_7(L_0);
		FSMStateU5BU5D_t1291452208* L_1 = Component_GetComponentsInChildren_TisFSMState_t3109806909_m3307343245(__this, /*hidden argument*/Component_GetComponentsInChildren_TisFSMState_t3109806909_m3307343245_MethodInfo_var);
		V_0 = L_1;
		FSMStateU5BU5D_t1291452208* L_2 = V_0;
		V_2 = L_2;
		V_3 = 0;
		goto IL_0030;
	}

IL_001b:
	{
		FSMStateU5BU5D_t1291452208* L_3 = V_2;
		int32_t L_4 = V_3;
		int32_t L_5 = L_4;
		FSMState_t3109806909 * L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		V_1 = L_6;
		FSMSystem_t2363122359 * L_7 = __this->get_fsm_7();
		FSMState_t3109806909 * L_8 = V_1;
		FSMSystem_AddState_m3680092715(L_7, L_8, __this, /*hidden argument*/NULL);
		int32_t L_9 = V_3;
		V_3 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_10 = V_3;
		FSMStateU5BU5D_t1291452208* L_11 = V_2;
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		MenuState_t2133158756 * L_12 = Component_GetComponentInChildren_TisMenuState_t2133158756_m2656785355(__this, /*hidden argument*/Component_GetComponentInChildren_TisMenuState_t2133158756_m2656785355_MethodInfo_var);
		V_4 = L_12;
		FSMSystem_t2363122359 * L_13 = __this->get_fsm_7();
		MenuState_t2133158756 * L_14 = V_4;
		FSMSystem_SetCurrentState_m2352184253(L_13, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::.ctor()
extern "C"  void DOTweenAnimation__ctor_m3603539837 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation__ctor_m3603539837_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_duration_20((1.0f));
		__this->set_easeType_21(6);
		KeyframeU5BU5D_t449065829* L_0 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		Keyframe_t1449471340  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m2042404667(&L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_0)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(0)))) = L_1;
		KeyframeU5BU5D_t449065829* L_2 = L_0;
		Keyframe_t1449471340  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m2042404667(&L_3, (1.0f), (1.0f), /*hidden argument*/NULL);
		(*(Keyframe_t1449471340 *)((L_2)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(1)))) = L_3;
		AnimationCurve_t3306541151 * L_4 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_4, L_2, /*hidden argument*/NULL);
		__this->set_easeCurve_22(L_4);
		__this->set_loops_24(1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_id_25(L_5);
		__this->set_autoKill_29((bool)1);
		__this->set_isActive_30((bool)1);
		__this->set_autoPlay_36((bool)1);
		Color_t2020392075  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Color__ctor_m1909920690(&L_6, (1.0f), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		__this->set_endValueColor_41(L_6);
		String_t* L_7 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		__this->set_endValueString_42(L_7);
		Rect_t3681755626  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Rect__ctor_m1220545469(&L_8, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->set_endValueRect_43(L_8);
		__this->set__playCount_52((-1));
		ABSAnimationComponent__ctor_m1064982270(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::Awake()
extern "C"  void DOTweenAnimation_Awake_m1720720800 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isActive_30();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		bool L_1 = __this->get_isValid_31();
		if (L_1)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		int32_t L_2 = __this->get_animationType_33();
		if ((!(((uint32_t)L_2) == ((uint32_t)1))))
		{
			goto IL_002e;
		}
	}
	{
		bool L_3 = __this->get_useTargetAsV3_37();
		if (L_3)
		{
			goto IL_003b;
		}
	}

IL_002e:
	{
		DOTweenAnimation_CreateTween_m1462837152(__this, /*hidden argument*/NULL);
		__this->set__tweenCreated_51((bool)1);
	}

IL_003b:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::Start()
extern "C"  void DOTweenAnimation_Start_m4260105581 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__tweenCreated_51();
		if (L_0)
		{
			goto IL_0021;
		}
	}
	{
		bool L_1 = __this->get_isActive_30();
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		bool L_2 = __this->get_isValid_31();
		if (L_2)
		{
			goto IL_0022;
		}
	}

IL_0021:
	{
		return;
	}

IL_0022:
	{
		DOTweenAnimation_CreateTween_m1462837152(__this, /*hidden argument*/NULL);
		__this->set__tweenCreated_51((bool)1);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::OnDestroy()
extern "C"  void DOTweenAnimation_OnDestroy_m4088019078 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		Tween_t278478013 * L_1 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		bool L_2 = TweenExtensions_IsActive_m918693289(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		Tween_t278478013 * L_3 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Kill_m3845739180(NULL /*static, unused*/, L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0027:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18((Tween_t278478013 *)NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::CreateTween()
extern "C"  void DOTweenAnimation_CreateTween_m1462837152 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_CreateTween_m1462837152_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t3349966182 * V_1 = NULL;
	RectTransform_t3349966182 * V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	Transform_t3275118058 * G_B39_0 = NULL;
	DOTweenAnimation_t858634588 * G_B39_1 = NULL;
	Transform_t3275118058 * G_B38_0 = NULL;
	DOTweenAnimation_t858634588 * G_B38_1 = NULL;
	Vector3_t2243707580  G_B40_0;
	memset(&G_B40_0, 0, sizeof(G_B40_0));
	Transform_t3275118058 * G_B40_1 = NULL;
	DOTweenAnimation_t858634588 * G_B40_2 = NULL;
	RectTransform_t3349966182 * G_B44_0 = NULL;
	DOTweenAnimation_t858634588 * G_B44_1 = NULL;
	RectTransform_t3349966182 * G_B43_0 = NULL;
	DOTweenAnimation_t858634588 * G_B43_1 = NULL;
	Vector2_t2243707579  G_B45_0;
	memset(&G_B45_0, 0, sizeof(G_B45_0));
	RectTransform_t3349966182 * G_B45_1 = NULL;
	DOTweenAnimation_t858634588 * G_B45_2 = NULL;
	{
		Component_t3819376471 * L_0 = __this->get_target_32();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t1756533147 * L_2 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_3 = Object_get_name_m2079638459(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral2760277904, L_3, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		return;
	}

IL_0032:
	{
		int32_t L_6 = __this->get_forcedTargetType_35();
		if (!L_6)
		{
			goto IL_0049;
		}
	}
	{
		int32_t L_7 = __this->get_forcedTargetType_35();
		__this->set_targetType_34(L_7);
	}

IL_0049:
	{
		int32_t L_8 = __this->get_targetType_34();
		if (L_8)
		{
			goto IL_006a;
		}
	}
	{
		Component_t3819376471 * L_9 = __this->get_target_32();
		Type_t * L_10 = Object_GetType_m191970594(L_9, /*hidden argument*/NULL);
		int32_t L_11 = DOTweenAnimation_TypeToDOTargetType_m2103550928(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->set_targetType_34(L_11);
	}

IL_006a:
	{
		int32_t L_12 = __this->get_animationType_33();
		V_0 = L_12;
		int32_t L_13 = V_0;
		switch (L_13)
		{
			case 0:
			{
				goto IL_00d4;
			}
			case 1:
			{
				goto IL_00d9;
			}
			case 2:
			{
				goto IL_02d3;
			}
			case 3:
			{
				goto IL_02fb;
			}
			case 4:
			{
				goto IL_03a8;
			}
			case 5:
			{
				goto IL_03d0;
			}
			case 6:
			{
				goto IL_0467;
			}
			case 7:
			{
				goto IL_0571;
			}
			case 8:
			{
				goto IL_06a6;
			}
			case 9:
			{
				goto IL_06fa;
			}
			case 10:
			{
				goto IL_07c2;
			}
			case 11:
			{
				goto IL_0794;
			}
			case 12:
			{
				goto IL_07f0;
			}
			case 13:
			{
				goto IL_08bb;
			}
			case 14:
			{
				goto IL_088c;
			}
			case 15:
			{
				goto IL_08ea;
			}
			case 16:
			{
				goto IL_0911;
			}
			case 17:
			{
				goto IL_0938;
			}
			case 18:
			{
				goto IL_095f;
			}
			case 19:
			{
				goto IL_0986;
			}
			case 20:
			{
				goto IL_09ad;
			}
			case 21:
			{
				goto IL_041e;
			}
		}
	}
	{
		goto IL_09d4;
	}

IL_00d4:
	{
		goto IL_09d4;
	}

IL_00d9:
	{
		bool L_14 = __this->get_useTargetAsV3_37();
		if (!L_14)
		{
			goto IL_01e5;
		}
	}
	{
		__this->set_isRelative_26((bool)0);
		Transform_t3275118058 * L_15 = __this->get_endValueTransform_44();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_012c;
		}
	}
	{
		GameObject_t1756533147 * L_17 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_18 = Object_get_name_m2079638459(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral946533051, L_18, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_20 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, L_19, L_20, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_endValueV3_39(L_21);
		goto IL_01e5;
	}

IL_012c:
	{
		int32_t L_22 = __this->get_targetType_34();
		if ((!(((uint32_t)L_22) == ((uint32_t)5))))
		{
			goto IL_01d4;
		}
	}
	{
		Transform_t3275118058 * L_23 = __this->get_endValueTransform_44();
		V_1 = ((RectTransform_t3349966182 *)IsInstSealed(L_23, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_24 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_25 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_24, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_25)
		{
			goto IL_0180;
		}
	}
	{
		GameObject_t1756533147 * L_26 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_27 = Object_get_name_m2079638459(L_26, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_28 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral4290420597, L_27, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_29 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		Vector3_t2243707580  L_30 = Vector3_get_zero_m1527993324(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_endValueV3_39(L_30);
		goto IL_01cf;
	}

IL_0180:
	{
		Component_t3819376471 * L_31 = __this->get_target_32();
		V_2 = ((RectTransform_t3349966182 *)IsInstSealed(L_31, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		RectTransform_t3349966182 * L_32 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_33 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_32, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_01bd;
		}
	}
	{
		GameObject_t1756533147 * L_34 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_35 = Object_get_name_m2079638459(L_34, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Format_m2024975688(NULL /*static, unused*/, _stringLiteral388623391, L_35, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_37 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogWarning_m1280021602(NULL /*static, unused*/, L_36, L_37, /*hidden argument*/NULL);
		goto IL_01cf;
	}

IL_01bd:
	{
		RectTransform_t3349966182 * L_38 = V_1;
		RectTransform_t3349966182 * L_39 = V_2;
		Vector2_t2243707579  L_40 = DOTweenUtils46_SwitchToRectTransform_m2431809528(NULL /*static, unused*/, L_38, L_39, /*hidden argument*/NULL);
		Vector3_t2243707580  L_41 = Vector2_op_Implicit_m176791411(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		__this->set_endValueV3_39(L_41);
	}

IL_01cf:
	{
		goto IL_01e5;
	}

IL_01d4:
	{
		Transform_t3275118058 * L_42 = __this->get_endValueTransform_44();
		Vector3_t2243707580  L_43 = Transform_get_position_m1104419803(L_42, /*hidden argument*/NULL);
		__this->set_endValueV3_39(L_43);
	}

IL_01e5:
	{
		int32_t L_44 = __this->get_targetType_34();
		V_3 = L_44;
		int32_t L_45 = V_3;
		switch (((int32_t)((int32_t)L_45-(int32_t)5)))
		{
			case 0:
			{
				goto IL_0215;
			}
			case 1:
			{
				goto IL_02ce;
			}
			case 2:
			{
				goto IL_02ce;
			}
			case 3:
			{
				goto IL_02a1;
			}
			case 4:
			{
				goto IL_026f;
			}
			case 5:
			{
				goto IL_02ce;
			}
			case 6:
			{
				goto IL_0242;
			}
		}
	}
	{
		goto IL_02ce;
	}

IL_0215:
	{
		Component_t3819376471 * L_46 = __this->get_target_32();
		Vector3_t2243707580  L_47 = __this->get_endValueV3_39();
		float L_48 = __this->get_duration_20();
		bool L_49 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_50 = ShortcutExtensions46_DOAnchorPos3D_m3279580891(NULL /*static, unused*/, ((RectTransform_t3349966182 *)CastclassSealed(L_46, RectTransform_t3349966182_il2cpp_TypeInfo_var)), L_47, L_48, L_49, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_50);
		goto IL_02ce;
	}

IL_0242:
	{
		Component_t3819376471 * L_51 = __this->get_target_32();
		Vector3_t2243707580  L_52 = __this->get_endValueV3_39();
		float L_53 = __this->get_duration_20();
		bool L_54 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_55 = ShortcutExtensions_DOMove_m813241662(NULL /*static, unused*/, ((Transform_t3275118058 *)CastclassClass(L_51, Transform_t3275118058_il2cpp_TypeInfo_var)), L_52, L_53, L_54, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_55);
		goto IL_02ce;
	}

IL_026f:
	{
		Component_t3819376471 * L_56 = __this->get_target_32();
		Vector3_t2243707580  L_57 = __this->get_endValueV3_39();
		Vector2_t2243707579  L_58 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_57, /*hidden argument*/NULL);
		float L_59 = __this->get_duration_20();
		bool L_60 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_61 = ShortcutExtensions43_DOMove_m620609262(NULL /*static, unused*/, ((Rigidbody2D_t502193897 *)CastclassSealed(L_56, Rigidbody2D_t502193897_il2cpp_TypeInfo_var)), L_58, L_59, L_60, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_61);
		goto IL_02ce;
	}

IL_02a1:
	{
		Component_t3819376471 * L_62 = __this->get_target_32();
		Vector3_t2243707580  L_63 = __this->get_endValueV3_39();
		float L_64 = __this->get_duration_20();
		bool L_65 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_66 = ShortcutExtensions_DOMove_m1933953753(NULL /*static, unused*/, ((Rigidbody_t4233889191 *)CastclassSealed(L_62, Rigidbody_t4233889191_il2cpp_TypeInfo_var)), L_63, L_64, L_65, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_66);
		goto IL_02ce;
	}

IL_02ce:
	{
		goto IL_09d4;
	}

IL_02d3:
	{
		Transform_t3275118058 * L_67 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_68 = __this->get_endValueV3_39();
		float L_69 = __this->get_duration_20();
		bool L_70 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_71 = ShortcutExtensions_DOLocalMove_m1432977613(NULL /*static, unused*/, L_67, L_68, L_69, L_70, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_71);
		goto IL_09d4;
	}

IL_02fb:
	{
		int32_t L_72 = __this->get_targetType_34();
		V_4 = L_72;
		int32_t L_73 = V_4;
		if ((((int32_t)L_73) == ((int32_t)((int32_t)11))))
		{
			goto IL_0322;
		}
	}
	{
		int32_t L_74 = V_4;
		if ((((int32_t)L_74) == ((int32_t)((int32_t)9))))
		{
			goto IL_034f;
		}
	}
	{
		int32_t L_75 = V_4;
		if ((((int32_t)L_75) == ((int32_t)8)))
		{
			goto IL_0376;
		}
	}
	{
		goto IL_03a3;
	}

IL_0322:
	{
		Component_t3819376471 * L_76 = __this->get_target_32();
		Vector3_t2243707580  L_77 = __this->get_endValueV3_39();
		float L_78 = __this->get_duration_20();
		int32_t L_79 = __this->get_optionalRotationMode_48();
		Tweener_t760404022 * L_80 = ShortcutExtensions_DORotate_m3434569207(NULL /*static, unused*/, ((Transform_t3275118058 *)CastclassClass(L_76, Transform_t3275118058_il2cpp_TypeInfo_var)), L_77, L_78, L_79, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_80);
		goto IL_03a3;
	}

IL_034f:
	{
		Component_t3819376471 * L_81 = __this->get_target_32();
		float L_82 = __this->get_endValueFloat_38();
		float L_83 = __this->get_duration_20();
		Tweener_t760404022 * L_84 = ShortcutExtensions43_DORotate_m177144424(NULL /*static, unused*/, ((Rigidbody2D_t502193897 *)CastclassSealed(L_81, Rigidbody2D_t502193897_il2cpp_TypeInfo_var)), L_82, L_83, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_84);
		goto IL_03a3;
	}

IL_0376:
	{
		Component_t3819376471 * L_85 = __this->get_target_32();
		Vector3_t2243707580  L_86 = __this->get_endValueV3_39();
		float L_87 = __this->get_duration_20();
		int32_t L_88 = __this->get_optionalRotationMode_48();
		Tweener_t760404022 * L_89 = ShortcutExtensions_DORotate_m458975576(NULL /*static, unused*/, ((Rigidbody_t4233889191 *)CastclassSealed(L_85, Rigidbody_t4233889191_il2cpp_TypeInfo_var)), L_86, L_87, L_88, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_89);
		goto IL_03a3;
	}

IL_03a3:
	{
		goto IL_09d4;
	}

IL_03a8:
	{
		Transform_t3275118058 * L_90 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_91 = __this->get_endValueV3_39();
		float L_92 = __this->get_duration_20();
		int32_t L_93 = __this->get_optionalRotationMode_48();
		Tweener_t760404022 * L_94 = ShortcutExtensions_DOLocalRotate_m2660994480(NULL /*static, unused*/, L_90, L_91, L_92, L_93, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_94);
		goto IL_09d4;
	}

IL_03d0:
	{
		Transform_t3275118058 * L_95 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		bool L_96 = __this->get_optionalBool0_45();
		G_B38_0 = L_95;
		G_B38_1 = __this;
		if (!L_96)
		{
			G_B39_0 = L_95;
			G_B39_1 = __this;
			goto IL_03fe;
		}
	}
	{
		float L_97 = __this->get_endValueFloat_38();
		float L_98 = __this->get_endValueFloat_38();
		float L_99 = __this->get_endValueFloat_38();
		Vector3_t2243707580  L_100;
		memset(&L_100, 0, sizeof(L_100));
		Vector3__ctor_m2638739322(&L_100, L_97, L_98, L_99, /*hidden argument*/NULL);
		G_B40_0 = L_100;
		G_B40_1 = G_B38_0;
		G_B40_2 = G_B38_1;
		goto IL_0404;
	}

IL_03fe:
	{
		Vector3_t2243707580  L_101 = __this->get_endValueV3_39();
		G_B40_0 = L_101;
		G_B40_1 = G_B39_0;
		G_B40_2 = G_B39_1;
	}

IL_0404:
	{
		float L_102 = __this->get_duration_20();
		Tweener_t760404022 * L_103 = ShortcutExtensions_DOScale_m2754995414(NULL /*static, unused*/, G_B40_1, G_B40_0, L_102, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)G_B40_2)->set_tween_18(L_103);
		goto IL_0419;
	}

IL_0419:
	{
		goto IL_09d4;
	}

IL_041e:
	{
		Component_t3819376471 * L_104 = __this->get_target_32();
		bool L_105 = __this->get_optionalBool0_45();
		G_B43_0 = ((RectTransform_t3349966182 *)CastclassSealed(L_104, RectTransform_t3349966182_il2cpp_TypeInfo_var));
		G_B43_1 = __this;
		if (!L_105)
		{
			G_B44_0 = ((RectTransform_t3349966182 *)CastclassSealed(L_104, RectTransform_t3349966182_il2cpp_TypeInfo_var));
			G_B44_1 = __this;
			goto IL_044b;
		}
	}
	{
		float L_106 = __this->get_endValueFloat_38();
		float L_107 = __this->get_endValueFloat_38();
		Vector2_t2243707579  L_108;
		memset(&L_108, 0, sizeof(L_108));
		Vector2__ctor_m3067419446(&L_108, L_106, L_107, /*hidden argument*/NULL);
		G_B45_0 = L_108;
		G_B45_1 = G_B43_0;
		G_B45_2 = G_B43_1;
		goto IL_0451;
	}

IL_044b:
	{
		Vector2_t2243707579  L_109 = __this->get_endValueV2_40();
		G_B45_0 = L_109;
		G_B45_1 = G_B44_0;
		G_B45_2 = G_B44_1;
	}

IL_0451:
	{
		float L_110 = __this->get_duration_20();
		Tweener_t760404022 * L_111 = ShortcutExtensions46_DOSizeDelta_m2859745733(NULL /*static, unused*/, G_B45_1, G_B45_0, L_110, (bool)0, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)G_B45_2)->set_tween_18(L_111);
		goto IL_09d4;
	}

IL_0467:
	{
		__this->set_isRelative_26((bool)0);
		int32_t L_112 = __this->get_targetType_34();
		V_5 = L_112;
		int32_t L_113 = V_5;
		switch (((int32_t)((int32_t)L_113-(int32_t)3)))
		{
			case 0:
			{
				goto IL_04f7;
			}
			case 1:
			{
				goto IL_0545;
			}
			case 2:
			{
				goto IL_056c;
			}
			case 3:
			{
				goto IL_04cb;
			}
			case 4:
			{
				goto IL_04a4;
			}
			case 5:
			{
				goto IL_056c;
			}
			case 6:
			{
				goto IL_056c;
			}
			case 7:
			{
				goto IL_051e;
			}
		}
	}
	{
		goto IL_056c;
	}

IL_04a4:
	{
		Component_t3819376471 * L_114 = __this->get_target_32();
		Color_t2020392075  L_115 = __this->get_endValueColor_41();
		float L_116 = __this->get_duration_20();
		Tweener_t760404022 * L_117 = ShortcutExtensions43_DOColor_m243415068(NULL /*static, unused*/, ((SpriteRenderer_t1209076198 *)CastclassSealed(L_114, SpriteRenderer_t1209076198_il2cpp_TypeInfo_var)), L_115, L_116, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_117);
		goto IL_056c;
	}

IL_04cb:
	{
		Component_t3819376471 * L_118 = __this->get_target_32();
		Material_t193706927 * L_119 = Renderer_get_material_m2553789785(((Renderer_t257310565 *)CastclassClass(L_118, Renderer_t257310565_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		Color_t2020392075  L_120 = __this->get_endValueColor_41();
		float L_121 = __this->get_duration_20();
		Tweener_t760404022 * L_122 = ShortcutExtensions_DOColor_m1029787631(NULL /*static, unused*/, L_119, L_120, L_121, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_122);
		goto IL_056c;
	}

IL_04f7:
	{
		Component_t3819376471 * L_123 = __this->get_target_32();
		Color_t2020392075  L_124 = __this->get_endValueColor_41();
		float L_125 = __this->get_duration_20();
		Tweener_t760404022 * L_126 = ShortcutExtensions46_DOColor_m3476021741(NULL /*static, unused*/, ((Image_t2042527209 *)CastclassClass(L_123, Image_t2042527209_il2cpp_TypeInfo_var)), L_124, L_125, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_126);
		goto IL_056c;
	}

IL_051e:
	{
		Component_t3819376471 * L_127 = __this->get_target_32();
		Color_t2020392075  L_128 = __this->get_endValueColor_41();
		float L_129 = __this->get_duration_20();
		Tweener_t760404022 * L_130 = ShortcutExtensions46_DOColor_m4265141523(NULL /*static, unused*/, ((Text_t356221433 *)CastclassClass(L_127, Text_t356221433_il2cpp_TypeInfo_var)), L_128, L_129, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_130);
		goto IL_056c;
	}

IL_0545:
	{
		Component_t3819376471 * L_131 = __this->get_target_32();
		Color_t2020392075  L_132 = __this->get_endValueColor_41();
		float L_133 = __this->get_duration_20();
		Tweener_t760404022 * L_134 = ShortcutExtensions_DOColor_m2540950828(NULL /*static, unused*/, ((Light_t494725636 *)CastclassSealed(L_131, Light_t494725636_il2cpp_TypeInfo_var)), L_132, L_133, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_134);
		goto IL_056c;
	}

IL_056c:
	{
		goto IL_09d4;
	}

IL_0571:
	{
		__this->set_isRelative_26((bool)0);
		int32_t L_135 = __this->get_targetType_34();
		V_6 = L_135;
		int32_t L_136 = V_6;
		switch (((int32_t)((int32_t)L_136-(int32_t)2)))
		{
			case 0:
			{
				goto IL_067a;
			}
			case 1:
			{
				goto IL_0605;
			}
			case 2:
			{
				goto IL_0653;
			}
			case 3:
			{
				goto IL_06a1;
			}
			case 4:
			{
				goto IL_05d9;
			}
			case 5:
			{
				goto IL_05b2;
			}
			case 6:
			{
				goto IL_06a1;
			}
			case 7:
			{
				goto IL_06a1;
			}
			case 8:
			{
				goto IL_062c;
			}
		}
	}
	{
		goto IL_06a1;
	}

IL_05b2:
	{
		Component_t3819376471 * L_137 = __this->get_target_32();
		float L_138 = __this->get_endValueFloat_38();
		float L_139 = __this->get_duration_20();
		Tweener_t760404022 * L_140 = ShortcutExtensions43_DOFade_m996156756(NULL /*static, unused*/, ((SpriteRenderer_t1209076198 *)CastclassSealed(L_137, SpriteRenderer_t1209076198_il2cpp_TypeInfo_var)), L_138, L_139, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_140);
		goto IL_06a1;
	}

IL_05d9:
	{
		Component_t3819376471 * L_141 = __this->get_target_32();
		Material_t193706927 * L_142 = Renderer_get_material_m2553789785(((Renderer_t257310565 *)CastclassClass(L_141, Renderer_t257310565_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		float L_143 = __this->get_endValueFloat_38();
		float L_144 = __this->get_duration_20();
		Tweener_t760404022 * L_145 = ShortcutExtensions_DOFade_m2221070795(NULL /*static, unused*/, L_142, L_143, L_144, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_145);
		goto IL_06a1;
	}

IL_0605:
	{
		Component_t3819376471 * L_146 = __this->get_target_32();
		float L_147 = __this->get_endValueFloat_38();
		float L_148 = __this->get_duration_20();
		Tweener_t760404022 * L_149 = ShortcutExtensions46_DOFade_m2221289113(NULL /*static, unused*/, ((Image_t2042527209 *)CastclassClass(L_146, Image_t2042527209_il2cpp_TypeInfo_var)), L_147, L_148, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_149);
		goto IL_06a1;
	}

IL_062c:
	{
		Component_t3819376471 * L_150 = __this->get_target_32();
		float L_151 = __this->get_endValueFloat_38();
		float L_152 = __this->get_duration_20();
		Tweener_t760404022 * L_153 = ShortcutExtensions46_DOFade_m2794982395(NULL /*static, unused*/, ((Text_t356221433 *)CastclassClass(L_150, Text_t356221433_il2cpp_TypeInfo_var)), L_151, L_152, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_153);
		goto IL_06a1;
	}

IL_0653:
	{
		Component_t3819376471 * L_154 = __this->get_target_32();
		float L_155 = __this->get_endValueFloat_38();
		float L_156 = __this->get_duration_20();
		Tweener_t760404022 * L_157 = ShortcutExtensions_DOIntensity_m2263101207(NULL /*static, unused*/, ((Light_t494725636 *)CastclassSealed(L_154, Light_t494725636_il2cpp_TypeInfo_var)), L_155, L_156, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_157);
		goto IL_06a1;
	}

IL_067a:
	{
		Component_t3819376471 * L_158 = __this->get_target_32();
		float L_159 = __this->get_endValueFloat_38();
		float L_160 = __this->get_duration_20();
		Tweener_t760404022 * L_161 = ShortcutExtensions46_DOFade_m70814363(NULL /*static, unused*/, ((CanvasGroup_t3296560743 *)CastclassSealed(L_158, CanvasGroup_t3296560743_il2cpp_TypeInfo_var)), L_159, L_160, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_161);
		goto IL_06a1;
	}

IL_06a1:
	{
		goto IL_09d4;
	}

IL_06a6:
	{
		int32_t L_162 = __this->get_targetType_34();
		V_7 = L_162;
		int32_t L_163 = V_7;
		if ((((int32_t)L_163) == ((int32_t)((int32_t)10))))
		{
			goto IL_06bc;
		}
	}
	{
		goto IL_06f5;
	}

IL_06bc:
	{
		Component_t3819376471 * L_164 = __this->get_target_32();
		String_t* L_165 = __this->get_endValueString_42();
		float L_166 = __this->get_duration_20();
		bool L_167 = __this->get_optionalBool0_45();
		int32_t L_168 = __this->get_optionalScrambleMode_49();
		String_t* L_169 = __this->get_optionalString_50();
		Tweener_t760404022 * L_170 = ShortcutExtensions46_DOText_m2936740990(NULL /*static, unused*/, ((Text_t356221433 *)CastclassClass(L_164, Text_t356221433_il2cpp_TypeInfo_var)), L_165, L_166, L_167, L_168, L_169, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_170);
		goto IL_06f5;
	}

IL_06f5:
	{
		goto IL_09d4;
	}

IL_06fa:
	{
		int32_t L_171 = __this->get_targetType_34();
		V_8 = L_171;
		int32_t L_172 = V_8;
		if ((((int32_t)L_172) == ((int32_t)5)))
		{
			goto IL_0718;
		}
	}
	{
		int32_t L_173 = V_8;
		if ((((int32_t)L_173) == ((int32_t)((int32_t)11))))
		{
			goto IL_0756;
		}
	}
	{
		goto IL_078f;
	}

IL_0718:
	{
		Component_t3819376471 * L_174 = __this->get_target_32();
		Vector3_t2243707580  L_175 = __this->get_endValueV3_39();
		Vector2_t2243707579  L_176 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_175, /*hidden argument*/NULL);
		float L_177 = __this->get_duration_20();
		int32_t L_178 = __this->get_optionalInt0_47();
		float L_179 = __this->get_optionalFloat0_46();
		bool L_180 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_181 = ShortcutExtensions46_DOPunchAnchorPos_m1987664933(NULL /*static, unused*/, ((RectTransform_t3349966182 *)CastclassSealed(L_174, RectTransform_t3349966182_il2cpp_TypeInfo_var)), L_176, L_177, L_178, L_179, L_180, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_181);
		goto IL_078f;
	}

IL_0756:
	{
		Component_t3819376471 * L_182 = __this->get_target_32();
		Vector3_t2243707580  L_183 = __this->get_endValueV3_39();
		float L_184 = __this->get_duration_20();
		int32_t L_185 = __this->get_optionalInt0_47();
		float L_186 = __this->get_optionalFloat0_46();
		bool L_187 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_188 = ShortcutExtensions_DOPunchPosition_m4022188990(NULL /*static, unused*/, ((Transform_t3275118058 *)CastclassClass(L_182, Transform_t3275118058_il2cpp_TypeInfo_var)), L_183, L_184, L_185, L_186, L_187, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_188);
		goto IL_078f;
	}

IL_078f:
	{
		goto IL_09d4;
	}

IL_0794:
	{
		Transform_t3275118058 * L_189 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_190 = __this->get_endValueV3_39();
		float L_191 = __this->get_duration_20();
		int32_t L_192 = __this->get_optionalInt0_47();
		float L_193 = __this->get_optionalFloat0_46();
		Tweener_t760404022 * L_194 = ShortcutExtensions_DOPunchScale_m3609052310(NULL /*static, unused*/, L_189, L_190, L_191, L_192, L_193, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_194);
		goto IL_09d4;
	}

IL_07c2:
	{
		Transform_t3275118058 * L_195 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_196 = __this->get_endValueV3_39();
		float L_197 = __this->get_duration_20();
		int32_t L_198 = __this->get_optionalInt0_47();
		float L_199 = __this->get_optionalFloat0_46();
		Tweener_t760404022 * L_200 = ShortcutExtensions_DOPunchRotation_m1103244634(NULL /*static, unused*/, L_195, L_196, L_197, L_198, L_199, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_200);
		goto IL_09d4;
	}

IL_07f0:
	{
		int32_t L_201 = __this->get_targetType_34();
		V_9 = L_201;
		int32_t L_202 = V_9;
		if ((((int32_t)L_202) == ((int32_t)5)))
		{
			goto IL_080e;
		}
	}
	{
		int32_t L_203 = V_9;
		if ((((int32_t)L_203) == ((int32_t)((int32_t)11))))
		{
			goto IL_084d;
		}
	}
	{
		goto IL_0887;
	}

IL_080e:
	{
		Component_t3819376471 * L_204 = __this->get_target_32();
		float L_205 = __this->get_duration_20();
		Vector3_t2243707580  L_206 = __this->get_endValueV3_39();
		Vector2_t2243707579  L_207 = Vector2_op_Implicit_m1064335535(NULL /*static, unused*/, L_206, /*hidden argument*/NULL);
		int32_t L_208 = __this->get_optionalInt0_47();
		float L_209 = __this->get_optionalFloat0_46();
		bool L_210 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_211 = ShortcutExtensions46_DOShakeAnchorPos_m1105810756(NULL /*static, unused*/, ((RectTransform_t3349966182 *)CastclassSealed(L_204, RectTransform_t3349966182_il2cpp_TypeInfo_var)), L_205, L_207, L_208, L_209, L_210, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_211);
		goto IL_0887;
	}

IL_084d:
	{
		Component_t3819376471 * L_212 = __this->get_target_32();
		float L_213 = __this->get_duration_20();
		Vector3_t2243707580  L_214 = __this->get_endValueV3_39();
		int32_t L_215 = __this->get_optionalInt0_47();
		float L_216 = __this->get_optionalFloat0_46();
		bool L_217 = __this->get_optionalBool0_45();
		Tweener_t760404022 * L_218 = ShortcutExtensions_DOShakePosition_m2989795727(NULL /*static, unused*/, ((Transform_t3275118058 *)CastclassClass(L_212, Transform_t3275118058_il2cpp_TypeInfo_var)), L_213, L_214, L_215, L_216, L_217, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_218);
		goto IL_0887;
	}

IL_0887:
	{
		goto IL_09d4;
	}

IL_088c:
	{
		Transform_t3275118058 * L_219 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_220 = __this->get_duration_20();
		Vector3_t2243707580  L_221 = __this->get_endValueV3_39();
		int32_t L_222 = __this->get_optionalInt0_47();
		float L_223 = __this->get_optionalFloat0_46();
		Tweener_t760404022 * L_224 = ShortcutExtensions_DOShakeScale_m2106531945(NULL /*static, unused*/, L_219, L_220, L_221, L_222, L_223, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_224);
		goto IL_09d4;
	}

IL_08bb:
	{
		Transform_t3275118058 * L_225 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		float L_226 = __this->get_duration_20();
		Vector3_t2243707580  L_227 = __this->get_endValueV3_39();
		int32_t L_228 = __this->get_optionalInt0_47();
		float L_229 = __this->get_optionalFloat0_46();
		Tweener_t760404022 * L_230 = ShortcutExtensions_DOShakeRotation_m3190170693(NULL /*static, unused*/, L_225, L_226, L_227, L_228, L_229, (bool)1, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_230);
		goto IL_09d4;
	}

IL_08ea:
	{
		Component_t3819376471 * L_231 = __this->get_target_32();
		float L_232 = __this->get_endValueFloat_38();
		float L_233 = __this->get_duration_20();
		Tweener_t760404022 * L_234 = ShortcutExtensions_DOAspect_m4263503343(NULL /*static, unused*/, ((Camera_t189460977 *)CastclassSealed(L_231, Camera_t189460977_il2cpp_TypeInfo_var)), L_232, L_233, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_234);
		goto IL_09d4;
	}

IL_0911:
	{
		Component_t3819376471 * L_235 = __this->get_target_32();
		Color_t2020392075  L_236 = __this->get_endValueColor_41();
		float L_237 = __this->get_duration_20();
		Tweener_t760404022 * L_238 = ShortcutExtensions_DOColor_m4070360217(NULL /*static, unused*/, ((Camera_t189460977 *)CastclassSealed(L_235, Camera_t189460977_il2cpp_TypeInfo_var)), L_236, L_237, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_238);
		goto IL_09d4;
	}

IL_0938:
	{
		Component_t3819376471 * L_239 = __this->get_target_32();
		float L_240 = __this->get_endValueFloat_38();
		float L_241 = __this->get_duration_20();
		Tweener_t760404022 * L_242 = ShortcutExtensions_DOFieldOfView_m2729074193(NULL /*static, unused*/, ((Camera_t189460977 *)CastclassSealed(L_239, Camera_t189460977_il2cpp_TypeInfo_var)), L_240, L_241, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_242);
		goto IL_09d4;
	}

IL_095f:
	{
		Component_t3819376471 * L_243 = __this->get_target_32();
		float L_244 = __this->get_endValueFloat_38();
		float L_245 = __this->get_duration_20();
		Tweener_t760404022 * L_246 = ShortcutExtensions_DOOrthoSize_m3058874688(NULL /*static, unused*/, ((Camera_t189460977 *)CastclassSealed(L_243, Camera_t189460977_il2cpp_TypeInfo_var)), L_244, L_245, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_246);
		goto IL_09d4;
	}

IL_0986:
	{
		Component_t3819376471 * L_247 = __this->get_target_32();
		Rect_t3681755626  L_248 = __this->get_endValueRect_43();
		float L_249 = __this->get_duration_20();
		Tweener_t760404022 * L_250 = ShortcutExtensions_DOPixelRect_m1451235077(NULL /*static, unused*/, ((Camera_t189460977 *)CastclassSealed(L_247, Camera_t189460977_il2cpp_TypeInfo_var)), L_248, L_249, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_250);
		goto IL_09d4;
	}

IL_09ad:
	{
		Component_t3819376471 * L_251 = __this->get_target_32();
		Rect_t3681755626  L_252 = __this->get_endValueRect_43();
		float L_253 = __this->get_duration_20();
		Tweener_t760404022 * L_254 = ShortcutExtensions_DORect_m1672543897(NULL /*static, unused*/, ((Camera_t189460977 *)CastclassSealed(L_251, Camera_t189460977_il2cpp_TypeInfo_var)), L_252, L_253, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_254);
		goto IL_09d4;
	}

IL_09d4:
	{
		Tween_t278478013 * L_255 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (L_255)
		{
			goto IL_09e0;
		}
	}
	{
		return;
	}

IL_09e0:
	{
		bool L_256 = __this->get_isFrom_27();
		if (!L_256)
		{
			goto IL_0a07;
		}
	}
	{
		Tween_t278478013 * L_257 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		bool L_258 = __this->get_isRelative_26();
		TweenSettingsExtensions_From_TisTweener_t760404022_m151127719(NULL /*static, unused*/, ((Tweener_t760404022 *)CastclassClass(L_257, Tweener_t760404022_il2cpp_TypeInfo_var)), L_258, /*hidden argument*/TweenSettingsExtensions_From_TisTweener_t760404022_m151127719_MethodInfo_var);
		goto IL_0a19;
	}

IL_0a07:
	{
		Tween_t278478013 * L_259 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		bool L_260 = __this->get_isRelative_26();
		TweenSettingsExtensions_SetRelative_TisTween_t278478013_m4137017946(NULL /*static, unused*/, L_259, L_260, /*hidden argument*/TweenSettingsExtensions_SetRelative_TisTween_t278478013_m4137017946_MethodInfo_var);
	}

IL_0a19:
	{
		Tween_t278478013 * L_261 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		GameObject_t1756533147 * L_262 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		Tween_t278478013 * L_263 = TweenSettingsExtensions_SetTarget_TisTween_t278478013_m1418126220(NULL /*static, unused*/, L_261, L_262, /*hidden argument*/TweenSettingsExtensions_SetTarget_TisTween_t278478013_m1418126220_MethodInfo_var);
		float L_264 = __this->get_delay_19();
		Tween_t278478013 * L_265 = TweenSettingsExtensions_SetDelay_TisTween_t278478013_m986297435(NULL /*static, unused*/, L_263, L_264, /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTween_t278478013_m986297435_MethodInfo_var);
		int32_t L_266 = __this->get_loops_24();
		int32_t L_267 = __this->get_loopType_23();
		Tween_t278478013 * L_268 = TweenSettingsExtensions_SetLoops_TisTween_t278478013_m618797951(NULL /*static, unused*/, L_265, L_266, L_267, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTween_t278478013_m618797951_MethodInfo_var);
		bool L_269 = __this->get_autoKill_29();
		Tween_t278478013 * L_270 = TweenSettingsExtensions_SetAutoKill_TisTween_t278478013_m4205239577(NULL /*static, unused*/, L_268, L_269, /*hidden argument*/TweenSettingsExtensions_SetAutoKill_TisTween_t278478013_m4205239577_MethodInfo_var);
		IntPtr_t L_271;
		L_271.set_m_value_0((void*)(void*)DOTweenAnimation_U3CCreateTweenU3Em__0_m657452871_MethodInfo_var);
		TweenCallback_t3697142134 * L_272 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_272, __this, L_271, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnKill_TisTween_t278478013_m2880655110(NULL /*static, unused*/, L_270, L_272, /*hidden argument*/TweenSettingsExtensions_OnKill_TisTween_t278478013_m2880655110_MethodInfo_var);
		bool L_273 = ((ABSAnimationComponent_t2205594551 *)__this)->get_isSpeedBased_3();
		if (!L_273)
		{
			goto IL_0a7a;
		}
	}
	{
		Tween_t278478013 * L_274 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenSettingsExtensions_SetSpeedBased_TisTween_t278478013_m783493617(NULL /*static, unused*/, L_274, /*hidden argument*/TweenSettingsExtensions_SetSpeedBased_TisTween_t278478013_m783493617_MethodInfo_var);
	}

IL_0a7a:
	{
		int32_t L_275 = __this->get_easeType_21();
		if ((!(((uint32_t)L_275) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_0a9e;
		}
	}
	{
		Tween_t278478013 * L_276 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		AnimationCurve_t3306541151 * L_277 = __this->get_easeCurve_22();
		TweenSettingsExtensions_SetEase_TisTween_t278478013_m2274347319(NULL /*static, unused*/, L_276, L_277, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTween_t278478013_m2274347319_MethodInfo_var);
		goto IL_0ab0;
	}

IL_0a9e:
	{
		Tween_t278478013 * L_278 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		int32_t L_279 = __this->get_easeType_21();
		TweenSettingsExtensions_SetEase_TisTween_t278478013_m3479639913(NULL /*static, unused*/, L_278, L_279, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTween_t278478013_m3479639913_MethodInfo_var);
	}

IL_0ab0:
	{
		String_t* L_280 = __this->get_id_25();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_281 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_280, /*hidden argument*/NULL);
		if (L_281)
		{
			goto IL_0ad2;
		}
	}
	{
		Tween_t278478013 * L_282 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		String_t* L_283 = __this->get_id_25();
		TweenSettingsExtensions_SetId_TisTween_t278478013_m3480976796(NULL /*static, unused*/, L_282, L_283, /*hidden argument*/TweenSettingsExtensions_SetId_TisTween_t278478013_m3480976796_MethodInfo_var);
	}

IL_0ad2:
	{
		Tween_t278478013 * L_284 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		bool L_285 = __this->get_isIndependentUpdate_28();
		TweenSettingsExtensions_SetUpdate_TisTween_t278478013_m696735837(NULL /*static, unused*/, L_284, L_285, /*hidden argument*/TweenSettingsExtensions_SetUpdate_TisTween_t278478013_m696735837_MethodInfo_var);
		bool L_286 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnStart_4();
		if (!L_286)
		{
			goto IL_0b1c;
		}
	}
	{
		UnityEvent_t408735097 * L_287 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStart_11();
		if (!L_287)
		{
			goto IL_0b17;
		}
	}
	{
		Tween_t278478013 * L_288 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		UnityEvent_t408735097 * L_289 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStart_11();
		IntPtr_t L_290;
		L_290.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_291 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_291, L_289, L_290, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnStart_TisTween_t278478013_m3936654790(NULL /*static, unused*/, L_288, L_291, /*hidden argument*/TweenSettingsExtensions_OnStart_TisTween_t278478013_m3936654790_MethodInfo_var);
	}

IL_0b17:
	{
		goto IL_0b23;
	}

IL_0b1c:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onStart_11((UnityEvent_t408735097 *)NULL);
	}

IL_0b23:
	{
		bool L_292 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnPlay_5();
		if (!L_292)
		{
			goto IL_0b5b;
		}
	}
	{
		UnityEvent_t408735097 * L_293 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onPlay_12();
		if (!L_293)
		{
			goto IL_0b56;
		}
	}
	{
		Tween_t278478013 * L_294 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		UnityEvent_t408735097 * L_295 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onPlay_12();
		IntPtr_t L_296;
		L_296.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_297 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_297, L_295, L_296, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnPlay_TisTween_t278478013_m1768194704(NULL /*static, unused*/, L_294, L_297, /*hidden argument*/TweenSettingsExtensions_OnPlay_TisTween_t278478013_m1768194704_MethodInfo_var);
	}

IL_0b56:
	{
		goto IL_0b62;
	}

IL_0b5b:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onPlay_12((UnityEvent_t408735097 *)NULL);
	}

IL_0b62:
	{
		bool L_298 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnUpdate_6();
		if (!L_298)
		{
			goto IL_0b9a;
		}
	}
	{
		UnityEvent_t408735097 * L_299 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onUpdate_13();
		if (!L_299)
		{
			goto IL_0b95;
		}
	}
	{
		Tween_t278478013 * L_300 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		UnityEvent_t408735097 * L_301 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onUpdate_13();
		IntPtr_t L_302;
		L_302.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_303 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_303, L_301, L_302, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnUpdate_TisTween_t278478013_m4256285963(NULL /*static, unused*/, L_300, L_303, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisTween_t278478013_m4256285963_MethodInfo_var);
	}

IL_0b95:
	{
		goto IL_0ba1;
	}

IL_0b9a:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onUpdate_13((UnityEvent_t408735097 *)NULL);
	}

IL_0ba1:
	{
		bool L_304 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnStepComplete_7();
		if (!L_304)
		{
			goto IL_0bd9;
		}
	}
	{
		UnityEvent_t408735097 * L_305 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStepComplete_14();
		if (!L_305)
		{
			goto IL_0bd4;
		}
	}
	{
		Tween_t278478013 * L_306 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		UnityEvent_t408735097 * L_307 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStepComplete_14();
		IntPtr_t L_308;
		L_308.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_309 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_309, L_307, L_308, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnStepComplete_TisTween_t278478013_m367519805(NULL /*static, unused*/, L_306, L_309, /*hidden argument*/TweenSettingsExtensions_OnStepComplete_TisTween_t278478013_m367519805_MethodInfo_var);
	}

IL_0bd4:
	{
		goto IL_0be0;
	}

IL_0bd9:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onStepComplete_14((UnityEvent_t408735097 *)NULL);
	}

IL_0be0:
	{
		bool L_310 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnComplete_8();
		if (!L_310)
		{
			goto IL_0c18;
		}
	}
	{
		UnityEvent_t408735097 * L_311 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onComplete_15();
		if (!L_311)
		{
			goto IL_0c13;
		}
	}
	{
		Tween_t278478013 * L_312 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		UnityEvent_t408735097 * L_313 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onComplete_15();
		IntPtr_t L_314;
		L_314.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_315 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_315, L_313, L_314, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTween_t278478013_m1217840999(NULL /*static, unused*/, L_312, L_315, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTween_t278478013_m1217840999_MethodInfo_var);
	}

IL_0c13:
	{
		goto IL_0c1f;
	}

IL_0c18:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onComplete_15((UnityEvent_t408735097 *)NULL);
	}

IL_0c1f:
	{
		bool L_316 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnRewind_10();
		if (!L_316)
		{
			goto IL_0c57;
		}
	}
	{
		UnityEvent_t408735097 * L_317 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onRewind_17();
		if (!L_317)
		{
			goto IL_0c52;
		}
	}
	{
		Tween_t278478013 * L_318 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		UnityEvent_t408735097 * L_319 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onRewind_17();
		IntPtr_t L_320;
		L_320.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_321 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_321, L_319, L_320, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnRewind_TisTween_t278478013_m3534007205(NULL /*static, unused*/, L_318, L_321, /*hidden argument*/TweenSettingsExtensions_OnRewind_TisTween_t278478013_m3534007205_MethodInfo_var);
	}

IL_0c52:
	{
		goto IL_0c5e;
	}

IL_0c57:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onRewind_17((UnityEvent_t408735097 *)NULL);
	}

IL_0c5e:
	{
		bool L_322 = __this->get_autoPlay_36();
		if (!L_322)
		{
			goto IL_0c7a;
		}
	}
	{
		Tween_t278478013 * L_323 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Play_TisTween_t278478013_m2584288383(NULL /*static, unused*/, L_323, /*hidden argument*/TweenExtensions_Play_TisTween_t278478013_m2584288383_MethodInfo_var);
		goto IL_0c86;
	}

IL_0c7a:
	{
		Tween_t278478013 * L_324 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Pause_TisTween_t278478013_m3798767189(NULL /*static, unused*/, L_324, /*hidden argument*/TweenExtensions_Pause_TisTween_t278478013_m3798767189_MethodInfo_var);
	}

IL_0c86:
	{
		bool L_325 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnTweenCreated_9();
		if (!L_325)
		{
			goto IL_0ca7;
		}
	}
	{
		UnityEvent_t408735097 * L_326 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onTweenCreated_16();
		if (!L_326)
		{
			goto IL_0ca7;
		}
	}
	{
		UnityEvent_t408735097 * L_327 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onTweenCreated_16();
		UnityEvent_Invoke_m4163344491(L_327, /*hidden argument*/NULL);
	}

IL_0ca7:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlay()
extern "C"  void DOTweenAnimation_DOPlay_m1488723384 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlay_m1488723384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Play_m164738169(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwards()
extern "C"  void DOTweenAnimation_DOPlayBackwards_m3876039504 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayBackwards_m3876039504_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_PlayBackwards_m2839273377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayForward()
extern "C"  void DOTweenAnimation_DOPlayForward_m1452188533 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayForward_m1452188533_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_PlayForward_m3077912960(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPause()
extern "C"  void DOTweenAnimation_DOPause_m1168707782 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPause_m1168707782_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Pause_m4110474415(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOTogglePause()
extern "C"  void DOTweenAnimation_DOTogglePause_m3112936206 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOTogglePause_m3112936206_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_TogglePause_m1571016433(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORewind()
extern "C"  void DOTweenAnimation_DORewind_m9460949 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DORewind_m9460949_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DOTweenAnimationU5BU5D_t3410086325* V_0 = NULL;
	int32_t V_1 = 0;
	Tween_t278478013 * V_2 = NULL;
	{
		__this->set__playCount_52((-1));
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		DOTweenAnimationU5BU5D_t3410086325* L_1 = GameObject_GetComponents_TisDOTweenAnimation_t858634588_m3627101310(L_0, /*hidden argument*/GameObject_GetComponents_TisDOTweenAnimation_t858634588_m3627101310_MethodInfo_var);
		V_0 = L_1;
		DOTweenAnimationU5BU5D_t3410086325* L_2 = V_0;
		V_1 = ((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))-(int32_t)1));
		goto IL_004a;
	}

IL_001e:
	{
		DOTweenAnimationU5BU5D_t3410086325* L_3 = V_0;
		int32_t L_4 = V_1;
		int32_t L_5 = L_4;
		DOTweenAnimation_t858634588 * L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		Tween_t278478013 * L_7 = ((ABSAnimationComponent_t2205594551 *)L_6)->get_tween_18();
		V_2 = L_7;
		Tween_t278478013 * L_8 = V_2;
		if (!L_8)
		{
			goto IL_0046;
		}
	}
	{
		Tween_t278478013 * L_9 = V_2;
		bool L_10 = TweenExtensions_IsInitialized_m1900413101(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0046;
		}
	}
	{
		DOTweenAnimationU5BU5D_t3410086325* L_11 = V_0;
		int32_t L_12 = V_1;
		int32_t L_13 = L_12;
		DOTweenAnimation_t858634588 * L_14 = (L_11)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_13));
		Tween_t278478013 * L_15 = ((ABSAnimationComponent_t2205594551 *)L_14)->get_tween_18();
		TweenExtensions_Rewind_m3286148139(NULL /*static, unused*/, L_15, (bool)1, /*hidden argument*/NULL);
	}

IL_0046:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16-(int32_t)1));
	}

IL_004a:
	{
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) > ((int32_t)(-1))))
		{
			goto IL_001e;
		}
	}
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORestart(System.Boolean)
extern "C"  void DOTweenAnimation_DORestart_m4058931822 (DOTweenAnimation_t858634588 * __this, bool ___fromHere0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DORestart_m4058931822_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__playCount_52((-1));
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (L_0)
		{
			goto IL_0029;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_0028;
		}
	}
	{
		Tween_t278478013 * L_2 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		Debugger_LogNullTween_m832479276(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}

IL_0029:
	{
		bool L_3 = ___fromHere0;
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		bool L_4 = __this->get_isRelative_26();
		if (!L_4)
		{
			goto IL_0040;
		}
	}
	{
		DOTweenAnimation_ReEvaluateRelativeTween_m314855898(__this, /*hidden argument*/NULL);
	}

IL_0040:
	{
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Restart_m570313680(NULL /*static, unused*/, L_5, (bool)1, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOComplete()
extern "C"  void DOTweenAnimation_DOComplete_m4267256299 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOComplete_m4267256299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Complete_m2005563247(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOKill()
extern "C"  void DOTweenAnimation_DOKill_m1786421626 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOKill_m1786421626_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Kill_m211473084(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18((Tween_t278478013 *)NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayById(System.String)
extern "C"  void DOTweenAnimation_DOPlayById_m13501458 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayById_m13501458_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Play_m32308619(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayAllById(System.String)
extern "C"  void DOTweenAnimation_DOPlayAllById_m682662527 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayAllById_m682662527_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Play_m164738169(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPauseAllById(System.String)
extern "C"  void DOTweenAnimation_DOPauseAllById_m2716110985 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPauseAllById_m2716110985_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Pause_m4110474415(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsById(System.String)
extern "C"  void DOTweenAnimation_DOPlayBackwardsById_m262489098 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayBackwardsById_m262489098_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_PlayBackwards_m544554603(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayBackwardsAllById(System.String)
extern "C"  void DOTweenAnimation_DOPlayBackwardsAllById_m168913475 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayBackwardsAllById_m168913475_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_PlayBackwards_m2839273377(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardById(System.String)
extern "C"  void DOTweenAnimation_DOPlayForwardById_m885204937 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayForwardById_m885204937_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_PlayForward_m1591690666(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayForwardAllById(System.String)
extern "C"  void DOTweenAnimation_DOPlayForwardAllById_m2751131570 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayForwardAllById_m2751131570_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_PlayForward_m3077912960(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DOPlayNext()
extern "C"  void DOTweenAnimation_DOPlayNext_m590559157 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DOPlayNext_m590559157_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DOTweenAnimationU5BU5D_t3410086325* V_0 = NULL;
	DOTweenAnimation_t858634588 * V_1 = NULL;
	{
		DOTweenAnimationU5BU5D_t3410086325* L_0 = Component_GetComponents_TisDOTweenAnimation_t858634588_m1959229896(__this, /*hidden argument*/Component_GetComponents_TisDOTweenAnimation_t858634588_m1959229896_MethodInfo_var);
		V_0 = L_0;
		goto IL_006b;
	}

IL_000c:
	{
		int32_t L_1 = __this->get__playCount_52();
		__this->set__playCount_52(((int32_t)((int32_t)L_1+(int32_t)1)));
		DOTweenAnimationU5BU5D_t3410086325* L_2 = V_0;
		int32_t L_3 = __this->get__playCount_52();
		int32_t L_4 = L_3;
		DOTweenAnimation_t858634588 * L_5 = (L_2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_4));
		V_1 = L_5;
		DOTweenAnimation_t858634588 * L_6 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_7 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_6, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006b;
		}
	}
	{
		DOTweenAnimation_t858634588 * L_8 = V_1;
		Tween_t278478013 * L_9 = ((ABSAnimationComponent_t2205594551 *)L_8)->get_tween_18();
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		DOTweenAnimation_t858634588 * L_10 = V_1;
		Tween_t278478013 * L_11 = ((ABSAnimationComponent_t2205594551 *)L_10)->get_tween_18();
		bool L_12 = TweenExtensions_IsPlaying_m3897946637(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_006b;
		}
	}
	{
		DOTweenAnimation_t858634588 * L_13 = V_1;
		Tween_t278478013 * L_14 = ((ABSAnimationComponent_t2205594551 *)L_13)->get_tween_18();
		bool L_15 = TweenExtensions_IsComplete_m3095330022(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_006b;
		}
	}
	{
		DOTweenAnimation_t858634588 * L_16 = V_1;
		Tween_t278478013 * L_17 = ((ABSAnimationComponent_t2205594551 *)L_16)->get_tween_18();
		TweenExtensions_Play_TisTween_t278478013_m2584288383(NULL /*static, unused*/, L_17, /*hidden argument*/TweenExtensions_Play_TisTween_t278478013_m2584288383_MethodInfo_var);
		goto IL_007b;
	}

IL_006b:
	{
		int32_t L_18 = __this->get__playCount_52();
		DOTweenAnimationU5BU5D_t3410086325* L_19 = V_0;
		if ((((int32_t)L_18) < ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_19)->max_length))))-(int32_t)1)))))
		{
			goto IL_000c;
		}
	}

IL_007b:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORewindAndPlayNext()
extern "C"  void DOTweenAnimation_DORewindAndPlayNext_m473560461 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DORewindAndPlayNext_m473560461_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__playCount_52((-1));
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Rewind_m660085617(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		DOTweenAnimation_DOPlayNext_m590559157(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORestartById(System.String)
extern "C"  void DOTweenAnimation_DORestartById_m2699658521 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DORestartById_m2699658521_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__playCount_52((-1));
		GameObject_t1756533147 * L_0 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		String_t* L_1 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Restart_m590221510(NULL /*static, unused*/, L_0, L_1, (bool)1, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::DORestartAllById(System.String)
extern "C"  void DOTweenAnimation_DORestartAllById_m1200781746 (DOTweenAnimation_t858634588 * __this, String_t* ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_DORestartAllById_m1200781746_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set__playCount_52((-1));
		String_t* L_0 = ___id0;
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		DOTween_Restart_m570313680(NULL /*static, unused*/, L_0, (bool)1, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<DG.Tweening.Tween> DG.Tweening.DOTweenAnimation::GetTweens()
extern "C"  List_1_t3942566441 * DOTweenAnimation_GetTweens_m2149779259 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_GetTweens_m2149779259_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3942566441 * V_0 = NULL;
	DOTweenAnimationU5BU5D_t3410086325* V_1 = NULL;
	DOTweenAnimation_t858634588 * V_2 = NULL;
	DOTweenAnimationU5BU5D_t3410086325* V_3 = NULL;
	int32_t V_4 = 0;
	{
		List_1_t3942566441 * L_0 = (List_1_t3942566441 *)il2cpp_codegen_object_new(List_1_t3942566441_il2cpp_TypeInfo_var);
		List_1__ctor_m1955107610(L_0, /*hidden argument*/List_1__ctor_m1955107610_MethodInfo_var);
		V_0 = L_0;
		DOTweenAnimationU5BU5D_t3410086325* L_1 = Component_GetComponents_TisDOTweenAnimation_t858634588_m1959229896(__this, /*hidden argument*/Component_GetComponents_TisDOTweenAnimation_t858634588_m1959229896_MethodInfo_var);
		V_1 = L_1;
		DOTweenAnimationU5BU5D_t3410086325* L_2 = V_1;
		V_3 = L_2;
		V_4 = 0;
		goto IL_002e;
	}

IL_0017:
	{
		DOTweenAnimationU5BU5D_t3410086325* L_3 = V_3;
		int32_t L_4 = V_4;
		int32_t L_5 = L_4;
		DOTweenAnimation_t858634588 * L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		V_2 = L_6;
		List_1_t3942566441 * L_7 = V_0;
		DOTweenAnimation_t858634588 * L_8 = V_2;
		Tween_t278478013 * L_9 = ((ABSAnimationComponent_t2205594551 *)L_8)->get_tween_18();
		List_1_Add_m2823288694(L_7, L_9, /*hidden argument*/List_1_Add_m2823288694_MethodInfo_var);
		int32_t L_10 = V_4;
		V_4 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_002e:
	{
		int32_t L_11 = V_4;
		DOTweenAnimationU5BU5D_t3410086325* L_12 = V_3;
		if ((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_12)->max_length)))))))
		{
			goto IL_0017;
		}
	}
	{
		List_1_t3942566441 * L_13 = V_0;
		return L_13;
	}
}
// DG.Tweening.Core.TargetType DG.Tweening.DOTweenAnimation::TypeToDOTargetType(System.Type)
extern "C"  int32_t DOTweenAnimation_TypeToDOTargetType_m2103550928 (Il2CppObject * __this /* static, unused */, Type_t * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_TypeToDOTargetType_m2103550928_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	{
		Type_t * L_0 = ___t0;
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		V_0 = L_1;
		String_t* L_2 = V_0;
		int32_t L_3 = String_LastIndexOf_m1975817115(L_2, _stringLiteral372029316, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = V_1;
		if ((((int32_t)L_4) == ((int32_t)(-1))))
		{
			goto IL_0024;
		}
	}
	{
		String_t* L_5 = V_0;
		int32_t L_6 = V_1;
		String_t* L_7 = String_Substring_m2032624251(L_5, ((int32_t)((int32_t)L_6+(int32_t)1)), /*hidden argument*/NULL);
		V_0 = L_7;
	}

IL_0024:
	{
		String_t* L_8 = V_0;
		int32_t L_9 = String_IndexOf_m4251815737(L_8, _stringLiteral3585144781, /*hidden argument*/NULL);
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_004b;
		}
	}
	{
		String_t* L_10 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_10, _stringLiteral438863844, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = _stringLiteral3585144781;
	}

IL_004b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_12 = Type_GetTypeFromHandle_m432505302(NULL /*static, unused*/, LoadTypeToken(TargetType_t2706200073_0_0_0_var), /*hidden argument*/NULL);
		String_t* L_13 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t2459695545_il2cpp_TypeInfo_var);
		Il2CppObject * L_14 = Enum_Parse_m2561000069(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		return ((*(int32_t*)((int32_t*)UnBox(L_14, TargetType_t2706200073_il2cpp_TypeInfo_var))));
	}
}
// System.Void DG.Tweening.DOTweenAnimation::ReEvaluateRelativeTween()
extern "C"  void DOTweenAnimation_ReEvaluateRelativeTween_m314855898 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenAnimation_ReEvaluateRelativeTween_m314855898_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_animationType_33();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_003e;
		}
	}
	{
		Tween_t278478013 * L_1 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_3 = Transform_get_position_m1104419803(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = __this->get_endValueV3_39();
		Vector3_t2243707580  L_5 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t2243707580  L_6 = L_5;
		Il2CppObject * L_7 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_6);
		VirtFuncInvoker2< Tweener_t760404022 *, Il2CppObject *, bool >::Invoke(11 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Boolean) */, ((Tweener_t760404022 *)CastclassClass(L_1, Tweener_t760404022_il2cpp_TypeInfo_var)), L_7, (bool)1);
		goto IL_0077;
	}

IL_003e:
	{
		int32_t L_8 = __this->get_animationType_33();
		if ((!(((uint32_t)L_8) == ((uint32_t)2))))
		{
			goto IL_0077;
		}
	}
	{
		Tween_t278478013 * L_9 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		Transform_t3275118058 * L_10 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_11 = Transform_get_localPosition_m2533925116(L_10, /*hidden argument*/NULL);
		Vector3_t2243707580  L_12 = __this->get_endValueV3_39();
		Vector3_t2243707580  L_13 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_11, L_12, /*hidden argument*/NULL);
		Vector3_t2243707580  L_14 = L_13;
		Il2CppObject * L_15 = Box(Vector3_t2243707580_il2cpp_TypeInfo_var, &L_14);
		VirtFuncInvoker2< Tweener_t760404022 *, Il2CppObject *, bool >::Invoke(11 /* DG.Tweening.Tweener DG.Tweening.Tweener::ChangeEndValue(System.Object,System.Boolean) */, ((Tweener_t760404022 *)CastclassClass(L_9, Tweener_t760404022_il2cpp_TypeInfo_var)), L_15, (bool)1);
	}

IL_0077:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenAnimation::<CreateTween>m__0()
extern "C"  void DOTweenAnimation_U3CCreateTweenU3Em__0_m657452871 (DOTweenAnimation_t858634588 * __this, const MethodInfo* method)
{
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18((Tween_t278478013 *)NULL);
		return;
	}
}
// System.Void FSMState::.ctor()
extern "C"  void FSMState__ctor_m2907867150 (FSMState_t3109806909 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMState__ctor_m2907867150_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1643289790 * L_0 = (Dictionary_2_t1643289790 *)il2cpp_codegen_object_new(Dictionary_2_t1643289790_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m2199854661(L_0, /*hidden argument*/Dictionary_2__ctor_m2199854661_MethodInfo_var);
		__this->set_map_4(L_0);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FSMState::set_Controller(Controller)
extern "C"  void FSMState_set_Controller_m2162207389 (FSMState_t3109806909 * __this, Controller_t1937198888 * ___value0, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ___value0;
		__this->set_controller_2(L_0);
		return;
	}
}
// System.Void FSMState::set_FSM(FSMSystem)
extern "C"  void FSMState_set_FSM_m3108072358 (FSMState_t3109806909 * __this, FSMSystem_t2363122359 * ___value0, const MethodInfo* method)
{
	{
		FSMSystem_t2363122359 * L_0 = ___value0;
		__this->set_fsm_3(L_0);
		return;
	}
}
// StateID FSMState::get_ID()
extern "C"  int32_t FSMState_get_ID_m3972705597 (FSMState_t3109806909 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_stateID_5();
		return L_0;
	}
}
// System.Void FSMState::AddTransition(Transition,StateID)
extern "C"  void FSMState_AddTransition_m3341888567 (FSMState_t3109806909 * __this, int32_t ___trans0, int32_t ___id1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMState_AddTransition_m3341888567_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___trans0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2792294009, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		int32_t L_1 = ___id1;
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral2764825468, /*hidden argument*/NULL);
		return;
	}

IL_0022:
	{
		Dictionary_2_t1643289790 * L_2 = __this->get_map_4();
		int32_t L_3 = ___trans0;
		bool L_4 = Dictionary_2_ContainsKey_m1345547490(L_2, L_3, /*hidden argument*/Dictionary_2_ContainsKey_m1345547490_MethodInfo_var);
		if (!L_4)
		{
			goto IL_0080;
		}
	}
	{
		StringU5BU5D_t1642385972* L_5 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		ArrayElementTypeCheck (L_5, _stringLiteral1280487602);
		(L_5)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral1280487602);
		StringU5BU5D_t1642385972* L_6 = L_5;
		int32_t* L_7 = __this->get_address_of_stateID_5();
		Il2CppObject * L_8 = Box(StateID_t3221758012_il2cpp_TypeInfo_var, L_7);
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_9);
		StringU5BU5D_t1642385972* L_10 = L_6;
		ArrayElementTypeCheck (L_10, _stringLiteral206097795);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral206097795);
		StringU5BU5D_t1642385972* L_11 = L_10;
		Il2CppObject * L_12 = Box(Transition_t3224717525_il2cpp_TypeInfo_var, (&___trans0));
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_12);
		ArrayElementTypeCheck (L_11, L_13);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_13);
		StringU5BU5D_t1642385972* L_14 = L_11;
		ArrayElementTypeCheck (L_14, _stringLiteral939293652);
		(L_14)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral939293652);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_15 = String_Concat_m626692867(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		return;
	}

IL_0080:
	{
		Dictionary_2_t1643289790 * L_16 = __this->get_map_4();
		int32_t L_17 = ___trans0;
		int32_t L_18 = ___id1;
		Dictionary_2_Add_m3011419773(L_16, L_17, L_18, /*hidden argument*/Dictionary_2_Add_m3011419773_MethodInfo_var);
		return;
	}
}
// System.Void FSMState::DeleteTransition(Transition)
extern "C"  void FSMState_DeleteTransition_m3401417729 (FSMState_t3109806909 * __this, int32_t ___trans0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMState_DeleteTransition_m3401417729_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___trans0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1311624908, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		Dictionary_2_t1643289790 * L_1 = __this->get_map_4();
		int32_t L_2 = ___trans0;
		bool L_3 = Dictionary_2_ContainsKey_m1345547490(L_1, L_2, /*hidden argument*/Dictionary_2_ContainsKey_m1345547490_MethodInfo_var);
		if (!L_3)
		{
			goto IL_0030;
		}
	}
	{
		Dictionary_2_t1643289790 * L_4 = __this->get_map_4();
		int32_t L_5 = ___trans0;
		Dictionary_2_Remove_m2584370466(L_4, L_5, /*hidden argument*/Dictionary_2_Remove_m2584370466_MethodInfo_var);
		return;
	}

IL_0030:
	{
		StringU5BU5D_t1642385972* L_6 = ((StringU5BU5D_t1642385972*)SZArrayNew(StringU5BU5D_t1642385972_il2cpp_TypeInfo_var, (uint32_t)5));
		ArrayElementTypeCheck (L_6, _stringLiteral2820183298);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral2820183298);
		StringU5BU5D_t1642385972* L_7 = L_6;
		Il2CppObject * L_8 = Box(Transition_t3224717525_il2cpp_TypeInfo_var, (&___trans0));
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (String_t*)L_9);
		StringU5BU5D_t1642385972* L_10 = L_7;
		ArrayElementTypeCheck (L_10, _stringLiteral56031043);
		(L_10)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral56031043);
		StringU5BU5D_t1642385972* L_11 = L_10;
		int32_t* L_12 = __this->get_address_of_stateID_5();
		Il2CppObject * L_13 = Box(StateID_t3221758012_il2cpp_TypeInfo_var, L_12);
		String_t* L_14 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_13);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(3), (String_t*)L_14);
		StringU5BU5D_t1642385972* L_15 = L_11;
		ArrayElementTypeCheck (L_15, _stringLiteral716588638);
		(L_15)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral716588638);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_16 = String_Concat_m626692867(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return;
	}
}
// StateID FSMState::GetOutputState(Transition)
extern "C"  int32_t FSMState_GetOutputState_m2828798026 (FSMState_t3109806909 * __this, int32_t ___trans0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMState_GetOutputState_m2828798026_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_t1643289790 * L_0 = __this->get_map_4();
		int32_t L_1 = ___trans0;
		bool L_2 = Dictionary_2_ContainsKey_m1345547490(L_0, L_1, /*hidden argument*/Dictionary_2_ContainsKey_m1345547490_MethodInfo_var);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t1643289790 * L_3 = __this->get_map_4();
		int32_t L_4 = ___trans0;
		int32_t L_5 = Dictionary_2_get_Item_m2924153579(L_3, L_4, /*hidden argument*/Dictionary_2_get_Item_m2924153579_MethodInfo_var);
		return L_5;
	}

IL_001e:
	{
		return (int32_t)(0);
	}
}
// System.Void FSMState::DoBeforeEntering()
extern "C"  void FSMState_DoBeforeEntering_m4256541916 (FSMState_t3109806909 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FSMState::DoBeforeLeaving()
extern "C"  void FSMState_DoBeforeLeaving_m770774864 (FSMState_t3109806909 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FSMState::Reason()
extern "C"  void FSMState_Reason_m2448565856 (FSMState_t3109806909 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FSMState::Act()
extern "C"  void FSMState_Act_m540409722 (FSMState_t3109806909 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void FSMSystem::.ctor()
extern "C"  void FSMSystem__ctor_m3437460506 (FSMSystem_t2363122359 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMSystem__ctor_m3437460506_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m2551263788(__this, /*hidden argument*/NULL);
		List_1_t2478928041 * L_0 = (List_1_t2478928041 *)il2cpp_codegen_object_new(List_1_t2478928041_il2cpp_TypeInfo_var);
		List_1__ctor_m2113850620(L_0, /*hidden argument*/List_1__ctor_m2113850620_MethodInfo_var);
		__this->set_states_0(L_0);
		return;
	}
}
// StateID FSMSystem::get_CurrentStateID()
extern "C"  int32_t FSMSystem_get_CurrentStateID_m1581698305 (FSMSystem_t2363122359 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_currentStateID_1();
		return L_0;
	}
}
// FSMState FSMSystem::get_CurrentState()
extern "C"  FSMState_t3109806909 * FSMSystem_get_CurrentState_m133616593 (FSMSystem_t2363122359 * __this, const MethodInfo* method)
{
	{
		FSMState_t3109806909 * L_0 = __this->get_currentState_2();
		return L_0;
	}
}
// System.Void FSMSystem::SetCurrentState(FSMState)
extern "C"  void FSMSystem_SetCurrentState_m2352184253 (FSMSystem_t2363122359 * __this, FSMState_t3109806909 * ___s0, const MethodInfo* method)
{
	{
		FSMState_t3109806909 * L_0 = ___s0;
		__this->set_currentState_2(L_0);
		FSMState_t3109806909 * L_1 = ___s0;
		int32_t L_2 = FSMState_get_ID_m3972705597(L_1, /*hidden argument*/NULL);
		__this->set_currentStateID_1(L_2);
		FSMState_t3109806909 * L_3 = ___s0;
		VirtActionInvoker0::Invoke(4 /* System.Void FSMState::DoBeforeEntering() */, L_3);
		return;
	}
}
// System.Void FSMSystem::AddState(FSMState,Controller)
extern "C"  void FSMSystem_AddState_m3680092715 (FSMSystem_t2363122359 * __this, FSMState_t3109806909 * ___s0, Controller_t1937198888 * ___controller1, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMSystem_AddState_m3680092715_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FSMState_t3109806909 * V_0 = NULL;
	Enumerator_t2013657715  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		FSMState_t3109806909 * L_0 = ___s0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral3477464233, /*hidden argument*/NULL);
	}

IL_0016:
	{
		FSMState_t3109806909 * L_2 = ___s0;
		FSMState_set_FSM_m3108072358(L_2, __this, /*hidden argument*/NULL);
		FSMState_t3109806909 * L_3 = ___s0;
		Controller_t1937198888 * L_4 = ___controller1;
		FSMState_set_Controller_m2162207389(L_3, L_4, /*hidden argument*/NULL);
		List_1_t2478928041 * L_5 = __this->get_states_0();
		int32_t L_6 = List_1_get_Count_m3256523046(L_5, /*hidden argument*/List_1_get_Count_m3256523046_MethodInfo_var);
		if (L_6)
		{
			goto IL_0041;
		}
	}
	{
		List_1_t2478928041 * L_7 = __this->get_states_0();
		FSMState_t3109806909 * L_8 = ___s0;
		List_1_Add_m2731754952(L_7, L_8, /*hidden argument*/List_1_Add_m2731754952_MethodInfo_var);
		return;
	}

IL_0041:
	{
		List_1_t2478928041 * L_9 = __this->get_states_0();
		Enumerator_t2013657715  L_10 = List_1_GetEnumerator_m3953815739(L_9, /*hidden argument*/List_1_GetEnumerator_m3953815739_MethodInfo_var);
		V_1 = L_10;
	}

IL_004d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0098;
		}

IL_0052:
		{
			FSMState_t3109806909 * L_11 = Enumerator_get_Current_m3369313255((&V_1), /*hidden argument*/Enumerator_get_Current_m3369313255_MethodInfo_var);
			V_0 = L_11;
			FSMState_t3109806909 * L_12 = V_0;
			int32_t L_13 = FSMState_get_ID_m3972705597(L_12, /*hidden argument*/NULL);
			FSMState_t3109806909 * L_14 = ___s0;
			int32_t L_15 = FSMState_get_ID_m3972705597(L_14, /*hidden argument*/NULL);
			if ((!(((uint32_t)L_13) == ((uint32_t)L_15))))
			{
				goto IL_0098;
			}
		}

IL_006b:
		{
			FSMState_t3109806909 * L_16 = ___s0;
			int32_t L_17 = FSMState_get_ID_m3972705597(L_16, /*hidden argument*/NULL);
			V_2 = L_17;
			Il2CppObject * L_18 = Box(StateID_t3221758012_il2cpp_TypeInfo_var, (&V_2));
			String_t* L_19 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_18);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			String_t* L_20 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral1491674556, L_19, _stringLiteral1873843773, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
			Debug_LogError_m3715728798(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0xC3, FINALLY_00a9);
		}

IL_0098:
		{
			bool L_21 = Enumerator_MoveNext_m653200627((&V_1), /*hidden argument*/Enumerator_MoveNext_m653200627_MethodInfo_var);
			if (L_21)
			{
				goto IL_0052;
			}
		}

IL_00a4:
		{
			IL2CPP_LEAVE(0xB7, FINALLY_00a9);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00a9;
	}

FINALLY_00a9:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3890009865((&V_1), /*hidden argument*/Enumerator_Dispose_m3890009865_MethodInfo_var);
		IL2CPP_END_FINALLY(169)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(169)
	{
		IL2CPP_JUMP_TBL(0xC3, IL_00c3)
		IL2CPP_JUMP_TBL(0xB7, IL_00b7)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00b7:
	{
		List_1_t2478928041 * L_22 = __this->get_states_0();
		FSMState_t3109806909 * L_23 = ___s0;
		List_1_Add_m2731754952(L_22, L_23, /*hidden argument*/List_1_Add_m2731754952_MethodInfo_var);
	}

IL_00c3:
	{
		return;
	}
}
// System.Void FSMSystem::DeleteState(StateID)
extern "C"  void FSMSystem_DeleteState_m2089744880 (FSMSystem_t2363122359 * __this, int32_t ___id0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMSystem_DeleteState_m2089744880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	FSMState_t3109806909 * V_0 = NULL;
	Enumerator_t2013657715  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___id0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral923137055, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		List_1_t2478928041 * L_1 = __this->get_states_0();
		Enumerator_t2013657715  L_2 = List_1_GetEnumerator_m3953815739(L_1, /*hidden argument*/List_1_GetEnumerator_m3953815739_MethodInfo_var);
		V_1 = L_2;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0048;
		}

IL_0022:
		{
			FSMState_t3109806909 * L_3 = Enumerator_get_Current_m3369313255((&V_1), /*hidden argument*/Enumerator_get_Current_m3369313255_MethodInfo_var);
			V_0 = L_3;
			FSMState_t3109806909 * L_4 = V_0;
			int32_t L_5 = FSMState_get_ID_m3972705597(L_4, /*hidden argument*/NULL);
			int32_t L_6 = ___id0;
			if ((!(((uint32_t)L_5) == ((uint32_t)L_6))))
			{
				goto IL_0048;
			}
		}

IL_0036:
		{
			List_1_t2478928041 * L_7 = __this->get_states_0();
			FSMState_t3109806909 * L_8 = V_0;
			List_1_Remove_m2051816421(L_7, L_8, /*hidden argument*/List_1_Remove_m2051816421_MethodInfo_var);
			IL2CPP_LEAVE(0x88, FINALLY_0059);
		}

IL_0048:
		{
			bool L_9 = Enumerator_MoveNext_m653200627((&V_1), /*hidden argument*/Enumerator_MoveNext_m653200627_MethodInfo_var);
			if (L_9)
			{
				goto IL_0022;
			}
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x67, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3890009865((&V_1), /*hidden argument*/Enumerator_Dispose_m3890009865_MethodInfo_var);
		IL2CPP_END_FINALLY(89)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x88, IL_0088)
		IL2CPP_JUMP_TBL(0x67, IL_0067)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0067:
	{
		Il2CppObject * L_10 = Box(StateID_t3221758012_il2cpp_TypeInfo_var, (&___id0));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m612901809(NULL /*static, unused*/, _stringLiteral2374734972, L_11, _stringLiteral3206117546, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void FSMSystem::PerformTransition(Transition)
extern "C"  void FSMSystem_PerformTransition_m4026309299 (FSMSystem_t2363122359 * __this, int32_t ___trans0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FSMSystem_PerformTransition_m4026309299_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	FSMState_t3109806909 * V_1 = NULL;
	Enumerator_t2013657715  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		int32_t L_0 = ___trans0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, _stringLiteral1366634108, /*hidden argument*/NULL);
		return;
	}

IL_0011:
	{
		FSMState_t3109806909 * L_1 = __this->get_currentState_2();
		int32_t L_2 = ___trans0;
		int32_t L_3 = FSMState_GetOutputState_m2828798026(L_1, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = V_0;
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		int32_t* L_5 = __this->get_address_of_currentStateID_1();
		Il2CppObject * L_6 = Box(StateID_t3221758012_il2cpp_TypeInfo_var, L_5);
		String_t* L_7 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		Il2CppObject * L_8 = Box(Transition_t3224717525_il2cpp_TypeInfo_var, (&___trans0));
		String_t* L_9 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m1561703559(NULL /*static, unused*/, _stringLiteral4134666537, L_7, _stringLiteral739081689, L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_LogError_m3715728798(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		return;
	}

IL_0057:
	{
		int32_t L_11 = V_0;
		__this->set_currentStateID_1(L_11);
		List_1_t2478928041 * L_12 = __this->get_states_0();
		Enumerator_t2013657715  L_13 = List_1_GetEnumerator_m3953815739(L_12, /*hidden argument*/List_1_GetEnumerator_m3953815739_MethodInfo_var);
		V_2 = L_13;
	}

IL_006a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00aa;
		}

IL_006f:
		{
			FSMState_t3109806909 * L_14 = Enumerator_get_Current_m3369313255((&V_2), /*hidden argument*/Enumerator_get_Current_m3369313255_MethodInfo_var);
			V_1 = L_14;
			FSMState_t3109806909 * L_15 = V_1;
			int32_t L_16 = FSMState_get_ID_m3972705597(L_15, /*hidden argument*/NULL);
			int32_t L_17 = __this->get_currentStateID_1();
			if ((!(((uint32_t)L_16) == ((uint32_t)L_17))))
			{
				goto IL_00aa;
			}
		}

IL_0088:
		{
			FSMState_t3109806909 * L_18 = __this->get_currentState_2();
			VirtActionInvoker0::Invoke(5 /* System.Void FSMState::DoBeforeLeaving() */, L_18);
			FSMState_t3109806909 * L_19 = V_1;
			__this->set_currentState_2(L_19);
			FSMState_t3109806909 * L_20 = __this->get_currentState_2();
			VirtActionInvoker0::Invoke(4 /* System.Void FSMState::DoBeforeEntering() */, L_20);
			goto IL_00b6;
		}

IL_00aa:
		{
			bool L_21 = Enumerator_MoveNext_m653200627((&V_2), /*hidden argument*/Enumerator_MoveNext_m653200627_MethodInfo_var);
			if (L_21)
			{
				goto IL_006f;
			}
		}

IL_00b6:
		{
			IL2CPP_LEAVE(0xC9, FINALLY_00bb);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_00bb;
	}

FINALLY_00bb:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m3890009865((&V_2), /*hidden argument*/Enumerator_Dispose_m3890009865_MethodInfo_var);
		IL2CPP_END_FINALLY(187)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(187)
	{
		IL2CPP_JUMP_TBL(0xC9, IL_00c9)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00c9:
	{
		return;
	}
}
// System.Void GameManager::.ctor()
extern "C"  void GameManager__ctor_m293624896 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		__this->set_isPause_2((bool)1);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.GameObject GameManager::get_CurrentShape()
extern "C"  GameObject_t1756533147 * GameManager_get_CurrentShape_m2990618538 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_currentShape_5();
		return L_0;
	}
}
// System.Void GameManager::set_CurrentShape(UnityEngine.GameObject)
extern "C"  void GameManager_set_CurrentShape_m115997281 (GameManager_t2252321495 * __this, GameObject_t1756533147 * ___value0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = ___value0;
		__this->set_currentShape_5(L_0);
		return;
	}
}
// System.Void GameManager::Awake()
extern "C"  void GameManager_Awake_m99497495 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Awake_m99497495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Controller_t1937198888 * L_0 = Component_GetComponent_TisController_t1937198888_m325252931(__this, /*hidden argument*/Component_GetComponent_TisController_t1937198888_m325252931_MethodInfo_var);
		__this->set_controller_6(L_0);
		return;
	}
}
// System.Void GameManager::Start()
extern "C"  void GameManager_Start_m2655388892 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameManager::Update()
extern "C"  void GameManager_Update_m969954595 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_Update_m969954595_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get_isPause_2();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		GameObject_t1756533147 * L_1 = __this->get_currentShape_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		GameManager_SpawnShape_m2959733954(__this, /*hidden argument*/NULL);
	}

IL_0023:
	{
		return;
	}
}
// System.Void GameManager::SpawnShape()
extern "C"  void GameManager_SpawnShape_m2959733954 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_SpawnShape_m2959733954_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		GameObjectU5BU5D_t3057952154* L_0 = __this->get_shapes_3();
		int32_t L_1 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length)))), /*hidden argument*/NULL);
		V_0 = L_1;
		ColorU5BU5D_t672350442* L_2 = __this->get_colors_4();
		int32_t L_3 = Random_Range_m694320887(NULL /*static, unused*/, 0, (((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length)))), /*hidden argument*/NULL);
		V_1 = L_3;
		GameObjectU5BU5D_t3057952154* L_4 = __this->get_shapes_3();
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		GameObject_t1756533147 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		GameObject_t1756533147 * L_8 = Object_Instantiate_TisGameObject_t1756533147_m3664764861(NULL /*static, unused*/, L_7, /*hidden argument*/Object_Instantiate_TisGameObject_t1756533147_m3664764861_MethodInfo_var);
		__this->set_currentShape_5(L_8);
		GameObject_t1756533147 * L_9 = __this->get_currentShape_5();
		Shape_t1303907469 * L_10 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_9, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		ColorU5BU5D_t672350442* L_11 = __this->get_colors_4();
		int32_t L_12 = V_1;
		Controller_t1937198888 * L_13 = __this->get_controller_6();
		Shape_Init_m2122446389(L_10, (*(Color_t2020392075 *)((L_11)->GetAddressAtUnchecked(static_cast<il2cpp_array_size_t>(L_12)))), L_13, __this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::StartGame()
extern "C"  void GameManager_StartGame_m4019248290 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_StartGame_m4019248290_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isPause_2((bool)0);
		GameObject_t1756533147 * L_0 = __this->get_currentShape_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_currentShape_5();
		Shape_t1303907469 * L_3 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_2, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		Shape_StartShapeFall_m3725894794(L_3, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void GameManager::PauseGame()
extern "C"  void GameManager_PauseGame_m1634217600 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameManager_PauseGame_m1634217600_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_isPause_2((bool)1);
		GameObject_t1756533147 * L_0 = __this->get_currentShape_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_0, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0028;
		}
	}
	{
		GameObject_t1756533147 * L_2 = __this->get_currentShape_5();
		Shape_t1303907469 * L_3 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_2, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		Shape_StopShapeFall_m1202009304(L_3, /*hidden argument*/NULL);
	}

IL_0028:
	{
		return;
	}
}
// System.Void GameManager::IsGameOver()
extern "C"  void GameManager_IsGameOver_m844888474 (GameManager_t2252321495 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = __this->get_controller_6();
		Model_t873752437 * L_1 = L_0->get_model_2();
		bool L_2 = Model_IsGameOver_m4267625588(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		Controller_t1937198888 * L_3 = __this->get_controller_6();
		GameManager_t2252321495 * L_4 = L_3->get_gameManager_5();
		GameManager_PauseGame_m1634217600(L_4, /*hidden argument*/NULL);
		Controller_t1937198888 * L_5 = __this->get_controller_6();
		FSMSystem_t2363122359 * L_6 = Controller_get_Fsm_m320623870(L_5, /*hidden argument*/NULL);
		FSMSystem_PerformTransition_m4026309299(L_6, 3, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void GameOverState::.ctor()
extern "C"  void GameOverState__ctor_m2293290176 (GameOverState_t4251729781 * __this, const MethodInfo* method)
{
	{
		FSMState__ctor_m2907867150(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameOverState::Awake()
extern "C"  void GameOverState_Awake_m1852053153 (GameOverState_t4251729781 * __this, const MethodInfo* method)
{
	{
		((FSMState_t3109806909 *)__this)->set_stateID_5(4);
		FSMState_AddTransition_m3341888567(__this, 1, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameOverState::DoBeforeEntering()
extern "C"  void GameOverState_DoBeforeEntering_m408932710 (GameOverState_t4251729781 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeEntering_m4256541916(__this, /*hidden argument*/NULL);
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_1 = L_0->get_view_3();
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_3 = L_2->get_model_2();
		int32_t L_4 = Model_get_Score_m1604402383(L_3, /*hidden argument*/NULL);
		View_ShowGameOverUI_m3030614956(L_1, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameOverState::DoBeforeLeaving()
extern "C"  void GameOverState_DoBeforeLeaving_m2791837438 (GameOverState_t4251729781 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeLeaving_m770774864(__this, /*hidden argument*/NULL);
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_1 = L_0->get_view_3();
		View_HideGameOverUI_m516871344(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameOverState::ReStartClickButton()
extern "C"  void GameOverState_ReStartClickButton_m117476519 (GameOverState_t4251729781 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_1 = L_0->get_model_2();
		Model_Restart_m1980656027(L_1, /*hidden argument*/NULL);
		FSMSystem_t2363122359 * L_2 = ((FSMState_t3109806909 *)__this)->get_fsm_3();
		FSMSystem_PerformTransition_m4026309299(L_2, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameOverState::HomeClickButton()
extern "C"  void GameOverState_HomeClickButton_m4240822815 (GameOverState_t4251729781 * __this, const MethodInfo* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Scene_t1684909666  L_0 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = Scene_get_buildIndex_m3735680091((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JarodInputController::.ctor()
extern "C"  void JarodInputController__ctor_m1449180527 (JarodInputController_t1982328090 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fingerActionSensitivity_2(((float)((float)(((float)((float)L_0)))*(float)(0.05f))));
		__this->set_FINGER_STATE_TOUCH_11(1);
		__this->set_FINGER_STATE_ADD_12(2);
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void JarodInputController::Awake()
extern "C"  void JarodInputController_Awake_m3854285658 (JarodInputController_t1982328090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JarodInputController_Awake_m3854285658_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Controller_t1937198888 * L_0 = Component_GetComponent_TisController_t1937198888_m325252931(__this, /*hidden argument*/Component_GetComponent_TisController_t1937198888_m325252931_MethodInfo_var);
		__this->set_controller_13(L_0);
		return;
	}
}
// System.Void JarodInputController::Start()
extern "C"  void JarodInputController_Start_m369703611 (JarodInputController_t1982328090 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Screen_get_width_m41137238(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_fingerActionSensitivity_2(((float)((float)(((float)((float)L_0)))*(float)(0.05f))));
		__this->set_fingerBeginX_3((0.0f));
		__this->set_fingerBeginY_4((0.0f));
		__this->set_fingerCurrentX_5((0.0f));
		__this->set_fingerCurrentY_6((0.0f));
		__this->set_fingerSegmentX_7((0.0f));
		__this->set_fingerSegmentY_8((0.0f));
		int32_t L_1 = __this->get_FINGER_STATE_NULL_10();
		__this->set_fingerTouchState_9(L_1);
		return;
	}
}
// System.Void JarodInputController::Update()
extern "C"  void JarodInputController_Update_m3112380776 (JarodInputController_t1982328090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JarodInputController_Update_m3112380776_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	float V_4 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)323), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_1 = __this->get_fingerTouchState_9();
		int32_t L_2 = __this->get_FINGER_STATE_NULL_10();
		if ((!(((uint32_t)L_1) == ((uint32_t)L_2))))
		{
			goto IL_0052;
		}
	}
	{
		int32_t L_3 = __this->get_FINGER_STATE_TOUCH_11();
		__this->set_fingerTouchState_9(L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_4 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_4;
		float L_5 = (&V_0)->get_x_1();
		__this->set_fingerBeginX_3(L_5);
		Vector3_t2243707580  L_6 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = (&V_1)->get_y_2();
		__this->set_fingerBeginY_4(L_7);
	}

IL_0052:
	{
		int32_t L_8 = __this->get_fingerTouchState_9();
		int32_t L_9 = __this->get_FINGER_STATE_TOUCH_11();
		if ((!(((uint32_t)L_8) == ((uint32_t)L_9))))
		{
			goto IL_00af;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		Vector3_t2243707580  L_10 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = (&V_2)->get_x_1();
		__this->set_fingerCurrentX_5(L_11);
		Vector3_t2243707580  L_12 = Input_get_mousePosition_m146923508(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_12;
		float L_13 = (&V_3)->get_y_2();
		__this->set_fingerCurrentY_6(L_13);
		float L_14 = __this->get_fingerCurrentX_5();
		float L_15 = __this->get_fingerBeginX_3();
		__this->set_fingerSegmentX_7(((float)((float)L_14-(float)L_15)));
		float L_16 = __this->get_fingerCurrentY_6();
		float L_17 = __this->get_fingerBeginY_4();
		__this->set_fingerSegmentY_8(((float)((float)L_16-(float)L_17)));
	}

IL_00af:
	{
		int32_t L_18 = __this->get_fingerTouchState_9();
		int32_t L_19 = __this->get_FINGER_STATE_TOUCH_11();
		if ((!(((uint32_t)L_18) == ((uint32_t)L_19))))
		{
			goto IL_00f7;
		}
	}
	{
		float L_20 = __this->get_fingerSegmentX_7();
		float L_21 = __this->get_fingerSegmentX_7();
		float L_22 = __this->get_fingerSegmentY_8();
		float L_23 = __this->get_fingerSegmentY_8();
		V_4 = ((float)((float)((float)((float)L_20*(float)L_21))+(float)((float)((float)L_22*(float)L_23))));
		float L_24 = V_4;
		float L_25 = __this->get_fingerActionSensitivity_2();
		float L_26 = __this->get_fingerActionSensitivity_2();
		if ((!(((float)L_24) > ((float)((float)((float)L_25*(float)L_26))))))
		{
			goto IL_00f7;
		}
	}
	{
		JarodInputController_ToAddFingerAction_m3253792990(__this, /*hidden argument*/NULL);
	}

IL_00f7:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_27 = Input_GetKeyUp_m1008512962(NULL /*static, unused*/, ((int32_t)323), /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0112;
		}
	}
	{
		int32_t L_28 = __this->get_FINGER_STATE_NULL_10();
		__this->set_fingerTouchState_9(L_28);
	}

IL_0112:
	{
		return;
	}
}
// System.Void JarodInputController::ToAddFingerAction()
extern "C"  void JarodInputController_ToAddFingerAction_m3253792990 (JarodInputController_t1982328090 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (JarodInputController_ToAddFingerAction_m3253792990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_FINGER_STATE_ADD_12();
		__this->set_fingerTouchState_9(L_0);
		float L_1 = __this->get_fingerSegmentX_7();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		float L_2 = fabsf(L_1);
		float L_3 = __this->get_fingerSegmentY_8();
		float L_4 = fabsf(L_3);
		if ((!(((float)L_2) > ((float)L_4))))
		{
			goto IL_0037;
		}
	}
	{
		__this->set_fingerSegmentY_8((0.0f));
		goto IL_0042;
	}

IL_0037:
	{
		__this->set_fingerSegmentX_7((0.0f));
	}

IL_0042:
	{
		float L_5 = __this->get_fingerSegmentX_7();
		if ((!(((float)L_5) == ((float)(0.0f)))))
		{
			goto IL_00b7;
		}
	}
	{
		float L_6 = __this->get_fingerSegmentY_8();
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_008c;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1543969241, /*hidden argument*/NULL);
		Controller_t1937198888 * L_7 = __this->get_controller_13();
		GameManager_t2252321495 * L_8 = L_7->get_gameManager_5();
		GameObject_t1756533147 * L_9 = GameManager_get_CurrentShape_m2990618538(L_8, /*hidden argument*/NULL);
		Shape_t1303907469 * L_10 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_9, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		Shape_ControllerInput_m1474408371(L_10, 2, /*hidden argument*/NULL);
		goto IL_00b2;
	}

IL_008c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral1367190538, /*hidden argument*/NULL);
		Controller_t1937198888 * L_11 = __this->get_controller_13();
		GameManager_t2252321495 * L_12 = L_11->get_gameManager_5();
		GameObject_t1756533147 * L_13 = GameManager_get_CurrentShape_m2990618538(L_12, /*hidden argument*/NULL);
		Shape_t1303907469 * L_14 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_13, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		Shape_ControllerInput_m1474408371(L_14, ((int32_t)-2), /*hidden argument*/NULL);
	}

IL_00b2:
	{
		goto IL_0126;
	}

IL_00b7:
	{
		float L_15 = __this->get_fingerSegmentY_8();
		if ((!(((float)L_15) == ((float)(0.0f)))))
		{
			goto IL_0126;
		}
	}
	{
		float L_16 = __this->get_fingerSegmentX_7();
		if ((!(((float)L_16) > ((float)(0.0f)))))
		{
			goto IL_0101;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral109637592, /*hidden argument*/NULL);
		Controller_t1937198888 * L_17 = __this->get_controller_13();
		GameManager_t2252321495 * L_18 = L_17->get_gameManager_5();
		GameObject_t1756533147 * L_19 = GameManager_get_CurrentShape_m2990618538(L_18, /*hidden argument*/NULL);
		Shape_t1303907469 * L_20 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_19, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		Shape_ControllerInput_m1474408371(L_20, 1, /*hidden argument*/NULL);
		goto IL_0126;
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t1368543263_il2cpp_TypeInfo_var);
		Debug_Log_m920475918(NULL /*static, unused*/, _stringLiteral3423761043, /*hidden argument*/NULL);
		Controller_t1937198888 * L_21 = __this->get_controller_13();
		GameManager_t2252321495 * L_22 = L_21->get_gameManager_5();
		GameObject_t1756533147 * L_23 = GameManager_get_CurrentShape_m2990618538(L_22, /*hidden argument*/NULL);
		Shape_t1303907469 * L_24 = GameObject_GetComponent_TisShape_t1303907469_m1545819496(L_23, /*hidden argument*/GameObject_GetComponent_TisShape_t1303907469_m1545819496_MethodInfo_var);
		Shape_ControllerInput_m1474408371(L_24, (-1), /*hidden argument*/NULL);
	}

IL_0126:
	{
		return;
	}
}
// System.Void MenuState::.ctor()
extern "C"  void MenuState__ctor_m1273303795 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		FSMState__ctor_m2907867150(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::Awake()
extern "C"  void MenuState_Awake_m2491835988 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		((FSMState_t3109806909 *)__this)->set_stateID_5(1);
		FSMState_AddTransition_m3341888567(__this, 1, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::DoBeforeEntering()
extern "C"  void MenuState_DoBeforeEntering_m3009233977 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeEntering_m4256541916(__this, /*hidden argument*/NULL);
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_1 = L_0->get_view_3();
		View_ShowMenu_m110806820(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		CameraManager_t2379859346 * L_3 = L_2->get_cameraManager_4();
		CameraManager_ZoomOut_m37434308(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::DoBeforeLeaving()
extern "C"  void MenuState_DoBeforeLeaving_m2132307439 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeLeaving_m770774864(__this, /*hidden argument*/NULL);
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_1 = L_0->get_view_3();
		View_HideMenu_m3662219881(L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::StartButtonClick()
extern "C"  void MenuState_StartButtonClick_m412913627 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		FSMSystem_t2363122359 * L_2 = ((FSMState_t3109806909 *)__this)->get_fsm_3();
		FSMSystem_PerformTransition_m4026309299(L_2, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::SetingButtonClick()
extern "C"  void MenuState_SetingButtonClick_m1075956129 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_3 = L_2->get_view_3();
		View_ShowSettingUI_m4255451743(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::SetAudioPauseButtonClick()
extern "C"  void MenuState_SetAudioPauseButtonClick_m4106265925 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_3 = L_2->get_audioManager_6();
		Controller_t1937198888 * L_4 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_5 = L_4->get_audioManager_6();
		bool L_6 = AudioManager_get_IsPause_m3310627223(L_5, /*hidden argument*/NULL);
		AudioManager_set_IsPause_m3955952632(L_3, (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0), /*hidden argument*/NULL);
		Controller_t1937198888 * L_7 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_8 = L_7->get_view_3();
		Controller_t1937198888 * L_9 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_10 = L_9->get_audioManager_6();
		bool L_11 = AudioManager_get_IsPause_m3310627223(L_10, /*hidden argument*/NULL);
		View_IsShowAudioLine_m2780770722(L_8, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::ExitSettingUI()
extern "C"  void MenuState_ExitSettingUI_m1910579453 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_3 = L_2->get_view_3();
		View_HideSettingUI_m2244255162(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::ReStartGame()
extern "C"  void MenuState_ReStartGame_m3360747774 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	Scene_t1684909666  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		Scene_t1684909666  L_2 = SceneManager_GetActiveScene_m2964039490(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = Scene_get_buildIndex_m3735680091((&V_0), /*hidden argument*/NULL);
		SceneManager_LoadScene_m87258056(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::RankButtonClick()
extern "C"  void MenuState_RankButtonClick_m3682979521 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_3 = L_2->get_view_3();
		Controller_t1937198888 * L_4 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_5 = L_4->get_model_2();
		int32_t L_6 = Model_get_Score_m1604402383(L_5, /*hidden argument*/NULL);
		Controller_t1937198888 * L_7 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_8 = L_7->get_model_2();
		int32_t L_9 = Model_get_HighScore_m635096529(L_8, /*hidden argument*/NULL);
		Controller_t1937198888 * L_10 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_11 = L_10->get_model_2();
		int32_t L_12 = Model_get_NumbersGame_m3909748289(L_11, /*hidden argument*/NULL);
		View_ShowRankUI_m2821821284(L_3, L_6, L_9, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::ExitRankButtonClick()
extern "C"  void MenuState_ExitRankButtonClick_m3994512009 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_3 = L_2->get_view_3();
		View_HideRankUI_m2187104746(L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MenuState::ClearClickButton()
extern "C"  void MenuState_ClearClickButton_m2084329684 (MenuState_t2133158756 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_1 = L_0->get_model_2();
		Model_ClearData_m3016253009(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_3 = L_2->get_view_3();
		Controller_t1937198888 * L_4 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_5 = L_4->get_model_2();
		int32_t L_6 = Model_get_Score_m1604402383(L_5, /*hidden argument*/NULL);
		Controller_t1937198888 * L_7 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_8 = L_7->get_model_2();
		int32_t L_9 = Model_get_HighScore_m635096529(L_8, /*hidden argument*/NULL);
		Controller_t1937198888 * L_10 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_11 = L_10->get_model_2();
		int32_t L_12 = Model_get_NumbersGame_m3909748289(L_11, /*hidden argument*/NULL);
		View_ShowRankData_m4259729890(L_3, L_6, L_9, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Model::.ctor()
extern "C"  void Model__ctor_m2741685172 (Model_t873752437 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Model::get_Score()
extern "C"  int32_t Model_get_Score_m1604402383 (Model_t873752437 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_score_5();
		return L_0;
	}
}
// System.Int32 Model::get_HighScore()
extern "C"  int32_t Model_get_HighScore_m635096529 (Model_t873752437 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_highScore_6();
		return L_0;
	}
}
// System.Int32 Model::get_NumbersGame()
extern "C"  int32_t Model_get_NumbersGame_m3909748289 (Model_t873752437 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_numbersGame_7();
		return L_0;
	}
}
// System.Void Model::Awake()
extern "C"  void Model_Awake_m3895385605 (Model_t873752437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_Awake_m3895385605_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		il2cpp_array_size_t L_1[] = { (il2cpp_array_size_t)((int32_t)10), (il2cpp_array_size_t)((int32_t)23) };
		TransformU5BU2CU5D_t3764228912* L_0 = (TransformU5BU2CU5D_t3764228912*)GenArrayNew(TransformU5BU2CU5D_t3764228912_il2cpp_TypeInfo_var, L_1);
		__this->set_map_9((TransformU5BU2CU5D_t3764228912*)L_0);
		Model_LoadData_m951443834(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Model::IsValidMapPosition(UnityEngine.Transform)
extern "C"  bool Model_IsValidMapPosition_m2662347148 (Model_t873752437 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_IsValidMapPosition_m2662347148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	bool V_3 = false;
	Il2CppObject * V_4 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___t0;
		Il2CppObject * L_1 = Transform_GetEnumerator_m3479720613(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007e;
		}

IL_000c:
		{
			Il2CppObject * L_2 = V_1;
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t3275118058 *)CastclassClass(L_3, Transform_t3275118058_il2cpp_TypeInfo_var));
			Transform_t3275118058 * L_4 = V_0;
			String_t* L_5 = Component_get_tag_m357168014(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_5, _stringLiteral3745862197, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0032;
			}
		}

IL_002d:
		{
			goto IL_007e;
		}

IL_0032:
		{
			Transform_t3275118058 * L_7 = V_0;
			Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
			Vector3_t2243707580  L_9 = Vector3Extension_Vector3Round_m4246609207(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			Vector3_t2243707580  L_10 = V_2;
			bool L_11 = Model_IsInsideMap_m1123263623(__this, L_10, /*hidden argument*/NULL);
			if (L_11)
			{
				goto IL_0051;
			}
		}

IL_004a:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0xA6, FINALLY_008e);
		}

IL_0051:
		{
			TransformU5BU2CU5D_t3764228912* L_12 = __this->get_map_9();
			float L_13 = (&V_2)->get_x_1();
			float L_14 = (&V_2)->get_y_2();
			Transform_t3275118058 * L_15 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_12)->GetAtUnchecked((((int32_t)((int32_t)L_13))), (((int32_t)((int32_t)L_14))));
			IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
			bool L_16 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
			if (!L_16)
			{
				goto IL_007e;
			}
		}

IL_0077:
		{
			V_3 = (bool)0;
			IL2CPP_LEAVE(0xA6, FINALLY_008e);
		}

IL_007e:
		{
			Il2CppObject * L_17 = V_1;
			bool L_18 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_17);
			if (L_18)
			{
				goto IL_000c;
			}
		}

IL_0089:
		{
			IL2CPP_LEAVE(0xA4, FINALLY_008e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_008e;
	}

FINALLY_008e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_19 = V_1;
			Il2CppObject * L_20 = ((Il2CppObject *)IsInst(L_19, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_4 = L_20;
			if (!L_20)
			{
				goto IL_00a3;
			}
		}

IL_009c:
		{
			Il2CppObject * L_21 = V_4;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_21);
		}

IL_00a3:
		{
			IL2CPP_END_FINALLY(142)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(142)
	{
		IL2CPP_JUMP_TBL(0xA6, IL_00a6)
		IL2CPP_JUMP_TBL(0xA4, IL_00a4)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_00a4:
	{
		return (bool)1;
	}

IL_00a6:
	{
		bool L_22 = V_3;
		return L_22;
	}
}
// System.Boolean Model::IsInsideMap(UnityEngine.Vector3)
extern "C"  bool Model_IsInsideMap_m1123263623 (Model_t873752437 * __this, Vector3_t2243707580  ___pos0, const MethodInfo* method)
{
	int32_t G_B4_0 = 0;
	{
		float L_0 = (&___pos0)->get_x_1();
		if ((!(((float)L_0) >= ((float)(0.0f)))))
		{
			goto IL_0035;
		}
	}
	{
		float L_1 = (&___pos0)->get_x_1();
		if ((!(((float)L_1) < ((float)(10.0f)))))
		{
			goto IL_0035;
		}
	}
	{
		float L_2 = (&___pos0)->get_y_2();
		G_B4_0 = ((((int32_t)((!(((float)L_2) >= ((float)(0.0f))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0036;
	}

IL_0035:
	{
		G_B4_0 = 0;
	}

IL_0036:
	{
		return (bool)G_B4_0;
	}
}
// System.Boolean Model::SetBlockMap(UnityEngine.Transform)
extern "C"  bool Model_SetBlockMap_m4073711356 (Model_t873752437 * __this, Transform_t3275118058 * ___t0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_SetBlockMap_m4073711356_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Vector3_t2243707580  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Il2CppObject * V_3 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = ___t0;
		Il2CppObject * L_1 = Transform_GetEnumerator_m3479720613(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_0007:
	try
	{ // begin try (depth: 1)
		{
			goto IL_005a;
		}

IL_000c:
		{
			Il2CppObject * L_2 = V_1;
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t3275118058 *)CastclassClass(L_3, Transform_t3275118058_il2cpp_TypeInfo_var));
			Transform_t3275118058 * L_4 = V_0;
			String_t* L_5 = Component_get_tag_m357168014(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Inequality_m304203149(NULL /*static, unused*/, L_5, _stringLiteral3745862197, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_0032;
			}
		}

IL_002d:
		{
			goto IL_005a;
		}

IL_0032:
		{
			Transform_t3275118058 * L_7 = V_0;
			Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
			Vector3_t2243707580  L_9 = Vector3Extension_Vector3Round_m4246609207(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_2 = L_9;
			TransformU5BU2CU5D_t3764228912* L_10 = __this->get_map_9();
			float L_11 = (&V_2)->get_x_1();
			float L_12 = (&V_2)->get_y_2();
			Transform_t3275118058 * L_13 = V_0;
			((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_10)->SetAtUnchecked((((int32_t)((int32_t)L_11))), (((int32_t)((int32_t)L_12))), L_13);
		}

IL_005a:
		{
			Il2CppObject * L_14 = V_1;
			bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_14);
			if (L_15)
			{
				goto IL_000c;
			}
		}

IL_0065:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_006a);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_006a;
	}

FINALLY_006a:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_16 = V_1;
			Il2CppObject * L_17 = ((Il2CppObject *)IsInst(L_16, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_3 = L_17;
			if (!L_17)
			{
				goto IL_007d;
			}
		}

IL_0077:
		{
			Il2CppObject * L_18 = V_3;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_18);
		}

IL_007d:
		{
			IL2CPP_END_FINALLY(106)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(106)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_007e:
	{
		bool L_19 = Model_CheckRowMap_m1906154264(__this, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean Model::CheckRowMap()
extern "C"  bool Model_CheckRowMap_m1906154264 (Model_t873752437 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_0031;
	}

IL_0009:
	{
		int32_t L_0 = V_1;
		bool L_1 = Model_CheckIsRowFull_m1669201880(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
		int32_t L_3 = V_1;
		Model_DeleteRow_m3074637084(__this, L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_1;
		Model_MoveDownRowsAbove_m1285959532(__this, ((int32_t)((int32_t)L_4+(int32_t)1)), /*hidden argument*/NULL);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5-(int32_t)1));
	}

IL_002d:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0031:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)23))))
		{
			goto IL_0009;
		}
	}
	{
		int32_t L_8 = V_0;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_0077;
		}
	}
	{
		int32_t L_9 = __this->get_score_5();
		int32_t L_10 = V_0;
		__this->set_score_5(((int32_t)((int32_t)L_9+(int32_t)((int32_t)((int32_t)L_10*(int32_t)((int32_t)100))))));
		int32_t L_11 = __this->get_score_5();
		int32_t L_12 = __this->get_highScore_6();
		if ((((int32_t)L_11) <= ((int32_t)L_12)))
		{
			goto IL_006e;
		}
	}
	{
		int32_t L_13 = __this->get_score_5();
		__this->set_highScore_6(L_13);
	}

IL_006e:
	{
		__this->set_isDataUpdate_8((bool)1);
		return (bool)1;
	}

IL_0077:
	{
		return (bool)0;
	}
}
// System.Boolean Model::CheckIsRowFull(System.Int32)
extern "C"  bool Model_CheckIsRowFull_m1669201880 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_CheckIsRowFull_m1669201880_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0025;
	}

IL_0007:
	{
		TransformU5BU2CU5D_t3764228912* L_0 = __this->get_map_9();
		int32_t L_1 = V_0;
		int32_t L_2 = ___row0;
		Transform_t3275118058 * L_3 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_0)->GetAtUnchecked(L_1, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		return (bool)0;
	}

IL_0021:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0025:
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		return (bool)1;
	}
}
// System.Void Model::DeleteRow(System.Int32)
extern "C"  void Model_DeleteRow_m3074637084 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_DeleteRow_m3074637084_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0030;
	}

IL_0007:
	{
		TransformU5BU2CU5D_t3764228912* L_0 = __this->get_map_9();
		int32_t L_1 = V_0;
		int32_t L_2 = ___row0;
		Transform_t3275118058 * L_3 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_0)->GetAtUnchecked(L_1, L_2);
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		TransformU5BU2CU5D_t3764228912* L_5 = __this->get_map_9();
		int32_t L_6 = V_0;
		int32_t L_7 = ___row0;
		((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_5)->SetAtUnchecked(L_6, L_7, (Transform_t3275118058 *)NULL);
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0030:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Model::MoveDownRowsAbove(System.Int32)
extern "C"  void Model_MoveDownRowsAbove_m1285959532 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___row0;
		V_0 = L_0;
		goto IL_0012;
	}

IL_0007:
	{
		int32_t L_1 = V_0;
		Model_MoveDownRow_m1192001122(__this, L_1, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		V_0 = ((int32_t)((int32_t)L_2+(int32_t)1));
	}

IL_0012:
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) < ((int32_t)((int32_t)23))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void Model::MoveDownRow(System.Int32)
extern "C"  void Model_MoveDownRow_m1192001122 (Model_t873752437 * __this, int32_t ___row0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_MoveDownRow_m1192001122_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0080;
	}

IL_0007:
	{
		TransformU5BU2CU5D_t3764228912* L_0 = __this->get_map_9();
		int32_t L_1 = V_0;
		int32_t L_2 = ___row0;
		Transform_t3275118058 * L_3 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_0)->GetAtUnchecked(L_1, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_007c;
		}
	}
	{
		TransformU5BU2CU5D_t3764228912* L_5 = __this->get_map_9();
		int32_t L_6 = V_0;
		int32_t L_7 = ___row0;
		TransformU5BU2CU5D_t3764228912* L_8 = __this->get_map_9();
		int32_t L_9 = V_0;
		int32_t L_10 = ___row0;
		Transform_t3275118058 * L_11 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_8)->GetAtUnchecked(L_9, L_10);
		((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_5)->SetAtUnchecked(L_6, ((int32_t)((int32_t)L_7-(int32_t)1)), L_11);
		TransformU5BU2CU5D_t3764228912* L_12 = __this->get_map_9();
		int32_t L_13 = V_0;
		int32_t L_14 = ___row0;
		((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_12)->SetAtUnchecked(L_13, L_14, (Transform_t3275118058 *)NULL);
		TransformU5BU2CU5D_t3764228912* L_15 = __this->get_map_9();
		int32_t L_16 = V_0;
		int32_t L_17 = ___row0;
		Transform_t3275118058 * L_18 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_15)->GetAtUnchecked(L_16, ((int32_t)((int32_t)L_17-(int32_t)1)));
		Transform_t3275118058 * L_19 = L_18;
		Vector3_t2243707580  L_20 = Transform_get_position_m1104419803(L_19, /*hidden argument*/NULL);
		Vector3_t2243707580  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Vector3__ctor_m2638739322(&L_21, (0.0f), (-1.0f), (0.0f), /*hidden argument*/NULL);
		Vector3_t2243707580  L_22 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		Transform_set_position_m2469242620(L_19, L_22, /*hidden argument*/NULL);
	}

IL_007c:
	{
		int32_t L_23 = V_0;
		V_0 = ((int32_t)((int32_t)L_23+(int32_t)1));
	}

IL_0080:
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Boolean Model::IsGameOver()
extern "C"  bool Model_IsGameOver_m4267625588 (Model_t873752437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_IsGameOver_m4267625588_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = ((int32_t)20);
		goto IL_004d;
	}

IL_0008:
	{
		V_1 = 0;
		goto IL_0041;
	}

IL_000f:
	{
		TransformU5BU2CU5D_t3764228912* L_0 = __this->get_map_9();
		int32_t L_1 = V_1;
		int32_t L_2 = V_0;
		Transform_t3275118058 * L_3 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_0)->GetAtUnchecked(L_1, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_5 = __this->get_numbersGame_7();
		__this->set_numbersGame_7(((int32_t)((int32_t)L_5+(int32_t)1)));
		Model_SaveData_m1825965529(__this, /*hidden argument*/NULL);
		return (bool)1;
	}

IL_003d:
	{
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0041:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)((int32_t)10))))
		{
			goto IL_000f;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_004d:
	{
		int32_t L_9 = V_0;
		if ((((int32_t)L_9) < ((int32_t)((int32_t)23))))
		{
			goto IL_0008;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void Model::LoadData()
extern "C"  void Model_LoadData_m951443834 (Model_t873752437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_LoadData_m951443834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral3131029212, 0, /*hidden argument*/NULL);
		__this->set_highScore_6(L_0);
		int32_t L_1 = PlayerPrefs_GetInt_m136681260(NULL /*static, unused*/, _stringLiteral1969896486, 0, /*hidden argument*/NULL);
		__this->set_numbersGame_7(L_1);
		return;
	}
}
// System.Void Model::SaveData()
extern "C"  void Model_SaveData_m1825965529 (Model_t873752437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_SaveData_m1825965529_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_highScore_6();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral3131029212, L_0, /*hidden argument*/NULL);
		int32_t L_1 = __this->get_numbersGame_7();
		PlayerPrefs_SetInt_m3351928596(NULL /*static, unused*/, _stringLiteral1969896486, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Model::Restart()
extern "C"  void Model_Restart_m1980656027 (Model_t873752437 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Model_Restart_m1980656027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		V_0 = 0;
		goto IL_005b;
	}

IL_0007:
	{
		V_1 = 0;
		goto IL_004f;
	}

IL_000e:
	{
		TransformU5BU2CU5D_t3764228912* L_0 = __this->get_map_9();
		int32_t L_1 = V_0;
		int32_t L_2 = V_1;
		Transform_t3275118058 * L_3 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_0)->GetAtUnchecked(L_1, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_3, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004b;
		}
	}
	{
		TransformU5BU2CU5D_t3764228912* L_5 = __this->get_map_9();
		int32_t L_6 = V_0;
		int32_t L_7 = V_1;
		Transform_t3275118058 * L_8 = ((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_5)->GetAtUnchecked(L_6, L_7);
		GameObject_t1756533147 * L_9 = Component_get_gameObject_m3105766835(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		TransformU5BU2CU5D_t3764228912* L_10 = __this->get_map_9();
		int32_t L_11 = V_0;
		int32_t L_12 = V_1;
		((TransformU5BU2CU5D_t3764228912*)(TransformU5BU2CU5D_t3764228912*)L_10)->SetAtUnchecked(L_11, L_12, (Transform_t3275118058 *)NULL);
	}

IL_004b:
	{
		int32_t L_13 = V_1;
		V_1 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_004f:
	{
		int32_t L_14 = V_1;
		if ((((int32_t)L_14) < ((int32_t)((int32_t)23))))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_15 = V_0;
		V_0 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_005b:
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) < ((int32_t)((int32_t)10))))
		{
			goto IL_0007;
		}
	}
	{
		__this->set_score_5(0);
		return;
	}
}
// System.Void Model::ClearData()
extern "C"  void Model_ClearData_m3016253009 (Model_t873752437 * __this, const MethodInfo* method)
{
	{
		__this->set_score_5(0);
		__this->set_highScore_6(0);
		__this->set_numbersGame_7(0);
		Model_SaveData_m1825965529(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseState::.ctor()
extern "C"  void PauseState__ctor_m1939583346 (PauseState_t1378269423 * __this, const MethodInfo* method)
{
	{
		FSMState__ctor_m2907867150(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseState::Awake()
extern "C"  void PauseState_Awake_m2569023007 (PauseState_t1378269423 * __this, const MethodInfo* method)
{
	{
		((FSMState_t3109806909 *)__this)->set_stateID_5(3);
		return;
	}
}
// System.Void PauseState::DoBeforeEntering()
extern "C"  void PauseState_DoBeforeEntering_m3165505564 (PauseState_t1378269423 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeEntering_m4256541916(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PauseState::DoBeforeLeaving()
extern "C"  void PauseState_DoBeforeLeaving_m4040212592 (PauseState_t1378269423 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeLeaving_m770774864(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayState::.ctor()
extern "C"  void PlayState__ctor_m1304817722 (PlayState_t1063998429 * __this, const MethodInfo* method)
{
	{
		FSMState__ctor_m2907867150(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayState::Awake()
extern "C"  void PlayState_Awake_m1555862153 (PlayState_t1063998429 * __this, const MethodInfo* method)
{
	{
		((FSMState_t3109806909 *)__this)->set_stateID_5(2);
		FSMState_AddTransition_m3341888567(__this, 2, 1, /*hidden argument*/NULL);
		FSMState_AddTransition_m3341888567(__this, 3, 4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayState::DoBeforeEntering()
extern "C"  void PlayState_DoBeforeEntering_m847713784 (PlayState_t1063998429 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeEntering_m4256541916(__this, /*hidden argument*/NULL);
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_1 = L_0->get_view_3();
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_3 = L_2->get_model_2();
		int32_t L_4 = Model_get_Score_m1604402383(L_3, /*hidden argument*/NULL);
		Controller_t1937198888 * L_5 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		Model_t873752437 * L_6 = L_5->get_model_2();
		int32_t L_7 = Model_get_HighScore_m635096529(L_6, /*hidden argument*/NULL);
		View_ShowGameUI_m3358246707(L_1, L_4, L_7, /*hidden argument*/NULL);
		Controller_t1937198888 * L_8 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		CameraManager_t2379859346 * L_9 = L_8->get_cameraManager_4();
		CameraManager_ZoomGame_m1271599204(L_9, /*hidden argument*/NULL);
		Controller_t1937198888 * L_10 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		GameManager_t2252321495 * L_11 = L_10->get_gameManager_5();
		GameManager_StartGame_m4019248290(L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayState::DoBeforeLeaving()
extern "C"  void PlayState_DoBeforeLeaving_m2323157804 (PlayState_t1063998429 * __this, const MethodInfo* method)
{
	{
		FSMState_DoBeforeLeaving_m770774864(__this, /*hidden argument*/NULL);
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_1 = L_0->get_view_3();
		View_HideGameUI_m3415331198(L_1, /*hidden argument*/NULL);
		Controller_t1937198888 * L_2 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		View_t854181575 * L_3 = L_2->get_view_3();
		View_ShowReStartButton_m2596324120(L_3, /*hidden argument*/NULL);
		Controller_t1937198888 * L_4 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		GameManager_t2252321495 * L_5 = L_4->get_gameManager_5();
		GameManager_PauseGame_m1634217600(L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayState::PauseButtonClick()
extern "C"  void PlayState_PauseButtonClick_m2931218514 (PlayState_t1063998429 * __this, const MethodInfo* method)
{
	{
		Controller_t1937198888 * L_0 = ((FSMState_t3109806909 *)__this)->get_controller_2();
		AudioManager_t4222704959 * L_1 = L_0->get_audioManager_6();
		AudioManager_PlayClickAudio_m41531298(L_1, /*hidden argument*/NULL);
		FSMSystem_t2363122359 * L_2 = ((FSMState_t3109806909 *)__this)->get_fsm_3();
		FSMSystem_PerformTransition_m4026309299(L_2, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shape::.ctor()
extern "C"  void Shape__ctor_m3511730760 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	{
		__this->set_stepTime_6((0.8f));
		__this->set_stepUpTime_7((0.05f));
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shape::Awake()
extern "C"  void Shape_Awake_m2151941817 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_stepTime_6();
		__this->set_stepTempTime_8(L_0);
		return;
	}
}
// System.Void Shape::Update()
extern "C"  void Shape_Update_m2057618253 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_isPause_4();
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		float L_1 = __this->get_timer_5();
		float L_2 = Time_get_deltaTime_m2233168104(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_timer_5(((float)((float)L_1+(float)L_2)));
		float L_3 = __this->get_timer_5();
		float L_4 = __this->get_stepTempTime_8();
		if ((!(((float)L_3) >= ((float)L_4))))
		{
			goto IL_0040;
		}
	}
	{
		Shape_Fall_m2648392503(__this, /*hidden argument*/NULL);
		__this->set_timer_5((0.0f));
	}

IL_0040:
	{
		return;
	}
}
// System.Void Shape::Init(UnityEngine.Color,Controller,GameManager)
extern "C"  void Shape_Init_m2122446389 (Shape_t1303907469 * __this, Color_t2020392075  ___color0, Controller_t1937198888 * ___controller1, GameManager_t2252321495 * ___gameManager2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shape_Init_m2122446389_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Transform_t3275118058 * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	Il2CppObject * V_2 = NULL;
	Exception_t1927440687 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t1927440687 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Il2CppObject * L_1 = Transform_GetEnumerator_m3479720613(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_003e;
		}

IL_0011:
		{
			Il2CppObject * L_2 = V_1;
			Il2CppObject * L_3 = InterfaceFuncInvoker0< Il2CppObject * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t3275118058 *)CastclassClass(L_3, Transform_t3275118058_il2cpp_TypeInfo_var));
			Transform_t3275118058 * L_4 = V_0;
			String_t* L_5 = Component_get_tag_m357168014(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
			bool L_6 = String_op_Equality_m1790663636(NULL /*static, unused*/, L_5, _stringLiteral3745862197, /*hidden argument*/NULL);
			if (!L_6)
			{
				goto IL_003e;
			}
		}

IL_0032:
		{
			Transform_t3275118058 * L_7 = V_0;
			SpriteRenderer_t1209076198 * L_8 = Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859(L_7, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859_MethodInfo_var);
			Color_t2020392075  L_9 = ___color0;
			SpriteRenderer_set_color_m2339931967(L_8, L_9, /*hidden argument*/NULL);
		}

IL_003e:
		{
			Il2CppObject * L_10 = V_1;
			bool L_11 = InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1466026749_il2cpp_TypeInfo_var, L_10);
			if (L_11)
			{
				goto IL_0011;
			}
		}

IL_0049:
		{
			IL2CPP_LEAVE(0x62, FINALLY_004e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t1927440687 *)e.ex;
		goto FINALLY_004e;
	}

FINALLY_004e:
	{ // begin finally (depth: 1)
		{
			Il2CppObject * L_12 = V_1;
			Il2CppObject * L_13 = ((Il2CppObject *)IsInst(L_12, IDisposable_t2427283555_il2cpp_TypeInfo_var));
			V_2 = L_13;
			if (!L_13)
			{
				goto IL_0061;
			}
		}

IL_005b:
		{
			Il2CppObject * L_14 = V_2;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t2427283555_il2cpp_TypeInfo_var, L_14);
		}

IL_0061:
		{
			IL2CPP_END_FINALLY(78)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(78)
	{
		IL2CPP_JUMP_TBL(0x62, IL_0062)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t1927440687 *)
	}

IL_0062:
	{
		GameManager_t2252321495 * L_15 = ___gameManager2;
		__this->set_gameManager_3(L_15);
		Controller_t1937198888 * L_16 = ___controller1;
		__this->set_controller_2(L_16);
		return;
	}
}
// System.Void Shape::ControllerInput(System.Int32)
extern "C"  void Shape_ControllerInput_m1474408371 (Shape_t1303907469 * __this, int32_t ___direction0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shape_ControllerInput_m1474408371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)276), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		int32_t L_1 = ___direction0;
		if ((!(((uint32_t)L_1) == ((uint32_t)(-1)))))
		{
			goto IL_008b;
		}
	}

IL_0016:
	{
		Controller_t1937198888 * L_2 = __this->get_controller_2();
		AudioManager_t4222704959 * L_3 = L_2->get_audioManager_6();
		AudioManager_PlayCtrlAudio_m2583450153(L_3, /*hidden argument*/NULL);
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Transform_get_position_m1104419803(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Vector3_t2243707580 * L_6 = (&V_0);
		float L_7 = L_6->get_x_1();
		L_6->set_x_1(((float)((float)L_7-(float)(1.0f))));
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_9 = V_0;
		Transform_set_position_m2469242620(L_8, L_9, /*hidden argument*/NULL);
		Controller_t1937198888 * L_10 = __this->get_controller_2();
		Model_t873752437 * L_11 = L_10->get_model_2();
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		bool L_13 = Model_IsValidMapPosition_m2662347148(L_11, L_12, /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_008b;
		}
	}
	{
		Vector3_t2243707580 * L_14 = (&V_0);
		float L_15 = L_14->get_x_1();
		L_14->set_x_1(((float)((float)L_15+(float)(1.0f))));
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_17 = V_0;
		Transform_set_position_m2469242620(L_16, L_17, /*hidden argument*/NULL);
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_18 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)275), /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_00a1;
		}
	}
	{
		int32_t L_19 = ___direction0;
		if ((!(((uint32_t)L_19) == ((uint32_t)1))))
		{
			goto IL_0116;
		}
	}

IL_00a1:
	{
		Controller_t1937198888 * L_20 = __this->get_controller_2();
		AudioManager_t4222704959 * L_21 = L_20->get_audioManager_6();
		AudioManager_PlayCtrlAudio_m2583450153(L_21, /*hidden argument*/NULL);
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_23 = Transform_get_position_m1104419803(L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		Vector3_t2243707580 * L_24 = (&V_1);
		float L_25 = L_24->get_x_1();
		L_24->set_x_1(((float)((float)L_25+(float)(1.0f))));
		Transform_t3275118058 * L_26 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_27 = V_1;
		Transform_set_position_m2469242620(L_26, L_27, /*hidden argument*/NULL);
		Controller_t1937198888 * L_28 = __this->get_controller_2();
		Model_t873752437 * L_29 = L_28->get_model_2();
		Transform_t3275118058 * L_30 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		bool L_31 = Model_IsValidMapPosition_m2662347148(L_29, L_30, /*hidden argument*/NULL);
		if (L_31)
		{
			goto IL_0116;
		}
	}
	{
		Vector3_t2243707580 * L_32 = (&V_1);
		float L_33 = L_32->get_x_1();
		L_32->set_x_1(((float)((float)L_33-(float)(1.0f))));
		Transform_t3275118058 * L_34 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_35 = V_1;
		Transform_set_position_m2469242620(L_34, L_35, /*hidden argument*/NULL);
	}

IL_0116:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_36 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)273), /*hidden argument*/NULL);
		if (L_36)
		{
			goto IL_012c;
		}
	}
	{
		int32_t L_37 = ___direction0;
		if ((!(((uint32_t)L_37) == ((uint32_t)2))))
		{
			goto IL_0142;
		}
	}

IL_012c:
	{
		Controller_t1937198888 * L_38 = __this->get_controller_2();
		AudioManager_t4222704959 * L_39 = L_38->get_audioManager_6();
		AudioManager_PlayCtrlAudio_m2583450153(L_39, /*hidden argument*/NULL);
		Shape_ShapeRotate_m289486130(__this, /*hidden argument*/NULL);
	}

IL_0142:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_40 = Input_GetKeyDown_m1771960377(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (L_40)
		{
			goto IL_0159;
		}
	}
	{
		int32_t L_41 = ___direction0;
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)-2)))))
		{
			goto IL_0165;
		}
	}

IL_0159:
	{
		float L_42 = __this->get_stepUpTime_7();
		__this->set_stepTempTime_8(L_42);
	}

IL_0165:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1785128008_il2cpp_TypeInfo_var);
		bool L_43 = Input_GetKeyUp_m1008512962(NULL /*static, unused*/, ((int32_t)274), /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0180;
		}
	}
	{
		float L_44 = __this->get_stepTime_6();
		__this->set_stepTempTime_8(L_44);
	}

IL_0180:
	{
		return;
	}
}
// System.Void Shape::Fall()
extern "C"  void Shape_Fall_m2648392503 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	bool V_1 = false;
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t2243707580 * L_2 = (&V_0);
		float L_3 = L_2->get_y_2();
		L_2->set_y_2(((float)((float)L_3-(float)(1.0f))));
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = V_0;
		Transform_set_position_m2469242620(L_4, L_5, /*hidden argument*/NULL);
		Controller_t1937198888 * L_6 = __this->get_controller_2();
		Model_t873752437 * L_7 = L_6->get_model_2();
		Transform_t3275118058 * L_8 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		bool L_9 = Model_IsValidMapPosition_m2662347148(L_7, L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_00fb;
		}
	}
	{
		Vector3_t2243707580 * L_10 = (&V_0);
		float L_11 = L_10->get_y_2();
		L_10->set_y_2(((float)((float)L_11+(float)(1.0f))));
		Transform_t3275118058 * L_12 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_13 = V_0;
		Transform_set_position_m2469242620(L_12, L_13, /*hidden argument*/NULL);
		__this->set_isPause_4((bool)1);
		Controller_t1937198888 * L_14 = __this->get_controller_2();
		Model_t873752437 * L_15 = L_14->get_model_2();
		Transform_t3275118058 * L_16 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		bool L_17 = Model_SetBlockMap_m4073711356(L_15, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		bool L_18 = V_1;
		if (!L_18)
		{
			goto IL_0099;
		}
	}
	{
		Controller_t1937198888 * L_19 = __this->get_controller_2();
		AudioManager_t4222704959 * L_20 = L_19->get_audioManager_6();
		AudioManager_PlayDeletRowAudio_m3316311464(L_20, /*hidden argument*/NULL);
	}

IL_0099:
	{
		Controller_t1937198888 * L_21 = __this->get_controller_2();
		Model_t873752437 * L_22 = L_21->get_model_2();
		bool L_23 = L_22->get_isDataUpdate_8();
		if (!L_23)
		{
			goto IL_00de;
		}
	}
	{
		Controller_t1937198888 * L_24 = __this->get_controller_2();
		View_t854181575 * L_25 = L_24->get_view_3();
		Controller_t1937198888 * L_26 = __this->get_controller_2();
		Model_t873752437 * L_27 = L_26->get_model_2();
		int32_t L_28 = Model_get_Score_m1604402383(L_27, /*hidden argument*/NULL);
		Controller_t1937198888 * L_29 = __this->get_controller_2();
		Model_t873752437 * L_30 = L_29->get_model_2();
		int32_t L_31 = Model_get_HighScore_m635096529(L_30, /*hidden argument*/NULL);
		View_UpdateGameUI_m3712888353(L_25, L_28, L_31, /*hidden argument*/NULL);
	}

IL_00de:
	{
		GameManager_t2252321495 * L_32 = __this->get_gameManager_3();
		GameManager_set_CurrentShape_m115997281(L_32, (GameObject_t1756533147 *)NULL, /*hidden argument*/NULL);
		Controller_t1937198888 * L_33 = __this->get_controller_2();
		GameManager_t2252321495 * L_34 = L_33->get_gameManager_5();
		GameManager_IsGameOver_m844888474(L_34, /*hidden argument*/NULL);
		return;
	}

IL_00fb:
	{
		Controller_t1937198888 * L_35 = __this->get_controller_2();
		AudioManager_t4222704959 * L_36 = L_35->get_audioManager_6();
		AudioManager_PlayFallAudio_m2178460935(L_36, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shape::ShapeRotate()
extern "C"  void Shape_ShapeRotate_m289486130 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Shape_ShapeRotate_m289486130_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Transform_Find_m3323476454(L_1, _stringLiteral2838555398, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Component_get_transform_m2697483695(L_2, /*hidden argument*/NULL);
		Vector3_t2243707580  L_4 = Transform_get_position_m1104419803(L_3, /*hidden argument*/NULL);
		Vector3_t2243707580  L_5 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		Transform_RotateAround_m3410686872(L_0, L_4, L_5, (90.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Shape::StartShapeFall()
extern "C"  void Shape_StartShapeFall_m3725894794 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	{
		__this->set_isPause_4((bool)0);
		return;
	}
}
// System.Void Shape::StopShapeFall()
extern "C"  void Shape_StopShapeFall_m1202009304 (Shape_t1303907469 * __this, const MethodInfo* method)
{
	{
		__this->set_isPause_4((bool)1);
		return;
	}
}
// UnityEngine.Vector3 Vector3Extension::Vector3Round(UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3Extension_Vector3Round_m4246609207 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  ___v0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Vector3Extension_Vector3Round_m4246609207_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = (&___v0)->get_x_1();
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t2336485820_il2cpp_TypeInfo_var);
		int32_t L_1 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		(&___v0)->set_x_1((((float)((float)L_1))));
		float L_2 = (&___v0)->get_y_2();
		int32_t L_3 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		(&___v0)->set_y_2((((float)((float)L_3))));
		float L_4 = (&___v0)->get_z_3();
		int32_t L_5 = Mathf_RoundToInt_m2927198556(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		(&___v0)->set_z_3((((float)((float)L_5))));
		Vector3_t2243707580  L_6 = ___v0;
		return L_6;
	}
}
// System.Void View::.ctor()
extern "C"  void View__ctor_m2968696088 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::Start()
extern "C"  void View_Start_m3247243028 (View_t854181575 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (View_Start_m3247243028_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_1 = Transform_Find_m3323476454(L_0, _stringLiteral373485295, /*hidden argument*/NULL);
		__this->set_logoName_2(((RectTransform_t3349966182 *)IsInstSealed(L_1, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_2 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_3 = Transform_Find_m3323476454(L_2, _stringLiteral1863692018, /*hidden argument*/NULL);
		__this->set_menuUI_3(((RectTransform_t3349966182 *)IsInstSealed(L_3, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_4 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_5 = Transform_Find_m3323476454(L_4, _stringLiteral2447773569, /*hidden argument*/NULL);
		__this->set_gameUI_4(((RectTransform_t3349966182 *)IsInstSealed(L_5, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Transform_Find_m3323476454(L_6, _stringLiteral3183887342, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_8 = Component_get_gameObject_m3105766835(L_7, /*hidden argument*/NULL);
		__this->set_ReStartButton_9(L_8);
		Transform_t3275118058 * L_9 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_10 = Transform_Find_m3323476454(L_9, _stringLiteral1933155027, /*hidden argument*/NULL);
		__this->set_gameOverUI_5(((RectTransform_t3349966182 *)IsInstSealed(L_10, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Transform_Find_m3323476454(L_11, _stringLiteral1933155027, /*hidden argument*/NULL);
		Transform_t3275118058 * L_13 = Transform_Find_m3323476454(L_12, _stringLiteral4045373120, /*hidden argument*/NULL);
		Text_t356221433 * L_14 = Component_GetComponent_TisText_t356221433_m1342661039(L_13, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_gameOverScore_12(L_14);
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_16 = Transform_Find_m3323476454(L_15, _stringLiteral3358479857, /*hidden argument*/NULL);
		__this->set_settingUI_6(((RectTransform_t3349966182 *)IsInstSealed(L_16, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_17 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_18 = Transform_Find_m3323476454(L_17, _stringLiteral3778205194, /*hidden argument*/NULL);
		GameObject_t1756533147 * L_19 = Component_get_gameObject_m3105766835(L_18, /*hidden argument*/NULL);
		__this->set_audioMute_7(L_19);
		Transform_t3275118058 * L_20 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_21 = Transform_Find_m3323476454(L_20, _stringLiteral2029194541, /*hidden argument*/NULL);
		__this->set_rankUI_8(((RectTransform_t3349966182 *)IsInstSealed(L_21, RectTransform_t3349966182_il2cpp_TypeInfo_var)));
		Transform_t3275118058 * L_22 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_23 = Transform_Find_m3323476454(L_22, _stringLiteral3676749550, /*hidden argument*/NULL);
		Text_t356221433 * L_24 = Component_GetComponent_TisText_t356221433_m1342661039(L_23, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_score_10(L_24);
		Transform_t3275118058 * L_25 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Transform_t3275118058 * L_26 = Transform_Find_m3323476454(L_25, _stringLiteral3060889074, /*hidden argument*/NULL);
		Text_t356221433 * L_27 = Component_GetComponent_TisText_t356221433_m1342661039(L_26, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		__this->set_highScore_11(L_27);
		return;
	}
}
// System.Void View::ShowMenu()
extern "C"  void View_ShowMenu_m110806820 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_logoName_2();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_2 = __this->get_logoName_2();
		ShortcutExtensions46_DOAnchorPosY_m1128768801(NULL /*static, unused*/, L_2, (-128.0f), (0.8f), (bool)0, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_3 = __this->get_menuUI_3();
		GameObject_t1756533147 * L_4 = Component_get_gameObject_m3105766835(L_3, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_4, (bool)1, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_5 = __this->get_menuUI_3();
		ShortcutExtensions46_DOAnchorPosY_m1128768801(NULL /*static, unused*/, L_5, (53.0f), (0.8f), (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::HideMenu()
extern "C"  void View_HideMenu_m3662219881 (View_t854181575 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (View_HideMenu_m3662219881_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = __this->get_logoName_2();
		Tweener_t760404022 * L_1 = ShortcutExtensions46_DOAnchorPosY_m1128768801(NULL /*static, unused*/, L_0, (119.0f), (0.8f), (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)View_U3CHideMenuU3Em__0_m60288546_MethodInfo_var);
		TweenCallback_t3697142134 * L_3 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_3, __this, L_2, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386_MethodInfo_var);
		RectTransform_t3349966182 * L_4 = __this->get_menuUI_3();
		Tweener_t760404022 * L_5 = ShortcutExtensions46_DOAnchorPosY_m1128768801(NULL /*static, unused*/, L_4, (-121.0f), (0.8f), (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_6;
		L_6.set_m_value_0((void*)(void*)View_U3CHideMenuU3Em__1_m1606169511_MethodInfo_var);
		TweenCallback_t3697142134 * L_7 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_7, __this, L_6, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386_MethodInfo_var);
		return;
	}
}
// System.Void View::ShowGameUI(System.Int32,System.Int32)
extern "C"  void View_ShowGameUI_m3358246707 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_score_10();
		String_t* L_1 = Int32_ToString_m2960866144((&___score0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		Text_t356221433 * L_2 = __this->get_highScore_11();
		String_t* L_3 = Int32_ToString_m2960866144((&___highScore1), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		RectTransform_t3349966182 * L_4 = __this->get_gameUI_4();
		GameObject_t1756533147 * L_5 = Component_get_gameObject_m3105766835(L_4, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_5, (bool)1, /*hidden argument*/NULL);
		RectTransform_t3349966182 * L_6 = __this->get_gameUI_4();
		ShortcutExtensions46_DOAnchorPosY_m1128768801(NULL /*static, unused*/, L_6, (-95.5f), (0.8f), (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::UpdateGameUI(System.Int32,System.Int32)
extern "C"  void View_UpdateGameUI_m3712888353 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, const MethodInfo* method)
{
	{
		Text_t356221433 * L_0 = __this->get_score_10();
		String_t* L_1 = Int32_ToString_m2960866144((&___score0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_1);
		Text_t356221433 * L_2 = __this->get_highScore_11();
		String_t* L_3 = Int32_ToString_m2960866144((&___highScore1), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		return;
	}
}
// System.Void View::HideGameUI()
extern "C"  void View_HideGameUI_m3415331198 (View_t854181575 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (View_HideGameUI_m3415331198_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = __this->get_gameUI_4();
		Tweener_t760404022 * L_1 = ShortcutExtensions46_DOAnchorPosY_m1128768801(NULL /*static, unused*/, L_0, (107.25f), (0.8f), (bool)0, /*hidden argument*/NULL);
		IntPtr_t L_2;
		L_2.set_m_value_0((void*)(void*)View_U3CHideGameUIU3Em__2_m3610395925_MethodInfo_var);
		TweenCallback_t3697142134 * L_3 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_3, __this, L_2, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweener_t760404022_m67810386_MethodInfo_var);
		return;
	}
}
// System.Void View::ShowReStartButton()
extern "C"  void View_ShowReStartButton_m2596324120 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_ReStartButton_9();
		GameObject_SetActive_m2887581199(L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::HideReStartButton()
extern "C"  void View_HideReStartButton_m3939664307 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_ReStartButton_9();
		GameObject_SetActive_m2887581199(L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::ShowGameOverUI(System.Int32)
extern "C"  void View_ShowGameOverUI_m3030614956 (View_t854181575 * __this, int32_t ___score0, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_gameOverUI_5();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		Text_t356221433 * L_2 = __this->get_gameOverScore_12();
		String_t* L_3 = Int32_ToString_m2960866144((&___score0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		return;
	}
}
// System.Void View::HideGameOverUI()
extern "C"  void View_HideGameOverUI_m516871344 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_gameOverUI_5();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::ShowSettingUI()
extern "C"  void View_ShowSettingUI_m4255451743 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_settingUI_6();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::HideSettingUI()
extern "C"  void View_HideSettingUI_m2244255162 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_settingUI_6();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::IsShowAudioLine(System.Boolean)
extern "C"  void View_IsShowAudioLine_m2780770722 (View_t854181575 * __this, bool ___flag0, const MethodInfo* method)
{
	{
		GameObject_t1756533147 * L_0 = __this->get_audioMute_7();
		bool L_1 = ___flag0;
		GameObject_SetActive_m2887581199(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::ShowRankUI(System.Int32,System.Int32,System.Int32)
extern "C"  void View_ShowRankUI_m2821821284 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, int32_t ___numberGames2, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_rankUI_8();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)1, /*hidden argument*/NULL);
		int32_t L_2 = ___score0;
		int32_t L_3 = ___highScore1;
		int32_t L_4 = ___numberGames2;
		View_ShowRankData_m4259729890(__this, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::HideRankUI()
extern "C"  void View_HideRankUI_m2187104746 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_rankUI_8();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::ShowRankData(System.Int32,System.Int32,System.Int32)
extern "C"  void View_ShowRankData_m4259729890 (View_t854181575 * __this, int32_t ___score0, int32_t ___highScore1, int32_t ___numberGames2, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (View_ShowRankData_m4259729890_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RectTransform_t3349966182 * L_0 = __this->get_rankUI_8();
		Transform_t3275118058 * L_1 = Component_get_transform_m2697483695(L_0, /*hidden argument*/NULL);
		Transform_t3275118058 * L_2 = Transform_Find_m3323476454(L_1, _stringLiteral3360931944, /*hidden argument*/NULL);
		Text_t356221433 * L_3 = Component_GetComponent_TisText_t356221433_m1342661039(L_2, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_4 = Int32_ToString_m2960866144((&___score0), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_3, L_4);
		RectTransform_t3349966182 * L_5 = __this->get_rankUI_8();
		Transform_t3275118058 * L_6 = Component_get_transform_m2697483695(L_5, /*hidden argument*/NULL);
		Transform_t3275118058 * L_7 = Transform_Find_m3323476454(L_6, _stringLiteral655579034, /*hidden argument*/NULL);
		Text_t356221433 * L_8 = Component_GetComponent_TisText_t356221433_m1342661039(L_7, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_9 = Int32_ToString_m2960866144((&___highScore1), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_8, L_9);
		RectTransform_t3349966182 * L_10 = __this->get_rankUI_8();
		Transform_t3275118058 * L_11 = Component_get_transform_m2697483695(L_10, /*hidden argument*/NULL);
		Transform_t3275118058 * L_12 = Transform_Find_m3323476454(L_11, _stringLiteral2256806354, /*hidden argument*/NULL);
		Text_t356221433 * L_13 = Component_GetComponent_TisText_t356221433_m1342661039(L_12, /*hidden argument*/Component_GetComponent_TisText_t356221433_m1342661039_MethodInfo_var);
		String_t* L_14 = Int32_ToString_m2960866144((&___numberGames2), /*hidden argument*/NULL);
		VirtActionInvoker1< String_t* >::Invoke(72 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_13, L_14);
		return;
	}
}
// System.Void View::<HideMenu>m__0()
extern "C"  void View_U3CHideMenuU3Em__0_m60288546 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_logoName_2();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::<HideMenu>m__1()
extern "C"  void View_U3CHideMenuU3Em__1_m1606169511 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_menuUI_3();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void View::<HideGameUI>m__2()
extern "C"  void View_U3CHideGameUIU3Em__2_m3610395925 (View_t854181575 * __this, const MethodInfo* method)
{
	{
		RectTransform_t3349966182 * L_0 = __this->get_gameUI_4();
		GameObject_t1756533147 * L_1 = Component_get_gameObject_m3105766835(L_0, /*hidden argument*/NULL);
		GameObject_SetActive_m2887581199(L_1, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
