﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Controller
struct Controller_t1937198888;
// GameManager
struct GameManager_t2252321495;
// UnityEngine.GameObject
struct GameObject_t1756533147;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shape
struct  Shape_t1303907469  : public MonoBehaviour_t1158329972
{
public:
	// Controller Shape::controller
	Controller_t1937198888 * ___controller_2;
	// GameManager Shape::gameManager
	GameManager_t2252321495 * ___gameManager_3;
	// System.Boolean Shape::isPause
	bool ___isPause_4;
	// System.Single Shape::timer
	float ___timer_5;
	// System.Single Shape::stepTime
	float ___stepTime_6;
	// System.Single Shape::stepUpTime
	float ___stepUpTime_7;
	// System.Single Shape::stepTempTime
	float ___stepTempTime_8;
	// UnityEngine.GameObject Shape::Pivot
	GameObject_t1756533147 * ___Pivot_9;
	// System.Boolean Shape::startPosFlag
	bool ___startPosFlag_10;
	// System.Int32 Shape::backValue
	int32_t ___backValue_11;

public:
	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___controller_2)); }
	inline Controller_t1937198888 * get_controller_2() const { return ___controller_2; }
	inline Controller_t1937198888 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(Controller_t1937198888 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___controller_2, value);
	}

	inline static int32_t get_offset_of_gameManager_3() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___gameManager_3)); }
	inline GameManager_t2252321495 * get_gameManager_3() const { return ___gameManager_3; }
	inline GameManager_t2252321495 ** get_address_of_gameManager_3() { return &___gameManager_3; }
	inline void set_gameManager_3(GameManager_t2252321495 * value)
	{
		___gameManager_3 = value;
		Il2CppCodeGenWriteBarrier(&___gameManager_3, value);
	}

	inline static int32_t get_offset_of_isPause_4() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___isPause_4)); }
	inline bool get_isPause_4() const { return ___isPause_4; }
	inline bool* get_address_of_isPause_4() { return &___isPause_4; }
	inline void set_isPause_4(bool value)
	{
		___isPause_4 = value;
	}

	inline static int32_t get_offset_of_timer_5() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___timer_5)); }
	inline float get_timer_5() const { return ___timer_5; }
	inline float* get_address_of_timer_5() { return &___timer_5; }
	inline void set_timer_5(float value)
	{
		___timer_5 = value;
	}

	inline static int32_t get_offset_of_stepTime_6() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___stepTime_6)); }
	inline float get_stepTime_6() const { return ___stepTime_6; }
	inline float* get_address_of_stepTime_6() { return &___stepTime_6; }
	inline void set_stepTime_6(float value)
	{
		___stepTime_6 = value;
	}

	inline static int32_t get_offset_of_stepUpTime_7() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___stepUpTime_7)); }
	inline float get_stepUpTime_7() const { return ___stepUpTime_7; }
	inline float* get_address_of_stepUpTime_7() { return &___stepUpTime_7; }
	inline void set_stepUpTime_7(float value)
	{
		___stepUpTime_7 = value;
	}

	inline static int32_t get_offset_of_stepTempTime_8() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___stepTempTime_8)); }
	inline float get_stepTempTime_8() const { return ___stepTempTime_8; }
	inline float* get_address_of_stepTempTime_8() { return &___stepTempTime_8; }
	inline void set_stepTempTime_8(float value)
	{
		___stepTempTime_8 = value;
	}

	inline static int32_t get_offset_of_Pivot_9() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___Pivot_9)); }
	inline GameObject_t1756533147 * get_Pivot_9() const { return ___Pivot_9; }
	inline GameObject_t1756533147 ** get_address_of_Pivot_9() { return &___Pivot_9; }
	inline void set_Pivot_9(GameObject_t1756533147 * value)
	{
		___Pivot_9 = value;
		Il2CppCodeGenWriteBarrier(&___Pivot_9, value);
	}

	inline static int32_t get_offset_of_startPosFlag_10() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___startPosFlag_10)); }
	inline bool get_startPosFlag_10() const { return ___startPosFlag_10; }
	inline bool* get_address_of_startPosFlag_10() { return &___startPosFlag_10; }
	inline void set_startPosFlag_10(bool value)
	{
		___startPosFlag_10 = value;
	}

	inline static int32_t get_offset_of_backValue_11() { return static_cast<int32_t>(offsetof(Shape_t1303907469, ___backValue_11)); }
	inline int32_t get_backValue_11() const { return ___backValue_11; }
	inline int32_t* get_address_of_backValue_11() { return &___backValue_11; }
	inline void set_backValue_11(int32_t value)
	{
		___backValue_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
