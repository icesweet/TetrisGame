﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.RectTransform
struct RectTransform_t3349966182;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// UnityEngine.UI.Text
struct Text_t356221433;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// View
struct  View_t854181575  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.RectTransform View::logoName
	RectTransform_t3349966182 * ___logoName_2;
	// UnityEngine.RectTransform View::menuUI
	RectTransform_t3349966182 * ___menuUI_3;
	// UnityEngine.RectTransform View::gameUI
	RectTransform_t3349966182 * ___gameUI_4;
	// UnityEngine.RectTransform View::gameOverUI
	RectTransform_t3349966182 * ___gameOverUI_5;
	// UnityEngine.RectTransform View::settingUI
	RectTransform_t3349966182 * ___settingUI_6;
	// UnityEngine.GameObject View::audioMute
	GameObject_t1756533147 * ___audioMute_7;
	// UnityEngine.RectTransform View::rankUI
	RectTransform_t3349966182 * ___rankUI_8;
	// UnityEngine.GameObject View::ReStartButton
	GameObject_t1756533147 * ___ReStartButton_9;
	// UnityEngine.UI.Text View::score
	Text_t356221433 * ___score_10;
	// UnityEngine.UI.Text View::highScore
	Text_t356221433 * ___highScore_11;
	// UnityEngine.UI.Text View::gameOverScore
	Text_t356221433 * ___gameOverScore_12;

public:
	inline static int32_t get_offset_of_logoName_2() { return static_cast<int32_t>(offsetof(View_t854181575, ___logoName_2)); }
	inline RectTransform_t3349966182 * get_logoName_2() const { return ___logoName_2; }
	inline RectTransform_t3349966182 ** get_address_of_logoName_2() { return &___logoName_2; }
	inline void set_logoName_2(RectTransform_t3349966182 * value)
	{
		___logoName_2 = value;
		Il2CppCodeGenWriteBarrier(&___logoName_2, value);
	}

	inline static int32_t get_offset_of_menuUI_3() { return static_cast<int32_t>(offsetof(View_t854181575, ___menuUI_3)); }
	inline RectTransform_t3349966182 * get_menuUI_3() const { return ___menuUI_3; }
	inline RectTransform_t3349966182 ** get_address_of_menuUI_3() { return &___menuUI_3; }
	inline void set_menuUI_3(RectTransform_t3349966182 * value)
	{
		___menuUI_3 = value;
		Il2CppCodeGenWriteBarrier(&___menuUI_3, value);
	}

	inline static int32_t get_offset_of_gameUI_4() { return static_cast<int32_t>(offsetof(View_t854181575, ___gameUI_4)); }
	inline RectTransform_t3349966182 * get_gameUI_4() const { return ___gameUI_4; }
	inline RectTransform_t3349966182 ** get_address_of_gameUI_4() { return &___gameUI_4; }
	inline void set_gameUI_4(RectTransform_t3349966182 * value)
	{
		___gameUI_4 = value;
		Il2CppCodeGenWriteBarrier(&___gameUI_4, value);
	}

	inline static int32_t get_offset_of_gameOverUI_5() { return static_cast<int32_t>(offsetof(View_t854181575, ___gameOverUI_5)); }
	inline RectTransform_t3349966182 * get_gameOverUI_5() const { return ___gameOverUI_5; }
	inline RectTransform_t3349966182 ** get_address_of_gameOverUI_5() { return &___gameOverUI_5; }
	inline void set_gameOverUI_5(RectTransform_t3349966182 * value)
	{
		___gameOverUI_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverUI_5, value);
	}

	inline static int32_t get_offset_of_settingUI_6() { return static_cast<int32_t>(offsetof(View_t854181575, ___settingUI_6)); }
	inline RectTransform_t3349966182 * get_settingUI_6() const { return ___settingUI_6; }
	inline RectTransform_t3349966182 ** get_address_of_settingUI_6() { return &___settingUI_6; }
	inline void set_settingUI_6(RectTransform_t3349966182 * value)
	{
		___settingUI_6 = value;
		Il2CppCodeGenWriteBarrier(&___settingUI_6, value);
	}

	inline static int32_t get_offset_of_audioMute_7() { return static_cast<int32_t>(offsetof(View_t854181575, ___audioMute_7)); }
	inline GameObject_t1756533147 * get_audioMute_7() const { return ___audioMute_7; }
	inline GameObject_t1756533147 ** get_address_of_audioMute_7() { return &___audioMute_7; }
	inline void set_audioMute_7(GameObject_t1756533147 * value)
	{
		___audioMute_7 = value;
		Il2CppCodeGenWriteBarrier(&___audioMute_7, value);
	}

	inline static int32_t get_offset_of_rankUI_8() { return static_cast<int32_t>(offsetof(View_t854181575, ___rankUI_8)); }
	inline RectTransform_t3349966182 * get_rankUI_8() const { return ___rankUI_8; }
	inline RectTransform_t3349966182 ** get_address_of_rankUI_8() { return &___rankUI_8; }
	inline void set_rankUI_8(RectTransform_t3349966182 * value)
	{
		___rankUI_8 = value;
		Il2CppCodeGenWriteBarrier(&___rankUI_8, value);
	}

	inline static int32_t get_offset_of_ReStartButton_9() { return static_cast<int32_t>(offsetof(View_t854181575, ___ReStartButton_9)); }
	inline GameObject_t1756533147 * get_ReStartButton_9() const { return ___ReStartButton_9; }
	inline GameObject_t1756533147 ** get_address_of_ReStartButton_9() { return &___ReStartButton_9; }
	inline void set_ReStartButton_9(GameObject_t1756533147 * value)
	{
		___ReStartButton_9 = value;
		Il2CppCodeGenWriteBarrier(&___ReStartButton_9, value);
	}

	inline static int32_t get_offset_of_score_10() { return static_cast<int32_t>(offsetof(View_t854181575, ___score_10)); }
	inline Text_t356221433 * get_score_10() const { return ___score_10; }
	inline Text_t356221433 ** get_address_of_score_10() { return &___score_10; }
	inline void set_score_10(Text_t356221433 * value)
	{
		___score_10 = value;
		Il2CppCodeGenWriteBarrier(&___score_10, value);
	}

	inline static int32_t get_offset_of_highScore_11() { return static_cast<int32_t>(offsetof(View_t854181575, ___highScore_11)); }
	inline Text_t356221433 * get_highScore_11() const { return ___highScore_11; }
	inline Text_t356221433 ** get_address_of_highScore_11() { return &___highScore_11; }
	inline void set_highScore_11(Text_t356221433 * value)
	{
		___highScore_11 = value;
		Il2CppCodeGenWriteBarrier(&___highScore_11, value);
	}

	inline static int32_t get_offset_of_gameOverScore_12() { return static_cast<int32_t>(offsetof(View_t854181575, ___gameOverScore_12)); }
	inline Text_t356221433 * get_gameOverScore_12() const { return ___gameOverScore_12; }
	inline Text_t356221433 ** get_address_of_gameOverScore_12() { return &___gameOverScore_12; }
	inline void set_gameOverScore_12(Text_t356221433 * value)
	{
		___gameOverScore_12 = value;
		Il2CppCodeGenWriteBarrier(&___gameOverScore_12, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
