﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_Object2689449295.h"
#include "AssemblyU2DCSharp_StateID3221758012.h"

// System.Collections.Generic.List`1<FSMState>
struct List_1_t2478928041;
// FSMState
struct FSMState_t3109806909;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FSMSystem
struct  FSMSystem_t2363122359  : public Il2CppObject
{
public:
	// System.Collections.Generic.List`1<FSMState> FSMSystem::states
	List_1_t2478928041 * ___states_0;
	// StateID FSMSystem::currentStateID
	int32_t ___currentStateID_1;
	// FSMState FSMSystem::currentState
	FSMState_t3109806909 * ___currentState_2;

public:
	inline static int32_t get_offset_of_states_0() { return static_cast<int32_t>(offsetof(FSMSystem_t2363122359, ___states_0)); }
	inline List_1_t2478928041 * get_states_0() const { return ___states_0; }
	inline List_1_t2478928041 ** get_address_of_states_0() { return &___states_0; }
	inline void set_states_0(List_1_t2478928041 * value)
	{
		___states_0 = value;
		Il2CppCodeGenWriteBarrier(&___states_0, value);
	}

	inline static int32_t get_offset_of_currentStateID_1() { return static_cast<int32_t>(offsetof(FSMSystem_t2363122359, ___currentStateID_1)); }
	inline int32_t get_currentStateID_1() const { return ___currentStateID_1; }
	inline int32_t* get_address_of_currentStateID_1() { return &___currentStateID_1; }
	inline void set_currentStateID_1(int32_t value)
	{
		___currentStateID_1 = value;
	}

	inline static int32_t get_offset_of_currentState_2() { return static_cast<int32_t>(offsetof(FSMSystem_t2363122359, ___currentState_2)); }
	inline FSMState_t3109806909 * get_currentState_2() const { return ___currentState_2; }
	inline FSMState_t3109806909 ** get_address_of_currentState_2() { return &___currentState_2; }
	inline void set_currentState_2(FSMState_t3109806909 * value)
	{
		___currentState_2 = value;
		Il2CppCodeGenWriteBarrier(&___currentState_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
