﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_DOVirtual1722510550.h"
#include "DOTween_DG_Tweening_DOVirtual_U3CU3Ec__DisplayClas3724530167.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "DOTween_DG_Tweening_EaseFactory4203735666.h"
#include "DOTween_DG_Tweening_EaseFactory_U3CU3Ec__DisplayCl2066423765.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "DOTween_DG_Tweening_RotateMode1177727514.h"
#include "DOTween_DG_Tweening_ScrambleMode385206138.h"
#include "DOTween_DG_Tweening_TweenExtensions405253783.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_Sequence110643099.h"
#include "DOTween_DG_Tweening_ShortcutExtensions3524050470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842963.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842866.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843029.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842932.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842831.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842734.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842897.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964842800.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843227.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis964843130.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978348.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691431.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653346.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366429.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628352.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341435.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303350.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678340.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391423.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978441.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691524.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653439.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366522.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628445.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341528.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303443.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016526.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678433.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391516.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978538.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691621.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653536.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366619.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628542.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341625.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303540.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016623.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678530.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391613.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978383.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691466.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653381.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366464.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628387.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341470.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303385.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016468.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678375.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391458.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978224.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691307.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653222.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366305.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628228.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341311.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303226.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678216.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391299.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978317.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691400.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653315.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366398.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628321.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341404.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303319.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016402.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678309.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391392.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978414.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691497.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653412.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1142366495.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3575628418.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1989341501.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3293303416.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1707016499.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1881678406.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Dis295391489.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di3010978755.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di1424691838.h"
#include "DOTween_DG_Tweening_ShortcutExtensions_U3CU3Ec__Di2728653753.h"
#include "DOTween_DG_Tweening_TweenParams2944325381.h"
#include "DOTween_DG_Tweening_TweenSettingsExtensions2285462830.h"
#include "DOTween_DG_Tweening_LogBehaviour3505725029.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1500 = { sizeof (DOVirtual_t1722510550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1501 = { sizeof (U3CU3Ec__DisplayClass0_0_t3724530167), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1501[2] = 
{
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_val_0(),
	U3CU3Ec__DisplayClass0_0_t3724530167::get_offset_of_onVirtualUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1502 = { sizeof (Ease_t2502520296)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1502[39] = 
{
	Ease_t2502520296::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1503 = { sizeof (EaseFactory_t4203735666), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1504 = { sizeof (U3CU3Ec__DisplayClass2_0_t2066423765), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1504[2] = 
{
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_motionDelay_0(),
	U3CU3Ec__DisplayClass2_0_t2066423765::get_offset_of_customEase_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1505 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1506 = { sizeof (PathMode_t1545785466)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1506[5] = 
{
	PathMode_t1545785466::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1507 = { sizeof (PathType_t2815988833)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1507[3] = 
{
	PathType_t2815988833::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1508 = { sizeof (RotateMode_t1177727514)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1508[5] = 
{
	RotateMode_t1177727514::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1509 = { sizeof (ScrambleMode_t385206138)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1509[7] = 
{
	ScrambleMode_t385206138::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1510 = { sizeof (TweenExtensions_t405253783), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1511 = { sizeof (LoopType_t2249218064)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1511[4] = 
{
	LoopType_t2249218064::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1512 = { sizeof (Sequence_t110643099), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1512[3] = 
{
	Sequence_t110643099::get_offset_of_sequencedTweens_51(),
	Sequence_t110643099::get_offset_of__sequencedObjs_52(),
	Sequence_t110643099::get_offset_of_lastTweenInsertTime_53(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1513 = { sizeof (ShortcutExtensions_t3524050470), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1514 = { sizeof (U3CU3Ec__DisplayClass0_0_t964842963), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1514[1] = 
{
	U3CU3Ec__DisplayClass0_0_t964842963::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1515 = { sizeof (U3CU3Ec__DisplayClass1_0_t964842866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1515[1] = 
{
	U3CU3Ec__DisplayClass1_0_t964842866::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1516 = { sizeof (U3CU3Ec__DisplayClass2_0_t964843029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1516[1] = 
{
	U3CU3Ec__DisplayClass2_0_t964843029::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1517 = { sizeof (U3CU3Ec__DisplayClass3_0_t964842932), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1517[1] = 
{
	U3CU3Ec__DisplayClass3_0_t964842932::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1518 = { sizeof (U3CU3Ec__DisplayClass4_0_t964842831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1518[1] = 
{
	U3CU3Ec__DisplayClass4_0_t964842831::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1519 = { sizeof (U3CU3Ec__DisplayClass5_0_t964842734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1519[1] = 
{
	U3CU3Ec__DisplayClass5_0_t964842734::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1520 = { sizeof (U3CU3Ec__DisplayClass6_0_t964842897), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1520[1] = 
{
	U3CU3Ec__DisplayClass6_0_t964842897::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1521 = { sizeof (U3CU3Ec__DisplayClass7_0_t964842800), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1521[1] = 
{
	U3CU3Ec__DisplayClass7_0_t964842800::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1522 = { sizeof (U3CU3Ec__DisplayClass8_0_t964843227), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1522[1] = 
{
	U3CU3Ec__DisplayClass8_0_t964843227::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1523 = { sizeof (U3CU3Ec__DisplayClass9_0_t964843130), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1523[1] = 
{
	U3CU3Ec__DisplayClass9_0_t964843130::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1524 = { sizeof (U3CU3Ec__DisplayClass10_0_t3010978348), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1524[1] = 
{
	U3CU3Ec__DisplayClass10_0_t3010978348::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1525 = { sizeof (U3CU3Ec__DisplayClass11_0_t1424691431), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1525[1] = 
{
	U3CU3Ec__DisplayClass11_0_t1424691431::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1526 = { sizeof (U3CU3Ec__DisplayClass12_0_t2728653346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1526[1] = 
{
	U3CU3Ec__DisplayClass12_0_t2728653346::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1527 = { sizeof (U3CU3Ec__DisplayClass13_0_t1142366429), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1527[1] = 
{
	U3CU3Ec__DisplayClass13_0_t1142366429::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1528 = { sizeof (U3CU3Ec__DisplayClass14_0_t3575628352), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1528[1] = 
{
	U3CU3Ec__DisplayClass14_0_t3575628352::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1529 = { sizeof (U3CU3Ec__DisplayClass15_0_t1989341435), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1529[1] = 
{
	U3CU3Ec__DisplayClass15_0_t1989341435::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1530 = { sizeof (U3CU3Ec__DisplayClass16_0_t3293303350), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1530[1] = 
{
	U3CU3Ec__DisplayClass16_0_t3293303350::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1531 = { sizeof (U3CU3Ec__DisplayClass17_0_t1707016433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1531[2] = 
{
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_startValue_0(),
	U3CU3Ec__DisplayClass17_0_t1707016433::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1532 = { sizeof (U3CU3Ec__DisplayClass18_0_t1881678340), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1532[1] = 
{
	U3CU3Ec__DisplayClass18_0_t1881678340::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1533 = { sizeof (U3CU3Ec__DisplayClass19_0_t295391423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1533[2] = 
{
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass19_0_t295391423::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1534 = { sizeof (U3CU3Ec__DisplayClass20_0_t3010978441), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1534[1] = 
{
	U3CU3Ec__DisplayClass20_0_t3010978441::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1535 = { sizeof (U3CU3Ec__DisplayClass21_0_t1424691524), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1535[2] = 
{
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass21_0_t1424691524::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1536 = { sizeof (U3CU3Ec__DisplayClass22_0_t2728653439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1536[2] = 
{
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass22_0_t2728653439::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1537 = { sizeof (U3CU3Ec__DisplayClass23_0_t1142366522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1537[1] = 
{
	U3CU3Ec__DisplayClass23_0_t1142366522::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1538 = { sizeof (U3CU3Ec__DisplayClass24_0_t3575628445), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1538[2] = 
{
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass24_0_t3575628445::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1539 = { sizeof (U3CU3Ec__DisplayClass25_0_t1989341528), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1539[1] = 
{
	U3CU3Ec__DisplayClass25_0_t1989341528::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1540 = { sizeof (U3CU3Ec__DisplayClass26_0_t3293303443), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1540[2] = 
{
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass26_0_t3293303443::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1541 = { sizeof (U3CU3Ec__DisplayClass27_0_t1707016526), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1541[2] = 
{
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass27_0_t1707016526::get_offset_of_property_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1542 = { sizeof (U3CU3Ec__DisplayClass28_0_t1881678433), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1542[1] = 
{
	U3CU3Ec__DisplayClass28_0_t1881678433::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1543 = { sizeof (U3CU3Ec__DisplayClass29_0_t295391516), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1543[1] = 
{
	U3CU3Ec__DisplayClass29_0_t295391516::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1544 = { sizeof (U3CU3Ec__DisplayClass30_0_t3010978538), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1544[1] = 
{
	U3CU3Ec__DisplayClass30_0_t3010978538::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1545 = { sizeof (U3CU3Ec__DisplayClass31_0_t1424691621), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1545[1] = 
{
	U3CU3Ec__DisplayClass31_0_t1424691621::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1546 = { sizeof (U3CU3Ec__DisplayClass32_0_t2728653536), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1546[1] = 
{
	U3CU3Ec__DisplayClass32_0_t2728653536::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1547 = { sizeof (U3CU3Ec__DisplayClass33_0_t1142366619), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1547[1] = 
{
	U3CU3Ec__DisplayClass33_0_t1142366619::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1548 = { sizeof (U3CU3Ec__DisplayClass34_0_t3575628542), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1548[6] = 
{
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass34_0_t3575628542::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1549 = { sizeof (U3CU3Ec__DisplayClass35_0_t1989341625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1549[1] = 
{
	U3CU3Ec__DisplayClass35_0_t1989341625::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1550 = { sizeof (U3CU3Ec__DisplayClass36_0_t3293303540), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1550[2] = 
{
	U3CU3Ec__DisplayClass36_0_t3293303540::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass36_0_t3293303540::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1551 = { sizeof (U3CU3Ec__DisplayClass37_0_t1707016623), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1551[1] = 
{
	U3CU3Ec__DisplayClass37_0_t1707016623::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1552 = { sizeof (U3CU3Ec__DisplayClass38_0_t1881678530), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1552[2] = 
{
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_trans_0(),
	U3CU3Ec__DisplayClass38_0_t1881678530::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1553 = { sizeof (U3CU3Ec__DisplayClass39_0_t295391613), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1553[1] = 
{
	U3CU3Ec__DisplayClass39_0_t295391613::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1554 = { sizeof (U3CU3Ec__DisplayClass40_0_t3010978383), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1554[1] = 
{
	U3CU3Ec__DisplayClass40_0_t3010978383::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1555 = { sizeof (U3CU3Ec__DisplayClass41_0_t1424691466), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1555[1] = 
{
	U3CU3Ec__DisplayClass41_0_t1424691466::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1556 = { sizeof (U3CU3Ec__DisplayClass42_0_t2728653381), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1556[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2728653381::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1557 = { sizeof (U3CU3Ec__DisplayClass43_0_t1142366464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1557[1] = 
{
	U3CU3Ec__DisplayClass43_0_t1142366464::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1558 = { sizeof (U3CU3Ec__DisplayClass44_0_t3575628387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1558[1] = 
{
	U3CU3Ec__DisplayClass44_0_t3575628387::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1559 = { sizeof (U3CU3Ec__DisplayClass45_0_t1989341470), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1559[1] = 
{
	U3CU3Ec__DisplayClass45_0_t1989341470::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1560 = { sizeof (U3CU3Ec__DisplayClass46_0_t3293303385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1560[1] = 
{
	U3CU3Ec__DisplayClass46_0_t3293303385::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1561 = { sizeof (U3CU3Ec__DisplayClass47_0_t1707016468), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1561[1] = 
{
	U3CU3Ec__DisplayClass47_0_t1707016468::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1562 = { sizeof (U3CU3Ec__DisplayClass48_0_t1881678375), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1562[1] = 
{
	U3CU3Ec__DisplayClass48_0_t1881678375::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1563 = { sizeof (U3CU3Ec__DisplayClass49_0_t295391458), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1563[1] = 
{
	U3CU3Ec__DisplayClass49_0_t295391458::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1564 = { sizeof (U3CU3Ec__DisplayClass50_0_t3010978224), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1564[1] = 
{
	U3CU3Ec__DisplayClass50_0_t3010978224::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1565 = { sizeof (U3CU3Ec__DisplayClass51_0_t1424691307), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1565[1] = 
{
	U3CU3Ec__DisplayClass51_0_t1424691307::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1566 = { sizeof (U3CU3Ec__DisplayClass52_0_t2728653222), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1566[1] = 
{
	U3CU3Ec__DisplayClass52_0_t2728653222::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1567 = { sizeof (U3CU3Ec__DisplayClass53_0_t1142366305), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1567[1] = 
{
	U3CU3Ec__DisplayClass53_0_t1142366305::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1568 = { sizeof (U3CU3Ec__DisplayClass54_0_t3575628228), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1568[1] = 
{
	U3CU3Ec__DisplayClass54_0_t3575628228::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1569 = { sizeof (U3CU3Ec__DisplayClass55_0_t1989341311), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1569[1] = 
{
	U3CU3Ec__DisplayClass55_0_t1989341311::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1570 = { sizeof (U3CU3Ec__DisplayClass56_0_t3293303226), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1570[1] = 
{
	U3CU3Ec__DisplayClass56_0_t3293303226::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1571 = { sizeof (U3CU3Ec__DisplayClass57_0_t1707016309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1571[1] = 
{
	U3CU3Ec__DisplayClass57_0_t1707016309::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1572 = { sizeof (U3CU3Ec__DisplayClass58_0_t1881678216), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1572[1] = 
{
	U3CU3Ec__DisplayClass58_0_t1881678216::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1573 = { sizeof (U3CU3Ec__DisplayClass59_0_t295391299), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1573[1] = 
{
	U3CU3Ec__DisplayClass59_0_t295391299::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1574 = { sizeof (U3CU3Ec__DisplayClass60_0_t3010978317), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1574[1] = 
{
	U3CU3Ec__DisplayClass60_0_t3010978317::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1575 = { sizeof (U3CU3Ec__DisplayClass61_0_t1424691400), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1575[1] = 
{
	U3CU3Ec__DisplayClass61_0_t1424691400::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1576 = { sizeof (U3CU3Ec__DisplayClass62_0_t2728653315), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1576[1] = 
{
	U3CU3Ec__DisplayClass62_0_t2728653315::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1577 = { sizeof (U3CU3Ec__DisplayClass63_0_t1142366398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1577[1] = 
{
	U3CU3Ec__DisplayClass63_0_t1142366398::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1578 = { sizeof (U3CU3Ec__DisplayClass64_0_t3575628321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1578[1] = 
{
	U3CU3Ec__DisplayClass64_0_t3575628321::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1579 = { sizeof (U3CU3Ec__DisplayClass65_0_t1989341404), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1579[1] = 
{
	U3CU3Ec__DisplayClass65_0_t1989341404::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1580 = { sizeof (U3CU3Ec__DisplayClass66_0_t3293303319), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1580[1] = 
{
	U3CU3Ec__DisplayClass66_0_t3293303319::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1581 = { sizeof (U3CU3Ec__DisplayClass67_0_t1707016402), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1581[1] = 
{
	U3CU3Ec__DisplayClass67_0_t1707016402::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1582 = { sizeof (U3CU3Ec__DisplayClass68_0_t1881678309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1582[6] = 
{
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass68_0_t1881678309::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1583 = { sizeof (U3CU3Ec__DisplayClass69_0_t295391392), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1583[6] = 
{
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_target_0(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_offsetYSet_1(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_offsetY_2(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_s_3(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_endValue_4(),
	U3CU3Ec__DisplayClass69_0_t295391392::get_offset_of_startPosY_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1584 = { sizeof (U3CU3Ec__DisplayClass70_0_t3010978414), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1584[1] = 
{
	U3CU3Ec__DisplayClass70_0_t3010978414::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1585 = { sizeof (U3CU3Ec__DisplayClass71_0_t1424691497), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1585[1] = 
{
	U3CU3Ec__DisplayClass71_0_t1424691497::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1586 = { sizeof (U3CU3Ec__DisplayClass72_0_t2728653412), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1586[1] = 
{
	U3CU3Ec__DisplayClass72_0_t2728653412::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1587 = { sizeof (U3CU3Ec__DisplayClass73_0_t1142366495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1587[1] = 
{
	U3CU3Ec__DisplayClass73_0_t1142366495::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1588 = { sizeof (U3CU3Ec__DisplayClass74_0_t3575628418), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1588[1] = 
{
	U3CU3Ec__DisplayClass74_0_t3575628418::get_offset_of_target_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1589 = { sizeof (U3CU3Ec__DisplayClass75_0_t1989341501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1589[2] = 
{
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass75_0_t1989341501::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1590 = { sizeof (U3CU3Ec__DisplayClass76_0_t3293303416), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1590[2] = 
{
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass76_0_t3293303416::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1591 = { sizeof (U3CU3Ec__DisplayClass77_0_t1707016499), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1591[3] = 
{
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_target_1(),
	U3CU3Ec__DisplayClass77_0_t1707016499::get_offset_of_property_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1592 = { sizeof (U3CU3Ec__DisplayClass78_0_t1881678406), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1592[2] = 
{
	U3CU3Ec__DisplayClass78_0_t1881678406::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass78_0_t1881678406::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1593 = { sizeof (U3CU3Ec__DisplayClass79_0_t295391489), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1593[2] = 
{
	U3CU3Ec__DisplayClass79_0_t295391489::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass79_0_t295391489::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1594 = { sizeof (U3CU3Ec__DisplayClass80_0_t3010978755), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1594[2] = 
{
	U3CU3Ec__DisplayClass80_0_t3010978755::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass80_0_t3010978755::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1595 = { sizeof (U3CU3Ec__DisplayClass81_0_t1424691838), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1595[2] = 
{
	U3CU3Ec__DisplayClass81_0_t1424691838::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass81_0_t1424691838::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1596 = { sizeof (U3CU3Ec__DisplayClass82_0_t2728653753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1596[2] = 
{
	U3CU3Ec__DisplayClass82_0_t2728653753::get_offset_of_to_0(),
	U3CU3Ec__DisplayClass82_0_t2728653753::get_offset_of_target_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1597 = { sizeof (TweenParams_t2944325381), -1, sizeof(TweenParams_t2944325381_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1597[24] = 
{
	TweenParams_t2944325381_StaticFields::get_offset_of_Params_0(),
	TweenParams_t2944325381::get_offset_of_id_1(),
	TweenParams_t2944325381::get_offset_of_target_2(),
	TweenParams_t2944325381::get_offset_of_updateType_3(),
	TweenParams_t2944325381::get_offset_of_isIndependentUpdate_4(),
	TweenParams_t2944325381::get_offset_of_onStart_5(),
	TweenParams_t2944325381::get_offset_of_onPlay_6(),
	TweenParams_t2944325381::get_offset_of_onRewind_7(),
	TweenParams_t2944325381::get_offset_of_onUpdate_8(),
	TweenParams_t2944325381::get_offset_of_onStepComplete_9(),
	TweenParams_t2944325381::get_offset_of_onComplete_10(),
	TweenParams_t2944325381::get_offset_of_onKill_11(),
	TweenParams_t2944325381::get_offset_of_onWaypointChange_12(),
	TweenParams_t2944325381::get_offset_of_isRecyclable_13(),
	TweenParams_t2944325381::get_offset_of_isSpeedBased_14(),
	TweenParams_t2944325381::get_offset_of_autoKill_15(),
	TweenParams_t2944325381::get_offset_of_loops_16(),
	TweenParams_t2944325381::get_offset_of_loopType_17(),
	TweenParams_t2944325381::get_offset_of_delay_18(),
	TweenParams_t2944325381::get_offset_of_isRelative_19(),
	TweenParams_t2944325381::get_offset_of_easeType_20(),
	TweenParams_t2944325381::get_offset_of_customEase_21(),
	TweenParams_t2944325381::get_offset_of_easeOvershootOrAmplitude_22(),
	TweenParams_t2944325381::get_offset_of_easePeriod_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1598 = { sizeof (TweenSettingsExtensions_t2285462830), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1599 = { sizeof (LogBehaviour_t3505725029)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1599[4] = 
{
	LogBehaviour_t3505725029::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
