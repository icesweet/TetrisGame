﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Model
struct Model_t873752437;
// View
struct View_t854181575;
// CameraManager
struct CameraManager_t2379859346;
// GameManager
struct GameManager_t2252321495;
// AudioManager
struct AudioManager_t4222704959;
// FSMSystem
struct FSMSystem_t2363122359;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Controller
struct  Controller_t1937198888  : public MonoBehaviour_t1158329972
{
public:
	// Model Controller::model
	Model_t873752437 * ___model_2;
	// View Controller::view
	View_t854181575 * ___view_3;
	// CameraManager Controller::cameraManager
	CameraManager_t2379859346 * ___cameraManager_4;
	// GameManager Controller::gameManager
	GameManager_t2252321495 * ___gameManager_5;
	// AudioManager Controller::audioManager
	AudioManager_t4222704959 * ___audioManager_6;
	// FSMSystem Controller::fsm
	FSMSystem_t2363122359 * ___fsm_7;

public:
	inline static int32_t get_offset_of_model_2() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___model_2)); }
	inline Model_t873752437 * get_model_2() const { return ___model_2; }
	inline Model_t873752437 ** get_address_of_model_2() { return &___model_2; }
	inline void set_model_2(Model_t873752437 * value)
	{
		___model_2 = value;
		Il2CppCodeGenWriteBarrier(&___model_2, value);
	}

	inline static int32_t get_offset_of_view_3() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___view_3)); }
	inline View_t854181575 * get_view_3() const { return ___view_3; }
	inline View_t854181575 ** get_address_of_view_3() { return &___view_3; }
	inline void set_view_3(View_t854181575 * value)
	{
		___view_3 = value;
		Il2CppCodeGenWriteBarrier(&___view_3, value);
	}

	inline static int32_t get_offset_of_cameraManager_4() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___cameraManager_4)); }
	inline CameraManager_t2379859346 * get_cameraManager_4() const { return ___cameraManager_4; }
	inline CameraManager_t2379859346 ** get_address_of_cameraManager_4() { return &___cameraManager_4; }
	inline void set_cameraManager_4(CameraManager_t2379859346 * value)
	{
		___cameraManager_4 = value;
		Il2CppCodeGenWriteBarrier(&___cameraManager_4, value);
	}

	inline static int32_t get_offset_of_gameManager_5() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___gameManager_5)); }
	inline GameManager_t2252321495 * get_gameManager_5() const { return ___gameManager_5; }
	inline GameManager_t2252321495 ** get_address_of_gameManager_5() { return &___gameManager_5; }
	inline void set_gameManager_5(GameManager_t2252321495 * value)
	{
		___gameManager_5 = value;
		Il2CppCodeGenWriteBarrier(&___gameManager_5, value);
	}

	inline static int32_t get_offset_of_audioManager_6() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___audioManager_6)); }
	inline AudioManager_t4222704959 * get_audioManager_6() const { return ___audioManager_6; }
	inline AudioManager_t4222704959 ** get_address_of_audioManager_6() { return &___audioManager_6; }
	inline void set_audioManager_6(AudioManager_t4222704959 * value)
	{
		___audioManager_6 = value;
		Il2CppCodeGenWriteBarrier(&___audioManager_6, value);
	}

	inline static int32_t get_offset_of_fsm_7() { return static_cast<int32_t>(offsetof(Controller_t1937198888, ___fsm_7)); }
	inline FSMSystem_t2363122359 * get_fsm_7() const { return ___fsm_7; }
	inline FSMSystem_t2363122359 ** get_address_of_fsm_7() { return &___fsm_7; }
	inline void set_fsm_7(FSMSystem_t2363122359 * value)
	{
		___fsm_7 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_7, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
