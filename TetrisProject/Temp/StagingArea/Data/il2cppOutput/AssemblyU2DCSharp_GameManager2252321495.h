﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t3057952154;
// UnityEngine.Color[]
struct ColorU5BU5D_t672350442;
// UnityEngine.GameObject
struct GameObject_t1756533147;
// Controller
struct Controller_t1937198888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameManager
struct  GameManager_t2252321495  : public MonoBehaviour_t1158329972
{
public:
	// System.Boolean GameManager::isPause
	bool ___isPause_2;
	// UnityEngine.GameObject[] GameManager::shapes
	GameObjectU5BU5D_t3057952154* ___shapes_3;
	// UnityEngine.Color[] GameManager::colors
	ColorU5BU5D_t672350442* ___colors_4;
	// UnityEngine.GameObject GameManager::currentShape
	GameObject_t1756533147 * ___currentShape_5;
	// Controller GameManager::controller
	Controller_t1937198888 * ___controller_6;

public:
	inline static int32_t get_offset_of_isPause_2() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___isPause_2)); }
	inline bool get_isPause_2() const { return ___isPause_2; }
	inline bool* get_address_of_isPause_2() { return &___isPause_2; }
	inline void set_isPause_2(bool value)
	{
		___isPause_2 = value;
	}

	inline static int32_t get_offset_of_shapes_3() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___shapes_3)); }
	inline GameObjectU5BU5D_t3057952154* get_shapes_3() const { return ___shapes_3; }
	inline GameObjectU5BU5D_t3057952154** get_address_of_shapes_3() { return &___shapes_3; }
	inline void set_shapes_3(GameObjectU5BU5D_t3057952154* value)
	{
		___shapes_3 = value;
		Il2CppCodeGenWriteBarrier(&___shapes_3, value);
	}

	inline static int32_t get_offset_of_colors_4() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___colors_4)); }
	inline ColorU5BU5D_t672350442* get_colors_4() const { return ___colors_4; }
	inline ColorU5BU5D_t672350442** get_address_of_colors_4() { return &___colors_4; }
	inline void set_colors_4(ColorU5BU5D_t672350442* value)
	{
		___colors_4 = value;
		Il2CppCodeGenWriteBarrier(&___colors_4, value);
	}

	inline static int32_t get_offset_of_currentShape_5() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___currentShape_5)); }
	inline GameObject_t1756533147 * get_currentShape_5() const { return ___currentShape_5; }
	inline GameObject_t1756533147 ** get_address_of_currentShape_5() { return &___currentShape_5; }
	inline void set_currentShape_5(GameObject_t1756533147 * value)
	{
		___currentShape_5 = value;
		Il2CppCodeGenWriteBarrier(&___currentShape_5, value);
	}

	inline static int32_t get_offset_of_controller_6() { return static_cast<int32_t>(offsetof(GameManager_t2252321495, ___controller_6)); }
	inline Controller_t1937198888 * get_controller_6() const { return ___controller_6; }
	inline Controller_t1937198888 ** get_address_of_controller_6() { return &___controller_6; }
	inline void set_controller_6(Controller_t1937198888 * value)
	{
		___controller_6 = value;
		Il2CppCodeGenWriteBarrier(&___controller_6, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
