﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Transform[0...,0...]
struct TransformU5BU2CU5D_t3764228912;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Model
struct  Model_t873752437  : public MonoBehaviour_t1158329972
{
public:
	// System.Int32 Model::score
	int32_t ___score_5;
	// System.Int32 Model::highScore
	int32_t ___highScore_6;
	// System.Int32 Model::numbersGame
	int32_t ___numbersGame_7;
	// System.Boolean Model::isDataUpdate
	bool ___isDataUpdate_8;
	// UnityEngine.Transform[0...,0...] Model::map
	TransformU5BU2CU5D_t3764228912* ___map_9;

public:
	inline static int32_t get_offset_of_score_5() { return static_cast<int32_t>(offsetof(Model_t873752437, ___score_5)); }
	inline int32_t get_score_5() const { return ___score_5; }
	inline int32_t* get_address_of_score_5() { return &___score_5; }
	inline void set_score_5(int32_t value)
	{
		___score_5 = value;
	}

	inline static int32_t get_offset_of_highScore_6() { return static_cast<int32_t>(offsetof(Model_t873752437, ___highScore_6)); }
	inline int32_t get_highScore_6() const { return ___highScore_6; }
	inline int32_t* get_address_of_highScore_6() { return &___highScore_6; }
	inline void set_highScore_6(int32_t value)
	{
		___highScore_6 = value;
	}

	inline static int32_t get_offset_of_numbersGame_7() { return static_cast<int32_t>(offsetof(Model_t873752437, ___numbersGame_7)); }
	inline int32_t get_numbersGame_7() const { return ___numbersGame_7; }
	inline int32_t* get_address_of_numbersGame_7() { return &___numbersGame_7; }
	inline void set_numbersGame_7(int32_t value)
	{
		___numbersGame_7 = value;
	}

	inline static int32_t get_offset_of_isDataUpdate_8() { return static_cast<int32_t>(offsetof(Model_t873752437, ___isDataUpdate_8)); }
	inline bool get_isDataUpdate_8() const { return ___isDataUpdate_8; }
	inline bool* get_address_of_isDataUpdate_8() { return &___isDataUpdate_8; }
	inline void set_isDataUpdate_8(bool value)
	{
		___isDataUpdate_8 = value;
	}

	inline static int32_t get_offset_of_map_9() { return static_cast<int32_t>(offsetof(Model_t873752437, ___map_9)); }
	inline TransformU5BU2CU5D_t3764228912* get_map_9() const { return ___map_9; }
	inline TransformU5BU2CU5D_t3764228912** get_address_of_map_9() { return &___map_9; }
	inline void set_map_9(TransformU5BU2CU5D_t3764228912* value)
	{
		___map_9 = value;
		Il2CppCodeGenWriteBarrier(&___map_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
