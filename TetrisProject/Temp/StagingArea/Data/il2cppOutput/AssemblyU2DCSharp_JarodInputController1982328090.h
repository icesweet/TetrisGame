﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// Controller
struct Controller_t1937198888;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// JarodInputController
struct  JarodInputController_t1982328090  : public MonoBehaviour_t1158329972
{
public:
	// System.Single JarodInputController::fingerActionSensitivity
	float ___fingerActionSensitivity_2;
	// System.Single JarodInputController::fingerBeginX
	float ___fingerBeginX_3;
	// System.Single JarodInputController::fingerBeginY
	float ___fingerBeginY_4;
	// System.Single JarodInputController::fingerCurrentX
	float ___fingerCurrentX_5;
	// System.Single JarodInputController::fingerCurrentY
	float ___fingerCurrentY_6;
	// System.Single JarodInputController::fingerSegmentX
	float ___fingerSegmentX_7;
	// System.Single JarodInputController::fingerSegmentY
	float ___fingerSegmentY_8;
	// System.Int32 JarodInputController::fingerTouchState
	int32_t ___fingerTouchState_9;
	// System.Int32 JarodInputController::FINGER_STATE_NULL
	int32_t ___FINGER_STATE_NULL_10;
	// System.Int32 JarodInputController::FINGER_STATE_TOUCH
	int32_t ___FINGER_STATE_TOUCH_11;
	// System.Int32 JarodInputController::FINGER_STATE_ADD
	int32_t ___FINGER_STATE_ADD_12;
	// Controller JarodInputController::controller
	Controller_t1937198888 * ___controller_13;

public:
	inline static int32_t get_offset_of_fingerActionSensitivity_2() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerActionSensitivity_2)); }
	inline float get_fingerActionSensitivity_2() const { return ___fingerActionSensitivity_2; }
	inline float* get_address_of_fingerActionSensitivity_2() { return &___fingerActionSensitivity_2; }
	inline void set_fingerActionSensitivity_2(float value)
	{
		___fingerActionSensitivity_2 = value;
	}

	inline static int32_t get_offset_of_fingerBeginX_3() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerBeginX_3)); }
	inline float get_fingerBeginX_3() const { return ___fingerBeginX_3; }
	inline float* get_address_of_fingerBeginX_3() { return &___fingerBeginX_3; }
	inline void set_fingerBeginX_3(float value)
	{
		___fingerBeginX_3 = value;
	}

	inline static int32_t get_offset_of_fingerBeginY_4() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerBeginY_4)); }
	inline float get_fingerBeginY_4() const { return ___fingerBeginY_4; }
	inline float* get_address_of_fingerBeginY_4() { return &___fingerBeginY_4; }
	inline void set_fingerBeginY_4(float value)
	{
		___fingerBeginY_4 = value;
	}

	inline static int32_t get_offset_of_fingerCurrentX_5() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerCurrentX_5)); }
	inline float get_fingerCurrentX_5() const { return ___fingerCurrentX_5; }
	inline float* get_address_of_fingerCurrentX_5() { return &___fingerCurrentX_5; }
	inline void set_fingerCurrentX_5(float value)
	{
		___fingerCurrentX_5 = value;
	}

	inline static int32_t get_offset_of_fingerCurrentY_6() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerCurrentY_6)); }
	inline float get_fingerCurrentY_6() const { return ___fingerCurrentY_6; }
	inline float* get_address_of_fingerCurrentY_6() { return &___fingerCurrentY_6; }
	inline void set_fingerCurrentY_6(float value)
	{
		___fingerCurrentY_6 = value;
	}

	inline static int32_t get_offset_of_fingerSegmentX_7() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerSegmentX_7)); }
	inline float get_fingerSegmentX_7() const { return ___fingerSegmentX_7; }
	inline float* get_address_of_fingerSegmentX_7() { return &___fingerSegmentX_7; }
	inline void set_fingerSegmentX_7(float value)
	{
		___fingerSegmentX_7 = value;
	}

	inline static int32_t get_offset_of_fingerSegmentY_8() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerSegmentY_8)); }
	inline float get_fingerSegmentY_8() const { return ___fingerSegmentY_8; }
	inline float* get_address_of_fingerSegmentY_8() { return &___fingerSegmentY_8; }
	inline void set_fingerSegmentY_8(float value)
	{
		___fingerSegmentY_8 = value;
	}

	inline static int32_t get_offset_of_fingerTouchState_9() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___fingerTouchState_9)); }
	inline int32_t get_fingerTouchState_9() const { return ___fingerTouchState_9; }
	inline int32_t* get_address_of_fingerTouchState_9() { return &___fingerTouchState_9; }
	inline void set_fingerTouchState_9(int32_t value)
	{
		___fingerTouchState_9 = value;
	}

	inline static int32_t get_offset_of_FINGER_STATE_NULL_10() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___FINGER_STATE_NULL_10)); }
	inline int32_t get_FINGER_STATE_NULL_10() const { return ___FINGER_STATE_NULL_10; }
	inline int32_t* get_address_of_FINGER_STATE_NULL_10() { return &___FINGER_STATE_NULL_10; }
	inline void set_FINGER_STATE_NULL_10(int32_t value)
	{
		___FINGER_STATE_NULL_10 = value;
	}

	inline static int32_t get_offset_of_FINGER_STATE_TOUCH_11() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___FINGER_STATE_TOUCH_11)); }
	inline int32_t get_FINGER_STATE_TOUCH_11() const { return ___FINGER_STATE_TOUCH_11; }
	inline int32_t* get_address_of_FINGER_STATE_TOUCH_11() { return &___FINGER_STATE_TOUCH_11; }
	inline void set_FINGER_STATE_TOUCH_11(int32_t value)
	{
		___FINGER_STATE_TOUCH_11 = value;
	}

	inline static int32_t get_offset_of_FINGER_STATE_ADD_12() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___FINGER_STATE_ADD_12)); }
	inline int32_t get_FINGER_STATE_ADD_12() const { return ___FINGER_STATE_ADD_12; }
	inline int32_t* get_address_of_FINGER_STATE_ADD_12() { return &___FINGER_STATE_ADD_12; }
	inline void set_FINGER_STATE_ADD_12(int32_t value)
	{
		___FINGER_STATE_ADD_12 = value;
	}

	inline static int32_t get_offset_of_controller_13() { return static_cast<int32_t>(offsetof(JarodInputController_t1982328090, ___controller_13)); }
	inline Controller_t1937198888 * get_controller_13() const { return ___controller_13; }
	inline Controller_t1937198888 ** get_address_of_controller_13() { return &___controller_13; }
	inline void set_controller_13(Controller_t1937198888 * value)
	{
		___controller_13 = value;
		Il2CppCodeGenWriteBarrier(&___controller_13, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
