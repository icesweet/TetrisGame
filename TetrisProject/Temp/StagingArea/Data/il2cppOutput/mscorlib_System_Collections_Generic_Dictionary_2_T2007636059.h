﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "mscorlib_System_MulticastDelegate3201952435.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23695602308.h"
#include "AssemblyU2DCSharp_Transition3224717525.h"
#include "AssemblyU2DCSharp_StateID3221758012.h"

// System.IAsyncResult
struct IAsyncResult_t1999651008;
// System.AsyncCallback
struct AsyncCallback_t163412349;
// System.Object
struct Il2CppObject;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Transform`1<Transition,StateID,System.Collections.Generic.KeyValuePair`2<Transition,StateID>>
struct  Transform_1_t2007636059  : public MulticastDelegate_t3201952435
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
