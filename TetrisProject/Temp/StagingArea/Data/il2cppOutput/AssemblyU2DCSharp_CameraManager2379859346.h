﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.Camera
struct Camera_t189460977;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraManager
struct  CameraManager_t2379859346  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.Camera CameraManager::myCamera
	Camera_t189460977 * ___myCamera_2;

public:
	inline static int32_t get_offset_of_myCamera_2() { return static_cast<int32_t>(offsetof(CameraManager_t2379859346, ___myCamera_2)); }
	inline Camera_t189460977 * get_myCamera_2() const { return ___myCamera_2; }
	inline Camera_t189460977 ** get_address_of_myCamera_2() { return &___myCamera_2; }
	inline void set_myCamera_2(Camera_t189460977 * value)
	{
		___myCamera_2 = value;
		Il2CppCodeGenWriteBarrier(&___myCamera_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
