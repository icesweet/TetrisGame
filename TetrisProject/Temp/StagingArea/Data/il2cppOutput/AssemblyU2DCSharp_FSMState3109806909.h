﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "AssemblyU2DCSharp_StateID3221758012.h"

// Controller
struct Controller_t1937198888;
// FSMSystem
struct FSMSystem_t2363122359;
// System.Collections.Generic.Dictionary`2<Transition,StateID>
struct Dictionary_2_t1643289790;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FSMState
struct  FSMState_t3109806909  : public MonoBehaviour_t1158329972
{
public:
	// Controller FSMState::controller
	Controller_t1937198888 * ___controller_2;
	// FSMSystem FSMState::fsm
	FSMSystem_t2363122359 * ___fsm_3;
	// System.Collections.Generic.Dictionary`2<Transition,StateID> FSMState::map
	Dictionary_2_t1643289790 * ___map_4;
	// StateID FSMState::stateID
	int32_t ___stateID_5;

public:
	inline static int32_t get_offset_of_controller_2() { return static_cast<int32_t>(offsetof(FSMState_t3109806909, ___controller_2)); }
	inline Controller_t1937198888 * get_controller_2() const { return ___controller_2; }
	inline Controller_t1937198888 ** get_address_of_controller_2() { return &___controller_2; }
	inline void set_controller_2(Controller_t1937198888 * value)
	{
		___controller_2 = value;
		Il2CppCodeGenWriteBarrier(&___controller_2, value);
	}

	inline static int32_t get_offset_of_fsm_3() { return static_cast<int32_t>(offsetof(FSMState_t3109806909, ___fsm_3)); }
	inline FSMSystem_t2363122359 * get_fsm_3() const { return ___fsm_3; }
	inline FSMSystem_t2363122359 ** get_address_of_fsm_3() { return &___fsm_3; }
	inline void set_fsm_3(FSMSystem_t2363122359 * value)
	{
		___fsm_3 = value;
		Il2CppCodeGenWriteBarrier(&___fsm_3, value);
	}

	inline static int32_t get_offset_of_map_4() { return static_cast<int32_t>(offsetof(FSMState_t3109806909, ___map_4)); }
	inline Dictionary_2_t1643289790 * get_map_4() const { return ___map_4; }
	inline Dictionary_2_t1643289790 ** get_address_of_map_4() { return &___map_4; }
	inline void set_map_4(Dictionary_2_t1643289790 * value)
	{
		___map_4 = value;
		Il2CppCodeGenWriteBarrier(&___map_4, value);
	}

	inline static int32_t get_offset_of_stateID_5() { return static_cast<int32_t>(offsetof(FSMState_t3109806909, ___stateID_5)); }
	inline int32_t get_stateID_5() const { return ___stateID_5; }
	inline int32_t* get_address_of_stateID_5() { return &___stateID_5; }
	inline void set_stateID_5(int32_t value)
	{
		___stateID_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
