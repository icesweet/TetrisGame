﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array3829468939.h"
#include "DOTweenPro_U3CModuleU3E3783534214.h"
#include "DOTweenPro_DG_Tweening_Core_ABSAnimationComponent2205594551.h"
#include "mscorlib_System_Void1841601450.h"
#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"
#include "DOTweenPro_DG_Tweening_Core_DOTweenAnimationType119935370.h"
#include "DOTweenPro_DG_Tweening_Core_OnDisableBehaviour125315118.h"
#include "DOTweenPro_DG_Tweening_Core_OnEnableBehaviour285142911.h"
#include "DOTweenPro_DG_Tweening_Core_TargetType2706200073.h"
#include "DOTweenPro_DG_Tweening_Core_VisualManagerPreset4087939440.h"
#include "DOTweenPro_DG_Tweening_DOTweenInspectorMode2739551672.h"
#include "DOTweenPro_DG_Tweening_DOTweenPath1397145371.h"
#include "DOTween_DG_Tweening_PathType2815988833.h"
#include "mscorlib_System_Object2689449295.h"
#include "mscorlib_System_IntPtr2504060609.h"
#include "DOTween_DG_Tweening_TweenCallback3697142134.h"
#include "UnityEngine_UnityEngine_Object1021602117.h"
#include "UnityEngine_UnityEngine_Vector32243707580.h"
#include "UnityEngine_UnityEngine_Rigidbody4233889191.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path2828565993.h"
#include "DOTween_DG_Tweening_PathMode1545785466.h"
#include "DOTween_DG_Tweening_AxisConstraint1244566668.h"
#include "UnityEngine_UnityEngine_Transform3275118058.h"
#include "mscorlib_System_Nullable_1_gen506773895.h"
#include "DOTween_DG_Tweening_LoopType2249218064.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"
#include "UnityEngine_UnityEngine_AnimationCurve3306541151.h"
#include "DOTween_DG_Tweening_Ease2502520296.h"
#include "mscorlib_System_String2029220233.h"
#include "DOTween_DG_Tweening_Core_TweenerCore_3_gen3035488489.h"
#include "mscorlib_System_Int322071877448.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlPo168081159.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1612828712.h"
#include "DOTween_DG_Tweening_DOTween2276353038.h"
#include "mscorlib_System_Boolean3825574718.h"
#include "mscorlib_System_Collections_Generic_List_1_gen3066263266.h"
#include "UnityEngine_UnityEngine_Color2020392075.h"
#include "UnityEngine_UnityEngine_Component3819376471.h"
#include "UnityEngine_UnityEngine_SpriteRenderer1209076198.h"
#include "mscorlib_System_Single2076509932.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent408735097.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "mscorlib_System_Nullable_1_gen283458390.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "UnityEngine_UnityEngine_Keyframe1449471340.h"
#include "DOTweenPro_DG_Tweening_DOTweenVisualManager2945673405.h"
#include "UnityEngine_UnityEngine_GameObject1756533147.h"
#include "DOTweenPro_DG_Tweening_HandlesDrawMode3273484032.h"
#include "DOTweenPro_DG_Tweening_HandlesType3201532857.h"

// DG.Tweening.Core.ABSAnimationComponent
struct ABSAnimationComponent_t2205594551;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t1158329972;
// DG.Tweening.DOTweenPath
struct DOTweenPath_t1397145371;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t1612828712;
// DG.Tweening.Plugins.Core.PathCore.Path
struct Path_t2828565993;
// DG.Tweening.TweenCallback
struct TweenCallback_t3697142134;
// System.Object
struct Il2CppObject;
// System.Collections.Generic.List`1<DG.Tweening.TweenCallback>
struct List_1_t3066263266;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t2058570427;
// UnityEngine.Component
struct Component_t3819376471;
// UnityEngine.Transform
struct Transform_t3275118058;
// UnityEngine.Object
struct Object_t1021602117;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t1209076198;
// UnityEngine.Rigidbody
struct Rigidbody_t4233889191;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>
struct TweenerCore_3_t3035488489;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3306541151;
// System.String
struct String_t;
// UnityEngine.Events.UnityEvent
struct UnityEvent_t408735097;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765;
// DG.Tweening.Tween
struct Tween_t278478013;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829;
// DG.Tweening.DOTweenVisualManager
struct DOTweenVisualManager_t2945673405;
// UnityEngine.GameObject
struct GameObject_t1756533147;
extern Il2CppClass* DOTween_t2276353038_il2cpp_TypeInfo_var;
extern Il2CppClass* TweenCallback_t3697142134_il2cpp_TypeInfo_var;
extern Il2CppClass* Object_t1021602117_il2cpp_TypeInfo_var;
extern Il2CppClass* Nullable_1_t506773895_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Count_m4027941115_MethodInfo_var;
extern const MethodInfo* Path_Draw_m755263667_MethodInfo_var;
extern const MethodInfo* List_1_Add_m2434052001_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t4233889191_m2973586618_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m3622474896_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3035488489_m2218308542_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3035488489_m770682058_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3035488489_m2792702168_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3035488489_m3712068125_MethodInfo_var;
extern const MethodInfo* DOTweenPath_U3CAwakeU3Eb__38_0_m2435648440_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3035488489_m3007568601_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3035488489_m2570913546_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m3271531062_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m1874820216_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_SetId_TisTweenerCore_3_t3035488489_m1640877877_MethodInfo_var;
extern const MethodInfo* UnityEvent_Invoke_m4163344491_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3035488489_m75273675_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3035488489_m3850970939_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3035488489_m1014346626_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3035488489_m689171214_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3035488489_m1168345000_MethodInfo_var;
extern const MethodInfo* TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3035488489_m118158764_MethodInfo_var;
extern const MethodInfo* TweenExtensions_Play_TisTweenerCore_3_t3035488489_m2599375885_MethodInfo_var;
extern const MethodInfo* TweenExtensions_Pause_TisTweenerCore_3_t3035488489_m2596183611_MethodInfo_var;
extern const uint32_t DOTweenPath_Awake_m4245485586_MetadataUsageId;
extern Il2CppClass* Path_t2828565993_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_ToArray_m2187942201_MethodInfo_var;
extern const MethodInfo* Nullable_1__ctor_m2519817708_MethodInfo_var;
extern const uint32_t DOTweenPath_Reset_m2024650022_MetadataUsageId;
extern const MethodInfo* TweenExtensions_Play_TisTween_t278478013_m2584288383_MethodInfo_var;
extern const uint32_t DOTweenPath_DOPlay_m1676033874_MetadataUsageId;
extern const MethodInfo* TweenExtensions_Pause_TisTween_t278478013_m3798767189_MethodInfo_var;
extern const uint32_t DOTweenPath_DOPause_m1913698988_MetadataUsageId;
extern Il2CppClass* Debugger_t1404542751_il2cpp_TypeInfo_var;
extern const uint32_t DOTweenPath_DORestart_m3437720452_MetadataUsageId;
extern const uint32_t DOTweenPath_GetTween_m2167448416_MetadataUsageId;
extern Il2CppCodeGenString* _stringLiteral3098430486;
extern const uint32_t DOTweenPath_GetDrawPoints_m1924516330_MetadataUsageId;
extern Il2CppClass* Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_get_Item_m2503489122_MethodInfo_var;
extern const uint32_t DOTweenPath_GetFullWps_m2783782328_MetadataUsageId;
extern Il2CppClass* KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var;
extern Il2CppClass* AnimationCurve_t3306541151_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t1612828712_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3655021515_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral371857150;
extern const uint32_t DOTweenPath__ctor_m577727613_MetadataUsageId;
extern const MethodInfo* Component_GetComponent_TisABSAnimationComponent_t2205594551_m3887019028_MethodInfo_var;
extern const uint32_t DOTweenVisualManager_Awake_m2217727324_MetadataUsageId;
extern const uint32_t DOTweenVisualManager_Update_m340061598_MetadataUsageId;
extern const uint32_t DOTweenVisualManager_OnEnable_m3831096439_MetadataUsageId;
extern const uint32_t DOTweenVisualManager_OnDisable_m1016672072_MetadataUsageId;

// UnityEngine.Vector3[]
struct Vector3U5BU5D_t1172311765  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Vector3_t2243707580  m_Items[1];

public:
	inline Vector3_t2243707580  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector3_t2243707580  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector3_t2243707580 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector3_t2243707580  value)
	{
		m_Items[index] = value;
	}
};
// DG.Tweening.Plugins.Core.PathCore.ControlPoint[]
struct ControlPointU5BU5D_t246481150  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) ControlPoint_t168081159  m_Items[1];

public:
	inline ControlPoint_t168081159  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline ControlPoint_t168081159 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, ControlPoint_t168081159  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline ControlPoint_t168081159  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline ControlPoint_t168081159 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, ControlPoint_t168081159  value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t449065829  : public Il2CppArray
{
public:
	ALIGN_FIELD (8) Keyframe_t1449471340  m_Items[1];

public:
	inline Keyframe_t1449471340  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Keyframe_t1449471340 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Keyframe_t1449471340  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Keyframe_t1449471340  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Keyframe_t1449471340 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Keyframe_t1449471340  value)
	{
		m_Items[index] = value;
	}
};


// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C"  int32_t List_1_get_Count_m4027941115_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C"  void List_1_Add_m4157722533_gshared (List_1_t2058570427 * __this, Il2CppObject * p0, const MethodInfo* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m4109961936_gshared (Component_t3819376471 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m3622474896_gshared (Nullable_1_t506773895 * __this, Vector3_t2243707580  p0, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetDelay_TisIl2CppObject_m899641841_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, float p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<System.Object>(!!0,System.Int32,DG.Tweening.LoopType)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, int32_t p2, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<System.Object>(!!0,System.Boolean)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetAutoKill_TisIl2CppObject_m3918711238_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, bool p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<System.Object>(!!0,DG.Tweening.UpdateType)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetUpdate_TisIl2CppObject_m1781134312_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnKill_TisIl2CppObject_m3077878346_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<System.Object>(!!0)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetSpeedBased_TisIl2CppObject_m2454034903_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,UnityEngine.AnimationCurve)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetEase_TisIl2CppObject_m3353632809_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, AnimationCurve_t3306541151 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, int32_t p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<System.Object>(!!0,System.Object)
extern "C"  Il2CppObject * TweenSettingsExtensions_SetId_TisIl2CppObject_m3739856218_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, Il2CppObject * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnStart_TisIl2CppObject_m1379091048_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnPlay_TisIl2CppObject_m4140701164_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnStepComplete_TisIl2CppObject_m267308340_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<System.Object>(!!0,DG.Tweening.TweenCallback)
extern "C"  Il2CppObject * TweenSettingsExtensions_OnRewind_TisIl2CppObject_m1366008935_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, TweenCallback_t3697142134 * p1, const MethodInfo* method);
// !!0 DG.Tweening.TweenExtensions::Play<System.Object>(!!0)
extern "C"  Il2CppObject * TweenExtensions_Play_TisIl2CppObject_m335062998_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !!0 DG.Tweening.TweenExtensions::Pause<System.Object>(!!0)
extern "C"  Il2CppObject * TweenExtensions_Pause_TisIl2CppObject_m453341176_gshared (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method);
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C"  Vector3U5BU5D_t1172311765* List_1_ToArray_m2187942201_gshared (List_1_t1612828712 * __this, const MethodInfo* method);
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(!0)
extern "C"  void Nullable_1__ctor_m2519817708_gshared (Nullable_1_t283458390 * __this, Color_t2020392075  p0, const MethodInfo* method);
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C"  Vector3_t2243707580  List_1_get_Item_m2503489122_gshared (List_1_t1612828712 * __this, int32_t p0, const MethodInfo* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C"  void List_1__ctor_m3655021515_gshared (List_1_t1612828712 * __this, const MethodInfo* method);

// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C"  void MonoBehaviour__ctor_m2464341955 (MonoBehaviour_t1158329972 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
#define List_1_get_Count_m4027941115(__this, method) ((  int32_t (*) (List_1_t1612828712 *, const MethodInfo*))List_1_get_Count_m4027941115_gshared)(__this, method)
// System.Void DG.Tweening.Plugins.Core.PathCore.Path::AssignDecoder(DG.Tweening.PathType)
extern "C"  void Path_AssignDecoder_m3072882543 (Path_t2828565993 * __this, int32_t p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenCallback::.ctor(System.Object,System.IntPtr)
extern "C"  void TweenCallback__ctor_m3479200459 (TweenCallback_t3697142134 * __this, Il2CppObject * p0, IntPtr_t p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<DG.Tweening.TweenCallback>::Add(!0)
#define List_1_Add_m2434052001(__this, p0, method) ((  void (*) (List_1_t3066263266 *, TweenCallback_t3697142134 *, const MethodInfo*))List_1_Add_m4157722533_gshared)(__this, p0, method)
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C"  Transform_t3275118058 * Component_get_transform_m2697483695 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C"  Transform_t3275118058 * Transform_get_parent_m147407266 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Inequality_m2402264703 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C"  Vector3_t2243707580  Transform_get_position_m1104419803 (Transform_t3275118058 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Subtraction_m2407545601 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
extern "C"  void DOTweenPath_ReEvaluateRelativeTween_m3742090720 (DOTweenPath_t1397145371 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859(__this, method) ((  SpriteRenderer_t1209076198 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t4233889191_m2973586618(__this, method) ((  Rigidbody_t4233889191 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3035488489 * ShortcutExtensions_DOPath_m1344615108 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * p0, Path_t2828565993 * p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetOptions(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Boolean,DG.Tweening.AxisConstraint,DG.Tweening.AxisConstraint)
extern "C"  TweenerCore_3_t3035488489 * TweenSettingsExtensions_SetOptions_m3069323763 (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3035488489 * p0, bool p1, int32_t p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3035488489 * ShortcutExtensions_DOLocalPath_m3055576589 (Il2CppObject * __this /* static, unused */, Rigidbody_t4233889191 * p0, Path_t2828565993 * p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3035488489 * ShortcutExtensions_DOPath_m251740173 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Path_t2828565993 * p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.ShortcutExtensions::DOLocalPath(UnityEngine.Transform,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern "C"  TweenerCore_3_t3035488489 * ShortcutExtensions_DOLocalPath_m712666778 (Il2CppObject * __this /* static, unused */, Transform_t3275118058 * p0, Path_t2828565993 * p1, float p2, int32_t p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Nullable`1<UnityEngine.Vector3>::.ctor(!0)
#define Nullable_1__ctor_m3622474896(__this, p0, method) ((  void (*) (Nullable_1_t506773895 *, Vector3_t2243707580 , const MethodInfo*))Nullable_1__ctor_m3622474896_gshared)(__this, p0, method)
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Transform,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  TweenerCore_3_t3035488489 * TweenSettingsExtensions_SetLookAt_m2149060425 (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3035488489 * p0, Transform_t3275118058 * p1, Nullable_1_t506773895  p2, Nullable_1_t506773895  p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,UnityEngine.Vector3,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  TweenerCore_3_t3035488489 * TweenSettingsExtensions_SetLookAt_m1482035211 (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3035488489 * p0, Vector3_t2243707580  p1, Nullable_1_t506773895  p2, Nullable_1_t506773895  p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.TweenSettingsExtensions::SetLookAt(DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>,System.Single,System.Nullable`1<UnityEngine.Vector3>,System.Nullable`1<UnityEngine.Vector3>)
extern "C"  TweenerCore_3_t3035488489 * TweenSettingsExtensions_SetLookAt_m4115581975 (Il2CppObject * __this /* static, unused */, TweenerCore_3_t3035488489 * p0, float p1, Nullable_1_t506773895  p2, Nullable_1_t506773895  p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Single)
#define TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3035488489_m2218308542(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, float, const MethodInfo*))TweenSettingsExtensions_SetDelay_TisIl2CppObject_m899641841_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetLoops<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Int32,DG.Tweening.LoopType)
#define TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3035488489_m770682058(__this /* static, unused */, p0, p1, p2, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, int32_t, int32_t, const MethodInfo*))TweenSettingsExtensions_SetLoops_TisIl2CppObject_m3228923250_gshared)(__this /* static, unused */, p0, p1, p2, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetAutoKill<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Boolean)
#define TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3035488489_m2792702168(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, bool, const MethodInfo*))TweenSettingsExtensions_SetAutoKill_TisIl2CppObject_m3918711238_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetUpdate<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.UpdateType)
#define TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3035488489_m3712068125(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetUpdate_TisIl2CppObject_m1781134312_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnKill<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3035488489_m3007568601(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnKill_TisIl2CppObject_m3077878346_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetSpeedBased<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
#define TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3035488489_m2570913546(__this /* static, unused */, p0, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, const MethodInfo*))TweenSettingsExtensions_SetSpeedBased_TisIl2CppObject_m2454034903_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,UnityEngine.AnimationCurve)
#define TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m3271531062(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, AnimationCurve_t3306541151 *, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m3353632809_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.Ease)
#define TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m1874820216(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, int32_t, const MethodInfo*))TweenSettingsExtensions_SetEase_TisIl2CppObject_m2346377224_gshared)(__this /* static, unused */, p0, p1, method)
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C"  bool String_IsNullOrEmpty_m2802126737 (Il2CppObject * __this /* static, unused */, String_t* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenSettingsExtensions::SetId<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,System.Object)
#define TweenSettingsExtensions_SetId_TisTweenerCore_3_t3035488489_m1640877877(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, Il2CppObject *, const MethodInfo*))TweenSettingsExtensions_SetId_TisIl2CppObject_m3739856218_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnStart<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3035488489_m75273675(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnStart_TisIl2CppObject_m1379091048_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnPlay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3035488489_m3850970939(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnPlay_TisIl2CppObject_m4140701164_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnUpdate<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3035488489_m1014346626(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnUpdate_TisIl2CppObject_m1056390452_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnStepComplete<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3035488489_m689171214(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnStepComplete_TisIl2CppObject_m267308340_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnComplete<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3035488489_m1168345000(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnComplete_TisIl2CppObject_m2769923597_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenSettingsExtensions::OnRewind<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0,DG.Tweening.TweenCallback)
#define TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3035488489_m118158764(__this /* static, unused */, p0, p1, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, TweenCallback_t3697142134 *, const MethodInfo*))TweenSettingsExtensions_OnRewind_TisIl2CppObject_m1366008935_gshared)(__this /* static, unused */, p0, p1, method)
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
#define TweenExtensions_Play_TisTweenerCore_3_t3035488489_m2599375885(__this /* static, unused */, p0, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, const MethodInfo*))TweenExtensions_Play_TisIl2CppObject_m335062998_gshared)(__this /* static, unused */, p0, method)
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions>>(!!0)
#define TweenExtensions_Pause_TisTweenerCore_3_t3035488489_m2596183611(__this /* static, unused */, p0, method) ((  TweenerCore_3_t3035488489 * (*) (Il2CppObject * /* static, unused */, TweenerCore_3_t3035488489 *, const MethodInfo*))TweenExtensions_Pause_TisIl2CppObject_m453341176_gshared)(__this /* static, unused */, p0, method)
// System.Void UnityEngine.Events.UnityEvent::Invoke()
extern "C"  void UnityEvent_Invoke_m4163344491 (UnityEvent_t408735097 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
#define List_1_ToArray_m2187942201(__this, method) ((  Vector3U5BU5D_t1172311765* (*) (List_1_t1612828712 *, const MethodInfo*))List_1_ToArray_m2187942201_gshared)(__this, method)
// System.Void System.Nullable`1<UnityEngine.Color>::.ctor(!0)
#define Nullable_1__ctor_m2519817708(__this, p0, method) ((  void (*) (Nullable_1_t283458390 *, Color_t2020392075 , const MethodInfo*))Nullable_1__ctor_m2519817708_gshared)(__this, p0, method)
// System.Void DG.Tweening.Plugins.Core.PathCore.Path::.ctor(DG.Tweening.PathType,UnityEngine.Vector3[],System.Int32,System.Nullable`1<UnityEngine.Color>)
extern "C"  void Path__ctor_m4253660657 (Path_t2828565993 * __this, int32_t p0, Vector3U5BU5D_t1172311765* p1, int32_t p2, Nullable_1_t283458390  p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Kill(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Kill_m3845739180 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenExtensions::Play<DG.Tweening.Tween>(!!0)
#define TweenExtensions_Play_TisTween_t278478013_m2584288383(__this /* static, unused */, p0, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, const MethodInfo*))TweenExtensions_Play_TisIl2CppObject_m335062998_gshared)(__this /* static, unused */, p0, method)
// System.Void DG.Tweening.TweenExtensions::PlayBackwards(DG.Tweening.Tween)
extern "C"  void TweenExtensions_PlayBackwards_m613887607 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::PlayForward(DG.Tweening.Tween)
extern "C"  void TweenExtensions_PlayForward_m3704378858 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 DG.Tweening.TweenExtensions::Pause<DG.Tweening.Tween>(!!0)
#define TweenExtensions_Pause_TisTween_t278478013_m3798767189(__this /* static, unused */, p0, method) ((  Tween_t278478013 * (*) (Il2CppObject * /* static, unused */, Tween_t278478013 *, const MethodInfo*))TweenExtensions_Pause_TisIl2CppObject_m453341176_gshared)(__this /* static, unused */, p0, method)
// System.Void DG.Tweening.TweenExtensions::TogglePause(DG.Tweening.Tween)
extern "C"  void TweenExtensions_TogglePause_m1054448567 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Rewind(DG.Tweening.Tween,System.Boolean)
extern "C"  void TweenExtensions_Rewind_m3286148139 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, bool p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogNullTween(DG.Tweening.Tween)
extern "C"  void Debugger_LogNullTween_m832479276 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Restart(DG.Tweening.Tween,System.Boolean,System.Single)
extern "C"  void TweenExtensions_Restart_m2056936246 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, bool p1, float p2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.TweenExtensions::Complete(DG.Tweening.Tween)
extern "C"  void TweenExtensions_Complete_m274286092 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogInvalidTween(DG.Tweening.Tween)
extern "C"  void Debugger_LogInvalidTween_m3454295394 (Il2CppObject * __this /* static, unused */, Tween_t278478013 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.Debugger::LogWarning(System.Object)
extern "C"  void Debugger_LogWarning_m1294243619 (Il2CppObject * __this /* static, unused */, Il2CppObject * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
#define List_1_get_Item_m2503489122(__this, p0, method) ((  Vector3_t2243707580  (*) (List_1_t1612828712 *, int32_t, const MethodInfo*))List_1_get_Item_m2503489122_gshared)(__this, p0, method)
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  bool Vector3_op_Equality_m305888255 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t2243707580  Vector3_op_Addition_m3146764857 (Il2CppObject * __this /* static, unused */, Vector3_t2243707580  p0, Vector3_t2243707580  p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single)
extern "C"  void Keyframe__ctor_m2042404667 (Keyframe_t1449471340 * __this, float p0, float p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C"  void AnimationCurve__ctor_m2814448007 (AnimationCurve_t3306541151 * __this, KeyframeU5BU5D_t449065829* p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern "C"  Vector3_t2243707580  Vector3_get_forward_m1201659139 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern "C"  Vector3_t2243707580  Vector3_get_up_m2725403797 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
#define List_1__ctor_m3655021515(__this, method) ((  void (*) (List_1_t1612828712 *, const MethodInfo*))List_1__ctor_m3655021515_gshared)(__this, method)
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C"  void Color__ctor_m1909920690 (Color_t2020392075 * __this, float p0, float p1, float p2, float p3, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
extern "C"  void ABSAnimationComponent__ctor_m1064982270 (ABSAnimationComponent_t2205594551 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// !!0 UnityEngine.Component::GetComponent<DG.Tweening.Core.ABSAnimationComponent>()
#define Component_GetComponent_TisABSAnimationComponent_t2205594551_m3887019028(__this, method) ((  ABSAnimationComponent_t2205594551 * (*) (Component_t3819376471 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m4109961936_gshared)(__this, method)
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C"  bool Object_op_Equality_m3764089466 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, Object_t1021602117 * p1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C"  GameObject_t1756533147 * Component_get_gameObject_m3105766835 (Component_t3819376471 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C"  void Object_Destroy_m4145850038 (Il2CppObject * __this /* static, unused */, Object_t1021602117 * p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DG.Tweening.Core.ABSAnimationComponent::.ctor()
extern "C"  void ABSAnimationComponent__ctor_m1064982270 (ABSAnimationComponent_t2205594551 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::Awake()
extern "C"  void DOTweenPath_Awake_m4245485586 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_Awake_m4245485586_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TweenerCore_3_t3035488489 * V_0 = NULL;
	Rigidbody_t4233889191 * V_1 = NULL;
	Transform_t3275118058 * V_2 = NULL;
	Vector3_t2243707580  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	ControlPoint_t168081159  V_7;
	memset(&V_7, 0, sizeof(V_7));
	int32_t V_8 = 0;
	Nullable_1_t506773895  V_9;
	memset(&V_9, 0, sizeof(V_9));
	TweenerCore_3_t3035488489 * G_B24_0 = NULL;
	TweenerCore_3_t3035488489 * G_B28_0 = NULL;
	{
		Path_t2828565993 * L_0 = __this->get_path_44();
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		List_1_t1612828712 * L_1 = __this->get_wps_42();
		int32_t L_2 = List_1_get_Count_m4027941115(L_1, /*hidden argument*/List_1_get_Count_m4027941115_MethodInfo_var);
		if ((((int32_t)L_2) < ((int32_t)1)))
		{
			goto IL_001f;
		}
	}
	{
		int32_t L_3 = __this->get_inspectorMode_45();
		if ((!(((uint32_t)L_3) == ((uint32_t)3))))
		{
			goto IL_0020;
		}
	}

IL_001f:
	{
		return;
	}

IL_0020:
	{
		Path_t2828565993 * L_4 = __this->get_path_44();
		Path_t2828565993 * L_5 = __this->get_path_44();
		int32_t L_6 = L_5->get_type_3();
		Path_AssignDecoder_m3072882543(L_4, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		bool L_7 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_isUnityEditor_18();
		if (!L_7)
		{
			goto IL_0069;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(DOTween_t2276353038_il2cpp_TypeInfo_var);
		List_1_t3066263266 * L_8 = ((DOTween_t2276353038_StaticFields*)DOTween_t2276353038_il2cpp_TypeInfo_var->static_fields)->get_GizmosDelegates_22();
		Path_t2828565993 * L_9 = __this->get_path_44();
		IntPtr_t L_10;
		L_10.set_m_value_0((void*)(void*)Path_Draw_m755263667_MethodInfo_var);
		TweenCallback_t3697142134 * L_11 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_11, L_9, L_10, /*hidden argument*/NULL);
		List_1_Add_m2434052001(L_8, L_11, /*hidden argument*/List_1_Add_m2434052001_MethodInfo_var);
		Path_t2828565993 * L_12 = __this->get_path_44();
		Color_t2020392075  L_13 = __this->get_pathColor_53();
		L_12->set_gizmoColor_20(L_13);
	}

IL_0069:
	{
		bool L_14 = __this->get_isLocal_33();
		if (!L_14)
		{
			goto IL_015c;
		}
	}
	{
		Transform_t3275118058 * L_15 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		V_2 = L_15;
		Transform_t3275118058 * L_16 = V_2;
		Transform_t3275118058 * L_17 = Transform_get_parent_m147407266(L_16, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_18 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_17, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_015c;
		}
	}
	{
		Transform_t3275118058 * L_19 = V_2;
		Transform_t3275118058 * L_20 = Transform_get_parent_m147407266(L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Transform_t3275118058 * L_21 = V_2;
		Vector3_t2243707580  L_22 = Transform_get_position_m1104419803(L_21, /*hidden argument*/NULL);
		V_3 = L_22;
		Path_t2828565993 * L_23 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_24 = L_23->get_wps_6();
		V_4 = (((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))));
		V_5 = 0;
		goto IL_00de;
	}

IL_00ae:
	{
		Path_t2828565993 * L_25 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_26 = L_25->get_wps_6();
		int32_t L_27 = V_5;
		Path_t2828565993 * L_28 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_29 = L_28->get_wps_6();
		int32_t L_30 = V_5;
		int32_t L_31 = L_30;
		Vector3_t2243707580  L_32 = (L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_31));
		Vector3_t2243707580  L_33 = V_3;
		Vector3_t2243707580  L_34 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		(L_26)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_27), (Vector3_t2243707580 )L_34);
		int32_t L_35 = V_5;
		V_5 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00de:
	{
		int32_t L_36 = V_5;
		int32_t L_37 = V_4;
		if ((((int32_t)L_36) < ((int32_t)L_37)))
		{
			goto IL_00ae;
		}
	}
	{
		Path_t2828565993 * L_38 = __this->get_path_44();
		ControlPointU5BU5D_t246481150* L_39 = L_38->get_controlPoints_7();
		V_4 = (((int32_t)((int32_t)(((Il2CppArray *)L_39)->max_length))));
		V_6 = 0;
		goto IL_0156;
	}

IL_00f8:
	{
		Path_t2828565993 * L_40 = __this->get_path_44();
		ControlPointU5BU5D_t246481150* L_41 = L_40->get_controlPoints_7();
		int32_t L_42 = V_6;
		int32_t L_43 = L_42;
		ControlPoint_t168081159  L_44 = (L_41)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_43));
		V_7 = L_44;
		Vector3_t2243707580 * L_45 = (&V_7)->get_address_of_a_0();
		Vector3_t2243707580 * L_46 = L_45;
		Vector3_t2243707580  L_47 = V_3;
		Vector3_t2243707580  L_48 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, (*(Vector3_t2243707580 *)L_46), L_47, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_46) = L_48;
		Vector3_t2243707580 * L_49 = (&V_7)->get_address_of_b_1();
		Vector3_t2243707580 * L_50 = L_49;
		Vector3_t2243707580  L_51 = V_3;
		Vector3_t2243707580  L_52 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, (*(Vector3_t2243707580 *)L_50), L_51, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_50) = L_52;
		Path_t2828565993 * L_53 = __this->get_path_44();
		ControlPointU5BU5D_t246481150* L_54 = L_53->get_controlPoints_7();
		int32_t L_55 = V_6;
		ControlPoint_t168081159  L_56 = V_7;
		(L_54)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_55), (ControlPoint_t168081159 )L_56);
		int32_t L_57 = V_6;
		V_6 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_0156:
	{
		int32_t L_58 = V_6;
		int32_t L_59 = V_4;
		if ((((int32_t)L_58) < ((int32_t)L_59)))
		{
			goto IL_00f8;
		}
	}

IL_015c:
	{
		bool L_60 = __this->get_relative_32();
		if (!L_60)
		{
			goto IL_016a;
		}
	}
	{
		DOTweenPath_ReEvaluateRelativeTween_m3742090720(__this, /*hidden argument*/NULL);
	}

IL_016a:
	{
		int32_t L_61 = __this->get_pathMode_36();
		if ((!(((uint32_t)L_61) == ((uint32_t)1))))
		{
			goto IL_0188;
		}
	}
	{
		SpriteRenderer_t1209076198 * L_62 = Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t1209076198_m1607822859_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_63 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_62, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_63)
		{
			goto IL_0188;
		}
	}
	{
		__this->set_pathMode_36(2);
	}

IL_0188:
	{
		Rigidbody_t4233889191 * L_64 = Component_GetComponent_TisRigidbody_t4233889191_m2973586618(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t4233889191_m2973586618_MethodInfo_var);
		V_1 = L_64;
		bool L_65 = __this->get_tweenRigidbody_41();
		if (!L_65)
		{
			goto IL_0201;
		}
	}
	{
		Rigidbody_t4233889191 * L_66 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_67 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_66, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_0201;
		}
	}
	{
		bool L_68 = __this->get_isLocal_33();
		if (L_68)
		{
			goto IL_01d4;
		}
	}
	{
		Rigidbody_t4233889191 * L_69 = V_1;
		Path_t2828565993 * L_70 = __this->get_path_44();
		float L_71 = __this->get_duration_20();
		int32_t L_72 = __this->get_pathMode_36();
		TweenerCore_3_t3035488489 * L_73 = ShortcutExtensions_DOPath_m1344615108(NULL /*static, unused*/, L_69, L_70, L_71, L_72, /*hidden argument*/NULL);
		bool L_74 = __this->get_isClosedPath_34();
		int32_t L_75 = __this->get_lockRotation_37();
		TweenerCore_3_t3035488489 * L_76 = TweenSettingsExtensions_SetOptions_m3069323763(NULL /*static, unused*/, L_73, L_74, 0, L_75, /*hidden argument*/NULL);
		G_B24_0 = L_76;
		goto IL_01fe;
	}

IL_01d4:
	{
		Rigidbody_t4233889191 * L_77 = V_1;
		Path_t2828565993 * L_78 = __this->get_path_44();
		float L_79 = __this->get_duration_20();
		int32_t L_80 = __this->get_pathMode_36();
		TweenerCore_3_t3035488489 * L_81 = ShortcutExtensions_DOLocalPath_m3055576589(NULL /*static, unused*/, L_77, L_78, L_79, L_80, /*hidden argument*/NULL);
		bool L_82 = __this->get_isClosedPath_34();
		int32_t L_83 = __this->get_lockRotation_37();
		TweenerCore_3_t3035488489 * L_84 = TweenSettingsExtensions_SetOptions_m3069323763(NULL /*static, unused*/, L_81, L_82, 0, L_83, /*hidden argument*/NULL);
		G_B24_0 = L_84;
	}

IL_01fe:
	{
		V_0 = G_B24_0;
		goto IL_026a;
	}

IL_0201:
	{
		bool L_85 = __this->get_isLocal_33();
		if (L_85)
		{
			goto IL_023a;
		}
	}
	{
		Transform_t3275118058 * L_86 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Path_t2828565993 * L_87 = __this->get_path_44();
		float L_88 = __this->get_duration_20();
		int32_t L_89 = __this->get_pathMode_36();
		TweenerCore_3_t3035488489 * L_90 = ShortcutExtensions_DOPath_m251740173(NULL /*static, unused*/, L_86, L_87, L_88, L_89, /*hidden argument*/NULL);
		bool L_91 = __this->get_isClosedPath_34();
		int32_t L_92 = __this->get_lockRotation_37();
		TweenerCore_3_t3035488489 * L_93 = TweenSettingsExtensions_SetOptions_m3069323763(NULL /*static, unused*/, L_90, L_91, 0, L_92, /*hidden argument*/NULL);
		G_B28_0 = L_93;
		goto IL_0269;
	}

IL_023a:
	{
		Transform_t3275118058 * L_94 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Path_t2828565993 * L_95 = __this->get_path_44();
		float L_96 = __this->get_duration_20();
		int32_t L_97 = __this->get_pathMode_36();
		TweenerCore_3_t3035488489 * L_98 = ShortcutExtensions_DOLocalPath_m712666778(NULL /*static, unused*/, L_94, L_95, L_96, L_97, /*hidden argument*/NULL);
		bool L_99 = __this->get_isClosedPath_34();
		int32_t L_100 = __this->get_lockRotation_37();
		TweenerCore_3_t3035488489 * L_101 = TweenSettingsExtensions_SetOptions_m3069323763(NULL /*static, unused*/, L_98, L_99, 0, L_100, /*hidden argument*/NULL);
		G_B28_0 = L_101;
	}

IL_0269:
	{
		V_0 = G_B28_0;
	}

IL_026a:
	{
		int32_t L_102 = __this->get_orientType_26();
		V_8 = L_102;
		int32_t L_103 = V_8;
		switch (((int32_t)((int32_t)L_103-(int32_t)1)))
		{
			case 0:
			{
				goto IL_0343;
			}
			case 1:
			{
				goto IL_028c;
			}
			case 2:
			{
				goto IL_02f3;
			}
		}
	}
	{
		goto IL_0391;
	}

IL_028c:
	{
		Transform_t3275118058 * L_104 = __this->get_lookAtTransform_27();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_105 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_104, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_105)
		{
			goto IL_0391;
		}
	}
	{
		bool L_106 = __this->get_assignForwardAndUp_38();
		if (!L_106)
		{
			goto IL_02cd;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_107 = V_0;
		Transform_t3275118058 * L_108 = __this->get_lookAtTransform_27();
		Vector3_t2243707580  L_109 = __this->get_forwardDirection_39();
		Nullable_1_t506773895  L_110;
		memset(&L_110, 0, sizeof(L_110));
		Nullable_1__ctor_m3622474896(&L_110, L_109, /*hidden argument*/Nullable_1__ctor_m3622474896_MethodInfo_var);
		Vector3_t2243707580  L_111 = __this->get_upDirection_40();
		Nullable_1_t506773895  L_112;
		memset(&L_112, 0, sizeof(L_112));
		Nullable_1__ctor_m3622474896(&L_112, L_111, /*hidden argument*/Nullable_1__ctor_m3622474896_MethodInfo_var);
		TweenSettingsExtensions_SetLookAt_m2149060425(NULL /*static, unused*/, L_107, L_108, L_110, L_112, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_02cd:
	{
		TweenerCore_3_t3035488489 * L_113 = V_0;
		Transform_t3275118058 * L_114 = __this->get_lookAtTransform_27();
		Initobj (Nullable_1_t506773895_il2cpp_TypeInfo_var, (&V_9));
		Nullable_1_t506773895  L_115 = V_9;
		Initobj (Nullable_1_t506773895_il2cpp_TypeInfo_var, (&V_9));
		Nullable_1_t506773895  L_116 = V_9;
		TweenSettingsExtensions_SetLookAt_m2149060425(NULL /*static, unused*/, L_113, L_114, L_115, L_116, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_02f3:
	{
		bool L_117 = __this->get_assignForwardAndUp_38();
		if (!L_117)
		{
			goto IL_0320;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_118 = V_0;
		Vector3_t2243707580  L_119 = __this->get_lookAtPosition_28();
		Vector3_t2243707580  L_120 = __this->get_forwardDirection_39();
		Nullable_1_t506773895  L_121;
		memset(&L_121, 0, sizeof(L_121));
		Nullable_1__ctor_m3622474896(&L_121, L_120, /*hidden argument*/Nullable_1__ctor_m3622474896_MethodInfo_var);
		Vector3_t2243707580  L_122 = __this->get_upDirection_40();
		Nullable_1_t506773895  L_123;
		memset(&L_123, 0, sizeof(L_123));
		Nullable_1__ctor_m3622474896(&L_123, L_122, /*hidden argument*/Nullable_1__ctor_m3622474896_MethodInfo_var);
		TweenSettingsExtensions_SetLookAt_m1482035211(NULL /*static, unused*/, L_118, L_119, L_121, L_123, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0320:
	{
		TweenerCore_3_t3035488489 * L_124 = V_0;
		Vector3_t2243707580  L_125 = __this->get_lookAtPosition_28();
		Initobj (Nullable_1_t506773895_il2cpp_TypeInfo_var, (&V_9));
		Nullable_1_t506773895  L_126 = V_9;
		Initobj (Nullable_1_t506773895_il2cpp_TypeInfo_var, (&V_9));
		Nullable_1_t506773895  L_127 = V_9;
		TweenSettingsExtensions_SetLookAt_m1482035211(NULL /*static, unused*/, L_124, L_125, L_126, L_127, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0343:
	{
		bool L_128 = __this->get_assignForwardAndUp_38();
		if (!L_128)
		{
			goto IL_0370;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_129 = V_0;
		float L_130 = __this->get_lookAhead_29();
		Vector3_t2243707580  L_131 = __this->get_forwardDirection_39();
		Nullable_1_t506773895  L_132;
		memset(&L_132, 0, sizeof(L_132));
		Nullable_1__ctor_m3622474896(&L_132, L_131, /*hidden argument*/Nullable_1__ctor_m3622474896_MethodInfo_var);
		Vector3_t2243707580  L_133 = __this->get_upDirection_40();
		Nullable_1_t506773895  L_134;
		memset(&L_134, 0, sizeof(L_134));
		Nullable_1__ctor_m3622474896(&L_134, L_133, /*hidden argument*/Nullable_1__ctor_m3622474896_MethodInfo_var);
		TweenSettingsExtensions_SetLookAt_m4115581975(NULL /*static, unused*/, L_129, L_130, L_132, L_134, /*hidden argument*/NULL);
		goto IL_0391;
	}

IL_0370:
	{
		TweenerCore_3_t3035488489 * L_135 = V_0;
		float L_136 = __this->get_lookAhead_29();
		Initobj (Nullable_1_t506773895_il2cpp_TypeInfo_var, (&V_9));
		Nullable_1_t506773895  L_137 = V_9;
		Initobj (Nullable_1_t506773895_il2cpp_TypeInfo_var, (&V_9));
		Nullable_1_t506773895  L_138 = V_9;
		TweenSettingsExtensions_SetLookAt_m4115581975(NULL /*static, unused*/, L_135, L_136, L_137, L_138, /*hidden argument*/NULL);
	}

IL_0391:
	{
		TweenerCore_3_t3035488489 * L_139 = V_0;
		float L_140 = __this->get_delay_19();
		TweenerCore_3_t3035488489 * L_141 = TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3035488489_m2218308542(NULL /*static, unused*/, L_139, L_140, /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t3035488489_m2218308542_MethodInfo_var);
		int32_t L_142 = __this->get_loops_23();
		int32_t L_143 = __this->get_loopType_25();
		TweenerCore_3_t3035488489 * L_144 = TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3035488489_m770682058(NULL /*static, unused*/, L_141, L_142, L_143, /*hidden argument*/TweenSettingsExtensions_SetLoops_TisTweenerCore_3_t3035488489_m770682058_MethodInfo_var);
		bool L_145 = __this->get_autoKill_31();
		TweenerCore_3_t3035488489 * L_146 = TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3035488489_m2792702168(NULL /*static, unused*/, L_144, L_145, /*hidden argument*/TweenSettingsExtensions_SetAutoKill_TisTweenerCore_3_t3035488489_m2792702168_MethodInfo_var);
		int32_t L_147 = ((ABSAnimationComponent_t2205594551 *)__this)->get_updateType_2();
		TweenerCore_3_t3035488489 * L_148 = TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3035488489_m3712068125(NULL /*static, unused*/, L_146, L_147, /*hidden argument*/TweenSettingsExtensions_SetUpdate_TisTweenerCore_3_t3035488489_m3712068125_MethodInfo_var);
		IntPtr_t L_149;
		L_149.set_m_value_0((void*)(void*)DOTweenPath_U3CAwakeU3Eb__38_0_m2435648440_MethodInfo_var);
		TweenCallback_t3697142134 * L_150 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_150, __this, L_149, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3035488489_m3007568601(NULL /*static, unused*/, L_148, L_150, /*hidden argument*/TweenSettingsExtensions_OnKill_TisTweenerCore_3_t3035488489_m3007568601_MethodInfo_var);
		bool L_151 = ((ABSAnimationComponent_t2205594551 *)__this)->get_isSpeedBased_3();
		if (!L_151)
		{
			goto IL_03e5;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_152 = V_0;
		TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3035488489_m2570913546(NULL /*static, unused*/, L_152, /*hidden argument*/TweenSettingsExtensions_SetSpeedBased_TisTweenerCore_3_t3035488489_m2570913546_MethodInfo_var);
	}

IL_03e5:
	{
		int32_t L_153 = __this->get_easeType_21();
		if ((!(((uint32_t)L_153) == ((uint32_t)((int32_t)37)))))
		{
			goto IL_03fe;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_154 = V_0;
		AnimationCurve_t3306541151 * L_155 = __this->get_easeCurve_22();
		TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m3271531062(NULL /*static, unused*/, L_154, L_155, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m3271531062_MethodInfo_var);
		goto IL_040b;
	}

IL_03fe:
	{
		TweenerCore_3_t3035488489 * L_156 = V_0;
		int32_t L_157 = __this->get_easeType_21();
		TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m1874820216(NULL /*static, unused*/, L_156, L_157, /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t3035488489_m1874820216_MethodInfo_var);
	}

IL_040b:
	{
		String_t* L_158 = __this->get_id_24();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_159 = String_IsNullOrEmpty_m2802126737(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		if (L_159)
		{
			goto IL_0425;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_160 = V_0;
		String_t* L_161 = __this->get_id_24();
		TweenSettingsExtensions_SetId_TisTweenerCore_3_t3035488489_m1640877877(NULL /*static, unused*/, L_160, L_161, /*hidden argument*/TweenSettingsExtensions_SetId_TisTweenerCore_3_t3035488489_m1640877877_MethodInfo_var);
	}

IL_0425:
	{
		bool L_162 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnStart_4();
		if (!L_162)
		{
			goto IL_044f;
		}
	}
	{
		UnityEvent_t408735097 * L_163 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStart_11();
		if (!L_163)
		{
			goto IL_0456;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_164 = V_0;
		UnityEvent_t408735097 * L_165 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStart_11();
		IntPtr_t L_166;
		L_166.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_167 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_167, L_165, L_166, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3035488489_m75273675(NULL /*static, unused*/, L_164, L_167, /*hidden argument*/TweenSettingsExtensions_OnStart_TisTweenerCore_3_t3035488489_m75273675_MethodInfo_var);
		goto IL_0456;
	}

IL_044f:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onStart_11((UnityEvent_t408735097 *)NULL);
	}

IL_0456:
	{
		bool L_168 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnPlay_5();
		if (!L_168)
		{
			goto IL_0480;
		}
	}
	{
		UnityEvent_t408735097 * L_169 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onPlay_12();
		if (!L_169)
		{
			goto IL_0487;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_170 = V_0;
		UnityEvent_t408735097 * L_171 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onPlay_12();
		IntPtr_t L_172;
		L_172.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_173 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_173, L_171, L_172, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3035488489_m3850970939(NULL /*static, unused*/, L_170, L_173, /*hidden argument*/TweenSettingsExtensions_OnPlay_TisTweenerCore_3_t3035488489_m3850970939_MethodInfo_var);
		goto IL_0487;
	}

IL_0480:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onPlay_12((UnityEvent_t408735097 *)NULL);
	}

IL_0487:
	{
		bool L_174 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnUpdate_6();
		if (!L_174)
		{
			goto IL_04b1;
		}
	}
	{
		UnityEvent_t408735097 * L_175 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onUpdate_13();
		if (!L_175)
		{
			goto IL_04b8;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_176 = V_0;
		UnityEvent_t408735097 * L_177 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onUpdate_13();
		IntPtr_t L_178;
		L_178.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_179 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_179, L_177, L_178, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3035488489_m1014346626(NULL /*static, unused*/, L_176, L_179, /*hidden argument*/TweenSettingsExtensions_OnUpdate_TisTweenerCore_3_t3035488489_m1014346626_MethodInfo_var);
		goto IL_04b8;
	}

IL_04b1:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onUpdate_13((UnityEvent_t408735097 *)NULL);
	}

IL_04b8:
	{
		bool L_180 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnStepComplete_7();
		if (!L_180)
		{
			goto IL_04e2;
		}
	}
	{
		UnityEvent_t408735097 * L_181 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStepComplete_14();
		if (!L_181)
		{
			goto IL_04e9;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_182 = V_0;
		UnityEvent_t408735097 * L_183 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onStepComplete_14();
		IntPtr_t L_184;
		L_184.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_185 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_185, L_183, L_184, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3035488489_m689171214(NULL /*static, unused*/, L_182, L_185, /*hidden argument*/TweenSettingsExtensions_OnStepComplete_TisTweenerCore_3_t3035488489_m689171214_MethodInfo_var);
		goto IL_04e9;
	}

IL_04e2:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onStepComplete_14((UnityEvent_t408735097 *)NULL);
	}

IL_04e9:
	{
		bool L_186 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnComplete_8();
		if (!L_186)
		{
			goto IL_0513;
		}
	}
	{
		UnityEvent_t408735097 * L_187 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onComplete_15();
		if (!L_187)
		{
			goto IL_051a;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_188 = V_0;
		UnityEvent_t408735097 * L_189 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onComplete_15();
		IntPtr_t L_190;
		L_190.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_191 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_191, L_189, L_190, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3035488489_m1168345000(NULL /*static, unused*/, L_188, L_191, /*hidden argument*/TweenSettingsExtensions_OnComplete_TisTweenerCore_3_t3035488489_m1168345000_MethodInfo_var);
		goto IL_051a;
	}

IL_0513:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onComplete_15((UnityEvent_t408735097 *)NULL);
	}

IL_051a:
	{
		bool L_192 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnRewind_10();
		if (!L_192)
		{
			goto IL_0544;
		}
	}
	{
		UnityEvent_t408735097 * L_193 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onRewind_17();
		if (!L_193)
		{
			goto IL_054b;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_194 = V_0;
		UnityEvent_t408735097 * L_195 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onRewind_17();
		IntPtr_t L_196;
		L_196.set_m_value_0((void*)(void*)UnityEvent_Invoke_m4163344491_MethodInfo_var);
		TweenCallback_t3697142134 * L_197 = (TweenCallback_t3697142134 *)il2cpp_codegen_object_new(TweenCallback_t3697142134_il2cpp_TypeInfo_var);
		TweenCallback__ctor_m3479200459(L_197, L_195, L_196, /*hidden argument*/NULL);
		TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3035488489_m118158764(NULL /*static, unused*/, L_194, L_197, /*hidden argument*/TweenSettingsExtensions_OnRewind_TisTweenerCore_3_t3035488489_m118158764_MethodInfo_var);
		goto IL_054b;
	}

IL_0544:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_onRewind_17((UnityEvent_t408735097 *)NULL);
	}

IL_054b:
	{
		bool L_198 = __this->get_autoPlay_30();
		if (!L_198)
		{
			goto IL_055c;
		}
	}
	{
		TweenerCore_3_t3035488489 * L_199 = V_0;
		TweenExtensions_Play_TisTweenerCore_3_t3035488489_m2599375885(NULL /*static, unused*/, L_199, /*hidden argument*/TweenExtensions_Play_TisTweenerCore_3_t3035488489_m2599375885_MethodInfo_var);
		goto IL_0563;
	}

IL_055c:
	{
		TweenerCore_3_t3035488489 * L_200 = V_0;
		TweenExtensions_Pause_TisTweenerCore_3_t3035488489_m2596183611(NULL /*static, unused*/, L_200, /*hidden argument*/TweenExtensions_Pause_TisTweenerCore_3_t3035488489_m2596183611_MethodInfo_var);
	}

IL_0563:
	{
		TweenerCore_3_t3035488489 * L_201 = V_0;
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18(L_201);
		bool L_202 = ((ABSAnimationComponent_t2205594551 *)__this)->get_hasOnTweenCreated_9();
		if (!L_202)
		{
			goto IL_0585;
		}
	}
	{
		UnityEvent_t408735097 * L_203 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onTweenCreated_16();
		if (!L_203)
		{
			goto IL_0585;
		}
	}
	{
		UnityEvent_t408735097 * L_204 = ((ABSAnimationComponent_t2205594551 *)__this)->get_onTweenCreated_16();
		UnityEvent_Invoke_m4163344491(L_204, /*hidden argument*/NULL);
	}

IL_0585:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::Reset()
extern "C"  void DOTweenPath_Reset_m2024650022 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_Reset_m2024650022_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = __this->get_pathType_46();
		List_1_t1612828712 * L_1 = __this->get_wps_42();
		Vector3U5BU5D_t1172311765* L_2 = List_1_ToArray_m2187942201(L_1, /*hidden argument*/List_1_ToArray_m2187942201_MethodInfo_var);
		Color_t2020392075  L_3 = __this->get_pathColor_53();
		Nullable_1_t283458390  L_4;
		memset(&L_4, 0, sizeof(L_4));
		Nullable_1__ctor_m2519817708(&L_4, L_3, /*hidden argument*/Nullable_1__ctor_m2519817708_MethodInfo_var);
		Path_t2828565993 * L_5 = (Path_t2828565993 *)il2cpp_codegen_object_new(Path_t2828565993_il2cpp_TypeInfo_var);
		Path__ctor_m4253660657(L_5, L_0, L_2, ((int32_t)10), L_4, /*hidden argument*/NULL);
		__this->set_path_44(L_5);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::OnDestroy()
extern "C"  void DOTweenPath_OnDestroy_m3424691388 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		Tween_t278478013 * L_1 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		bool L_2 = L_1->get_active_35();
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		Tween_t278478013 * L_3 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Kill_m3845739180(NULL /*static, unused*/, L_3, (bool)0, /*hidden argument*/NULL);
	}

IL_0021:
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18((Tween_t278478013 *)NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlay()
extern "C"  void DOTweenPath_DOPlay_m1676033874 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_DOPlay_m1676033874_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Play_TisTween_t278478013_m2584288383(NULL /*static, unused*/, L_0, /*hidden argument*/TweenExtensions_Play_TisTween_t278478013_m2584288383_MethodInfo_var);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlayBackwards()
extern "C"  void DOTweenPath_DOPlayBackwards_m1068562042 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_PlayBackwards_m613887607(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPlayForward()
extern "C"  void DOTweenPath_DOPlayForward_m2641353125 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_PlayForward_m3704378858(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOPause()
extern "C"  void DOTweenPath_DOPause_m1913698988 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_DOPause_m1913698988_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Pause_TisTween_t278478013_m3798767189(NULL /*static, unused*/, L_0, /*hidden argument*/TweenExtensions_Pause_TisTween_t278478013_m3798767189_MethodInfo_var);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOTogglePause()
extern "C"  void DOTweenPath_DOTogglePause_m1113541172 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_TogglePause_m1054448567(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DORewind()
extern "C"  void DOTweenPath_DORewind_m2434078477 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Rewind_m3286148139(NULL /*static, unused*/, L_0, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DORestart(System.Boolean)
extern "C"  void DOTweenPath_DORestart_m3437720452 (DOTweenPath_t1397145371 * __this, bool ___fromHere0, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_DORestart_m3437720452_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (L_0)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_1 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_1) <= ((int32_t)1)))
		{
			goto IL_001b;
		}
	}
	{
		Tween_t278478013 * L_2 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		Debugger_LogNullTween_m832479276(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		return;
	}

IL_001c:
	{
		bool L_3 = ___fromHere0;
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		bool L_4 = __this->get_relative_32();
		if (!L_4)
		{
			goto IL_0035;
		}
	}
	{
		bool L_5 = __this->get_isLocal_33();
		if (L_5)
		{
			goto IL_0035;
		}
	}
	{
		DOTweenPath_ReEvaluateRelativeTween_m3742090720(__this, /*hidden argument*/NULL);
	}

IL_0035:
	{
		Tween_t278478013 * L_6 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Restart_m2056936246(NULL /*static, unused*/, L_6, (bool)1, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOComplete()
extern "C"  void DOTweenPath_DOComplete_m3005641335 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Complete_m274286092(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::DOKill()
extern "C"  void DOTweenPath_DOKill_m3156241080 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		TweenExtensions_Kill_m3845739180(NULL /*static, unused*/, L_0, (bool)0, /*hidden argument*/NULL);
		return;
	}
}
// DG.Tweening.Tween DG.Tweening.DOTweenPath::GetTween()
extern "C"  Tween_t278478013 * DOTweenPath_GetTween_m2167448416 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_GetTween_m2167448416_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Tween_t278478013 * L_0 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		Tween_t278478013 * L_1 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		bool L_2 = L_1->get_active_35();
		if (L_2)
		{
			goto IL_003f;
		}
	}

IL_0015:
	{
		int32_t L_3 = ((Debugger_t1404542751_StaticFields*)Debugger_t1404542751_il2cpp_TypeInfo_var->static_fields)->get_logPriority_0();
		if ((((int32_t)L_3) <= ((int32_t)1)))
		{
			goto IL_003d;
		}
	}
	{
		Tween_t278478013 * L_4 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		Tween_t278478013 * L_5 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		Debugger_LogNullTween_m832479276(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		goto IL_003d;
	}

IL_0032:
	{
		Tween_t278478013 * L_6 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		Debugger_LogInvalidTween_m3454295394(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_003d:
	{
		return (Tween_t278478013 *)NULL;
	}

IL_003f:
	{
		Tween_t278478013 * L_7 = ((ABSAnimationComponent_t2205594551 *)__this)->get_tween_18();
		return L_7;
	}
}
// UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetDrawPoints()
extern "C"  Vector3U5BU5D_t1172311765* DOTweenPath_GetDrawPoints_m1924516330 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_GetDrawPoints_m1924516330_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Path_t2828565993 * L_0 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_1 = L_0->get_wps_6();
		if (!L_1)
		{
			goto IL_001a;
		}
	}
	{
		Path_t2828565993 * L_2 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_3 = L_2->get_nonLinearDrawWps_17();
		if (L_3)
		{
			goto IL_0026;
		}
	}

IL_001a:
	{
		Debugger_LogWarning_m1294243619(NULL /*static, unused*/, _stringLiteral3098430486, /*hidden argument*/NULL);
		return (Vector3U5BU5D_t1172311765*)NULL;
	}

IL_0026:
	{
		int32_t L_4 = __this->get_pathType_46();
		if (L_4)
		{
			goto IL_003a;
		}
	}
	{
		Path_t2828565993 * L_5 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_6 = L_5->get_wps_6();
		return L_6;
	}

IL_003a:
	{
		Path_t2828565993 * L_7 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_8 = L_7->get_nonLinearDrawWps_17();
		return L_8;
	}
}
// UnityEngine.Vector3[] DG.Tweening.DOTweenPath::GetFullWps()
extern "C"  Vector3U5BU5D_t1172311765* DOTweenPath_GetFullWps_m2783782328 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath_GetFullWps_m2783782328_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Vector3U5BU5D_t1172311765* V_2 = NULL;
	int32_t V_3 = 0;
	{
		List_1_t1612828712 * L_0 = __this->get_wps_42();
		int32_t L_1 = List_1_get_Count_m4027941115(L_0, /*hidden argument*/List_1_get_Count_m4027941115_MethodInfo_var);
		V_0 = L_1;
		int32_t L_2 = V_0;
		V_1 = ((int32_t)((int32_t)L_2+(int32_t)1));
		bool L_3 = __this->get_isClosedPath_34();
		if (!L_3)
		{
			goto IL_001c;
		}
	}
	{
		int32_t L_4 = V_1;
		V_1 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_5 = V_1;
		V_2 = ((Vector3U5BU5D_t1172311765*)SZArrayNew(Vector3U5BU5D_t1172311765_il2cpp_TypeInfo_var, (uint32_t)L_5));
		Vector3U5BU5D_t1172311765* L_6 = V_2;
		Transform_t3275118058 * L_7 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_8 = Transform_get_position_m1104419803(L_7, /*hidden argument*/NULL);
		(L_6)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Vector3_t2243707580 )L_8);
		V_3 = 0;
		goto IL_0052;
	}

IL_0039:
	{
		Vector3U5BU5D_t1172311765* L_9 = V_2;
		int32_t L_10 = V_3;
		List_1_t1612828712 * L_11 = __this->get_wps_42();
		int32_t L_12 = V_3;
		Vector3_t2243707580  L_13 = List_1_get_Item_m2503489122(L_11, L_12, /*hidden argument*/List_1_get_Item_m2503489122_MethodInfo_var);
		(L_9)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_10+(int32_t)1))), (Vector3_t2243707580 )L_13);
		int32_t L_14 = V_3;
		V_3 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0052:
	{
		int32_t L_15 = V_3;
		int32_t L_16 = V_0;
		if ((((int32_t)L_15) < ((int32_t)L_16)))
		{
			goto IL_0039;
		}
	}
	{
		bool L_17 = __this->get_isClosedPath_34();
		if (!L_17)
		{
			goto IL_006e;
		}
	}
	{
		Vector3U5BU5D_t1172311765* L_18 = V_2;
		int32_t L_19 = V_1;
		Vector3U5BU5D_t1172311765* L_20 = V_2;
		int32_t L_21 = 0;
		Vector3_t2243707580  L_22 = (L_20)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_21));
		(L_18)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_19-(int32_t)1))), (Vector3_t2243707580 )L_22);
	}

IL_006e:
	{
		Vector3U5BU5D_t1172311765* L_23 = V_2;
		return L_23;
	}
}
// System.Void DG.Tweening.DOTweenPath::ReEvaluateRelativeTween()
extern "C"  void DOTweenPath_ReEvaluateRelativeTween_m3742090720 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	Vector3_t2243707580  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t2243707580  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	ControlPoint_t168081159  V_5;
	memset(&V_5, 0, sizeof(V_5));
	{
		Transform_t3275118058 * L_0 = Component_get_transform_m2697483695(__this, /*hidden argument*/NULL);
		Vector3_t2243707580  L_1 = Transform_get_position_m1104419803(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Vector3_t2243707580  L_2 = V_0;
		Vector3_t2243707580  L_3 = __this->get_lastSrcPosition_54();
		bool L_4 = Vector3_op_Equality_m305888255(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_001b;
		}
	}
	{
		return;
	}

IL_001b:
	{
		Vector3_t2243707580  L_5 = V_0;
		Vector3_t2243707580  L_6 = __this->get_lastSrcPosition_54();
		Vector3_t2243707580  L_7 = Vector3_op_Subtraction_m2407545601(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Path_t2828565993 * L_8 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_9 = L_8->get_wps_6();
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))));
		V_3 = 0;
		goto IL_0066;
	}

IL_003a:
	{
		Path_t2828565993 * L_10 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_11 = L_10->get_wps_6();
		int32_t L_12 = V_3;
		Path_t2828565993 * L_13 = __this->get_path_44();
		Vector3U5BU5D_t1172311765* L_14 = L_13->get_wps_6();
		int32_t L_15 = V_3;
		int32_t L_16 = L_15;
		Vector3_t2243707580  L_17 = (L_14)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_16));
		Vector3_t2243707580  L_18 = V_1;
		Vector3_t2243707580  L_19 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, L_17, L_18, /*hidden argument*/NULL);
		(L_11)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_12), (Vector3_t2243707580 )L_19);
		int32_t L_20 = V_3;
		V_3 = ((int32_t)((int32_t)L_20+(int32_t)1));
	}

IL_0066:
	{
		int32_t L_21 = V_3;
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_003a;
		}
	}
	{
		Path_t2828565993 * L_23 = __this->get_path_44();
		ControlPointU5BU5D_t246481150* L_24 = L_23->get_controlPoints_7();
		V_2 = (((int32_t)((int32_t)(((Il2CppArray *)L_24)->max_length))));
		V_4 = 0;
		goto IL_00db;
	}

IL_007d:
	{
		Path_t2828565993 * L_25 = __this->get_path_44();
		ControlPointU5BU5D_t246481150* L_26 = L_25->get_controlPoints_7();
		int32_t L_27 = V_4;
		int32_t L_28 = L_27;
		ControlPoint_t168081159  L_29 = (L_26)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_28));
		V_5 = L_29;
		Vector3_t2243707580 * L_30 = (&V_5)->get_address_of_a_0();
		Vector3_t2243707580 * L_31 = L_30;
		Vector3_t2243707580  L_32 = V_1;
		Vector3_t2243707580  L_33 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, (*(Vector3_t2243707580 *)L_31), L_32, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_31) = L_33;
		Vector3_t2243707580 * L_34 = (&V_5)->get_address_of_b_1();
		Vector3_t2243707580 * L_35 = L_34;
		Vector3_t2243707580  L_36 = V_1;
		Vector3_t2243707580  L_37 = Vector3_op_Addition_m3146764857(NULL /*static, unused*/, (*(Vector3_t2243707580 *)L_35), L_36, /*hidden argument*/NULL);
		(*(Vector3_t2243707580 *)L_35) = L_37;
		Path_t2828565993 * L_38 = __this->get_path_44();
		ControlPointU5BU5D_t246481150* L_39 = L_38->get_controlPoints_7();
		int32_t L_40 = V_4;
		ControlPoint_t168081159  L_41 = V_5;
		(L_39)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(L_40), (ControlPoint_t168081159 )L_41);
		int32_t L_42 = V_4;
		V_4 = ((int32_t)((int32_t)L_42+(int32_t)1));
	}

IL_00db:
	{
		int32_t L_43 = V_4;
		int32_t L_44 = V_2;
		if ((((int32_t)L_43) < ((int32_t)L_44)))
		{
			goto IL_007d;
		}
	}
	{
		Vector3_t2243707580  L_45 = V_0;
		__this->set_lastSrcPosition_54(L_45);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::.ctor()
extern "C"  void DOTweenPath__ctor_m577727613 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenPath__ctor_m577727613_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_duration_20((1.0f));
		__this->set_easeType_21(6);
		KeyframeU5BU5D_t449065829* L_0 = ((KeyframeU5BU5D_t449065829*)SZArrayNew(KeyframeU5BU5D_t449065829_il2cpp_TypeInfo_var, (uint32_t)2));
		Keyframe_t1449471340  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Keyframe__ctor_m2042404667(&L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		(L_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), (Keyframe_t1449471340 )L_1);
		KeyframeU5BU5D_t449065829* L_2 = L_0;
		Keyframe_t1449471340  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Keyframe__ctor_m2042404667(&L_3, (1.0f), (1.0f), /*hidden argument*/NULL);
		(L_2)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), (Keyframe_t1449471340 )L_3);
		AnimationCurve_t3306541151 * L_4 = (AnimationCurve_t3306541151 *)il2cpp_codegen_object_new(AnimationCurve_t3306541151_il2cpp_TypeInfo_var);
		AnimationCurve__ctor_m2814448007(L_4, L_2, /*hidden argument*/NULL);
		__this->set_easeCurve_22(L_4);
		__this->set_loops_23(1);
		__this->set_id_24(_stringLiteral371857150);
		__this->set_lookAhead_29((0.01f));
		__this->set_autoPlay_30((bool)1);
		__this->set_autoKill_31((bool)1);
		__this->set_pathResolution_35(((int32_t)10));
		__this->set_pathMode_36(1);
		Vector3_t2243707580  L_5 = Vector3_get_forward_m1201659139(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_forwardDirection_39(L_5);
		Vector3_t2243707580  L_6 = Vector3_get_up_m2725403797(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_upDirection_40(L_6);
		List_1_t1612828712 * L_7 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m3655021515(L_7, /*hidden argument*/List_1__ctor_m3655021515_MethodInfo_var);
		__this->set_wps_42(L_7);
		List_1_t1612828712 * L_8 = (List_1_t1612828712 *)il2cpp_codegen_object_new(List_1_t1612828712_il2cpp_TypeInfo_var);
		List_1__ctor_m3655021515(L_8, /*hidden argument*/List_1__ctor_m3655021515_MethodInfo_var);
		__this->set_fullWps_43(L_8);
		__this->set_livePreview_48((bool)1);
		__this->set_perspectiveHandleSize_50((0.5f));
		__this->set_showIndexes_51((bool)1);
		Color_t2020392075  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Color__ctor_m1909920690(&L_9, (1.0f), (1.0f), (1.0f), (0.5f), /*hidden argument*/NULL);
		__this->set_pathColor_53(L_9);
		ABSAnimationComponent__ctor_m1064982270(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenPath::<Awake>b__38_0()
extern "C"  void DOTweenPath_U3CAwakeU3Eb__38_0_m2435648440 (DOTweenPath_t1397145371 * __this, const MethodInfo* method)
{
	{
		((ABSAnimationComponent_t2205594551 *)__this)->set_tween_18((Tween_t278478013 *)NULL);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::Awake()
extern "C"  void DOTweenVisualManager_Awake_m2217727324 (DOTweenVisualManager_t2945673405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_Awake_m2217727324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ABSAnimationComponent_t2205594551 * L_0 = Component_GetComponent_TisABSAnimationComponent_t2205594551_m3887019028(__this, /*hidden argument*/Component_GetComponent_TisABSAnimationComponent_t2205594551_m3887019028_MethodInfo_var);
		__this->set__animComponent_6(L_0);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::Update()
extern "C"  void DOTweenVisualManager_Update_m340061598 (DOTweenVisualManager_t2945673405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_Update_m340061598_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = __this->get__requiresRestartFromSpawnPoint_5();
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_1 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m3764089466(NULL /*static, unused*/, L_1, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0017;
		}
	}

IL_0016:
	{
		return;
	}

IL_0017:
	{
		__this->set__requiresRestartFromSpawnPoint_5((bool)0);
		ABSAnimationComponent_t2205594551 * L_3 = __this->get__animComponent_6();
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean) */, L_3, (bool)1);
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::OnEnable()
extern "C"  void DOTweenVisualManager_OnEnable_m3831096439 (DOTweenVisualManager_t2945673405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_OnEnable_m3831096439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_onEnableBehaviour_3();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)1)))
		{
			case 0:
			{
				goto IL_001c;
			}
			case 1:
			{
				goto IL_0036;
			}
			case 2:
			{
				goto IL_0051;
			}
		}
	}
	{
		return;
	}

IL_001c:
	{
		ABSAnimationComponent_t2205594551 * L_2 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0058;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_4 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(4 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOPlay() */, L_4);
		return;
	}

IL_0036:
	{
		ABSAnimationComponent_t2205594551 * L_5 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0058;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_7 = __this->get__animComponent_6();
		VirtActionInvoker1< bool >::Invoke(10 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORestart(System.Boolean) */, L_7, (bool)0);
		return;
	}

IL_0051:
	{
		__this->set__requiresRestartFromSpawnPoint_5((bool)1);
	}

IL_0058:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::OnDisable()
extern "C"  void DOTweenVisualManager_OnDisable_m1016672072 (DOTweenVisualManager_t2945673405 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DOTweenVisualManager_OnDisable_m1016672072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->set__requiresRestartFromSpawnPoint_5((bool)0);
		int32_t L_0 = __this->get_onDisableBehaviour_4();
		V_0 = L_0;
		int32_t L_1 = V_0;
		switch (((int32_t)((int32_t)L_1-(int32_t)1)))
		{
			case 0:
			{
				goto IL_002b;
			}
			case 1:
			{
				goto IL_0048;
			}
			case 2:
			{
				goto IL_0062;
			}
			case 3:
			{
				goto IL_007c;
			}
			case 4:
			{
				goto IL_00a1;
			}
		}
	}
	{
		return;
	}

IL_002b:
	{
		ABSAnimationComponent_t2205594551 * L_2 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_2, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_4 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(7 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOPause() */, L_4);
		return;
	}

IL_0048:
	{
		ABSAnimationComponent_t2205594551 * L_5 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_5, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_7 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(9 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DORewind() */, L_7);
		return;
	}

IL_0062:
	{
		ABSAnimationComponent_t2205594551 * L_8 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_8, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_10 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_10);
		return;
	}

IL_007c:
	{
		ABSAnimationComponent_t2205594551 * L_11 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_12 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_11, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c5;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_13 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(11 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOComplete() */, L_13);
		ABSAnimationComponent_t2205594551 * L_14 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_14);
		return;
	}

IL_00a1:
	{
		ABSAnimationComponent_t2205594551 * L_15 = __this->get__animComponent_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		bool L_16 = Object_op_Inequality_m2402264703(NULL /*static, unused*/, L_15, (Object_t1021602117 *)NULL, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00ba;
		}
	}
	{
		ABSAnimationComponent_t2205594551 * L_17 = __this->get__animComponent_6();
		VirtActionInvoker0::Invoke(12 /* System.Void DG.Tweening.Core.ABSAnimationComponent::DOKill() */, L_17);
	}

IL_00ba:
	{
		GameObject_t1756533147 * L_18 = Component_get_gameObject_m3105766835(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t1021602117_il2cpp_TypeInfo_var);
		Object_Destroy_m4145850038(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
	}

IL_00c5:
	{
		return;
	}
}
// System.Void DG.Tweening.DOTweenVisualManager::.ctor()
extern "C"  void DOTweenVisualManager__ctor_m2110064643 (DOTweenVisualManager_t2945673405 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2464341955(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
