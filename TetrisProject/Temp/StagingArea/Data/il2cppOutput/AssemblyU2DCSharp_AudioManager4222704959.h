﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

#include "UnityEngine_UnityEngine_MonoBehaviour1158329972.h"

// UnityEngine.AudioClip
struct AudioClip_t1932558630;
// UnityEngine.AudioSource
struct AudioSource_t1135106623;




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AudioManager
struct  AudioManager_t4222704959  : public MonoBehaviour_t1158329972
{
public:
	// UnityEngine.AudioClip AudioManager::clickClip
	AudioClip_t1932558630 * ___clickClip_2;
	// UnityEngine.AudioClip AudioManager::fallClip
	AudioClip_t1932558630 * ___fallClip_3;
	// UnityEngine.AudioClip AudioManager::ctrlClip
	AudioClip_t1932558630 * ___ctrlClip_4;
	// UnityEngine.AudioClip AudioManager::deleteRowClip
	AudioClip_t1932558630 * ___deleteRowClip_5;
	// UnityEngine.AudioSource AudioManager::audioSource
	AudioSource_t1135106623 * ___audioSource_6;
	// System.Boolean AudioManager::isPause
	bool ___isPause_7;

public:
	inline static int32_t get_offset_of_clickClip_2() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___clickClip_2)); }
	inline AudioClip_t1932558630 * get_clickClip_2() const { return ___clickClip_2; }
	inline AudioClip_t1932558630 ** get_address_of_clickClip_2() { return &___clickClip_2; }
	inline void set_clickClip_2(AudioClip_t1932558630 * value)
	{
		___clickClip_2 = value;
		Il2CppCodeGenWriteBarrier(&___clickClip_2, value);
	}

	inline static int32_t get_offset_of_fallClip_3() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___fallClip_3)); }
	inline AudioClip_t1932558630 * get_fallClip_3() const { return ___fallClip_3; }
	inline AudioClip_t1932558630 ** get_address_of_fallClip_3() { return &___fallClip_3; }
	inline void set_fallClip_3(AudioClip_t1932558630 * value)
	{
		___fallClip_3 = value;
		Il2CppCodeGenWriteBarrier(&___fallClip_3, value);
	}

	inline static int32_t get_offset_of_ctrlClip_4() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___ctrlClip_4)); }
	inline AudioClip_t1932558630 * get_ctrlClip_4() const { return ___ctrlClip_4; }
	inline AudioClip_t1932558630 ** get_address_of_ctrlClip_4() { return &___ctrlClip_4; }
	inline void set_ctrlClip_4(AudioClip_t1932558630 * value)
	{
		___ctrlClip_4 = value;
		Il2CppCodeGenWriteBarrier(&___ctrlClip_4, value);
	}

	inline static int32_t get_offset_of_deleteRowClip_5() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___deleteRowClip_5)); }
	inline AudioClip_t1932558630 * get_deleteRowClip_5() const { return ___deleteRowClip_5; }
	inline AudioClip_t1932558630 ** get_address_of_deleteRowClip_5() { return &___deleteRowClip_5; }
	inline void set_deleteRowClip_5(AudioClip_t1932558630 * value)
	{
		___deleteRowClip_5 = value;
		Il2CppCodeGenWriteBarrier(&___deleteRowClip_5, value);
	}

	inline static int32_t get_offset_of_audioSource_6() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___audioSource_6)); }
	inline AudioSource_t1135106623 * get_audioSource_6() const { return ___audioSource_6; }
	inline AudioSource_t1135106623 ** get_address_of_audioSource_6() { return &___audioSource_6; }
	inline void set_audioSource_6(AudioSource_t1135106623 * value)
	{
		___audioSource_6 = value;
		Il2CppCodeGenWriteBarrier(&___audioSource_6, value);
	}

	inline static int32_t get_offset_of_isPause_7() { return static_cast<int32_t>(offsetof(AudioManager_t4222704959, ___isPause_7)); }
	inline bool get_isPause_7() const { return ___isPause_7; }
	inline bool* get_address_of_isPause_7() { return &___isPause_7; }
	inline void set_isPause_7(bool value)
	{
		___isPause_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
