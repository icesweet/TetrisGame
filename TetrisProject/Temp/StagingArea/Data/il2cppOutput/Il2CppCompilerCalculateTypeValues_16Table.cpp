﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "DOTween_DG_Tweening_Tween278478013.h"
#include "DOTween_DG_Tweening_Tweener760404022.h"
#include "DOTween_DG_Tweening_TweenType169444141.h"
#include "DOTween_DG_Tweening_UpdateType3357224513.h"
#include "DOTween_DG_Tweening_Plugins_Color2Plugin3433430606.h"
#include "DOTween_DG_Tweening_Plugins_DoublePlugin266400784.h"
#include "DOTween_DG_Tweening_Plugins_LongPlugin1941283029.h"
#include "DOTween_DG_Tweening_Plugins_UlongPlugin3231465400.h"
#include "DOTween_DG_Tweening_Plugins_Vector3ArrayPlugin2378569512.h"
#include "DOTween_DG_Tweening_Plugins_PathPlugin4171842066.h"
#include "DOTween_DG_Tweening_Plugins_ColorPlugin4063724482.h"
#include "DOTween_DG_Tweening_Plugins_IntPlugin180838436.h"
#include "DOTween_DG_Tweening_Plugins_QuaternionPlugin1696644323.h"
#include "DOTween_DG_Tweening_Plugins_RectOffsetPlugin664509336.h"
#include "DOTween_DG_Tweening_Plugins_RectPlugin391797831.h"
#include "DOTween_DG_Tweening_Plugins_UintPlugin1040977389.h"
#include "DOTween_DG_Tweening_Plugins_Vector2Plugin2164285386.h"
#include "DOTween_DG_Tweening_Plugins_Vector4Plugin2164361360.h"
#include "DOTween_DG_Tweening_Plugins_StringPlugin3620786088.h"
#include "DOTween_DG_Tweening_Plugins_StringPluginExtensions3910942986.h"
#include "DOTween_DG_Tweening_Plugins_FloatPlugin3639480371.h"
#include "DOTween_DG_Tweening_Plugins_Vector3Plugin2164530409.h"
#include "DOTween_DG_Tweening_Plugins_Options_OrientType1755667719.h"
#include "DOTween_DG_Tweening_Plugins_Options_PathOptions2659884781.h"
#include "DOTween_DG_Tweening_Plugins_Options_QuaternionOptio466049668.h"
#include "DOTween_DG_Tweening_Plugins_Options_UintOptions2267095136.h"
#include "DOTween_DG_Tweening_Plugins_Options_Vector3ArrayOp2672570171.h"
#include "DOTween_DG_Tweening_Plugins_Options_NoOptions2508431845.h"
#include "DOTween_DG_Tweening_Plugins_Options_ColorOptions2213017305.h"
#include "DOTween_DG_Tweening_Plugins_Options_FloatOptions1421548266.h"
#include "DOTween_DG_Tweening_Plugins_Options_RectOptions3393635162.h"
#include "DOTween_DG_Tweening_Plugins_Options_StringOptions2885323933.h"
#include "DOTween_DG_Tweening_Plugins_Options_VectorOptions293385261.h"
#include "DOTween_DG_Tweening_Plugins_Core_SpecialPluginsUti2241999250.h"
#include "DOTween_DG_Tweening_Plugins_Core_PluginsManager3052451537.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ControlPo168081159.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_ABSPathD3294469411.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_CatmullR3014762178.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_LinearDe2073524639.h"
#include "DOTween_DG_Tweening_Plugins_Core_PathCore_Path2828565993.h"
#include "DOTween_DG_Tweening_CustomPlugins_PureQuaternionPl3400666973.h"
#include "DOTween_DG_Tweening_Core_ABSSequentiable2284140720.h"
#include "DOTween_DG_Tweening_Core_Debugger1404542751.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent696744215.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2205815884.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF1216126710.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2574075760.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF1259698564.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF3956751645.h"
#include "DOTween_DG_Tweening_Core_DOTweenComponent_U3CWaitF2341562413.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings873123119.h"
#include "DOTween_DG_Tweening_Core_DOTweenSettings_SettingsLo514961325.h"
#include "DOTween_DG_Tweening_Core_Extensions507052800.h"
#include "DOTween_DG_Tweening_Core_SequenceCallback2782183128.h"
#include "DOTween_DG_Tweening_Core_TweenManager1979661952.h"
#include "DOTween_DG_Tweening_Core_TweenManager_CapacityIncr1969140739.h"
#include "DOTween_DG_Tweening_Core_Utils2524017187.h"
#include "DOTween_DG_Tweening_Core_Enums_FilterType1425068526.h"
#include "DOTween_DG_Tweening_Core_Enums_OperationType2600045009.h"
#include "DOTween_DG_Tweening_Core_Enums_SpecialStartupMode1501334721.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateNotice2468589887.h"
#include "DOTween_DG_Tweening_Core_Enums_UpdateMode2539919096.h"
#include "DOTween_DG_Tweening_Core_Easing_Bounce3273339050.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseManager1514337917.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseManager_U3CU3E1609106043.h"
#include "DOTween_DG_Tweening_Core_Easing_EaseCurve1295352409.h"
#include "DOTween_DG_Tweening_Core_Easing_Flash1282698556.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E1486305137.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Static978476011.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Stati3707359366.h"
#include "DOTween_U3CPrivateImplementationDetailsU3E___Stati1468992140.h"
#include "UnityEngine_UI_U3CModuleU3E3783534214.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventHandle942672932.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSyste3466835263.h"







#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1600 = { sizeof (Tween_t278478013), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1600[47] = 
{
	Tween_t278478013::get_offset_of_timeScale_4(),
	Tween_t278478013::get_offset_of_isBackwards_5(),
	Tween_t278478013::get_offset_of_id_6(),
	Tween_t278478013::get_offset_of_target_7(),
	Tween_t278478013::get_offset_of_updateType_8(),
	Tween_t278478013::get_offset_of_isIndependentUpdate_9(),
	Tween_t278478013::get_offset_of_onPlay_10(),
	Tween_t278478013::get_offset_of_onPause_11(),
	Tween_t278478013::get_offset_of_onRewind_12(),
	Tween_t278478013::get_offset_of_onUpdate_13(),
	Tween_t278478013::get_offset_of_onStepComplete_14(),
	Tween_t278478013::get_offset_of_onComplete_15(),
	Tween_t278478013::get_offset_of_onKill_16(),
	Tween_t278478013::get_offset_of_onWaypointChange_17(),
	Tween_t278478013::get_offset_of_isFrom_18(),
	Tween_t278478013::get_offset_of_isBlendable_19(),
	Tween_t278478013::get_offset_of_isRecyclable_20(),
	Tween_t278478013::get_offset_of_isSpeedBased_21(),
	Tween_t278478013::get_offset_of_autoKill_22(),
	Tween_t278478013::get_offset_of_duration_23(),
	Tween_t278478013::get_offset_of_loops_24(),
	Tween_t278478013::get_offset_of_loopType_25(),
	Tween_t278478013::get_offset_of_delay_26(),
	Tween_t278478013::get_offset_of_isRelative_27(),
	Tween_t278478013::get_offset_of_easeType_28(),
	Tween_t278478013::get_offset_of_customEase_29(),
	Tween_t278478013::get_offset_of_easeOvershootOrAmplitude_30(),
	Tween_t278478013::get_offset_of_easePeriod_31(),
	Tween_t278478013::get_offset_of_typeofT1_32(),
	Tween_t278478013::get_offset_of_typeofT2_33(),
	Tween_t278478013::get_offset_of_typeofTPlugOptions_34(),
	Tween_t278478013::get_offset_of_active_35(),
	Tween_t278478013::get_offset_of_isSequenced_36(),
	Tween_t278478013::get_offset_of_sequenceParent_37(),
	Tween_t278478013::get_offset_of_activeId_38(),
	Tween_t278478013::get_offset_of_specialStartupMode_39(),
	Tween_t278478013::get_offset_of_creationLocked_40(),
	Tween_t278478013::get_offset_of_startupDone_41(),
	Tween_t278478013::get_offset_of_playedOnce_42(),
	Tween_t278478013::get_offset_of_position_43(),
	Tween_t278478013::get_offset_of_fullDuration_44(),
	Tween_t278478013::get_offset_of_completedLoops_45(),
	Tween_t278478013::get_offset_of_isPlaying_46(),
	Tween_t278478013::get_offset_of_isComplete_47(),
	Tween_t278478013::get_offset_of_elapsedDelay_48(),
	Tween_t278478013::get_offset_of_delayComplete_49(),
	Tween_t278478013::get_offset_of_miscInt_50(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1601 = { sizeof (Tweener_t760404022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1601[2] = 
{
	Tweener_t760404022::get_offset_of_hasManuallySetStartValue_51(),
	Tweener_t760404022::get_offset_of_isFromAllowed_52(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1602 = { sizeof (TweenType_t169444141)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1602[4] = 
{
	TweenType_t169444141::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1603 = { sizeof (UpdateType_t3357224513)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1603[5] = 
{
	UpdateType_t3357224513::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1604 = { sizeof (Color2Plugin_t3433430606), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1605 = { sizeof (DoublePlugin_t266400784), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1606 = { sizeof (LongPlugin_t1941283029), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1607 = { sizeof (UlongPlugin_t3231465400), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1608 = { sizeof (Vector3ArrayPlugin_t2378569512), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1609 = { sizeof (PathPlugin_t4171842066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1609[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1610 = { sizeof (ColorPlugin_t4063724482), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1611 = { sizeof (IntPlugin_t180838436), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1612 = { sizeof (QuaternionPlugin_t1696644323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1613 = { sizeof (RectOffsetPlugin_t664509336), -1, sizeof(RectOffsetPlugin_t664509336_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1613[1] = 
{
	RectOffsetPlugin_t664509336_StaticFields::get_offset_of__r_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1614 = { sizeof (RectPlugin_t391797831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1615 = { sizeof (UintPlugin_t1040977389), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1616 = { sizeof (Vector2Plugin_t2164285386), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1617 = { sizeof (Vector4Plugin_t2164361360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1618 = { sizeof (StringPlugin_t3620786088), -1, sizeof(StringPlugin_t3620786088_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1618[2] = 
{
	StringPlugin_t3620786088_StaticFields::get_offset_of__Buffer_0(),
	StringPlugin_t3620786088_StaticFields::get_offset_of__OpenedTags_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1619 = { sizeof (StringPluginExtensions_t3910942986), -1, sizeof(StringPluginExtensions_t3910942986_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1619[5] = 
{
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsAll_0(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsUppercase_1(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsLowercase_2(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of_ScrambledCharsNumerals_3(),
	StringPluginExtensions_t3910942986_StaticFields::get_offset_of__lastRndSeed_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1620 = { sizeof (FloatPlugin_t3639480371), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1621 = { sizeof (Vector3Plugin_t2164530409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1622 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1623 = { sizeof (OrientType_t1755667719)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1623[5] = 
{
	OrientType_t1755667719::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1624 = { sizeof (PathOptions_t2659884781)+ sizeof (Il2CppObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1624[15] = 
{
	PathOptions_t2659884781::get_offset_of_mode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_orientType_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockPositionAxis_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lockRotationAxis_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isClosedPath_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtPosition_5() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAtTransform_6() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_lookAhead_7() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_hasCustomForwardDirection_8() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_forward_9() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_useLocalPosition_10() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_parent_11() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_isRigidbody_12() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupRot_13() + static_cast<int32_t>(sizeof(Il2CppObject)),
	PathOptions_t2659884781::get_offset_of_startupZRot_14() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1625 = { sizeof (QuaternionOptions_t466049668)+ sizeof (Il2CppObject), sizeof(QuaternionOptions_t466049668 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1625[3] = 
{
	QuaternionOptions_t466049668::get_offset_of_rotateMode_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_axisConstraint_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	QuaternionOptions_t466049668::get_offset_of_up_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1626 = { sizeof (UintOptions_t2267095136)+ sizeof (Il2CppObject), sizeof(UintOptions_t2267095136_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1626[1] = 
{
	UintOptions_t2267095136::get_offset_of_isNegativeChangeValue_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1627 = { sizeof (Vector3ArrayOptions_t2672570171)+ sizeof (Il2CppObject), sizeof(Vector3ArrayOptions_t2672570171_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1627[3] = 
{
	Vector3ArrayOptions_t2672570171::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	Vector3ArrayOptions_t2672570171::get_offset_of_durations_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1628 = { sizeof (NoOptions_t2508431845)+ sizeof (Il2CppObject), sizeof(NoOptions_t2508431845 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1629 = { sizeof (ColorOptions_t2213017305)+ sizeof (Il2CppObject), sizeof(ColorOptions_t2213017305_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1629[1] = 
{
	ColorOptions_t2213017305::get_offset_of_alphaOnly_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1630 = { sizeof (FloatOptions_t1421548266)+ sizeof (Il2CppObject), sizeof(FloatOptions_t1421548266_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1630[1] = 
{
	FloatOptions_t1421548266::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1631 = { sizeof (RectOptions_t3393635162)+ sizeof (Il2CppObject), sizeof(RectOptions_t3393635162_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1631[1] = 
{
	RectOptions_t3393635162::get_offset_of_snapping_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1632 = { sizeof (StringOptions_t2885323933)+ sizeof (Il2CppObject), sizeof(StringOptions_t2885323933_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1632[5] = 
{
	StringOptions_t2885323933::get_offset_of_richTextEnabled_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambleMode_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_scrambledChars_2() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_startValueStrippedLength_3() + static_cast<int32_t>(sizeof(Il2CppObject)),
	StringOptions_t2885323933::get_offset_of_changeValueStrippedLength_4() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1633 = { sizeof (VectorOptions_t293385261)+ sizeof (Il2CppObject), sizeof(VectorOptions_t293385261_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1633[2] = 
{
	VectorOptions_t293385261::get_offset_of_axisConstraint_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	VectorOptions_t293385261::get_offset_of_snapping_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1634 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1635 = { sizeof (SpecialPluginsUtils_t2241999250), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1636 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1637 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1638 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1639 = { sizeof (PluginsManager_t3052451537), -1, sizeof(PluginsManager_t3052451537_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1639[18] = 
{
	PluginsManager_t3052451537_StaticFields::get_offset_of__floatPlugin_0(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__doublePlugin_1(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__intPlugin_2(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__uintPlugin_3(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__longPlugin_4(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__ulongPlugin_5(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector2Plugin_6(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3Plugin_7(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector4Plugin_8(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__quaternionPlugin_9(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__colorPlugin_10(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectPlugin_11(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__rectOffsetPlugin_12(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__stringPlugin_13(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__vector3ArrayPlugin_14(),
	PluginsManager_t3052451537_StaticFields::get_offset_of__color2Plugin_15(),
	0,
	PluginsManager_t3052451537_StaticFields::get_offset_of__customPlugins_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1640 = { sizeof (ControlPoint_t168081159)+ sizeof (Il2CppObject), sizeof(ControlPoint_t168081159 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1640[2] = 
{
	ControlPoint_t168081159::get_offset_of_a_0() + static_cast<int32_t>(sizeof(Il2CppObject)),
	ControlPoint_t168081159::get_offset_of_b_1() + static_cast<int32_t>(sizeof(Il2CppObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1641 = { sizeof (ABSPathDecoder_t3294469411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1642 = { sizeof (CatmullRomDecoder_t3014762178), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1643 = { sizeof (LinearDecoder_t2073524639), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1644 = { sizeof (Path_t2828565993), -1, sizeof(Path_t2828565993_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1644[21] = 
{
	Path_t2828565993_StaticFields::get_offset_of__catmullRomDecoder_0(),
	Path_t2828565993_StaticFields::get_offset_of__linearDecoder_1(),
	Path_t2828565993::get_offset_of_wpLengths_2(),
	Path_t2828565993::get_offset_of_type_3(),
	Path_t2828565993::get_offset_of_subdivisionsXSegment_4(),
	Path_t2828565993::get_offset_of_subdivisions_5(),
	Path_t2828565993::get_offset_of_wps_6(),
	Path_t2828565993::get_offset_of_controlPoints_7(),
	Path_t2828565993::get_offset_of_length_8(),
	Path_t2828565993::get_offset_of_isFinalized_9(),
	Path_t2828565993::get_offset_of_timesTable_10(),
	Path_t2828565993::get_offset_of_lengthsTable_11(),
	Path_t2828565993::get_offset_of_linearWPIndex_12(),
	Path_t2828565993::get_offset_of__incrementalClone_13(),
	Path_t2828565993::get_offset_of__incrementalIndex_14(),
	Path_t2828565993::get_offset_of__decoder_15(),
	Path_t2828565993::get_offset_of__changed_16(),
	Path_t2828565993::get_offset_of_nonLinearDrawWps_17(),
	Path_t2828565993::get_offset_of_targetPosition_18(),
	Path_t2828565993::get_offset_of_lookAtPosition_19(),
	Path_t2828565993::get_offset_of_gizmoColor_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1645 = { sizeof (PureQuaternionPlugin_t3400666973), -1, sizeof(PureQuaternionPlugin_t3400666973_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1645[1] = 
{
	PureQuaternionPlugin_t3400666973_StaticFields::get_offset_of__plug_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1646 = { sizeof (ABSSequentiable_t2284140720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1646[4] = 
{
	ABSSequentiable_t2284140720::get_offset_of_tweenType_0(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedPosition_1(),
	ABSSequentiable_t2284140720::get_offset_of_sequencedEndPosition_2(),
	ABSSequentiable_t2284140720::get_offset_of_onStart_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1647 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1648 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1649 = { sizeof (Debugger_t1404542751), -1, sizeof(Debugger_t1404542751_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1649[1] = 
{
	Debugger_t1404542751_StaticFields::get_offset_of_logPriority_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1650 = { sizeof (DOTweenComponent_t696744215), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1650[4] = 
{
	DOTweenComponent_t696744215::get_offset_of_inspectorUpdater_2(),
	DOTweenComponent_t696744215::get_offset_of__unscaledTime_3(),
	DOTweenComponent_t696744215::get_offset_of__unscaledDeltaTime_4(),
	DOTweenComponent_t696744215::get_offset_of__duplicateToDestroy_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1651 = { sizeof (U3CWaitForCompletionU3Ed__14_t2205815884), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1651[3] = 
{
	U3CWaitForCompletionU3Ed__14_t2205815884::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForCompletionU3Ed__14_t2205815884::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForCompletionU3Ed__14_t2205815884::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1652 = { sizeof (U3CWaitForRewindU3Ed__15_t1216126710), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1652[3] = 
{
	U3CWaitForRewindU3Ed__15_t1216126710::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForRewindU3Ed__15_t1216126710::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForRewindU3Ed__15_t1216126710::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1653 = { sizeof (U3CWaitForKillU3Ed__16_t2574075760), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1653[3] = 
{
	U3CWaitForKillU3Ed__16_t2574075760::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForKillU3Ed__16_t2574075760::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForKillU3Ed__16_t2574075760::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1654 = { sizeof (U3CWaitForElapsedLoopsU3Ed__17_t1259698564), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1654[4] = 
{
	U3CWaitForElapsedLoopsU3Ed__17_t1259698564::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForElapsedLoopsU3Ed__17_t1259698564::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForElapsedLoopsU3Ed__17_t1259698564::get_offset_of_t_2(),
	U3CWaitForElapsedLoopsU3Ed__17_t1259698564::get_offset_of_elapsedLoops_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1655 = { sizeof (U3CWaitForPositionU3Ed__18_t3956751645), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1655[4] = 
{
	U3CWaitForPositionU3Ed__18_t3956751645::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPositionU3Ed__18_t3956751645::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPositionU3Ed__18_t3956751645::get_offset_of_t_2(),
	U3CWaitForPositionU3Ed__18_t3956751645::get_offset_of_position_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1656 = { sizeof (U3CWaitForStartU3Ed__19_t2341562413), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1656[3] = 
{
	U3CWaitForStartU3Ed__19_t2341562413::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForStartU3Ed__19_t2341562413::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForStartU3Ed__19_t2341562413::get_offset_of_t_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1657 = { sizeof (DOTweenSettings_t873123119), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1657[18] = 
{
	0,
	DOTweenSettings_t873123119::get_offset_of_useSafeMode_3(),
	DOTweenSettings_t873123119::get_offset_of_timeScale_4(),
	DOTweenSettings_t873123119::get_offset_of_useSmoothDeltaTime_5(),
	DOTweenSettings_t873123119::get_offset_of_maxSmoothUnscaledTime_6(),
	DOTweenSettings_t873123119::get_offset_of_showUnityEditorReport_7(),
	DOTweenSettings_t873123119::get_offset_of_logBehaviour_8(),
	DOTweenSettings_t873123119::get_offset_of_drawGizmos_9(),
	DOTweenSettings_t873123119::get_offset_of_defaultRecyclable_10(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoPlay_11(),
	DOTweenSettings_t873123119::get_offset_of_defaultUpdateType_12(),
	DOTweenSettings_t873123119::get_offset_of_defaultTimeScaleIndependent_13(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseType_14(),
	DOTweenSettings_t873123119::get_offset_of_defaultEaseOvershootOrAmplitude_15(),
	DOTweenSettings_t873123119::get_offset_of_defaultEasePeriod_16(),
	DOTweenSettings_t873123119::get_offset_of_defaultAutoKill_17(),
	DOTweenSettings_t873123119::get_offset_of_defaultLoopType_18(),
	DOTweenSettings_t873123119::get_offset_of_storeSettingsLocation_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1658 = { sizeof (SettingsLocation_t514961325)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1658[4] = 
{
	SettingsLocation_t514961325::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1659 = { sizeof (Extensions_t507052800), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1660 = { sizeof (SequenceCallback_t2782183128), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1661 = { sizeof (TweenManager_t1979661952), -1, sizeof(TweenManager_t1979661952_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1661[33] = 
{
	0,
	0,
	0,
	TweenManager_t1979661952_StaticFields::get_offset_of_maxActive_3(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxTweeners_4(),
	TweenManager_t1979661952_StaticFields::get_offset_of_maxSequences_5(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveTweens_6(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveDefaultTweens_7(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveLateTweens_8(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveFixedTweens_9(),
	TweenManager_t1979661952_StaticFields::get_offset_of_hasActiveManualTweens_10(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweens_11(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveDefaultTweens_12(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveLateTweens_13(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveFixedTweens_14(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveManualTweens_15(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveTweeners_16(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totActiveSequences_17(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledTweeners_18(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totPooledSequences_19(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totTweeners_20(),
	TweenManager_t1979661952_StaticFields::get_offset_of_totSequences_21(),
	TweenManager_t1979661952_StaticFields::get_offset_of_isUpdateLoop_22(),
	TweenManager_t1979661952_StaticFields::get_offset_of__activeTweens_23(),
	TweenManager_t1979661952_StaticFields::get_offset_of__pooledTweeners_24(),
	TweenManager_t1979661952_StaticFields::get_offset_of__PooledSequences_25(),
	TweenManager_t1979661952_StaticFields::get_offset_of__KillList_26(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxActiveLookupId_27(),
	TweenManager_t1979661952_StaticFields::get_offset_of__requiresActiveReorganization_28(),
	TweenManager_t1979661952_StaticFields::get_offset_of__reorganizeFromId_29(),
	TweenManager_t1979661952_StaticFields::get_offset_of__minPooledTweenerId_30(),
	TweenManager_t1979661952_StaticFields::get_offset_of__maxPooledTweenerId_31(),
	TweenManager_t1979661952_StaticFields::get_offset_of__despawnAllCalledFromUpdateLoopCallback_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1662 = { sizeof (CapacityIncreaseMode_t1969140739)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1662[4] = 
{
	CapacityIncreaseMode_t1969140739::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1663 = { sizeof (Utils_t2524017187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1664 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable1664[8] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1665 = { sizeof (FilterType_t1425068526)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1665[6] = 
{
	FilterType_t1425068526::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1666 = { sizeof (OperationType_t2600045009)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1666[14] = 
{
	OperationType_t2600045009::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1667 = { sizeof (SpecialStartupMode_t1501334721)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1667[6] = 
{
	SpecialStartupMode_t1501334721::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1668 = { sizeof (UpdateNotice_t2468589887)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1668[3] = 
{
	UpdateNotice_t2468589887::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1669 = { sizeof (UpdateMode_t2539919096)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1669[4] = 
{
	UpdateMode_t2539919096::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1670 = { sizeof (Bounce_t3273339050), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1671 = { sizeof (EaseManager_t1514337917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1671[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1672 = { sizeof (U3CU3Ec_t1609106043), -1, sizeof(U3CU3Ec_t1609106043_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1672[37] = 
{
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_1_2(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_2_3(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_3_4(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_4_5(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_5_6(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_6_7(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_7_8(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_8_9(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_9_10(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_10_11(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_11_12(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_12_13(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_13_14(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_14_15(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_15_16(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_16_17(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_17_18(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_18_19(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_19_20(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_20_21(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_21_22(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_22_23(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_23_24(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_24_25(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_25_26(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_26_27(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_27_28(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_28_29(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_29_30(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_30_31(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_31_32(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_32_33(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_33_34(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_34_35(),
	U3CU3Ec_t1609106043_StaticFields::get_offset_of_U3CU3E9__4_35_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1673 = { sizeof (EaseCurve_t1295352409), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1673[1] = 
{
	EaseCurve_t1295352409::get_offset_of__animCurve_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1674 = { sizeof (Flash_t1282698556), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1675 = { sizeof (U3CPrivateImplementationDetailsU3E_t1486305141), -1, sizeof(U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1675[4] = 
{
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U36F98278EFCD257898AD01BE39D1D0AEFB78FC551_0(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U38C4C6C4E493EC2BEEF5F6F6A9C4472C13BED42E8_1(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_U39DC5F4D0A1418B4EC71B22D21E93D134922FA735_2(),
	U3CPrivateImplementationDetailsU3E_t1486305141_StaticFields::get_offset_of_FD0BD55CDDDFD0B323012A45F83437763AF58952_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1676 = { sizeof (__StaticArrayInitTypeSizeU3D20_t978476011)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D20_t978476011 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1677 = { sizeof (__StaticArrayInitTypeSizeU3D50_t3707359366)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D50_t3707359366 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1678 = { sizeof (__StaticArrayInitTypeSizeU3D120_t1468992140)+ sizeof (Il2CppObject), sizeof(__StaticArrayInitTypeSizeU3D120_t1468992140 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1679 = { sizeof (U3CModuleU3E_t3783534220), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1680 = { sizeof (EventHandle_t942672932)+ sizeof (Il2CppObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1680[3] = 
{
	EventHandle_t942672932::get_offset_of_value___1() + static_cast<int32_t>(sizeof(Il2CppObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1681 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1682 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1683 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1684 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1685 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1686 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1687 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1688 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1689 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1690 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1691 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1692 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1693 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1694 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1695 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1696 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1697 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1698 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1699 = { sizeof (EventSystem_t3466835263), -1, sizeof(EventSystem_t3466835263_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1699[12] = 
{
	EventSystem_t3466835263::get_offset_of_m_SystemInputModules_2(),
	EventSystem_t3466835263::get_offset_of_m_CurrentInputModule_3(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CcurrentU3Ek__BackingField_4(),
	EventSystem_t3466835263::get_offset_of_m_FirstSelected_5(),
	EventSystem_t3466835263::get_offset_of_m_sendNavigationEvents_6(),
	EventSystem_t3466835263::get_offset_of_m_DragThreshold_7(),
	EventSystem_t3466835263::get_offset_of_m_CurrentSelected_8(),
	EventSystem_t3466835263::get_offset_of_m_Paused_9(),
	EventSystem_t3466835263::get_offset_of_m_SelectionGuard_10(),
	EventSystem_t3466835263::get_offset_of_m_DummyData_11(),
	EventSystem_t3466835263_StaticFields::get_offset_of_s_RaycastComparer_12(),
	EventSystem_t3466835263_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_13(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
